<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// $config['ci_my_admin_template_dir'] = ""; // you need this line
// $config['ci_my_admin_template_dir_public'] = $config['ci_my_admin_template_dir'] . "public/themes/default/";
// $config['ci_my_admin_template_dir_admin'] = $config['ci_my_admin_template_dir'] . "admin/themes/default/";
// $config['ci_my_admin_template_dir_welcome'] = $config['ci_my_admin_template_dir'] . "welcome/themes/default/";

$config['template_dir'] = ""; // you need this line
$config['template_dir_public'] = $config['template_dir'] . "/";
$config['template_dir_admin'] = $config['template_dir'] . "admin/";
$config['template_dir_artist'] = $config['template_dir'] . "artist/";
$config['template_dir_agent'] = $config['template_dir'] . "agent/";