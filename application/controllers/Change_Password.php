<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Change_Password extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if(!is_logged_in())  // check if logged in
        {
            //redirect to login
            redirect('/login');
        }

        $this->load->model('User_model');
        $this->load->library("pagination");
    }

    public function index()
    {
        // set default metas
        $header['page_title'] = 'Change Password';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = 'dissertation, final year projects, project topics, essays, coursework, assignment, computer science, information technology, software engineering, management science, law students, masters, bsc, phd, thesis, abstract';
        $header['meta_description'] = 'This is a one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engine…';

        $meta_from_db = getPageMeta();

        if(!empty($meta_from_db))
        {
            $header['page_title'] = $meta_from_db['page_title'];
            $header['meta_title'] = $meta_from_db['meta_title'];
            $header['meta_tags'] = $meta_from_db['meta_tags'];
            $header['meta_description'] = $meta_from_db['meta_description'];
        }
        //---------------------- end meta ---------------------------------

        $this->load->library('form_validation');
        $this->form_validation->set_rules('current_password', 'Current Password', 'required|max_length[8000]');
        $this->form_validation->set_rules('new_password', 'New Password', 'required|max_length[8000]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|max_length[8000]|matches[new_password]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));

            if(empty($this->session->flashdata('error'))) // not currently updated
            {
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
            }

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');

            $this->load->view('header', $header);  // load header view
            // $this->load->view('menu');  // load menu view
            $this->load->view('change-password', $data);  // load content view
        }
        else
        {
            $changed = $this->User_model->changePassword($this->session->userdata('user_id'));
            if(!$changed)
            {
                $this->session->set_flashdata('error', "Invalid Password!");
                $this->session->set_flashdata('error_code', 1);
            }
            else {
                $this->session->set_flashdata('error', "Password Changed Successfully!");
                $this->session->set_flashdata('error_code', 0);
            }
            
            redirect('/Change-Password');
        }
    }
}