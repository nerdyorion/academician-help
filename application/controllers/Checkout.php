<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Checkout extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        if(!is_logged_in())  // check if logged in
        {
            //redirect to login
            redirect('/login?return=checkout');
        }
        $this->load->model("Order_model");
        $this->load->model("Payment_model");
        $this->load->model("Coupon_model");
        $this->load->library("stripe");
    }

    public function index()
    {
        $user_id = (int) $this->session->userdata("user_id");

        // set default metas
        $header['page_title'] = 'Checkout';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = 'dissertation, final year projects, project topics, essays, coursework, assignment, computer science, information technology, software engineering, management science, law students, masters, bsc, phd, thesis, abstract';
        $header['meta_description'] = 'This is a one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engine…';

        $meta_from_db = getPageMeta();

        if(!empty($meta_from_db))
        {
            $header['page_title'] = $meta_from_db['page_title'];
            $header['meta_title'] = $meta_from_db['meta_title'];
            $header['meta_tags'] = $meta_from_db['meta_tags'];
            $header['meta_description'] = $meta_from_db['meta_description'];
        }
        //---------------------- end meta ---------------------------------

        $data['rows'] = $this->getCart();

        $data['calculated_discount_amount'] = 0;

        // check and apply coupon
        $coupon_code = isset($_GET['coupon_code']) ? trim($_GET['coupon_code']) : FALSE;

        if(!empty($coupon_code))
        {
            $coupon = $this->Coupon_model->getRowByCode($coupon_code);

            if(empty($coupon))
            {
                $this->session->set_flashdata('error_code', 1);
                $this->session->set_flashdata('error', "Invalid Coupon!");
            }
            else
            {
                $expired = 1;
                
                //-->get coupon expiry date, if not null - check if expiry date has passed
                if(empty($coupon['expiry_date']) || is_null($coupon['expiry_date']))
                {
                    $expired = 0; // coupon has no expiry
                }
                elseif( strtotime(date('Y-m-d')) > strtotime($coupon['expiry_date']) )
                {
                    $expired = 1; // coupon has expired
                }
                else
                {
                    $expired = 0; // coupon has not expired
                }

                if($expired == 1)
                {
                    $this->session->set_flashdata('error_code', 1);
                    $this->session->set_flashdata('error', "Coupon has expired.");
                }
                else
                {
                    //-->check if coupon used by participant already
                    if($this->Coupon_model->is_used($coupon['id'], $user_id)) // where user_id is user_id and coupon_is is id and is_paid == 1
                    {
                        $this->session->set_flashdata('error_code', 1);
                        $this->session->set_flashdata('error', "You have already used this coupon!");
                    }
                    else
                    {
                        $cart_total = $data['rows']['cart_total'];
                        $calculated_discount_amount = 0;

                        //-->if is_fixed == 1, subtract discount_amount from price else subtract discount_amount percent of price from price
                        if((int) $coupon['is_fixed'] == 1)
                        {
                            // subtract discount_amount from total
                            $calculated_discount_amount = (float) $coupon['discount_amount'];
                        }
                        else
                        {
                            // subtract discount_amount percent of price from price
                            $calculated_discount_amount = $cart_total * ( (int) $coupon['discount_amount'] * 0.01);
                        }

                        $data['calculated_discount_amount'] = $calculated_discount_amount;

                        $this->session->set_userdata("coupon_id", $coupon['id']);
                        $this->session->set_userdata("coupon_code", $coupon_code);
                        $this->session->set_userdata("calculated_discount_amount", $calculated_discount_amount);

                        //-->then insert price to pay in order and add coupon_id

                        $this->session->set_flashdata('error_code', 0);
                        $this->session->set_flashdata('error', "You have successfully applied this coupon");
                    }
                }
            }
        }

        if(isset($_GET['remove_coupon_code']))
        {
            // clear coupon
            $this->session->unset_userdata('coupon_id');
            $this->session->unset_userdata('coupon_code');
            $this->session->unset_userdata('calculated_discount_amount');

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Coupon removed successfully.");
        }

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('checkout', $data);  // load content view
    }

    public function secure()
    {
        $user_id = (int) $this->session->userdata("user_id");

        $cart = $this->getCart();
        $countItems = count($cart['cart_items']);
        $payload = '';

        $email_cart_details = '<ul>';

        // save order - get id
        $coupon_id = is_null($this->session->userdata("coupon_id")) ? NULL : (int) $this->session->userdata("coupon_id");
        $order_comment = NULL;
        $delivery_email = $this->session->userdata("user_email");
        $total_price = $cart['cart_total'];

        $order_id = $this->Order_model->add($coupon_id, $order_comment, $delivery_email, $total_price);

        // add order items
        foreach ($cart['cart_items'] as $key => $item) {
            // var_dump($item); die;
            $item_id = $item[0]['product_id'];
            $item_name = urldecode($item[0]['product_name']);
            $item_price = $item[0]['product_price'];

            // generate cart details
            $email_cart_details .= "<b>$item_name</b> (&pound;" . number_format($item_price) . ")<br />";

            // add to db
            $this->Order_model->addItems($order_id, $item_id, $item_name, $item_price);
        }
        $email_cart_details .= "</ul>";
        $email_cart_details .= "<b>Total</b> &pound;" . number_format($cart['cart_total']) . "";

        // proceed with payment
        $description = 'Charge for ' . $this->session->userdata("user_email") . ': Purchased ';
        $description .= $countItems > 1 ? $countItems . ' items' : $countItems . ' item';

        if(isset($_GET['complete']) && $_GET['complete'] == 'paypal')
        {
                $payload = '{"method":"paypal"}';

                // add to db payment status
                $successful = 1;
                $error_code = '';
                $error_message = '';

                $this->Payment_model->add($order_id, $successful, $error_code, $error_message, $payload);

                // update order to paid
                $this->Order_model->paid($order_id);


                // ---------------------add notification -----------------------------
                $student_name = $this->session->userdata('user_full_name');
                $to = $this->session->userdata('user_email');
                $subject = "New Order";

                $order_details .= '<b>Email</b>: ' . $to . '<br />';
                $order_details .= $email_cart_details . '<br /><br />';
                $order_details .= 'Regards,';
                $message = $this->template_new_order($student_name, $order_details);

                // send welcome mail to user
                // sendmail($to, $to_name, $subject, $message);

                $to = filter_var(strtolower($to), FILTER_SANITIZE_EMAIL);
                $to_name = filter_var(ucwords($student_name), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

                // $this->SEND_MAIL($subject, $message);

                // add notification to admin dashboard
                $notification_user_id = 0; // for super admin and admin

                $notification_message = 'New order from <a href="' . base_url() . 'admin123/users/view/' . $user_id . '">' . $to_name . '</a>';
                add_notification($notification_user_id, $notification_message);
                // --------------------------------------------------

                // clear cart
                $this->session->unset_userdata('cart');

                // clear coupon
                $this->session->unset_userdata('coupon_id');
                $this->session->unset_userdata('coupon_code');
                $this->session->unset_userdata('calculated_discount_amount');

                $this->session->set_flashdata('error_code', 0);
                $this->session->set_flashdata('error', "Payment successful. <a href='orders'>Proceed to download&raquo;</a>");

                redirect('/Order-Complete');
        }
        if(!is_null($this->input->post("stripeToken")))
        {
            $stripeToken = $this->input->post("stripeToken");
            echo "gotten stripeToken: <b>$stripeToken</b>";
            echo "<br /><br />";

            // hit stripe
            try
            {
                \Stripe\Stripe::setApiKey("sk_test_Z0s0IhTDc7uL5sd0kbzL16T5");

                $response = \Stripe\Charge::create(
                    array(
                        "amount" => $cart['cart_total'] * 100,
                        "currency" => "gbp",
                        "source" => $stripeToken,
                        "description" => $description,
                        "metadata" => array("order_id" => $order_id)
                    )
                );

                // $payload = (array) $response; 
                $payload = json_decode(json_encode($response),TRUE);
                // var_dump($payload); die;

                // add to db payment status
                $successful = is_null($payload['failure_code']) || empty($payload['failure_code']) ? 1 : 0;
                $error_code = $payload['failure_code'];
                $error_message = $payload['failure_message'];

                $this->Payment_model->add($order_id, $successful, $error_code, $error_message, json_encode($payload));

                // update order to paid
                $this->Order_model->paid($order_id);


                // ---------------------add notification -----------------------------
                $student_name = $this->session->userdata('user_full_name');
                $to = $this->session->userdata('user_email');
                $subject = "New Order";

                $order_details .= '<b>Email</b>: ' . $to . '<br />';
                $order_details .= $email_cart_details . '<br /><br />';
                $order_details .= 'Regards,';
                $message = $this->template_new_order($student_name, $order_details);

                // send welcome mail to user
                // sendmail($to, $to_name, $subject, $message);

                $to = filter_var(strtolower($to), FILTER_SANITIZE_EMAIL);
                $to_name = filter_var(ucwords($student_name), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

                // $this->SEND_MAIL($subject, $message);

                // add notification to admin dashboard
                $notification_user_id = 0; // for super admin and admin

                $notification_message = 'New order from <a href="' . base_url() . 'admin123/users/view/' . $user_id . '">' . $to_name . '</a>';
                add_notification($notification_user_id, $notification_message);
                // --------------------------------------------------

                // clear cart
                $this->session->unset_userdata('cart');

                // clear coupon
                $this->session->unset_userdata('coupon_id');
                $this->session->unset_userdata('coupon_code');
                $this->session->unset_userdata('calculated_discount_amount');

                $this->session->set_flashdata('error_code', 0);
                $this->session->set_flashdata('error', "Payment successful. <a href='orders'>Proceed to download&raquo;</a>");

                redirect('/Order-Complete');

            }
            catch(\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught
                $body = $e->getJsonBody();
                $err  = $body['error'];

                $error = 'Status is:' . $e->getHttpStatus() . "\n";
                $error .= 'Type is:' . $err['type'] . "\n";
                $error .= 'Code is:' . $err['code'] . "\n";
                // param is '' in this case
                $error .= 'Param is:' . $err['param'] . "\n";
                $error .= 'Message is:' . $err['message'] . "\n";

                error_log($error);
                $this->SEND_MAIL("omotayo@brilloconnetz.com", "Omotayo Odupitan", "AcademicianHelp Stripe Payment Error", $error);

                $errors = "Unable to complete transaction. <a href='/checkout'>Please refresh page</a>";
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

                redirect('/checkout');

            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly
                $error .= 'Error Message: Too many requests made to the API too quickly' . "\n";

                error_log($error);
                $this->SEND_MAIL("omotayo@brilloconnetz.com", "Omotayo Odupitan", "AcademicianHelp Stripe Payment Error", $error);

                $errors = "Too many concurrent transactions. <a href='/checkout'>Please refresh page</a>";
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

                redirect('/checkout');
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API
                $error .= 'Error Message is: Invalid parameters were supplied to Stripe\'s API' . "\n";
                
                error_log($error);
                $this->SEND_MAIL("omotayo@brilloconnetz.com", "Omotayo Odupitan", "AcademicianHelp Stripe Payment Error", $error);

                $errors = "Unable to complete transaction. Invalid parameters. <a href='/checkout'>Please refresh page</a>";
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

                redirect('/checkout');
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)
                $error .= 'Error Message is: Authentication with Stripe\'s API failed (maybe you changed API keys recently)' . "\n";
                
                error_log($error);
                $this->SEND_MAIL("omotayo@brilloconnetz.com", "Omotayo Odupitan", "AcademicianHelp Stripe Payment Error", $error);

                $errors = "Unable to authenticating transaction. <a href='/checkout'>Please refresh page</a>";
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

                redirect('/checkout');
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed
                $error .= 'Error Message is: Network communication with Stripe failed' . "\n";
                
                error_log($error);
                $this->SEND_MAIL("omotayo@brilloconnetz.com", "Omotayo Odupitan", "AcademicianHelp Stripe Payment Error", $error);

                $errors = "Unable to communicating to gateway. <a href='/checkout'>Please refresh page</a>";
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

                redirect('/checkout');
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email
                $error .= 'Error Message is: Display a very generic error to the user, and maybe send yourself an email' . "\n";
                
                error_log($error);
                $this->SEND_MAIL("omotayo@brilloconnetz.com", "Omotayo Odupitan", "AcademicianHelp Stripe Payment Error", $error);

                $errors = "Unable to complete transaction. <a href='/checkout'>Please refresh page</a>";
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

                redirect('/checkout');
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe
                $error .= 'Error Message is: Something else happened, completely unrelated to Stripe' . "\n";
                
                error_log($error);
                $this->SEND_MAIL("omotayo@brilloconnetz.com", "Omotayo Odupitan", "AcademicianHelp Stripe Payment Error", $error);

                $errors = "Unable to complete transaction. <a href='/checkout'>Please refresh page</a>";
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

                redirect('/checkout');
            }
        }
        else
        {
            $errors = "Unable to complete transaction. <a href='/checkout'>Please refresh page</a>";
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect('/checkout');
        }
    }

    public function paypal($action)
    {
        if($action == 'createPayment')
        {
            echo $action;
            
        }
        if($action == 'executePayment')
        {
            echo $action;
        }
        if($action == 'complete')
        {
            echo $action;
        }
    }

    public function testshow($action)
    {
        if($action == 'createPayment')
        {
            // echo $action;
            echo 
            '<!DOCTYPE html>
            <html lang="en">
            <head>
            <meta charset="utf-8">
            <base href="http://academicianhelp.test">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
            <title>Test Paypal.</title>
            </head>

            <body>

            <script src="https://www.paypalobjects.com/api/checkout.js"></script>
            <div id="paypal-button" style="display: inline; float: left;"></div>


            <script type="text/javascript" src="assets/js/paypal.js"></script>

            </body>
            </html>';
        }
        if($action == 'executePayment')
        {
            echo $action;
        }
    }

    public function testshow2()
    {
            echo 
            '<!DOCTYPE html>
            <html lang="en">
            <head>
            <meta charset="utf-8">
            <base href="http://academicianhelp.test">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
            <title>Test Paypal.</title>
            </head>

            <body>

            
            <form target="_blank" action="https://www.paypal.com/cgi-bin/webscr" method="post">
            <input type="hidden" name="cmd" value="_xclick">
            <input type="hidden" name="business" value="a.adeseye007@gmail.com">
            <input type="hidden" name="item_name" value="Dissertation: Smart Car Park System">
            <input type="hidden" name="return" value="https://www.yoursite.com/checkout_complete.php?complete=paypal">
            <input type="hidden" name="item_number" value="1">
            <input type="hidden" name="amount" value="45">
            <input type="hidden" name="quantity" value="1">
            <input type="hidden" name="currency_code" value="GBP">

            <input type="image" name="submit"
            src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/checkout-logo-medium.png"
            alt="Check out with PayPal" />
            </form>

            </body>
            </html>';
    }

    public function test($action)
    {
        if($action == 'createPayment')
        {
            // echo $action;

            $items_for_api = array(
                array
                (
                    "name" => "Dissertation: Smart Car Park System",
                    "description" => "Dissertation: Smart Car Park System",
                    // "quantity" => "5",
                    "price" => "45",
                    // "tax" => "0.01",
                    "sku" => "1",
                    "currency" => "GBP"
                )
            );

            $data = array(
                "intent" => "sale",
                "payer" => array(
                    "payment_method" => "paypal"
                ),
                "transactions" => array(
                    "amount" => array(
                        "total" => "30.11",
                        "currency" => "GBP",
                        "details" => array(
                            "subtotal" => "30.11"
                        )
                    ),
                    "description" => "Charge for nerdyorion@gmail.com: Purchased 1 item",
                    "invoice_number" => "INV-1",
                    "payment_options" => array(
                        "allowed_payment_method" => "INSTANT_FUNDING_SOURCE"
                    ),
                    "soft_descriptor" => "ECHI5786786",
                    "item_list" => array(
                        "items" => $items_for_api,
                    ),
                ),
                "note_to_payer" => "Contact us on +442039502729 or tutor@academicianhelp.co.uk for any questions on your order.",
                "redirect_urls" => array(
                    "return_url" => $this->config->item('base_url') . "Order-Complete",
                    "cancel_url" => $this->config->item('base_url') . "checkout"
                )
            );

            // send payment request
            $token = $this->getToken();
            $url = '/v1/payments/payment';

            $response = $this->HTTP_Post($data, $token, $url);
            $response_array = json_decode($response, true);

            if(isset($response_array['id']))
            {
                // successful
                echo $response_array['id'];
            }
            else
            {
                echo $response;
            }
        }
        if($action == 'executePayment')
        {
            echo $action;
        }
    }

    private function getToken()
    {
        // get current_token from db
        $token = $this->Payment_model->getPayPalToken($url);

        return $token;
    }

    private function getNewToken()
    {
        // get token from PayPal
        $response = $this->getTokenFromPayPal();
        $response = json_decode($response, true);

        $token = $response['access_token'];
        $expires_in = $response['expires_in'];

        // save token in db
        $this->Payment_model->updatePayPalToken($token, $expires_in);

        return $token;
    }


    private function getCart()
    {
        // ------------- from fetch
        $cart_items = array();
        $cart_total = 0;

        $output = array();

        if(!is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart'))) // cart not empty
        {
            foreach ($this->session->userdata('cart') as $key => $item) {
                // var_dump($this->session->userdata('cart')); die;
                $cart = array(
                    'product_id' => $key,
                    'product_name' => $item['product_name'],
                    'product_name_short' => urldecode(ellipsize($item['product_name'], 33)),
                    'product_price' => number_format($item['product_price'])
                );

                $cart_total += $item['product_price'];

                $cart_items[] = array($cart);
            }
            // add format
            $cart_total = number_format($cart_total); 
        }
        else // cart empty
        {
            redirect('/Past-Exam-Solution');
        }

        $output = array(
            'cart_items' => $cart_items,
            'cart_total' => $cart_total,
        );
        // ------------- from getch
        return $output;
    }

    private function template_new_order($student_name, $order_details)
    {
        $url = $this->config->base_url() . 'login';

        return "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/xhtml\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\">

        <body style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\">

        <table class=\"body-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
        <td class=\"container\" width=\"600\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; width: 100% !important; margin: 0 auto; padding: 0;\" valign=\"top\">
            <div class=\"content\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 10px;\">
                <table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; padding: 0; border: 1px solid #e9e9e9;\" bgcolor=\"#fff\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"alert alert-warning\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #4aa3df; margin: 0; padding: 20px;\" align=\"center\" bgcolor=\"#4aa3df\" valign=\"top\">
                            AcademicianHelp
                        </td>
                    </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;\" valign=\"top\">
                            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Hello,
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        $student_name just placed an order. Details below:
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        $order_details
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Login to continue:
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        <a href=\"$url\" class=\"btn-primary\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #348eda; margin: 0; padding: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;\">Login &raquo;</a>
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Thanks for choosing AcademicianHelp.
                                    </td>
                                </tr></table></td>
                    </tr></table><div class=\"footer\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;\">
                    <table width=\"100%\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"aligncenter content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;\" align=\"center\" valign=\"top\">
                    <!--
                    AcademicianHelp is a registered trademark in Nigeria. 
                    <br />No 4, Whales Street,
                    <br />Admiralty Way, Landon,
                    <br />Lagos, Nigeria
                    <br />Phone: 0123456789
                    -->
                    <br />admin@academicianhelp.co.uk
                    <br />&copy; " . date("Y") . " AcademicianHelp.</td>
                        </tr></table></div></div>
                </td>
                <td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
            </tr></table></body>
        </html>";
    }

    private function SEND_MAIL($subject, $message) {

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://md-59.webhostbox.net',
            'smtp_port' => 465,
            'smtp_user' => 'smtp-mailer@brilloconnetz.com', 
            'smtp_pass' => 'smtp-mailer123', 
            'mailtype' => 'html',
            'charset' => 'utf-8', // utf-8 iso-8859-1
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from('website@academicianhelp.co.uk', 'AcademicianHelp Website');
        $this->email->to('admin@academicianhelp.co.uk', 'AcademicianHelp');
        // $this->email->to('omotayo@brilloconnetz.com', 'Omotayo Odupitan');
        $this->email->bcc($this->config->item('admin_bcc_email'));
        $this->email->subject($subject);
        $this->email->message($message);

        if($this->email->send())
        {
            return TRUE;
        }
        else
        {
            $error = "Message could not be sent.<br />";
            $error .= "Message: $message" . "<br />";
            $error .= "Mailer Error: " . $this->email->print_debugger() . "<br />";
            // error_log($this->email->print_debugger());
            error_log($error);
            return FALSE;
        }
    }

    private function HTTP_Post($data, $token, $url)
    {
        $url =  $this->config->item('paypal_api_url') . $url;

        $data = json_encode($data); // echo $data; die;

        $ch = curl_init();  

        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "Bearer $token");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60); // in seconds

        $output = curl_exec($ch);
        $info = curl_getinfo($ch);

        // get error code
        $http_code = (string) $info['http_code'];


        if($http_code[0] == '2') // success, return response
        {
            curl_close($ch);
            return $output;

        }
        if($http_code != '401' && (($http_code[0] == '4') || ($http_code[0] == '5')) ) // failure, return error_messageByCode and send mail
        {
            curl_close($ch);

            error_log($output);

            $error_message = $this->getErrorMessage($http_code);

            $subject = 'PayPal API Error';

            $message = 'Hello,' . '<br /><br />';
            $message .= 'An error just occurred on the PayPal API. See below.' . '<br />';
            $message .= '<code>' . $error_message . '</code>' . '<br />';
            $message .= 'ERROR: <code>' . $output . '</code>' . '<br />';
            $message .= 'Please resolve.' . '<br /><br />';
            $message .= 'Thanks,';

            $this->SEND_MAIL($subject, $message);

            return $error_message;

        }
        if($http_code == '401')  // unauthorized, $this->getNewToken() and RetryHTTP_Post($data, $token, $url)
        {
            curl_close($ch);
            
            error_log($output . ' ...retrying new token');

            $new_token = $this->getNewToken();

            $this->RetryHTTP_Post($data, $new_token, $url);
        }
    }

    private function RetryHTTP_Post($data, $token, $url)
    {
        // $url =  $this->config->item('paypal_api_url') . $url; // already concatenated url

        // $data = json_encode($data); // already encoded

        $ch = curl_init();  

        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "Bearer $token");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60); // in seconds

        $output = curl_exec($ch);
        $info = curl_getinfo($ch);

        // get error code
        $http_code = (string) $info['http_code'];


        if($http_code[0] == '2') // success, return response
        {
            curl_close($ch);
            return $output;

        }
        if($http_code[0] == '4' || $http_code[0] == '5') // failure, return error_messageByCode and send mail
        {
            curl_close($ch);

            error_log($output);

            $error_message = $this->getErrorMessage($http_code);

            $subject = 'PayPal API Error';

            $message = 'Hello,' . '<br /><br />';
            $message .= 'An error just occurred on the PayPal API. See below.' . '<br />';
            $message .= '<code>' . $error_message . '</code>' . '<br />';
            $message .= 'ERROR: <code>' . $output . '</code>' . '<br />';
            $message .= 'Please resolve.' . '<br /><br />';
            $message .= 'Thanks,';

            $this->SEND_MAIL($subject, $message);

            return $error_message;
        }
    }

    private function getTokenFromPayPal()
    {
        $url =  $this->config->item('paypal_api_url') . '/v1/oauth2/token';

        $client_id =  $this->config->item('paypal_client_id');
        $secret =  $this->config->item('paypal_secret');

        $data = array(
            "grant_type" => "client_credentials"
        );

        $data = http_build_query($data);

        $ch = curl_init();  

        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$client_id:$secret");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60); // in seconds

        $output = curl_exec($ch);
        $info = curl_getinfo($ch);

        // get error code
        $http_code = (string) $info['http_code'];


        if($http_code == '401')
        {
            curl_close($ch);

            error_log($output);

            $error_message = $this->getErrorMessage($http_code);

            $subject = 'PayPal API Error (Get Token)';

            $message = 'Hello,' . '<br /><br />';
            $message .= 'An error just occurred on the PayPal API (Get Token). See below.' . '<br />';
            $message .= '<code>' . $error_message . '</code>' . '<br />';
            $message .= 'ERROR: <code>' . $output . '</code>' . '<br />';
            $message .= 'Please resolve.' . '<br /><br />';
            $message .= 'Thanks,';

            $this->SEND_MAIL($subject, $message);

            return $error_message;
        }
        else
        {
            curl_close($ch);
            return $output;

        }
    }

    private function getErrorMessage($code) {
        $code = (int) $code;
        $message = '';

        switch ($code) {
            case 200:
            $message = 'Request completed successfully';
            break;

            case 201:
            $message = 'Resource created successfully';
            break;

            case 202:
            $message = 'Request accepted successfully';
            break;

            case 204:
            $message = 'Request executed successfully';
            break;

            case 400:
            $message = 'Request is not well-formed. {Bad Request | INVALID_REQUEST}';
            break;

            case 401:
            $message = 'Authentication failed due to invalid authentication credentials. {Unauthorized | AUTHENTICATION_FAILURE}';
            break;

            case 403:
            $message = 'Authorization failed due to insufficient permissions. {Forbidden | NOT_AUTHORIZED}';
            break;

            case 404:
            $message = 'The specified resource does not exist. {Not Found | RESOURCE_NOT_FOUND}';
            break;

            case 405:
            $message = 'The server does not implement the requested HTTP method. {Method Not Allowed | METHOD_NOT_SUPPORTED}';
            break;

            case 406:
            $message = 'The server does not implement the media type that would be acceptable to the client. {Not Acceptable | MEDIA_TYPE_NOT_ACCEPTABLE}';
            break;

            case 415:
            $message = 'The server does not support the request payload’s media type. {Unsupported Media Type | UNSUPPORTED_MEDIA_TYPE}';
            break;

            case 422:
            $message = 'The API cannot complete the requested action, or the request action is semantically incorrect or fails business validation. {Unprocessable Entity | UNPROCCESSABLE_ENTITY}';
            break;

            case 429:
            $message = 'Too many requests. Blocked due to rate limiting. {Unprocessable Entity | RATE_LIMIT_REACHED}';
            break;

            case 500:
            $message = 'An internal server error has occurred. {Internal Server Error | INTERNAL_SERVER_ERROR}';
            break;

            case 503:
            $message = 'Service Unavailable. {Service Unavailable | SERVICE_UNAVAILABLE}';
            break;

            default:
            $message = $code;
            break;
        }
        return $message;
    }











    public function fetch()
    {
        // echo "about to fetch :) ..."; die;
        $cart_items = array();
        $cart_total = 0;
        
        $output = array();

        if(!is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart'))) // cart not empty
        {
            foreach ($this->session->userdata('cart') as $key => $item) {
                // var_dump($this->session->userdata('cart')); die;
                $cart = array(
                    'product_id' => $key,
                    'product_name' => $item['product_name'],
                    'product_name_short' => urldecode(ellipsize($item['product_name'], 33)),
                    'product_price' => number_format($item['product_price'])
                );

                $cart_total += $item['product_price'];

                $cart_items[] = array($cart);
            }
            // add format
            $cart_total = number_format($cart_total);
        }
        else // cart empty
        {
            // do nothing
        }

        $output = array(
            'cart_items' => $cart_items,
            'cart_total' => $cart_total,
        );

        echo json_encode($output);
    }
}