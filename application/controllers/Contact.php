<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            /*
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/login');
            }
            */
            $this->load->model('Contact_model');
    }

	public function index()
	{
        $header['page_title'] = 'Contact';

        $data['rows'] = 'rows';
        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('contact', $data);  // load content view
	}
}
