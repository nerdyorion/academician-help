<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// use \Firebase\JWT\JWT;

class Forgot_Password extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function index()
    {
        // set default metas
        $header['page_title'] = 'Forgot Password';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = 'dissertation, final year projects, project topics, essays, coursework, assignment, computer science, information technology, software engineering, management science, law students, masters, bsc, phd, thesis, abstract';
        $header['meta_description'] = 'This is a one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engine…';

        $meta_from_db = getPageMeta();

        if(!empty($meta_from_db))
        {
            $header['page_title'] = $meta_from_db['page_title'];
            $header['meta_title'] = $meta_from_db['meta_title'];
            $header['meta_tags'] = $meta_from_db['meta_tags'];
            $header['meta_description'] = $meta_from_db['meta_description'];
        }
        //---------------------- end meta ---------------------------------

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('forgot-password', $data);  // load content view
    }

    public function send_token()
    {
        $header['page_title'] = 'Forgot password';

        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 

        if($this->form_validation->run() == FALSE) {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        else
        {
            $email = $this->input->post('email');  
            $clean = $this->security->xss_clean($email);
            $userInfo = $this->User_model->getRowByEmail($clean);

            if(!$userInfo){
                $this->session->set_flashdata('error', 'We cant find your email address');
                $this->session->set_flashdata('error_code', 1);
                redirect('Forgot-Password');
            }

            //build token 

            $token = $this->User_model->insertToken($userInfo['id']);                        
            $qstring = base64url_encode($token);                  
            $url = base_url() . 'Reset_Password/token/' . $qstring;
            // $link = '<a href="' . $url . '">' . $url . '</a>'; 

            $this->session->set_flashdata('error', 'A password reset link has been emailed to you successfully.');
            $this->session->set_flashdata('error_code', 0);

            // send token in mail to user
            $to = $userInfo['email'];
            $to_name = $userInfo['full_name'];
            $subject = "Reset Password";
            $message = $this->template_to_user_forgot_password($to_name, $url); // echo $message; die;

            $this->SEND_MAIL($to, $to_name, $subject, $message);
            // --------------------------------------------
        }
        redirect('Forgot-Password');
    }

    private function template_to_user_forgot_password($to_name, $token_link)
    {
        // $url = $this->config->base_url() . 'login';
        // $url = urlencode($token_link);
        $url = $token_link;

        return "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/xhtml\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\">

        <body style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\">

        <table class=\"body-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
        <td class=\"container\" width=\"600\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; width: 100% !important; margin: 0 auto; padding: 0;\" valign=\"top\">
            <div class=\"content\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 10px;\">
                <table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; padding: 0; border: 1px solid #e9e9e9;\" bgcolor=\"#fff\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"alert alert-warning\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #4aa3df; margin: 0; padding: 20px;\" align=\"center\" bgcolor=\"#4aa3df\" valign=\"top\">
                            Reset Password
                        </td>
                    </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;\" valign=\"top\">
                            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Hello $to_name,
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        A password reset request was initiated for your account.
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        <a href=\"$url\" class=\"btn-primary\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #348eda; margin: 0; padding: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;\">Reset Password &raquo;</a>
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        If you did not request a password request, no further action is required.
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Thanks for choosing " . $this->config->item('app_name') . ".
                                    </td>
                                </tr></table></td>
                    </tr></table><div class=\"footer\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;\">
                    <table width=\"100%\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"aligncenter content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;\" align=\"center\" valign=\"top\">
                    <br />admin@academicianhelp.co.uk
                    <br />&copy; " . date("Y") . " " . $this->config->item('app_name') . ".</td>
                        </tr></table></div></div>
                </td>
                <td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
            </tr></table></body>
        </html>";
    }

    private function SEND_MAIL($to, $to_name, $subject, $message) {

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://md-59.webhostbox.net',
            'smtp_port' => 465,
            'smtp_user' => 'smtp-mailer@brilloconnetz.com', 
            'smtp_pass' => 'smtp-mailer123', 
            'mailtype' => 'html',
            'charset' => 'utf-8', // utf-8 iso-8859-1
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from('admin@academicianhelp.co.uk', 'AcademicianHelp');
        $this->email->to($to, $to_name);
        $this->email->bcc($this->config->item('admin_bcc_email'));
        $this->email->subject($subject);
        $this->email->message($message);

        if($this->email->send())
        {
            return TRUE;
        }
        else
        {
            $error = "Message could not be sent.<br />";
            $error .= "Message: $message" . "<br />";
            $error .= "Mailer Error: " . $this->email->print_debugger() . "<br />";
            // error_log($this->email->print_debugger());
            error_log($error);
            return FALSE;
        }
    }
}