<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Free_Project_Proposals extends CI_Controller {

    private $upload_save_path = '/var/www/html/academicianhelp-free-project-proposal-files/';
    // private $upload_save_path = '/home/brilll8h/public_html/academicianhelp-free-project-proposal-files/';

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Level_model');
        $this->load->model('Free_Project_Proposal_model');
        $this->load->model('Download_model');
        $this->load->library("pagination");
    }

    public function index()
    {
        // set default metas
        $header['page_title'] = 'Free Project Proposals';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = 'dissertation, final year projects, project topics, essays, coursework, assignment, computer science, information technology, software engineering, management science, law students, masters, bsc, phd, thesis, abstract';
        $header['meta_description'] = 'This is a one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engine…';

        $meta_from_db = getPageMeta();

        if(!empty($meta_from_db))
        {
            $header['page_title'] = $meta_from_db['page_title'];
            $header['meta_title'] = $meta_from_db['meta_title'];
            $header['meta_tags'] = $meta_from_db['meta_tags'];
            $header['meta_description'] = $meta_from_db['meta_description'];
        }
        //---------------------- end meta ---------------------------------
        
        $data['levels'] = $this->Level_model->getRandom(5);
        $data['levels_all'] = $this->Level_model->getRowsDropDown();
        $data['recent_items'] = $this->Free_Project_Proposal_model->getRecentItems(5); // get last 5

        // Pagination
        $id = isset($_GET['id']) ? trim($_GET['id']) : FALSE;
        $level_id = isset($_GET['level_id']) ? trim($_GET['level_id']) : FALSE;
        $title = isset($_GET['title']) ? trim($_GET['title']) : FALSE;

        if($id || $level_id || $title)
        {
            $config["total_rows"] = $this->Free_Project_Proposal_model->filter_record_count($id, $level_id, $title);
        }
        else
        {
            $config["total_rows"] = $this->Free_Project_Proposal_model->record_count();
        }



        $config["base_url"] = base_url() . "Free-Project-Proposals/index";
        $config["per_page"] = 4;
        $config["uri_segment"] = 3;
        $config['use_page_numbers']  = TRUE;
        $config['reuse_query_string']=TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        // $config['cur_tag_open'] = "<li class='active'><span><b>";
        // $config['cur_tag_close'] = "</b></span></li>";
        $config['cur_tag_open'] = "<li class=\"active\"><a href=\"" . current_url() . "#\">";
        $config['cur_tag_close'] = " <span class=\"sr-only\">(current)</span></a></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        if($id || $level_id || $title)
        {
            $data['rows'] = $this->Free_Project_Proposal_model->filter($config["per_page"], $offset, $id, $level_id, $title);
        }
        else
        {
            $data['rows'] = $this->Free_Project_Proposal_model->getRows($config["per_page"], $offset);
        }
        $data["links"] = $this->pagination->create_links();

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('free-project-proposals', $data);  // load content view
    }

    public function view($id = NULL)
    {
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $header['page_title'] = 'Free Project Proposal';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = 'dissertation, final year projects, project topics, essays, coursework, assignment, computer science, information technology, software engineering, management science, law students, masters, bsc, phd, thesis, abstract';
        $header['meta_description'] = 'This is a one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engine…';

        
        $data['levels'] = $this->Level_model->getRandom(5);
        $data['recent_items'] = $this->Free_Project_Proposal_model->getRecentItems(5); // get last 5

        $data['row'] = $this->Free_Project_Proposal_model->getRows(0, 0, $id);

        if(empty($data['row']))
        {
            redirect('Free-Project-Proposals');
        }

        // $header['page_title'] = ellipsize($data['row']['title'], 40) . ' | Free Project Proposals';
        $header['page_title'] = $data['row']['title'];
        
        //-- set meta with project info   //////////////////////////
        $header['page_title'] = $data['row']['page_title'];
        $header['meta_title'] = $data['row']['meta_title'];
        $header['meta_tags'] = $data['row']['meta_tags'];
        $header['meta_description'] = $data['row']['meta_description'];
        ////-- end set meta ---------------------------------------

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('free-project-proposal-details', $data);  // load content view
    }

    public function download($id)
    {
        if(!is_logged_in())  // check if logged in
        {
            //redirect to login
            redirect('/login?return=Free-Project-Proposals');
        }

        $user_id = (int) $this->session->userdata("user_id");

        $id = (int) $id;
    
        // $this->load->library('zip');
        $this->load->helper('download');
        
        $row = $this->Free_Project_Proposal_model->getRows(0, 0, $id);

        // echo '<pre>'; var_dump($row); die;

        $path = $this->upload_save_path;

        if(!empty($row))
        {
            $this->Download_model->add($id); // default 0 for free_project_proposals
            force_download($path . $row['file_url'], NULL, TRUE);

            // foreach ($rows as $row)
            // {
            //     $this->Download_model->add($row['item_id']);
            //     $this->zip->read_file($path . $row['file_url']);
            // }

            // $this->zip->download('free_proposal_#' . $id . '_academicianhelp.zip');
        }
        else
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Unable to download file(s)! Please try again later.");
            redirect('Free-Project-Proposals', 'refresh');
        }
    }
}