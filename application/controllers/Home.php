<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Portfolio_model');
        $this->load->model('Service_model');
        $this->load->model('User_model');
        $this->load->helper('captcha');
    }

    public function index()
    {
        // set default metas
        $header['page_title'] = 'Home';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = 'dissertation, final year projects, project topics, essays, coursework, assignment, computer science, information technology, software engineering, management science, law students, masters, bsc, phd, thesis, abstract';
        $header['meta_description'] = 'This is a one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engine…';

        $meta_from_db = getPageMeta();

        if(!empty($meta_from_db))
        {
            $header['page_title'] = $meta_from_db['page_title'];
            $header['meta_title'] = $meta_from_db['meta_title'];
            $header['meta_tags'] = $meta_from_db['meta_tags'];
            $header['meta_description'] = $meta_from_db['meta_description'];
        }
        //---------------------- end meta ---------------------------------


        $data['featured'] = '';

        $data['captcha_image'] = $this->generateCaptcha();

        if(empty($data['captcha_image']))
        {
            $data['captcha_image'] = "Captcha not generated. Please reload page.";
            // echo "Captcha not generated. Please reload page."; exit;
        }

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('home', $data);  // load content view
    }

    public function csrf()
    {
        echo $this->security->get_csrf_hash();
    }

    public function forgot()
    {
        $header['page_title'] = 'Home';

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 

        if($this->form_validation->run() == FALSE) {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        else
        {
            $email = $this->input->post('email');  
            $clean = $this->security->xss_clean($email);
            $userInfo = $this->user_model->getUserInfoByEmail($clean);

            if(!$userInfo){
                $this->session->set_flashdata('error', 'We cant find your email address');
                $this->session->set_flashdata('error_code', 1);
                redirect('login');
            }

            //build token 

            $token = $this->user_model->insertToken($userInfo->id);                        
            $qstring = $this->base64url_encode($token);                  
            $url = base_url() . 'reset_password/token/' . $qstring;
            $link = '<a href="' . $url . '">' . $url . '</a>'; 

            $message = '';                     
            $message .= '<strong>A password reset has been requested for this email account</strong><br>';
            $message .= '<strong>Please click:</strong> ' . $link;             

            echo $message; //send this through mail
            exit;

        }
        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('home', $data);  // load content view

    }

    private function generateCaptcha($ajax = FALSE)
    {
        $vals = array(
            // 'word'          => 'Random word',
            'img_path'      => './assets/images/captcha/',
            'img_url'       => base_url() . 'assets/images/captcha/',
            'font_path'     => base_url() . 'assets/fonts/raleway/Raleway-ExtraBoldItalic.ttf',
            'img_width'     => '150',
            'img_height'    => 30,
            'expiration'    => 7200,
            'word_length'   => 8,
            'font_size'     => 16,
            'img_id'        => 'captcha_image',
            // 'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'pool'          => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',

            // White background and border, black text and red grid
            'colors'        => array(
                // 'background' => array(255, 255, 255),
                'background' => array(0, 0, 0),
                'border' => array(255, 255, 255),
                // 'text' => array(0, 0, 0),
                'text' => array(255, 255, 255),
                // 'grid' => array(220, 220, 220)
                'grid' => array(20, 22, 220)
            )
        );

        $cap = create_captcha($vals);

        $data = array(
            'captcha_time'  => $cap['time'],
            'ip_address'    => $this->input->ip_address(),
            'word'          => $cap['word']
        );

        if(!empty($data['word']))
        {
            $query = $this->db->insert_string('captcha', $data);
            $this->db->query($query);
        }

        if($ajax == FALSE)
        {
            return $cap['image'];
        }
        else
        {
            echo $cap['image'];
        }
    }

    private function deleteExpiredCaptcha()
    {
        // First, delete old captchas
        $expiration = time() - 7200; // Two hour limit
        $this->db->where('captcha_time < ', $expiration)
            ->delete('captcha');
    }

    private function validateCaptcha()
    {
        // Then see if a captcha exists:
        $expiration = time() - 7200; // Two hour limit
        $sql = 'SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?';
        $binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
        $query = $this->db->query($sql, $binds);
        $row = $query->row();

        if ($row->count == 0)
        {
            // echo 'You must submit the word that appears in the image.';
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    

    public function zview($username)
    {
        // $data['row'] = $this->User_model->getRows(0, 0, $id);
        $data['row'] = $this->User_model->getUserFullProfile($username);

        if(empty($data['row']))
        {
            redirect("/");
        }

        $header['page_title'] = $data['row']['stage_name'];

        $data['username'] = $username;
        $data['portfolio'] = $this->Portfolio_model->getAllRowsByUserID($data['row']['id']);
        $data['services'] = $this->Service_model->getAllRowsByUserID($data['row']['id']);

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('artist-profile', $data);  // load content view
    }

    public function zservices($username)
    {
        // $data['row'] = $this->User_model->getRows(0, 0, $id);
        $data['row'] = $this->User_model->getUserFullProfile($username);

        if(empty($data['row']))
        {
            redirect("/");
        }

        $header['page_title'] = $data['row']['stage_name'];

        $data['username'] = $username;
        $data['services'] = $this->Service_model->getAllRowsByUserID($data['row']['id']);

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('artist-services', $data);  // load content view
    }
}