<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    private $ref_pages = array("checkout", "cart", "Free-Project-Proposals");

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
            }
            else
            {
                go_to_dashboard();
            }
            $this->load->model('User_model');
            // $this->load->helper('url_helper');
    }

    public function index()
    {
        // set default metas
        $header['page_title'] = 'Login';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = 'dissertation, final year projects, project topics, essays, coursework, assignment, computer science, information technology, software engineering, management science, law students, masters, bsc, phd, thesis, abstract';
        $header['meta_description'] = 'This is a one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engine…';

        $meta_from_db = getPageMeta();

        if(!empty($meta_from_db))
        {
            $header['page_title'] = $meta_from_db['page_title'];
            $header['meta_title'] = $meta_from_db['meta_title'];
            $header['meta_tags'] = $meta_from_db['meta_tags'];
            $header['meta_description'] = $meta_from_db['meta_description'];
        }
        //---------------------- end meta ---------------------------------

        $data['return'] = '';

        // if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
        // {
        //     $return = $_SERVER['HTTP_REFERER']; // use basename
        //     var_dump($return); die;
        // }
        $pages = $this->ref_pages;

        if(isset($_GET['return']) && !empty($_GET['return']) && in_array($_GET['return'], $pages))
        {
            $data['return'] = trim($_GET['return']);
            // var_dump($data['return']); die;
        }

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $data['page_title'] = 'Login';  // set page title
        $this->load->view('header', $header);  // load header view
        $this->load->view('login', $data);  // load content view

        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[8000]');
    }

    public function secure()
    {
        $data['return'] = '';
        $query = '';

        $pages = $this->ref_pages;

        // if(isset($_GET['return']) && !empty($_GET['return']) && in_array($_GET['return'], $pages))
        // {
        //     $data['return'] = trim($_GET['return']);
        //     $query = '?ref=' . $data['return'];
        //     // var_dump($data['return']); die;
        // }

        if(!is_null($this->input->post('return')) && !empty($this->input->post('return')) && in_array(trim($this->input->post('return')), $pages))
        {
            $data['return'] = trim($this->input->post('return'));
            $query = '?ref=' . $data['return'];
            // var_dump($data['return']); die;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[8000]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect('/login' . $query);
        }
        else
        {
            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $data['page_title'] = 'Login';  // set page title
            $data['login'] = $this->User_model->login();  // login
            if(!$data['login'])
            {
                $errors = "Invalid Login!";
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

                redirect('/login' . $query);
            }
            else
            {
                // check if suspended an display message
                // var_dump($data['login']['is_suspended']); die;
                if((int) $data['login']['is_suspended'] == 1)
                {
                    $errors = "Your account has been suspended. Kindly contact us to resolve.";
                    $this->session->set_flashdata('error', $errors);
                    $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

                    redirect('/login' . $query);
                }

                $this->User_model->updateLastLogin($data['login']['id']);  //  update last login

                if(!empty($data['return']))
                {
                    redirect($data['return']);
                }
                else
                {
                    go_to_dashboard();
                }
            }
        }
    }
}