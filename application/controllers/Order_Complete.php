<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_Complete extends CI_Controller {

    public function __construct()
    {
            parent::__construct();

            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/login');
            }
            $this->load->model('User_model');

            // NB: Roles
            // Role ID 1 - Super Administrator
            // Role ID 2 - Administrator
            // Role ID 3 - Student
    }

    public function index()
    {
        // set default metas
        $header['page_title'] = 'Order Complete';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = $this->config->item('meta_tags_default');
        $header['meta_description'] = $this->config->item('meta_description_default');

        $meta_from_db = getPageMeta();

        if(!empty($meta_from_db))
        {
            $header['page_title'] = $meta_from_db['page_title'];
            $header['meta_title'] = $meta_from_db['meta_title'];
            $header['meta_tags'] = $meta_from_db['meta_tags'];
            $header['meta_description'] = $meta_from_db['meta_description'];
        }
        //---------------------- end meta ---------------------------------

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $data['rows'] = '';
        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('order-complete', $data);  // load content view
    }
}
