<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

    // private $upload_save_path = '/var/www/html/academicianhelp-past-exam-solution-files/';
    private $upload_save_path = '/home/brilll8h/public_html/academicianhelp-past-exam-solution-files/';

    public function __construct()
    {
        parent::__construct();
        if(!is_logged_in())  // check if logged in
        {
            //redirect to login
            redirect('/login');
        }

        $this->load->model('User_model');
        $this->load->model('Order_model');
        $this->load->model('Download_model');
        $this->load->library("pagination");
    }

    public function index()
    {
        $user_id = (int) $this->session->userdata("user_id");

        // set default metas
        $header['page_title'] = 'My Orders';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = 'dissertation, final year projects, project topics, essays, coursework, assignment, computer science, information technology, software engineering, management science, law students, masters, bsc, phd, thesis, abstract';
        $header['meta_description'] = 'This is a one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engine…';

        $meta_from_db = getPageMeta();

        if(!empty($meta_from_db))
        {
            $header['page_title'] = $meta_from_db['page_title'];
            $header['meta_title'] = $meta_from_db['meta_title'];
            $header['meta_tags'] = $meta_from_db['meta_tags'];
            $header['meta_description'] = $meta_from_db['meta_description'];
        }
        //---------------------- end meta ---------------------------------
        
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        // Pagination
        $config["base_url"] = base_url() . "orders/index";
        $config["total_rows"] = $this->Order_model->record_count("student", $user_id);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config['use_page_numbers']  = TRUE;
        $config['reuse_query_string']=TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class=\"active\"><a href=\"" . current_url() . "#\">";
        $config['cur_tag_close'] = " <span class=\"sr-only\">(current)</span></a></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        $data["rows"] = $this->Order_model->getRowsByUserID($config["per_page"], $offset, $user_id);
        $data["links"] = $this->pagination->create_links();
    
        // echo "<pre>"; var_dump($data["rows"]); die;

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('orders', $data);  // load content view
    }

    public function download($id)
    {
        $user_id = (int) $this->session->userdata("user_id");

        $id = (int) $id;
    
        $this->load->library('zip');
        // $this->load->helper('download');
        
        $rows = $this->Order_model->getOrderItemsByUserID($user_id, $id);

        // echo '<pre>'; var_dump($rows); die;

        $path = $this->upload_save_path;

        if(!empty($rows))
        {
            // force_download($path . $data['row']['file_url'], NULL, TRUE);

            foreach ($rows as $row)
            {
                $this->Download_model->add($row['item_id'], 1); // 1 for paid past_exam_solution
                $this->zip->read_file($path . $row['file_url']);
            }

            $this->zip->download('order_#' . $id . '_academicianhelp.zip');
        }
        else
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Unable to download file(s)! Please try again later.");
            redirect('/orders', 'refresh');
        }
    }
}