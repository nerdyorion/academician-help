<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Past_Exam_Solution extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Country_model');
        $this->load->model('University_model');
        $this->load->model('Past_Exam_Solution_model');
        $this->load->library("pagination");
    }

    public function index()
    {
        // set default metas
        $header['page_title'] = 'Past Examination Papers';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = 'dissertation, final year projects, project topics, essays, coursework, assignment, computer science, information technology, software engineering, management science, law students, masters, bsc, phd, thesis, abstract';
        $header['meta_description'] = 'This is a one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engine…';

        $meta_from_db = getPageMeta();

        if(!empty($meta_from_db))
        {
            $header['page_title'] = $meta_from_db['page_title'];
            $header['meta_title'] = $meta_from_db['meta_title'];
            $header['meta_tags'] = $meta_from_db['meta_tags'];
            $header['meta_description'] = $meta_from_db['meta_description'];
        }
        //---------------------- end meta ---------------------------------
        
        $data['countries'] = $this->Country_model->getRows(0, 0);
        $data['universities'] = $this->University_model->getRandom(5);
        $data['universities_all'] = $this->University_model->getRowsDropDown();
        $data['recent_items'] = $this->Past_Exam_Solution_model->getRecentItems(5); // get last 5

        // Pagination
        $id = isset($_GET['id']) ? trim($_GET['id']) : FALSE;
        $country_id = isset($_GET['country_id']) ? trim($_GET['country_id']) : FALSE;
        $university_id = isset($_GET['university_id']) ? trim($_GET['university_id']) : FALSE;
        $title = isset($_GET['title']) ? trim($_GET['title']) : FALSE;

        if($country_id)
        {
            $data['universities_all'] = $this->University_model->getRowsByCountryIDDropDown($country_id);
        }

        if($id || $country_id || $university_id || $title)
        {
            $config["total_rows"] = $this->Past_Exam_Solution_model->filter_record_count($id, $country_id, $university_id, $title);
        }
        else
        {
            $config["total_rows"] = $this->Past_Exam_Solution_model->record_count();
        }



        $config["base_url"] = base_url() . "Past-Exam-Solution/index";
        $config["per_page"] = 4;
        $config["uri_segment"] = 3;
        $config['use_page_numbers']  = TRUE;
        $config['reuse_query_string']=TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        // $config['cur_tag_open'] = "<li class='active'><span><b>";
        // $config['cur_tag_close'] = "</b></span></li>";
        $config['cur_tag_open'] = "<li class=\"active\"><a href=\"" . current_url() . "#\">";
        $config['cur_tag_close'] = " <span class=\"sr-only\">(current)</span></a></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        if($id || $country_id || $university_id || $title)
        {
            $data['rows'] = $this->Past_Exam_Solution_model->filter($config["per_page"], $offset, $id, $country_id, $university_id, $title);
        }
        else
        {
            $data['rows'] = $this->Past_Exam_Solution_model->getRows($config["per_page"], $offset);
        }
        $data["links"] = $this->pagination->create_links();

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('past-exam-solution', $data);  // load content view
    }

    public function getUniversitiesByCountryID($country_id)
    {
        $country_id = (int) $country_id;

        $universities = $country_id == 0 ? $this->University_model->getRowsDropDown() : $this->University_model->getRowsByCountryIDDropDown($country_id);

        $output = '<option value="0" selected="selected">All</option>';

        foreach ($universities as $university) {
            $output .= '<option value="' . $university['id'] . '" >' . $university['name'] . '</option>';
        }

        echo $output;
    }

    public function view($id = NULL)
    {
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $header['page_title'] = 'Past Examination Paper';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = 'dissertation, final year projects, project topics, essays, coursework, assignment, computer science, information technology, software engineering, management science, law students, masters, bsc, phd, thesis, abstract';
        $header['meta_description'] = 'This is a one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engine…';
        
        $data['universities'] = $this->University_model->getRandom(5);
        $data['recent_items'] = $this->Past_Exam_Solution_model->getRecentItems(5); // get last 5

        $data['row'] = $this->Past_Exam_Solution_model->getRows(0, 0, $id);

        if(empty($data['row']))
        {
            redirect('Past-Exam-Solution');
        }

        // $header['page_title'] = ellipsize($data['row']['title'], 40) . ' | Past Examination Papers';
        $header['page_title'] = $data['row']['title'];
        
        //-- set meta with project info   //////////////////////////
        $header['page_title'] = $data['row']['page_title'];
        $header['meta_title'] = $data['row']['meta_title'];
        $header['meta_tags'] = $data['row']['meta_tags'];
        $header['meta_description'] = $data['row']['meta_description'];
        ////-- end set meta ---------------------------------------

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('past-exam-solution-details', $data);  // load content view
    }
}