<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->load->model('Product_model');
            $this->load->model('Subscription_model');
            $this->load->library("pagination");
            // $this->load->helper('url_helper');
    }

    public function index()
    {
        $header['page_title'] = 'Product Title';

        $data['mobile_contents'] = $this->Product_model->getRowsFrontEnd(8, 0, 1); // fetching 8 records for homepage
        $data['music_crbt'] = $this->Product_model->getRowsFrontEnd(8, 0, 2); // fetching 8 records for homepage

        $this->load->view('header', $header);  // load header view
        $this->load->view('products', $data);  // load content view
    }

    public function mobileContents()
    {
        $header['page_title'] = 'Mobile Contents';

        $enriched_headers = getHeaderEnrichment();
        $response_api_error = "";

        $data['HTTP_TELCO'] = empty($enriched_headers) ? '' : $enriched_headers['telco'];
        $data['HTTP_MSISDN'] = empty($enriched_headers) ? '' : $enriched_headers['msisdn'];

        // Pagination
        // $config = array();
        $config["base_url"] = base_url() . "mobile-contents";
        $config["total_rows"] = $this->Product_model->record_count(1);
        $config["per_page"] = 8;
        $config["uri_segment"] = 2;
        $config['use_page_numbers']  = TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
            // (page_no * per_page) - per_page
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        $data['rows'] = $this->Product_model->getRowsFrontEndSingleCategory($config["per_page"], $offset, 1);
        $data["links"] = $this->pagination->create_links();
        $data["category"] = "mobile-contents";


        $this->load->view('header', $header);  // load header view
        $this->load->view('products', $data);  // load content view
    }

    public function musicCrbt()
    {
        $header['page_title'] = 'Music / CRBT';

        $enriched_headers = getHeaderEnrichment();
        $response_api_error = "";

        $data['HTTP_TELCO'] = empty($enriched_headers) ? '' : $enriched_headers['telco'];
        $data['HTTP_MSISDN'] = empty($enriched_headers) ? '' : $enriched_headers['msisdn'];

        // Pagination
        // $config = array();
        $config["base_url"] = base_url() . "music-crbt";
        $config["total_rows"] = $this->Product_model->record_count(2);
        $config["per_page"] = 8;
        $config["uri_segment"] = 2;
        $config['use_page_numbers']  = TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
            // (page_no * per_page) - per_page
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        $data['rows'] = $this->Product_model->getRowsFrontEndSingleCategory($config["per_page"], $offset, 2);
        $data["links"] = $this->pagination->create_links();
        $data["category"] = "crbt";



        $this->load->view('header', $header);  // load header view
        $this->load->view('products', $data);  // load content view
    }

    public function view($id)
    {
        $data['row'] = $this->Product_model->getRows(0, 0, $id);

        if(empty($data['row']))
        {
            redirect("/");
        }

        $enriched_headers = getHeaderEnrichment();
        $response_api_error = "";

        $data['HTTP_TELCO'] = empty($enriched_headers) ? '' : $enriched_headers['telco'];
        $data['HTTP_MSISDN'] = empty($enriched_headers) ? '' : $enriched_headers['msisdn'];

        // define('SUBSCRIBE_API', 'https://portal.hollatags.com/processor/subscribe_web/?confirmation=yes&');
        define('SUBSCRIBE_API', 'https://portal.hollatags.com/processor/subscribe_web/?');
        define('SUBSCRIBE_RBT_API', 'http://rbt.api.hollatags.com/rbt/v1/subscription?token=' . md5('hollatagshollatags123'.date('Ymd')) . '&channel=funmobile&');

        // subscription table params
        $product_id = $id;
        // $phone
        $via = $_SERVER['HTTP_USER_AGENT'];
        // $successful
        // $error_reason

        // if header is enriched, initiate subscription
        if(!empty($data['HTTP_TELCO']) && !empty($data['HTTP_MSISDN']))
        {
            //CALL_API
            // add request to log
            $params = array();

            $params["ip"] = $_SERVER['REMOTE_ADDR'];
            $params["time"] = date('h:i:sa');
            $params["header_enriched"] = "yes";
            $params["network"] = $data['HTTP_TELCO'];
            $params["msisdn"] = $data['HTTP_MSISDN'];
            //$logContent = $logContent . "SERVICE: " . $service "\n";
            //$logContent = $logContent . "SUBSCRIBED: YES" . "\n";

            $logContent = "\n" . json_encode($params);

            #log for debuging
            $input_data = $logContent. "\n";

            $text = $input_data;

            log_subscribe($text);

            $json = array();
            // $json['error']['status'] = false;
            // $json['error']['message'] = "";
            $response_api_error = '';

            //Get Phone Number
            $phone_number = format_msisdn(filter_var($data['HTTP_MSISDN'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH));

            //Get header network
            $network_selection = strtoupper($data['HTTP_TELCO']);
            $service_code_text = $network_selection;

            $service_code = '';
            $service_keyword = '';
            $service_code_error = true;
            $service_keyword_error = true;

            switch ($network_selection) {
                case 'MTN':
                    $service_code = $data['row']['mtn_service_code'];
                    $service_keyword = $data['row']['mtn_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'AIRTEL':
                    $service_code = $data['row']['airtel_service_code'];
                    $service_keyword = $data['row']['airtel_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'GLO':
                    $service_code = $data['row']['glo_service_code'];
                    $service_keyword = $data['row']['glo_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'EMTS':
                    $service_code = $data['row']['emts_service_code'];
                    $service_keyword = $data['row']['emts_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'NTEL':
                    $service_code = $data['row']['ntel_service_code'];
                    $service_keyword = $data['row']['ntel_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                default:
                    # code...
                    break;
            }
            // $json['error']['status'] = true;
            // $json['error']['message'] = "Service not yet availabe for selected network!!! Please subscribe to our newsletter to stay updated of new additions or check back later. Thank you.";
            if((empty($phone_number)) || (strlen($phone_number) != 13) || (empty($network_selection)) )
            {
                $json['error']['status'] = true;
                $json['error']['message'] = "Invalid Phone / Network.; Phone: $phone_number; Network: $network_selection";
            }
            elseif(($service_code_error == true) || ($service_keyword_error == true))
            {
                $json['error']['status'] = true;
                $json['error']['message'] = "Service not yet availabe for selected network!!! Please check back later. Thank you.";
            }
            else
            {
                if((int) $data['row']['category_id'] == 1) // Mobile Content
                {
                    // $response = HTTPGet(SUBSCRIBE_API . "msisdn=$phone_number&keyword=$service_keyword&source=MobileFun&shortcode=$service_code&operator=$service_code_text");
                    $response = HTTPGet(SUBSCRIBE_API . "confirmation=yes&msisdn=$phone_number&keyword=YES&source=MobileFun&shortcode=$service_code&operator=$service_code_text");
                }
                else // Music / CRBT
                {
                    $phone_number = substr($phone_number, 3, strlen($phone_number)); // change phone format to 8183988...
                    if($service_code_text == "EMTS") // RBT available only to EMTS for now
                    {
                        $response_array = json_decode(HTTPGet(SUBSCRIBE_RBT_API . "msisdn=$phone_number&song_code=$service_code&action=ADDRBT"), true);
                        //var_dump($response_array); die();
                        $response = $response_array['message'];
                        if($response == "Invalid msisdn, a valid msisdn is in the formart 80XXXXXXXX")
                        {
                            $response = "ERROR-INVALID-NUMBER";
                        }
                        elseif($response != "Success. The requested operation is completed.")
                        {
                            $json['error']['raw_response'] = $response;
                            $response_api_error = $response;
                            $response = "Unable to complete subscription, please try again later.";
                        }
                        else
                        {
                            //
                        }
                    }
                    else
                    {
                        $response = "RBT currently not available for selected network, please try again later.";

                        $params = array();

                        $params["network"] = $service_code_text;
                        $params["service_code"] = $service_code;
                        $params["service_keyword"] = $service_keyword;
                        $params["subscribed"] = "NO; RBT currently not available for selected network, please try again later.";

                        $text = json_encode($params);
                        log_subscribe($text);

                        // $json['redirect'] = ;
                    }
                }
                switch ($response) {
                    case "ok":
                        $json['error']['status'] = false;
                        $json['success'] = 'Thank you, You have successfully subscribed to this service.';

                        // confirm subscription
                        $response = HTTPGet(SUBSCRIBE_API . "msisdn=$phone_number&keyword=YES&source=MobileFun&shortcode=$service_code&operator=$service_code_text");
                        break;

                    case "Success. The requested operation is completed.": // rbt success message
                        $json['error']['status'] = false;
                        $json['success'] = 'Thank you, You have successfully subscribed to this tune.';
                        break;

                    case "ERROR-INVALID-SHORTCODE":
                        $json['error']['status'] = true;
                        $json['error']['message'] = "Unable to complete subscription at the moment. Please try again later. (invalid shortcode)";
                        break;

                    case "ERROR-INVALID-NUMBER":
                        $json['error']['status'] = true;
                        $json['error']['message'] = "Invalid phone number! Please enter the correct number and try again.";
                        break;

                    case "ERROR-PARAMETER-MISSING":
                        $json['error']['status'] = true;
                        $json['error']['message'] = "Unable to complete subscription at the moment. Please try again later. (missing parameters)";
                        break;

                    case "ERROR-INVALID-KEYWORD":
                        $json['error']['status'] = true;
                        $json['error']['message'] = "Unable to complete subscription at the moment. Please try again later. (invalid keyword)";
                        break;

                    case "ERROR-INVALID-KEYWORDok":
                        $json['error']['status'] = true;
                        $json['error']['message'] = "Unable to complete subscription at the moment. Please try again later. (invalid keyword)";
                        break;

                    default:
                        $json['error']['status'] = true;
                        $json['error']['message'] = $response;
                        break;
                }
            }

            if(isset($json['success']))
            {
                // add subscription successful to log
                $params = array();

                $params["network"] = $service_code_text;
                $params["service_code"] = $service_code;
                $params["service_keyword"] = $service_keyword;
                $params["subscribed"] = "YES";

                $text = json_encode($params);
                log_subscribe($text);

                // log status to subscription
                $this->Subscription_model->add($product_id, $phone_number, $via, 1, '');

                $this->session->set_flashdata('error', $json['success']);
                $this->session->set_flashdata('error_code', 0);
            }
            else
            {
                // add subscription unsuccessful to log
                $params = array();

                $params["network"] = $service_code_text;
                $params["service_code"] = $service_code;
                $params["service_keyword"] = $service_keyword;
                $params["subscribed"] = "NO; " . $json['error']['message'] . " Error: " . $response_api_error;

                $text = json_encode($params);
                log_subscribe($text . "\n");

                // log status to subscription
                $this->Subscription_model->add($product_id, $phone_number, $via, 0, $json['error']['message'] . "; RAW Error: " . $response_api_error);

                $this->session->set_flashdata('error', $json['error']['message']);
                $this->session->set_flashdata('error_code', 1);
            }
        }

        //if token exists, subscribeDirect, else display Details page to manually enter number and subscribe

        $token = isset($_GET['token']) ? $_GET['token'] : ''; // echo "Token: $token";
        // var_dump($_SERVER['REQUEST_URI']); 
        // echo "<br/> Token: " . $this->input->get('some_variable', TRUE);
        // echo "<br/> URL: ". current_url();
        // die;

        if(!empty($token))
        {
            // echo "Got Token: $token"; die;

            $token_arr = explode('.', $token);
            $payload = $token_arr[1]; // the string after the first dot is the payload .... see http://jwt.io
            try
            {
                $decoded = json_decode(base64_decode($payload), true);

                // var_dump($decoded);
                // echo "Got Phone from Token: " . $decoded['sub']; die;
                $this->subscribeDirect($id, $decoded['sub'], $token);
            }
            catch(Exception $e)
            {
                $errors = trim($e->getMessage());

                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

                $data['error'] = $this->session->flashdata('error');
                $data['error_code'] = $this->session->flashdata('error_code');
                $header['page_title'] = $data['row']['name'];

                $data['rows'] = 'rows';
                $this->load->view('header', $header);  // load header view
                $this->load->view('product-details', $data);  // load content view
            }
        }
        else
        {
            // echo "Did not get Token"; die;

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = $data['row']['name'];

            $data['rows'] = 'rows';
            $this->load->view('header', $header);  // load header view
            $this->load->view('product-details', $data);  // load content view
        }

    }

    public function subscribe($id)
    {
        $data['row'] = $this->Product_model->getRows(0, 0, $id);

        if(empty($data['row']))
        {
            redirect("/");
        }

        $enriched_headers = getHeaderEnrichment();
        $response_api_error = "";

        $data['HTTP_TELCO'] = empty($enriched_headers) ? '' : $enriched_headers['telco'];
        $data['HTTP_MSISDN'] = empty($enriched_headers) ? '' : $enriched_headers['msisdn'];

        // define('SUBSCRIBE_API', 'https://portal.hollatags.com/processor/subscribe_web/?confirmation=yes&');
        define('SUBSCRIBE_API', 'https://portal.hollatags.com/processor/subscribe_web/?');
        define('SUBSCRIBE_RBT_API', 'http://rbt.api.hollatags.com/rbt/v1/subscription?token=' . md5('hollatagshollatags123'.date('Ymd')) . '&channel=funmobile&');

        // subscription table params
        $product_id = $id;
        // $phone
        $via = $_SERVER['HTTP_USER_AGENT'];
        // $successful
        // $error_reason

        $this->load->library('form_validation');
        $this->form_validation->set_rules('network', 'Network', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|max_length[14]');

        // header not enriched, if posted phone and network manually, trigger subscription, else show page
        if($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
            
            redirect($data['row']['slug']);
        }
        else
        {
            // subscribe api with post values

            $response_api_error = "";
            $category_name = "";
            $service_code_text = "";
            $service_code = "";
            $service_keyword = "";
            $service_code_error = true;
            $service_keyword_error = true;
            $phone_number = "";


            //Get Phone Number
            $phone_number = format_msisdn(filter_var(trim($this->input->post('phone')), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH));

            //Get header network
            $network_selection = filter_var(trim($this->input->post('network')), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $service_code_text = $network_selection;

            $service_code = '';
            $service_keyword = '';

            switch ($network_selection) {
                case 'MTN':
                    $service_code = $data['row']['mtn_service_code'];
                    $service_keyword = $data['row']['mtn_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'AIRTEL':
                    $service_code = $data['row']['airtel_service_code'];
                    $service_keyword = $data['row']['airtel_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'GLO':
                    $service_code = $data['row']['glo_service_code'];
                    $service_keyword = $data['row']['glo_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'EMTS':
                    $service_code = $data['row']['emts_service_code'];
                    $service_keyword = $data['row']['emts_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'NTEL':
                    $service_code = $data['row']['ntel_service_code'];
                    $service_keyword = $data['row']['ntel_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                default:
                    # code...
                    break;
            }

            if((empty($phone_number)) || (strlen($phone_number) != 13))
            {
                $json['error']['status'] = true;
                $json['error']['message'] = "Invalid phone number! Please enter the correct number and try again";
            }
            elseif(empty($network_selection))
            {
                $json['error']['status'] = true;
                $json['error']['message'] = "Invalid network! Please try again";
            }
            elseif(empty($network_selection))
            {
                $json['error']['status'] = true;
                $json['error']['message'] = "Subscribed: NO; Invalid network! Please try again";
            }
            elseif(($service_code_error == true) || ($service_keyword_error == true))
            {
                $json['error']['status'] = true;
                $json['error']['message'] = "Service not yet availabe for selected network!!! Please check back later. Thank you.";
            }
            else
            {
                if((int) $data['row']['category_id'] == 1) // Mobile Content
                {
                    // var_dump(SUBSCRIBE_API . "msisdn=$phone_number&keyword=$service_keyword&source=MobileFun&shortcode=$service_code&operator=$service_code_text"); die;
                    $response = HTTPGet(SUBSCRIBE_API . "msisdn=$phone_number&keyword=$service_keyword&source=MobileFun&shortcode=$service_code&operator=$service_code_text");
                    sleep(1);
                    $response = HTTPGet(SUBSCRIBE_API . "confirmation=yes&msisdn=$phone_number&keyword=YES&source=MobileFun&shortcode=$service_code&operator=$service_code_text");
                }
                else // Music / CRBT
                {
                    $phone_number = substr($phone_number, 3, strlen($phone_number)); // change phone format to 8183988...
                    if($service_code_text == "EMTS") // RBT available only to EMTS for now
                    {
                        $response_array = json_decode(HTTPGet(SUBSCRIBE_RBT_API . "msisdn=$phone_number&song_code=$service_code&action=ADDRBT"), true);
                        //var_dump($response_array); die();
                        $response = $response_array['message'];
                        if($response == "Invalid msisdn, a valid msisdn is in the formart 80XXXXXXXX")
                        {
                            $response = "ERROR-INVALID-NUMBER";
                        }
                        elseif($response != "Success. The requested operation is completed.")
                        {
                            $json['error']['raw_response'] = $response;
                            $response_api_error = $response;
                            $response = "Unable to complete subscription, please try again later.";
                        }
                        else
                        {
                            //
                        }
                    }
                    else
                    {
                        $response = "RBT currently not available for selected network, please try again later.";

                        $params = array();

                        $params["network"] = $service_code_text;
                        $params["service_code"] = $service_code;
                        $params["service_keyword"] = $service_keyword;
                        $params["subscribed"] = "NO; RBT currently not available for selected network, please try again later.";

                        $text = json_encode($params);
                        log_subscribe($text);

                        // $json['redirect'] = ;
                    }
                }
                switch ($response) {
                    case "ok":
                        $json['error']['status'] = false;
                        $json['success'] = 'Thank you, You have successfully subscribed to this service.';

                        // confirm subscription
                        $response = HTTPGet(SUBSCRIBE_API . "msisdn=$phone_number&keyword=YES&source=MobileFun&shortcode=$service_code&operator=$service_code_text");
                        break;

                    case "Success. The requested operation is completed.": // rbt success message
                        $json['error']['status'] = false;
                        $json['success'] = 'Thank you, You have successfully subscribed to this tune.';
                        break;

                    case "ERROR-INVALID-SHORTCODE":
                        $json['error']['status'] = true;
                        $json['error']['message'] = "Unable to complete subscription at the moment. Please try again later. (invalid shortcode)";
                        break;

                    case "ERROR-INVALID-NUMBER":
                        $json['error']['status'] = true;
                        $json['error']['message'] = "Invalid phone number! Please enter the correct number and try again.";
                        break;

                    case "ERROR-PARAMETER-MISSING":
                        $json['error']['status'] = true;
                        $json['error']['message'] = "Unable to complete subscription at the moment. Please try again later. (missing parameters)";
                        break;

                    case "ERROR-INVALID-KEYWORD":
                        $json['error']['status'] = true;
                        $json['error']['message'] = "Unable to complete subscription at the moment. Please try again later. (invalid keyword)";
                        break;

                    case "ERROR-INVALID-KEYWORDok":
                        $json['error']['status'] = true;
                        $json['error']['message'] = "Unable to complete subscription at the moment. Please try again later. (invalid keyword)";
                        break;

                    default:
                        $json['error']['status'] = true;
                        $json['error']['message'] = $response;
                        break;
                }
            }

            if(isset($json['success']))
            {
                // add subscription successful to log
                $params = array();

                $params["network"] = $service_code_text;
                $params["service_code"] = $service_code;
                $params["service_keyword"] = $service_keyword;
                $params["subscribed"] = "YES";

                $text = json_encode($params);
                log_subscribe($text);

                // log status to subscription
                $this->Subscription_model->add($product_id, $phone_number, $via, 1, '');

                $this->session->set_flashdata('error', $json['success']);
                $this->session->set_flashdata('error_code', 0);
            }
            else
            {
                // add subscription unsuccessful to log
                $params = array();

                $params["network"] = $service_code_text;
                $params["service_code"] = $service_code;
                $params["service_keyword"] = $service_keyword;
                $params["subscribed"] = "NO; " . $json['error']['message'] . " Error: " . $response_api_error;

                $text = json_encode($params);
                log_subscribe($text . "\n");

                // log status to subscription
                $this->Subscription_model->add($product_id, $phone_number, $via, 0, $json['error']['message'] . "; RAW Error: " . $response_api_error);

                $this->session->set_flashdata('error', $json['error']['message']);
                $this->session->set_flashdata('error_code', 1);
            }

            redirect($data['row']['slug']);
        }
    }

    public function subscribeDirect($id, $phone, $token)
    {
        $data['row'] = $this->Product_model->getRows(0, 0, $id);

        if(empty($data['row']))
        {
            redirect("/");
        }

        $enriched_headers = getHeaderEnrichment();
        $response_api_error = "";

        $data['HTTP_TELCO'] = empty($enriched_headers) ? '' : $enriched_headers['telco'];
        $data['HTTP_MSISDN'] = empty($enriched_headers) ? '' : $enriched_headers['msisdn'];

        // define('SUBSCRIBE_API', 'https://portal.hollatags.com/processor/subscribe_web/?confirmation=yes&');
        // define('SUBSCRIBE_API', 'https://portal.hollatags.com/processor/subscribe_web/?');
        // define('SUBSCRIBE_RBT_API', 'http://rbt.api.hollatags.com/rbt/v1/subscription?token=' . md5('hollatagshollatags123'.date('Ymd')) . '&channel=funmobile&');

        // subscription table params
        $product_id = $id;
        // $phone
        $via = $_SERVER['HTTP_USER_AGENT'];
        // $successful
        // $error_reason

        $this->load->library('form_validation');
        $this->form_validation->set_rules('network', 'Network', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|max_length[14]');

        // header not enriched, if posted phone and network manually, trigger subscription, else show page
        if(empty($phone))
        {            
            redirect($data['row']['slug']);
        }
        else
        {
            // subscribe api with post values

            $response_api_error = "";
            $category_name = "";
            $service_code_text = "";
            $service_code = "";
            $service_keyword = "";
            $service_code_error = true;
            $service_keyword_error = true;
            $phone_number = "";


            //Get Phone Number
            $phone_number = format_msisdn($phone);

            //Get header network
            $network_selection = 'EMTS';
            $service_code_text = $network_selection;

            $service_code = '';
            $service_keyword = '';

            switch ($network_selection) {
                case 'MTN':
                    $service_code = $data['row']['mtn_service_code'];
                    $service_keyword = $data['row']['mtn_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'AIRTEL':
                    $service_code = $data['row']['airtel_service_code'];
                    $service_keyword = $data['row']['airtel_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'GLO':
                    $service_code = $data['row']['glo_service_code'];
                    $service_keyword = $data['row']['glo_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'EMTS':
                    $service_code = $data['row']['emts_service_code'];
                    $service_keyword = $data['row']['emts_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'NTEL':
                    $service_code = $data['row']['ntel_service_code'];
                    $service_keyword = $data['row']['ntel_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                default:
                    # code...
                    break;
            }

            if((empty($phone_number)) || (strlen($phone_number) != 13))
            {
                $json['error']['status'] = true;
                $json['error']['message'] = "Invalid phone number! Please enter the correct number and try again";
            }
            elseif(empty($network_selection))
            {
                $json['error']['status'] = true;
                $json['error']['message'] = "Invalid network! Please try again";
            }
            elseif(empty($network_selection))
            {
                $json['error']['status'] = true;
                $json['error']['message'] = "Subscribed: NO; Invalid network! Please try again";
            }
            elseif(($service_code_error == true) || ($service_keyword_error == true))
            {
                $json['error']['status'] = true;
                $json['error']['message'] = "Service not yet availabe for selected network!!! Please check back later. Thank you.";
            }
            else
            {
                if((int) $data['row']['category_id'] == 1) // Mobile Content
                {
                    // var_dump(SUBSCRIBE_API . "msisdn=$phone_number&keyword=$service_keyword&source=MobileFun&shortcode=$service_code&operator=$service_code_text"); die;
                    $response = HTTPGet(SUBSCRIBE_API . "msisdn=$phone_number&keyword=$service_keyword&source=MobileFun&shortcode=$service_code&operator=$service_code_text");
                    sleep(1);
                    $response = HTTPGet(SUBSCRIBE_API . "confirmation=yes&msisdn=$phone_number&keyword=YES&source=MobileFun&shortcode=$service_code&operator=$service_code_text");
                }
                else // Music / CRBT
                {
                    $phone_number = substr($phone_number, 3, strlen($phone_number)); // change phone format to 8183988...
                    if($service_code_text == "EMTS") // RBT available only to EMTS for now
                    {
                        $response_array = json_decode(HTTPGet(SUBSCRIBE_RBT_API . "msisdn=$phone_number&song_code=$service_code&action=ADDRBT"), true);
                        //var_dump($response_array); die();
                        $response = $response_array['message'];
                        if($response == "Invalid msisdn, a valid msisdn is in the formart 80XXXXXXXX")
                        {
                            $response = "ERROR-INVALID-NUMBER";
                        }
                        elseif($response != "Success. The requested operation is completed.")
                        {
                            $json['error']['raw_response'] = $response;
                            $response_api_error = $response;
                            $response = "Unable to complete subscription, please try again later.";
                        }
                        else
                        {
                            //
                        }
                    }
                    else
                    {
                        $response = "RBT currently not available for selected network, please try again later.";

                        $params = array();

                        $params["network"] = $service_code_text;
                        $params["service_code"] = $service_code;
                        $params["service_keyword"] = $service_keyword;
                        $params["subscribed"] = "NO; RBT currently not available for selected network, please try again later.";

                        $text = json_encode($params);
                        log_subscribe($text);

                        // $json['redirect'] = ;
                    }
                }
                switch ($response) {
                    case "ok":
                        $json['error']['status'] = false;
                        $json['success'] = 'Thank you, You have successfully subscribed to this service.';

                        // confirm subscription
                        $response = HTTPGet(SUBSCRIBE_API . "msisdn=$phone_number&keyword=YES&source=MobileFun&shortcode=$service_code&operator=$service_code_text");
                        break;

                    case "Success. The requested operation is completed.": // rbt success message
                        $json['error']['status'] = false;
                        $json['success'] = 'Thank you, You have successfully subscribed to this tune.';
                        break;

                    case "ERROR-INVALID-SHORTCODE":
                        $json['error']['status'] = true;
                        $json['error']['message'] = "Unable to complete subscription at the moment. Please try again later. (invalid shortcode)";
                        break;

                    case "ERROR-INVALID-NUMBER":
                        $json['error']['status'] = true;
                        $json['error']['message'] = "Invalid phone number! Please enter the correct number and try again.";
                        break;

                    case "ERROR-PARAMETER-MISSING":
                        $json['error']['status'] = true;
                        $json['error']['message'] = "Unable to complete subscription at the moment. Please try again later. (missing parameters)";
                        break;

                    case "ERROR-INVALID-KEYWORD":
                        $json['error']['status'] = true;
                        $json['error']['message'] = "Unable to complete subscription at the moment. Please try again later. (invalid keyword)";
                        break;

                    case "ERROR-INVALID-KEYWORDok":
                        $json['error']['status'] = true;
                        $json['error']['message'] = "Unable to complete subscription at the moment. Please try again later. (invalid keyword)";
                        break;

                    default:
                        $json['error']['status'] = true;
                        $json['error']['message'] = $response;
                        break;
                }
            }

            if(isset($json['success']))
            {
                // add subscription successful to log
                $params = array();

                $params["token"] = $token;
                $params["network"] = $service_code_text;
                $params["service_code"] = $service_code;
                $params["service_keyword"] = $service_keyword;
                $params["subscribed"] = "YES";

                $text = json_encode($params);
                log_subscribe($text);

                // log status to subscription
                $this->Subscription_model->add($product_id, $phone_number, $via, 1, '');

                // echo 'got success'; die;

                $this->session->set_flashdata('error', $json['success']);
                $this->session->set_flashdata('error_code', 0);
            }
            else
            {
                // add subscription unsuccessful to log
                $params = array();

                $params["token"] = $token;
                $params["network"] = $service_code_text;
                $params["service_code"] = $service_code;
                $params["service_keyword"] = $service_keyword;
                $params["subscribed"] = "NO; " . $json['error']['message'] . " Error: " . $response_api_error;

                $text = json_encode($params);
                log_subscribe($text . "\n");

                // log status to subscription
                $this->Subscription_model->add($product_id, $phone_number, $via, 0, $json['error']['message'] . "; RAW Error: " . $response_api_error);


                // echo 'got error'; die;

                $this->session->set_flashdata('error', $json['error']['message']);
                $this->session->set_flashdata('error_code', 1);
            }

            redirect($data['row']['slug']);
        }
    }
}
