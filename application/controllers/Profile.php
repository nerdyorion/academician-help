<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if(!is_logged_in())  // check if logged in
        {
            //redirect to login
            redirect('/login');
        }

        $this->load->model('User_model');
        $this->load->model('Country_model');
        $this->load->model('Past_Exam_Solution_model');
        $this->load->library("pagination");
    }

    public function index()
    {
        // set default metas
        $header['page_title'] = 'My Profile';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = 'dissertation, final year projects, project topics, essays, coursework, assignment, computer science, information technology, software engineering, management science, law students, masters, bsc, phd, thesis, abstract';
        $header['meta_description'] = 'This is a one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engine…';

        $meta_from_db = getPageMeta();

        if(!empty($meta_from_db))
        {
            $header['page_title'] = $meta_from_db['page_title'];
            $header['meta_title'] = $meta_from_db['meta_title'];
            $header['meta_tags'] = $meta_from_db['meta_tags'];
            $header['meta_description'] = $meta_from_db['meta_description'];
        }
        //---------------------- end meta ---------------------------------

        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'First Name', 'required|max_length[2000]');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|max_length[2000]');
        // $this->form_validation->set_rules('phone', 'Phone', 'numeric|max_length[13]');
        // $this->form_validation->set_rules('gender', 'Gender', 'required|max_length[255]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));

            if(empty($this->session->flashdata('error'))) // not currently updated
            {
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
            }

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');

            $data['countries'] = $this->Country_model->getRows(0, 0);

            $data['row'] = $this->User_model->getRows(0, 0, $this->session->userdata('user_id'));  // get user profile
            if(empty($data['row']))
                redirect('/logout');
            
            $this->load->view('header', $header);  // load header view
            // $this->load->view('menu');  // load menu view
            $this->load->view('profile', $data);  // load content view
        }
        else
        {
            $this->User_model->updateProfileFrontend($this->session->userdata('user_id'));  // update profile

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Profile updated successfully!");
            
            redirect('/profile');
        }
    }
}