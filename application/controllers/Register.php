<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            /*
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/login');
            }
            */
            //$this->load->helper('url_helper');
            $this->load->model('User_model');
            $this->load->model('Country_model');
            $this->load->model('Coupon_model');

            // NB: Roles
            // Role ID 1 - Super Administrator
            // Role ID 2 - Administrator
            // Role ID 3 - Student
    }

    public function index()
    {
        // set default metas
        $header['page_title'] = 'Register';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = 'dissertation, final year projects, project topics, essays, coursework, assignment, computer science, information technology, software engineering, management science, law students, masters, bsc, phd, thesis, abstract';
        $header['meta_description'] = 'This is a one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engine…';

        $meta_from_db = getPageMeta();

        if(!empty($meta_from_db))
        {
            $header['page_title'] = $meta_from_db['page_title'];
            $header['meta_title'] = $meta_from_db['meta_title'];
            $header['meta_tags'] = $meta_from_db['meta_tags'];
            $header['meta_description'] = $meta_from_db['meta_description'];
        }
        //---------------------- end meta ---------------------------------

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $data['countries'] = $this->Country_model->getRows(0, 0);

        $data['rows'] = '';
        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('register', $data);  // load content view
    }

    public function secure()
    {
        $role_id = 3;

        $_POST['role_id'] = $role_id;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[255]|valid_email');
        $this->form_validation->set_rules('country_id', 'Country', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[8000]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|max_length[8000]|matches[password]');

        $full_name = ucfirst(trim($this->input->post('first_name'))) . ' ' . ucfirst(trim($this->input->post('last_name'))) ;
        $email = trim($this->input->post('email'));
        $register_route = '/register';

        // You could set the title in session like above for example
        $this->session->set_flashdata('first_name', $this->input->post('first_name'));
        $this->session->set_flashdata('last_name', $this->input->post('last_name'));
        $this->session->set_flashdata('country_id', $this->input->post('country_id'));
        $this->session->set_flashdata('email', $this->input->post('email'));

        if($this->User_model->emailExists($email))
        {
            $errors = "You are already registered. <a href='login'>Please login to proceed &raquo;</a>";
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect($register_route);
        }
        elseif ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $this->session->mark_as_flash('validation_errors'); 

            redirect($register_route);
        }
        else
        {
            $insert_id = $this->User_model->addSignup();

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Registration successful. <a href='login'>Please login to proceed &raquo;</a>");

            $first_name = trim($this->input->post('first_name'));
            $last_name = trim($this->input->post('last_name'));
            $email = trim($this->input->post('email'));

            $to = $email;
            $to_name = "$first_name $last_name";
            $subject = "Welcome to AcademicianHelp";
            $message = $this->template_new_user($to_name);

            // send welcome mail to user
            // sendmail($to, $to_name, $subject, $message);

            $to = filter_var(strtolower($to), FILTER_SANITIZE_EMAIL);
            $to_name = filter_var(ucwords($to_name), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

            $this->SEND_MAIL($to, $to_name, $subject, $message);

            // add notification to admin dashboard
            $notification_user_id = 0; // for super admin and admin

            if ($role_id == 3) // stage name for student alone
            {
                $notification_message = 'New student sign-up: <a href="' . base_url() . 'admin123/users/view/' . $insert_id . '">' . $first_name . ' ' . $last_name . '</a>';
            }
            add_notification($notification_user_id, $notification_message);
            // --------------------------------------------------

            if(!is_null($this->input->post('newsletter'))) // subscribe to newsletter
            {
                redirect('/subscribe/direct/' . $to . '/register');
            }

            redirect($register_route);
        }
    }

    public function userExists($username)
    {
        echo $this->User_model->usernameExists($username);
    }

    private function template_new_user($to_name)
    {
        $url = $this->config->base_url() . 'login';


        $new_user_coupon_code = '-';
        $new_user_coupon = $this->Coupon_model->getNewUserCoupon();

        if(!empty($new_user_coupon) && isset($new_user_coupon['code']) && !empty($new_user_coupon['code']) )
        {
            $new_user_coupon_code = $new_user_coupon['code'];
        }

        return "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/xhtml\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\">

        <body style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\">

        <table class=\"body-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
        <td class=\"container\" width=\"600\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; width: 100% !important; margin: 0 auto; padding: 0;\" valign=\"top\">
            <div class=\"content\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 10px;\">
                <table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; padding: 0; border: 1px solid #e9e9e9;\" bgcolor=\"#fff\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"alert alert-warning\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #4aa3df; margin: 0; padding: 20px;\" align=\"center\" bgcolor=\"#4aa3df\" valign=\"top\">
                            AcademicianHelp
                        </td>
                    </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;\" valign=\"top\">
                            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Hello $to_name,
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Welcome to AcademicianHelp, the one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engineering, e.t.c.
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Here is a coupon code for your first purchase with us: <b>$new_user_coupon_code</b>
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Login to continue:
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        <a href=\"$url\" class=\"btn-primary\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #348eda; margin: 0; padding: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;\">Login &raquo;</a>
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Thanks for choosing AcademicianHelp.
                                    </td>
                                </tr></table></td>
                    </tr></table><div class=\"footer\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;\">
                    <table width=\"100%\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"aligncenter content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;\" align=\"center\" valign=\"top\">
                    <br />admin@academicianhelp.co.uk
                    <br />&copy; " . date("Y") . " AcademicianHelp.</td>
                        </tr></table></div></div>
                </td>
                <td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
            </tr></table></body>
        </html>";
    }

    private function SEND_MAIL($to, $to_name, $subject, $message) {

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://md-59.webhostbox.net',
            'smtp_port' => 465,
            'smtp_user' => 'smtp-mailer@brilloconnetz.com', 
            'smtp_pass' => 'smtp-mailer123', 
            'mailtype' => 'html',
            'charset' => 'utf-8', // utf-8 iso-8859-1
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from('admin@academicianhelp.co.uk', 'AcademicianHelp');
        $this->email->to($to, $to_name);
        $this->email->bcc($this->config->item('admin_bcc_email'));
        $this->email->subject($subject);
        $this->email->message($message);

        if($this->email->send())
        {
            return TRUE;
        }
        else
        {
            $error = "Message could not be sent.<br />";
            $error .= "Message: $message" . "<br />";
            $error .= "Mailer Error: " . $this->email->print_debugger() . "<br />";
            // error_log($this->email->print_debugger());
            error_log($error);
            return FALSE;
        }
    }
}
