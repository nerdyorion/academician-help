<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Request_Quote extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('captcha');
    }

    public function index()
    {
        // set default metas
        $header['page_title'] = 'Request Quote';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = 'dissertation, final year projects, project topics, essays, coursework, assignment, computer science, information technology, software engineering, management science, law students, masters, bsc, phd, thesis, abstract';
        $header['meta_description'] = 'This is a one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engine…';

        $meta_from_db = getPageMeta();

        if(!empty($meta_from_db))
        {
            $header['page_title'] = $meta_from_db['page_title'];
            $header['meta_title'] = $meta_from_db['meta_title'];
            $header['meta_tags'] = $meta_from_db['meta_tags'];
            $header['meta_description'] = $meta_from_db['meta_description'];
        }
        //---------------------- end meta ---------------------------------

        $data['row'] = '';

        $data['captcha_image'] = $this->generateCaptcha();

        if(empty($data['captcha_image']))
        {
            $data['captcha_image'] = "Captcha not generated. Please reload page.";
            // echo "Captcha not generated. Please reload page."; exit;
        }


        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('request-quote', $data);  // load content view
    }

    public function getCaptcha()
    {
        return $this->generateCaptcha(TRUE); // ajax request
    }

    public function ajax()
    {
        if(isset($_POST["action"])) {
            $name = $_POST['name'];        // Sender's name
            $email = $_POST['email'];      // Sender's email address
            $phone  = $_POST['phone'];     // Sender's email address
            $message = $_POST['message'];  // Sender's message
            $headers = 'From: ' . $name . ' <website@academicianhelp.co.uk>' . "\r\n";
            $attachments = array();

            $to = 'omotayo@brilloconnetz.com';     // Recipient's email address
            // $to = 'admin@academicianhelp.co.uk';     // Recipient's email address
            $subject = 'Message from AcademicianHelp Request Quote Form'; // Message title

            $body = "Hello, <br /><br /><b>Quote Request from $name </b><br /><br /><b>E-Mail</b>: $email <br /><b>Phone</b>: $phone <br /><b>Relevant Details</b>: $message <br /><br />Regards,"  ;

            // init error message
            $errmsg='';

            // check captcha
            $this->deleteExpiredCaptcha();

            if($this->validateCaptcha() == FALSE)
            {
                $errmsg .= '<p>Please enter the correct captcha.</p>';
            }

            // Check if name has been entered
            if (isset($_POST['name']) && $_POST['name'] == '') {
                // $errmsg .= '<p>Please enter your name.</p>';
            }
            // Check if email has been entered and is valid
            if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $errmsg .= '<p>Please enter a valid email address.</p>';
            }
            //Check if phone number has been entered
            if ( isset($_POST['phone']) && $_POST['phone'] == '') {
                // $errmsg .= '<p>Please enter your phone number.</p>';
            }

            //Check if message has been entered
            if ( isset($_POST['message']) && $_POST['message'] == '') {
                $errmsg .= '<p>Please enter relevant details.</p>';
            }

            //Check if attachments has been uploaded
            if ( isset($_FILES['attachments']['name']) ) {
                $files = array_map('trim', $_FILES['attachments']['name']);
                //$files = array_filter($_FILES['upload']['name']); something like that to be used before processing files.
                // Count # of uploaded files in array
                // $total = count($files);
                $total = count($_FILES['attachments']['name']);

                // Loop through each file
                for($i=0; $i<$total; $i++) {
                    //Get the temp file path
                    $tmpFilePath = $_FILES['attachments']['tmp_name'][$i];

                    //Make sure we have a filepath
                    if ($tmpFilePath != ""){
                        //Setup our new file path
                        // $newFilePath = "/var/www/html/academicianhelp/assets/attachments/" . date('YmdHis') . '_' . $files[$i];
                        $newFilePath = "/home/brilll8h/public_html/academicianhelp.co.uk/assets/attachments/" . date('YmdHis') . '_' . $files[$i];
                        // $newFilePath = "attachments/" . $files[$i];

                        //Upload the file into the temp dir
                        if(move_uploaded_file($tmpFilePath, $newFilePath))
                        {
                            // Handle other code here
                            array_push($attachments, $newFilePath);
                        }
                        else
                        {
                            error_log(json_encode(error_get_last()));
                        }
                    }
                }
            }

            /* Check Google captch validation */
            if( isset( $_POST['g-recaptcha-response'] ) ){
                $error_message = validation_google_captcha( $_POST['g-recaptcha-response'] );
                if($error_message!=''){
                    $errmsg .= $error_message;
                }
            }   

            $result='';
            // If there are no errors, send the email
            if (!$errmsg)
            {
                $inquirer_email = filter_var(strtolower(trim($_POST['email'])), FILTER_SANITIZE_EMAIL);
                $inquirer_name = filter_var(ucwords(trim($_POST['name'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

                $response = $this->SEND_MAIL($inquirer_email, $inquirer_name, $subject, $body, $attachments);

                if ($response === true)
                {
                    $result='<div class="alert alert-success">Thank you for contacting us. Your message has been successfully sent. We will contact you very soon!</div>';
                }
                else
                {
                    $result='<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later. ' . $response . '</div>';
                }
            }
            else{
                $result='<div class="alert alert-danger">'.$errmsg.'</div>';
            }
            echo $result;
        }   
    }

    private function generateCaptcha($ajax = FALSE)
    {
        $vals = array(
            // 'word'          => 'Random word',
            'img_path'      => './assets/images/captcha/',
            'img_url'       => base_url() . 'assets/images/captcha/',
            'font_path'     => base_url() . 'assets/fonts/raleway/Raleway-ExtraBoldItalic.ttf',
            'img_width'     => '150',
            'img_height'    => 30,
            'expiration'    => 7200,
            'word_length'   => 8,
            'font_size'     => 16,
            'img_id'        => 'captcha_image',
            // 'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'pool'          => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',

            // White background and border, black text and red grid
            'colors'        => array(
                // 'background' => array(255, 255, 255),
                'background' => array(0, 0, 0),
                'border' => array(255, 255, 255),
                // 'text' => array(0, 0, 0),
                'text' => array(255, 255, 255),
                // 'grid' => array(220, 220, 220)
                'grid' => array(20, 22, 220)
            )
        );

        $cap = create_captcha($vals);

        $data = array(
            'captcha_time'  => $cap['time'],
            'ip_address'    => $this->input->ip_address(),
            'word'          => $cap['word']
        );

        if(!empty($data['word']))
        {
            $query = $this->db->insert_string('captcha', $data);
            $this->db->query($query);
        }

        if($ajax == FALSE)
        {
            return $cap['image'];
        }
        else
        {
            echo $cap['image'];
        }
    }

    private function deleteExpiredCaptcha()
    {
        // First, delete old captchas
        $expiration = time() - 7200; // Two hour limit
        $this->db->where('captcha_time < ', $expiration)
            ->delete('captcha');
    }

    private function validateCaptcha()
    {
        // Then see if a captcha exists:
        $expiration = time() - 7200; // Two hour limit
        $sql = 'SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?';
        $binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
        $query = $this->db->query($sql, $binds);
        $row = $query->row();

        if ($row->count == 0)
        {
            // echo 'You must submit the word that appears in the image.';
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    private function SEND_MAIL($inquirer_email, $inquirer_name, $subject, $message, $attachments) {

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://md-59.webhostbox.net',
            'smtp_port' => 465,
            'smtp_user' => 'smtp-mailer@brilloconnetz.com', 
            'smtp_pass' => 'smtp-mailer123', 
            'mailtype' => 'html',
            'charset' => 'utf-8', // utf-8 iso-8859-1
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from('website@academicianhelp.co.uk', 'AcademicianHelp Website');
        $this->email->to('admin@academicianhelp.co.uk', 'AcademicianHelp');
        // $this->email->to('omotayo@brilloconnetz.com', 'Omotayo Odupitan');
        $this->email->bcc($this->config->item('admin_bcc_email'));
        $this->email->reply_to($inquirer_email, $inquirer_name);
        if(!empty($attachments))
        {
            foreach ($attachments as $attachment) {
                if(file_exists($attachment))
                {
                    $this->email->attach($attachment);         // Add attachments
                }
            }
        }
        $this->email->subject($subject);
        $this->email->message($message);

        if($this->email->send())
        {
            return TRUE;
        }
        else
        {
            $error = "Message could not be sent.<br />";
            $error .= "Message: $message" . "<br />";
            $error .= "Mailer Error: " . $this->email->print_debugger() . "<br />";
            // error_log($this->email->print_debugger());
            error_log($error);
            return FALSE;
        }
    }

    private function ztest()
    {

        $email = 'nerdyorion@gmail.com';
        $subject = 'Test Subject CI';
        $message = 'Test Message';


        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://md-59.webhostbox.net',
            'smtp_port' => 465,
            'smtp_user' => 'smtp-mailer@brilloconnetz.com', 
            'smtp_pass' => 'smtp-mailer123', 
            'mailtype' => 'html',
            'charset' => 'utf-8', // utf-8 iso-8859-1
            'wordwrap' => TRUE
        );


        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from('website@academicianhelp.co.uk', 'AcademicianHelp Website');
        $this->email->to($email);
        $this->email->bcc($this->config->item('admin_bcc_email'));
        $this->email->reply_to('admin@academicianhelp.co.uk', 'AcademicianHelp Admin');
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->attach('/var/www/html/academicianhelp/assets/images/logo.png');
        if($this->email->send())
        {
            echo 'Email sent.';
        }
        else
        {
            error_log($this->email->print_debugger());
        }
    }

    private function zSEND_MAIL($inquirer_email, $inquirer_name, $subject, $message, $attachments) {
        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'md-59.webhostbox.net';  // Specify main and backup SMTP servers md-59.webhostbox.net 465 // mail.brilloconnetz.com 25
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'smtp-mailer@brilloconnetz.com';                 // SMTP username
        $mail->Password = 'smtp-mailer123';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to
        $mail->Timeout =   15;                                // set the timeout (seconds)

        $mail->setFrom('website@academicianhelp.co.uk', 'AcademicianHelp Website');
        $mail->addAddress('admin@academicianhelp.co.uk', 'AcademicianHelp');     // Add a recipient 
        $mail->addReplyTo($inquirer_email, $inquirer_name);
        // $mail->addAddress('admin@academicianhelp.co.uk', 'AcademicianHelp');     // Add a recipient
        // $mail->addReplyTo('info@example.com', 'Information');
        // $mail->addCC('em@jj.com', 'EM Support');
        //$mail->addBCC('bcc@example.com');
        if(!empty($attachments))
        {
            foreach ($attachments as $attachment) {
                if(file_exists($attachment))
                {
                    $mail->addAttachment($attachment);         // Add attachments
                }
            }
        }

        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = $subject;
        $mail->Body    = $message;
        $mail->AltBody = strip_tags($message);

        if(!$mail->send()) {
            $error = "Message could not be sent.<br />";
            $error .= "Message: $message" . "<br />";
            $error .= "Mailer Error: " . $mail->ErrorInfo . "<br />";
            error_log($error);
            return FALSE;
        } else {
            return TRUE;
        }
    }
}