<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// use \Firebase\JWT\JWT;

class Reset_Password extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function index($token)
    {
        // set default metas
        $header['page_title'] = 'Reset Password';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = $this->config->item('meta_tags_default');
        $header['meta_description'] = $this->config->item('meta_description_default');

        $meta_from_db = getPageMeta();

        if(!empty($meta_from_db))
        {
            $header['page_title'] = $meta_from_db['page_title'];
            $header['meta_title'] = $meta_from_db['meta_title'];
            $header['meta_tags'] = $meta_from_db['meta_tags'];
            $header['meta_description'] = $meta_from_db['meta_description'];
        }
        //---------------------- end meta ---------------------------------

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $data['token'] = $token;

        // $token_raw = $token;   
        $token = base64_decode($token);       
        $cleanToken = $this->security->xss_clean($token);

        $user_info = $this->User_model->isTokenValid($cleanToken); //either false or array();           
            
        if(!$user_info){
            $this->session->set_flashdata('error', 'Token is invalid or expired');
            $this->session->set_flashdata('error_code', 1);
            redirect('login');
        }
        // $data = array(
        //     'firstName' => $user_info['first_name'], 
        //     'email' => $user_info['email'], 
        //     'user_id' => $user_info['id'], 
        //     'token' => base64url_encode($token)
        // );
        $data['email'] = $user_info['email'];
        $data['user_id'] = $user_info['id'];

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('reset-password', $data);  // load content view
    }

    public function token($token)
    {
        $header['page_title'] = 'Reset password';

        // $token = base64_decode($this->uri->segment(4)); 
        $token_raw = $token;   
        $token = base64_decode($token);       
        $cleanToken = $this->security->xss_clean($token);

        $user_info = $this->User_model->isTokenValid($cleanToken); //either false or array();           
            
        if(!$user_info){
            $this->session->set_flashdata('error', 'Token is invalid or expired');
            $this->session->set_flashdata('error_code', 1);
            redirect('login');
        }
        $data = array(
            'firstName' => $user_info['first_name'], 
            'email' => $user_info['email'], 
            'user_id' => $user_info['id'], 
            'token' => base64url_encode($token)
        );
        // $data['email'] = $user_info['email'];
        // $data['user_id'] = $user_info['id'];

        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

        if($this->form_validation->run() == FALSE) {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect('Reset-Password/index/' . $token_raw);
        }
        else
        {
            $this->User_model->updatePassword($data['user_id']);

            $this->session->set_flashdata('error', 'Password changed successfully. Please login now.');
            $this->session->set_flashdata('error_code', 0);

            redirect('login');
        }
    }
}