<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Subscribe extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function ajax()
    {
        $email = $_POST['url'];
        if(!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL) === false)
        {
            $subject = 'Message from AcademicianHelp Email Newsletter Form'; // Message title

            $message = "Hello, <br /><br />Newsletter Subscription from user <br />E-Mail: $email <br /><br />Regards,";

            // Add to AcademicianHelp subscription list on MailChimp
            $this->HTTP_Post($email);

            $response = $this->SEND_MAIL($subject, $message);

            if ($response === true)
            {
                $msg= '<p style="color: #34A853">'."Successfully Subscribed. Thank you.".'</p>';
            }
            else
            {
                $msg= '<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later. ' . $response . '</div>';
            }
            echo $msg;
        }
    }

    public function direct($email, $ref)
    {
        $pages = array("register");

        if(!empty($ref) && in_array($ref, $pages))
        {
            $redirect_route = trim($ref);
            // var_dump($data['return']); die;
        }

        if($ref == "register")
        {
            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Registration successful. <a href='login'>Please login to proceed &raquo;</a>");
        }

        if(!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL) === false)
        {
            $subject = 'Message from AcademicianHelp Email Newsletter Form'; // Message title

            $message = "Hello, <br /><br />Newsletter Subscription from user <br />E-Mail: $email <br /><br />Regards,";

            // Add to AcademicianHelp subscription list on MailChimp
            $this->HTTP_Post($email);

            $response = $this->SEND_MAIL($subject, $message);

            if ($response === true)
            {
                $msg= 1;
                error_log("$email subscribed to newsletter");
            }
            else
            {
                $msg= 0;
                error_log("Error sending mail to admin: $email subscribed to newsletter");
            }
            redirect($redirect_route);
        }
    }


    private function HTTP_Post($email)
    {
        $url = 'https://us17.api.mailchimp.com/3.0/lists/6487b9d3ba/members/';
        $api_key = 'e569c6754f94251cb4c16dc65d5a07cf-us17';
        $username = 'academicianhelp';

        $email = filter_var(strtolower(trim($email)), FILTER_SANITIZE_EMAIL);

        $fields = array(
            "email_address" => $email,
            "status" => "subscribed",
            "merge_fields" => array(
                "FNAME" => "AcademicianHelp User",
                "LNAME" => ""
            )
        );

        $data = json_encode($fields); // echo $data; die;

        $ch = curl_init();  

        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$api_key");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60); // in seconds

        $output = curl_exec($ch);

        if($output === false)
        {
            $output = "Error Number: " . curl_errno($ch) . "<br />";
            $output = $output . "Error String: " . curl_error($ch) . "<br />";
            error_log($output);

            // SEND MAIL (OPTIONAL)
            $message = "Hello," . "<br />" . "<br />";
            $message .= "The MailChimp Subscription URL seems to be currently unavailable. See error below:" . "<br />" . "<br />";
            $message .= "<code>$output</code>" . "<br />" . "<br />";
            $message .= "Please resolve issue." . "<br />" . "<br />";
            $message .= "Regards." . "<br />";

            $to = "omotayo@brilloconnetz.com";
            // $to = "omotayo@brilloconnetz.com, somebodyelse@domain.com";
            $subject = "AcademicianHelp: MailChimp Subscription URL not available";
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // More headers
            $headers .= 'From: <website@academicianhelp.co.uk>' . "\r\n";
            $headers .= 'Bcc: ' . $this->config->item('admin_bcc_email') . "\r\n";
            // $headers .= 'Cc: techsupport@domain.com' . "\r\n";

            mail($to, $subject, $message, $headers);

        }
        else
        {
            // Success
            error_log($output);
        }

        curl_close($ch); // die($output);
        // return $output;
    }

    private function SEND_MAIL($subject, $message) {

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://md-59.webhostbox.net',
            'smtp_port' => 465,
            'smtp_user' => 'smtp-mailer@brilloconnetz.com', 
            'smtp_pass' => 'smtp-mailer123', 
            'mailtype' => 'html',
            'charset' => 'utf-8', // utf-8 iso-8859-1
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from('website@academicianhelp.co.uk', 'AcademicianHelp Website');
        $this->email->to('admin@academicianhelp.co.uk', 'AcademicianHelp');
        $this->email->bcc($this->config->item('admin_bcc_email'));
        $this->email->subject($subject);
        $this->email->message($message);

        if($this->email->send())
        {
            return TRUE;
        }
        else
        {
            $error = "Message could not be sent.<br />";
            $error .= "Message: $message" . "<br />";
            $error .= "Mailer Error: " . $this->email->print_debugger() . "<br />";
            // error_log($this->email->print_debugger());
            error_log($error);
            return FALSE;
        }
    }
}