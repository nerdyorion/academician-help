<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Terms_And_Conditions extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        // set default metas
        $header['page_title'] = 'Testimonials';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = $this->config->item('meta_tags_default');
        $header['meta_description'] = $this->config->item('meta_description_default');

        $meta_from_db = getPageMeta();

        if(!empty($meta_from_db))
        {
            $header['page_title'] = $meta_from_db['page_title'];
            $header['meta_title'] = $meta_from_db['meta_title'];
            $header['meta_tags'] = $meta_from_db['meta_tags'];
            $header['meta_description'] = $meta_from_db['meta_description'];
        }
        //---------------------- end meta ---------------------------------

        $data['row'] = '';

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('terms-and-conditions', $data);  // load content view
    }
}