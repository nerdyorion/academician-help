<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User_Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if(!is_logged_in())  // check if logged in
        {
            //redirect to login
            redirect('/login');
        }

        $this->load->model('User_model');
        $this->load->model('Past_Exam_Solution_model');
        $this->load->model('Download_model');
        $this->load->model('Order_Item_model');
        $this->load->library("pagination");
    }

    public function index()
    {
        $user_id = (int) $this->session->userdata("user_id");

        $full_name = $this->session->userdata("user_full_name");

        // set default metas
        $header['page_title'] = "Welcome $full_name";
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = 'dissertation, final year projects, project topics, essays, coursework, assignment, computer science, information technology, software engineering, management science, law students, masters, bsc, phd, thesis, abstract';
        $header['meta_description'] = 'This is a one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engine…';

        $meta_from_db = getPageMeta();

        if(!empty($meta_from_db))
        {
            $header['page_title'] = $meta_from_db['page_title'];
            $header['meta_title'] = $meta_from_db['meta_title'];
            $header['meta_tags'] = $meta_from_db['meta_tags'];
            $header['meta_description'] = $meta_from_db['meta_description'];
        }
        //---------------------- end meta ---------------------------------

        $data['past_exam_solution_ordered_paid_items_total'] = (int) $this->Order_Item_model->countOrderItemsByUserID('paid', $user_id);
        // $data['past_exam_solution_downloads_total'] = (int) $this->Download_model->record_count('student', 'paid', $user_id);
        $data['free_downloads_total'] = (int) $this->Download_model->record_count('student', 'free', $user_id);
        
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
    
        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('user-home', $data);  // load content view
    }

    public function switchbacktoadmin($id) // log back in to admin account
    {
        $id = (int) $id;
        $data['row'] = $this->User_model->loginAs($id, true); // second param siginifies loggin back as admin
        if($data['row'])
        {
            redirect('/admin123', 'refresh');
        }
        else
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Unable to grant access. Please try again.");
            redirect('/dashboard', 'refresh');
        }
    }
}