<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agents extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            if(!is_super_admin())  // check if is super admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            $this->load->model('User_model');
            //$this->load->helper('url_helper');
    }

	public function index()
	{
     $data['error'] = $this->session->flashdata('error');
     $data['error_code'] = $this->session->flashdata('error_code');
     $header['page_title'] = 'Agents';
     $data['rows'] = $this->User_model->getRowsAgent();

     $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
     $this->load->view($this->config->item('template_dir_admin') . 'menu');
     $this->load->view($this->config->item('template_dir_admin') . 'agents', $data);  // load content view
	}

    public function view($id = NULL)
    {
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $header['page_title'] = 'User Details';
        $data['row'] = $this->User_model->getRows($id);

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'user-details', $data);  // load content view
    }

    public function edit($id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('full_name', 'Full Name', 'required|max_length[2000]');
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|valid_email');
        $this->form_validation->set_rules('gender', 'Gender', 'required|max_length[255]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Update User';  // set page title
            $data['row'] = $this->User_model->getRows($id);
            if(empty($data['row']))
                redirect("/admin123/agents");
            
            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'user-edit', $data);  // load content view
        }
        else
        {
            $header['page_title'] = 'Update User';  // set page title
            $this->User_model->update($id);
            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");
            // get new data after updating
            $data['row'] = $this->User_model->getRows($id);
            if(empty($data['row']))
                redirect("/admin123/agents");

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');

            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'user-edit', $data);

            //redirect('/users/edit/' . $id, 'refresh');
        }
    }

    public function delete($id)
    {
        $data['row'] = $this->User_model->delete($id);
        redirect('/admin123/agents', 'refresh');
    }

    public function switchto($id) // login to agent account
    {
        $id = (int) $id;
        $data['row'] = $this->User_model->loginAs($id);
        if($data['row'])
        {
            redirect('/agent', 'refresh');
        }
        else
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Unable to grant access. Please try again.");
            redirect('/admin123/agents', 'refresh');
        }
    }

    public function suspend($id)
    {
        $data['row'] = $this->User_model->setSuspend($id, 1);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Agent suspended successfully!");

        // send notification to agent dashboard
        $notification_user_id = $id;

        $notification_message = 'Account suspended! Please contact support to resolve.';
        add_notification($notification_user_id, $notification_message);
        // -----------------------------------------------------------------

        redirect('/admin123/agents', 'refresh');
    }

    public function unsuspend($id)
    {
        $data['row'] = $this->User_model->setSuspend($id, 0);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Artist unsuspended successfully!");

        // send notification to agent dashboard
        $notification_user_id = $id;

        $notification_message = 'Account unsuspended! Thank you for choosing US Uber.';
        add_notification($notification_user_id, $notification_message);
        // -----------------------------------------------------------------

        redirect('/admin123/agents', 'refresh');
    }
}
