<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HappyTunes extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
    }

	public function index()
	{
        echo $this->getHeaderEnrichment();
    }

    private function getHeaderEnrichment()
    {
        $msisdn = '';
        if(isset($_SERVER['HTTP_MSISDN']))
        {
        // MTN or GLO
            $msisdn = $_SERVER['HTTP_MSISDN'];
        }
        elseif(isset($_SERVER['HTTP_X_UP_CALLING_LINE_ID']))
        {
        // ETISALAT
            $msisdn = $_SERVER['HTTP_X_UP_CALLING_LINE_ID'];
        }
        else
        {
        // HEADER NOT ENRICHED
        }
        return $msisdn;
    }
}
