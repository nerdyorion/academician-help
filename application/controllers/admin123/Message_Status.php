<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 set_time_limit(0); 

class Message_Status extends Admin_Controller {


    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            if(is_report_admin())  // check if is dnd_search admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            $this->load->model('Message_Status_model');
            //$this->load->helper('url_helper');
    }

	public function index()
	{
    	$this->load->library('form_validation');
        $this->form_validation->set_rules('client_id', 'Client ID', 'trim|required|max_length[8000]');
        $this->form_validation->set_rules('date', 'Date', 'trim|required|max_length[20]');
        $this->form_validation->set_rules('msisdn', 'MSISDN', 'trim|required|max_length[2000]');

    	if ($this->form_validation->run() === FALSE)
    	{
        	$errors = str_replace("<p>","", validation_errors());
        	$errors = str_replace("</p>","", trim($errors));
        	$this->session->set_flashdata('error', $errors);
        	$this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

			$data['error'] = $this->session->flashdata('error');
			$data['error_code'] = $this->session->flashdata('error_code');
        	$header['page_title'] = 'Check Message Status';
        	$data['rows'] = '';
        	
        	$this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        	$this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        	$this->load->view($this->config->item('template_dir_admin') . 'message-status', $data);  // load content view
        }
    	else
    	{
			$data['error'] = $this->session->flashdata('error');
			$data['error_code'] = $this->session->flashdata('error_code');
    	    $header['page_title'] = 'Check Message Status';
			$data['rows'] = $this->Message_Status_model->checkStatus();

        	$this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        	$this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
    	    $this->load->view($this->config->item('template_dir_admin') . 'message-status', $data);
    	}
	}
}
