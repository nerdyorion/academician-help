<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends Admin_Controller {

    private $upload_save_path = '/var/www/html/academicianhelp-past-exam-solution-files/';

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            if(!is_super_admin())  // check if is super admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            $this->load->model('Order_model');
            $this->load->model('Order_Item_model');
            $this->load->model('Payment_model');
            $this->load->library("pagination");
    }

	public function index()
	{
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = 'Orders';

        // Pagination
        $id = isset($_GET['id']) ? trim($_GET['id']) : FALSE;
        $title = isset($_GET['title']) ? trim($_GET['title']) : FALSE;
        $paid = isset($_GET['paid']) ? trim($_GET['paid']) : FALSE;
        $date = isset($_GET['date']) ? trim($_GET['date']) : FALSE;

        // same as false conditions
        if($paid == "-1")
        {
            $paid = FALSE;
        }
        if(empty($title))
        {
            $title = FALSE;
        }
        if(empty($date))
        {
            $date = FALSE;
        }

        // if($country_id)
        // {
        //     $data['universities_all'] = $this->University_model->getRowsByCountryIDDropDown($country_id);
        // }

        if($id || $title || ($paid != '-1' && $paid != '')  || $date)
        {
            $config["total_rows"] = $this->Order_model->filter_record_count(FALSE, $id, $title, $paid, $date);
        }
        else
        {
            $config["total_rows"] = $this->Order_model->record_count("all");
        }


        $config["base_url"] = base_url() . "admin123/orders/index";
        // $config["total_rows"] = $this->Order_model->record_count("all");
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $config['use_page_numbers']  = TRUE;
        $config['reuse_query_string']=TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        if($id || $title || ($paid != '-1' && $paid != '') || $date)
        {
            $data['rows'] = $this->Order_model->filter($config["per_page"], $offset, FALSE, $id, $title, $paid, $date);
        }
        else
        {
            $data['rows'] = $this->Order_model->getRows($config["per_page"], $offset);
        }
        // $data["rows"] = $this->Order_model->getRows($config["per_page"], $offset);
        $data["links"] = $this->pagination->create_links();

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
        $this->load->view($this->config->item('template_dir_admin') . 'menu');
        $this->load->view($this->config->item('template_dir_admin') . 'orders', $data);  // load content view
	}

    public function download($user_id, $id)
    {
        $user_id = (int) $user_id;
        $id = (int) $id;
    
        $this->load->library('zip');
        // $this->load->helper('download');
        
        $rows = $this->Order_model->getOrderItemsByUserID($user_id, $id);

        // echo '<pre>'; var_dump($rows); die;

        $path = $this->upload_save_path;

        if(!empty($rows))
        {
            // force_download($path . $data['row']['file_url'], NULL, TRUE);

            foreach ($rows as $row)
            {
                // $this->Download_model->add($row['item_id']);
                $this->zip->read_file($path . $row['file_url']);
            }

            $this->zip->download('order_#' . $id . '_academicianhelp.zip');
        }
        else
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Unable to download file(s)! Please try again later.");
            redirect('/orders', 'refresh');
        }
    }

    public function view($id = NULL)
    {
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $header['page_title'] = 'Order Details';
        $data['row'] = $this->Order_model->getRows(0, 0, $id);

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'order-details', $data);  // load content view
    }

    public function delete($id)
    {
        // delete order
        $this->Order_model->delete($id);

        // delete order items
        $this->Order_Item_model->deleteByOrderID($id);

        // delete order payment record
        $this->Payment_model->deleteByOrderID($id);

        redirect('/admin123/orders', 'refresh');
    }

    public function switchto($id) // login to user account
    {
        $id = (int) $id;
        $data['row'] = $this->User_model->loginAs($id);
        if($data['row'])
        {
            redirect('/User-Home', 'refresh');
        }
        else
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Unable to grant access. Please try again.");
            redirect('/admin123/students', 'refresh');
        }
    }

    public function suspend($id)
    {
        $data['row'] = $this->User_model->setSuspend($id, 1);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Student suspended successfully!");

        // // send notification to agent dashboard
        // $notification_user_id = $id;

        // $notification_message = 'Account suspended! Please contact support to resolve.';
        // add_notification($notification_user_id, $notification_message);
        // // -----------------------------------------------------------------

        redirect('/admin123/students', 'refresh');
    }

    public function unsuspend($id)
    {
        $data['row'] = $this->User_model->setSuspend($id, 0);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Student unsuspended successfully!");

        // // send notification to agent dashboard
        // $notification_user_id = $id;

        // $notification_message = 'Account unsuspended! Thank you for choosing US Uber.';
        // add_notification($notification_user_id, $notification_message);
        // // -----------------------------------------------------------------

        redirect('/admin123/students', 'refresh');
    }
}
