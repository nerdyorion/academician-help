<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            if(!is_super_admin())  // check if is super admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            if(is_report_admin())  // check if is dnd_search admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            $this->load->model('Page_model');
            $this->load->library("pagination");
    }

    public function index()
    {
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = 'Pages';

        // Pagination
        $id = isset($_GET['id']) ? trim($_GET['id']) : FALSE;
        $title = isset($_GET['title']) ? trim($_GET['title']) : FALSE;

        if($id || $title)
        {
            $config["total_rows"] = $this->Page_model->filter_record_count($id, $title);
        }
        else
        {
            $config["total_rows"] = $this->Page_model->record_count();
        }




        $config["base_url"] = base_url() . "admin123/pages/index";
        // $config["total_rows"] = $this->Page_model->record_count("admin");
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $config['use_page_numbers']  = TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        if($id || $title)
        {
            $data['rows'] = $this->Page_model->filter($config["per_page"], $offset, $id, $title);
        }
        else
        {
            $data['rows'] = $this->Page_model->getRows($config["per_page"], $offset);
        }
        // $data["rows"] = $this->Page_model->getRows($config["per_page"], $offset);
        $data["links"] = $this->pagination->create_links();

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
        $this->load->view($this->config->item('template_dir_admin') . 'menu');
        $this->load->view($this->config->item('template_dir_admin') . 'pages', $data);  // load content view
    }

    public function create()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('page_title', 'Page Title', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('url', 'URL', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('meta_title', 'Meta Title', 'trim|required');
        $this->form_validation->set_rules('meta_tags', 'Meta Tags', 'trim|required');
        $this->form_validation->set_rules('meta_description', 'Meta Description', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        elseif($this->Page_model->urlExists(trim($this->input->post('url'))))
        {
            $this->session->set_flashdata('error', 'URL already exist for another page.');
            $this->session->set_flashdata('error_code', 1);
        }
        else
        {
            $this->Page_model->add();
            $this->session->set_flashdata('error', 'Record(s) added successfully.');
            $this->session->set_flashdata('error_code', 0);
        }

        redirect("/admin123/pages");
    }

    public function edit($id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('page_title', 'Page Title', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('url', 'URL', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('meta_title', 'Meta Title', 'trim|required');
        $this->form_validation->set_rules('meta_tags', 'Meta Tags', 'trim|required');
        $this->form_validation->set_rules('meta_description', 'Meta Description', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Update Page';  // set page title
            $data['row'] = $this->Page_model->getRows(0, 0, $id);
            if(empty($data['row']))
                redirect("/admin123/pages");
            
            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'page-edit', $data);  // load content view
        }
        elseif($this->Page_model->urlExists(trim($this->input->post('url')), $id))
        {
            $this->session->set_flashdata('error', 'URL already exist for another page.');
            $this->session->set_flashdata('error_code', 1);
            redirect("/admin123/pages");
        }
        else
        {
            $this->Page_model->update($id);

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");

            redirect("/admin123/pages");

            //redirect('/pages/edit/' . $id, 'refresh');
        }
    }

    public function delete($id)
    {
        $data['row'] = $this->Page_model->delete($id);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");
        redirect('/admin123/pages', 'refresh');
    }
}
