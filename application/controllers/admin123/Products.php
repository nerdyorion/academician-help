<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            if(!is_super_admin())  // check if is not super admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            if(is_report_admin())  // check if is report admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            $this->load->helper("url");
            $this->load->model('Product_model');
            $this->load->model('Category_model');
            $this->load->library("pagination");
            //$this->load->helper('url_helper');
    }

	public function index($id = false)
	{   
        // var_dump($this->uri->segment(3)); die;
    	$this->load->library('form_validation');
        $this->form_validation->set_rules('category_id', 'Product Category', 'trim|required|numeric');
        $this->form_validation->set_rules('name', 'Product Name', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('price', 'Product Price', 'trim|numeric');
        $this->form_validation->set_rules('duration', 'Product Duration', 'trim|required|max_length[255]');

    	if ($this->form_validation->run() === FALSE)
    	{
        	$errors = str_replace("<p>","", validation_errors());
        	$errors = str_replace("</p>","", trim($errors));
        	$this->session->set_flashdata('error', $errors);
        	$this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

			$data['error'] = $this->session->flashdata('error');
			$data['error_code'] = $this->session->flashdata('error_code');
        	$header['page_title'] = 'Products';  // set page title
        	$data['categories'] = $this->Category_model->getRows();

            // Pagination
            // $config = array();
            $config["base_url"] = base_url() . "admin123/Products/index";
            $config["total_rows"] = $this->Product_model->record_count();
            $config["per_page"] = 10;
            $config["uri_segment"] = 4;
            // $choice = $config["total_rows"] / $config["per_page"];
            // $config["num_links"] = round($choice);
            $config['use_page_numbers']  = TRUE;

            $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
            $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li><span><b>";
            $config['cur_tag_close'] = "</b></span></li>";

            $this->pagination->initialize($config);

            // page 1 (1-5) -- per_page = 5, offset = 0
            // page 2 (6-10) -- per_page = 5, offset = 5           (page_no * per_page) - per_page
            // page 3 (11-15) -- per_page = 5, offset = 10           (page_no * per_page) - per_page
            // page 4 (16-20) -- per_page = 5, offset = 15           (page_no * per_page) - per_page
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $offset = 0;
            if($page > 1)
            {
                $offset = ($page * $config["per_page"]) - $config["per_page"];
                // (page_no * per_page) - per_page
            }
            
            $data['sn'] = $offset == 0 ? 1 : $offset + 1;
            $data["rows"] = $this->Product_model->getRows($config["per_page"], $offset);
            $data["links"] = $this->pagination->create_links();



        	
        	$this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        	$this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        	$this->load->view($this->config->item('template_dir_admin') . 'products', $data);  // load content view
        }
    	else
    	{
            // if ((is_null($this->input->post('category_id'))) || ($this->input->post('category_id') == 0))
            // {
            //     $errors = "Please select category!";
            //     $this->session->set_flashdata('error', $errors);
            //     $this->session->set_flashdata('error_code', 1);
            //     redirect("/admin123/Products");
            // }

            // check if slug exists
            $_POST['slug'] = $this->createPostSlug(trim($_POST['name']));
            $slugExists = $this->Product_model->slugExists($_POST['slug']);

            if ($slugExists !== false)
            {
                // get last number in slug if exist and increase
                $slug_array = explode("-", $slugExists);
                $duplicate_no = end($slug_array); // var_dump($duplicate_no); die;

                if(is_numeric($duplicate_no)) // duplicated already
                {
                    $duplicate_no++; // increase duplicate number by 1

                    array_pop($slug_array); // remove last duplicate number

                    $_POST['slug'] = implode('-', $slug_array) . '-' . $duplicate_no; // add new duplicate to array and form string

                    // echo "existing slug: $slugExists<br />new slug: " . $_POST['slug'] . "<br />"; die;
                }
                else // new duplicate
                {
                    // append 2 to slug
                    $_POST['slug'] .= '-2';

                    // echo "existing duplicate slug: $slugExists<br />new slug: " . $_POST['slug'] . "<br />";  die;
                }
            }

            // for image upload
            $config['upload_path']          = './assets/images/products/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240; // 10MB
            $new_name = "";
            $new_name_thumb = "";
            $user_id = $this->session->userdata("user_id");
            
            if(isset($_FILES["image"]['name']) && !empty($_FILES["image"]['name']))
            {
                $file_name = strtolower(trim($_FILES["image"]['name']));
                $tmp = explode(".", $file_name);
                $file_ext = end($tmp);

                array_pop($tmp);                //   var_dump($tmp); die;
                $new_name = implode('', $tmp);       // var_dump($new_name); die;

                $config['file_name'] = strtolower($new_name . '_' . date("YmdHis"));  // var_dump($config['file_name']); // die;
                $config['file_name_thumb'] = strtolower($new_name . '_' . date("YmdHis") . '_thumb' . '.' . $file_ext);  
                // var_dump($config['file_name_thumb']); die;
            }
            else
            {
                // redirect with file error
                $errors = "Please upload file!";
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', 1);
                redirect("/admin123/Products");
            }
            $this->load->library('upload', $config);

            if( !$this->upload->do_upload('image')) // upload image and check if error
            {
                $errors = str_replace("<p>","", $this->upload->display_errors());
                $errors = str_replace("</p>","", $errors);
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', 1);
                redirect("/admin123/Products");
            }
            else // upload successful .. now resize
            {
                $image_data =   $this->upload->data();

                $configer =  array(
                  'image_library'   => 'gd2',
                  'source_image'    =>  strtolower($image_data['full_path']),
                  'maintain_ratio'  =>  TRUE,
                  'width'           =>  650,
                  'height'          =>  650,
                );
                $this->load->library('image_lib', $configer);
                $this->image_lib->initialize($configer);
                $this->image_lib->resize();
                $this->image_lib->clear();
            }

            // for generate thumb image
            $config_resize['image_library'] = 'gd2'; // gd2
            // $config_resize['source_image'] = './assets/images/products/combined_web_languages_20170713124519.png';
            $config_resize['source_image'] = $config['upload_path'] . $config['file_name'] . '.' . $file_ext;
            
            $config_resize['create_thumb'] = TRUE;
            $config_resize['maintain_ratio'] = TRUE;
            $config_resize['width']         = 40;
            $config_resize['height']       = 40;

            $this->load->library('image_lib', $config_resize);
            $this->image_lib->initialize($config_resize);


            if( !$this->image_lib->resize())
            {
                $errors = str_replace("<p>","", $this->image_lib->display_errors());
                $errors = str_replace("</p>","", $errors);
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', 1);
                redirect("/admin123/Products");
            }


            $_POST['image_url'] =  $config['file_name'] . '.' . $file_ext;
            $_POST['image_url_thumb'] =  $config['file_name_thumb'];


            $this->Product_model->add();
            $this->session->set_flashdata('error', 'Record(s) added successfully.');
            $this->session->set_flashdata('error_code', 0);

            // re-generate slug url routes in routes file
            save_routes();

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Products';  // set page title
            $data['categories'] = $this->Category_model->getRows();

            // Pagination
            // $config = array();
            $config["base_url"] = base_url() . "admin123/Products/index";
            $config["total_rows"] = $this->Product_model->record_count();
            $config["per_page"] = 10;
            $config["uri_segment"] = 4;
            // $choice = $config["total_rows"] / $config["per_page"];
            // $config["num_links"] = round($choice);
            $config['use_page_numbers']  = TRUE;

            $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
            $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li><span><b>";
            $config['cur_tag_close'] = "</b></span></li>";

            $this->pagination->initialize($config);

            // page 1 (1-5) -- per_page = 5, offset = 0
            // page 2 (6-10) -- per_page = 5, offset = 5           (page_no * per_page) - per_page
            // page 3 (11-15) -- per_page = 5, offset = 10           (page_no * per_page) - per_page
            // page 4 (16-20) -- per_page = 5, offset = 15           (page_no * per_page) - per_page
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $offset = 0;
            if($page > 1)
            {
                $offset = ($page * $config["per_page"]) - $config["per_page"];
                // (page_no * per_page) - per_page
            }
            
            $data['sn'] = $offset == 0 ? 1 : $offset + 1;
            $data["rows"] = $this->Product_model->getRows($config["per_page"], $offset);
            $data["links"] = $this->pagination->create_links();

            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'products', $data);
    	}
	}

    public function view($id = NULL)
    {
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $header['page_title'] = 'Product Details';
        $data['row'] = $this->Product_model->getRows(0, 0, $id);

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'product-details', $data);  // load content view
    }

    public function edit($id)
    {
        $id = (int) $id;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('category_id', 'Product Category', 'trim|required|numeric');
        $this->form_validation->set_rules('name', 'Product Name', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('price', 'Product Price', 'trim|numeric');
        $this->form_validation->set_rules('duration', 'Product Duration', 'trim|required|max_length[255]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
            $data['categories'] = $this->Category_model->getRows();

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Update Product';  // set page title
            $data['row'] = $this->Product_model->getRows(0, 0, $id);
            if(empty($data['row']))
                redirect("/admin123/Products");
            
            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'product-edit', $data);  // load content view
        }
        else
        {
            // for image upload
            $config['upload_path']          = './assets/images/products/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240; // 10MB
            $new_name = "";
            $new_name_thumb = "";
            $user_id = $this->session->userdata("user_id");
            
            if(isset($_FILES["image"]['name']) && !empty($_FILES["image"]['name']))
            {
                $file_name = strtolower(trim($_FILES["image"]['name']));
                $tmp = explode(".", $file_name);
                $file_ext = end($tmp);

                array_pop($tmp);                //   var_dump($tmp); die;
                $new_name = implode('', $tmp);       // var_dump($new_name); die;

                $config['file_name'] = strtolower($new_name . '_' . date("YmdHis"));  // var_dump($config['file_name']); // die;
                $config['file_name_thumb'] = strtolower($new_name . '_' . date("YmdHis") . '_thumb' . '.' . $file_ext);  
                // var_dump($config['file_name_thumb']); die;

                $this->load->library('upload', $config);

                if( !$this->upload->do_upload('image')) // upload image and check if error
                {
                    $errors = str_replace("<p>","", $this->upload->display_errors());
                    $errors = str_replace("</p>","", $errors);
                    $this->session->set_flashdata('error', $errors);
                    $this->session->set_flashdata('error_code', 1);
                    redirect("/admin123/Products");
                }
                else // upload successful .. now resize
                {
                    $image_data =   $this->upload->data();

                    $configer =  array(
                      'image_library'   => 'gd2',
                      'source_image'    =>  strtolower($image_data['full_path']),
                      'maintain_ratio'  =>  TRUE,
                      'width'           =>  650,
                      'height'          =>  650,
                      );
                    $this->load->library('image_lib', $configer);
                    $this->image_lib->initialize($configer);
                    $this->image_lib->resize();
                    $this->image_lib->clear();
                }

                // var_dump("should have uploaded and resized"); die;

                // for generate thumb image
                $config_resize['image_library'] = 'gd2'; // gd2
                // $config_resize['source_image'] = './assets/images/products/combined_web_languages_20170713124519.png';
                $config_resize['source_image'] = $config['upload_path'] . $config['file_name'] . '.' . $file_ext;

                $config_resize['create_thumb'] = TRUE;
                $config_resize['maintain_ratio'] = TRUE;
                $config_resize['width']         = 40;
                $config_resize['height']       = 40;

                $this->load->library('image_lib', $config_resize);
                $this->image_lib->initialize($config_resize);


                if( !$this->image_lib->resize())
                {
                    $errors = str_replace("<p>","", $this->image_lib->display_errors());
                    $errors = str_replace("</p>","", $errors);
                    $this->session->set_flashdata('error', $errors);
                    $this->session->set_flashdata('error_code', 1);
                    redirect("/admin123/Products");
                }

                $_POST['image_url'] =  $config['file_name'] . '.' . $file_ext;
                $_POST['image_url_thumb'] =  $config['file_name_thumb'];

                $this->Product_model->update($id);

                // delete former image
                unlink('./assets/images/products/' . $this->input->post('image_url_old'));
                unlink('./assets/images/products/' . $this->input->post('image_url_thumb_old'));
            }
            else
            {
                // file not uploaded, update without image
                $this->Product_model->updateWithoutImage($id);
            }

            ///////////////////////////////////////////////////////////////////////////

            // re-generate slug url routes in routes file
            save_routes();

            $header['page_title'] = 'Update Product';  // set page title
            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");
            $data['categories'] = $this->Category_model->getRows();

            // get new data after updating
            $data['row'] = $this->Product_model->getRows(0, 0, $id);
            if(empty($data['row']))
                redirect("/admin123/Products");

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');

            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'product-edit', $data);

            // redirect('/admin123/Products/');

            //redirect('/products/edit/' . $id, 'refresh');
        }
    }

    public function delete($id)
    {
        $data['row'] = $this->Product_model->getRows(0, 0, $id);
        unlink('./assets/images/products/' . $data['row']['image_url']);
        unlink('./assets/images/products/' . $data['row']['image_url_thumb']);

        $this->Product_model->delete($id);

        // re-generate slug url routes in routes file
        save_routes();

        redirect('/admin123/Products', 'refresh');
    }

    public function enable($id)
    {
        $data['row'] = $this->Product_model->enable($id);
        redirect('/admin123/Products', 'refresh');
    }

    public function disable($id)
    {
        $data['row'] = $this->Product_model->disable($id);
        redirect('/admin123/Products', 'refresh');
    }

    private function createPostSlug($url){
        # Prep string with some basic normalization
        $url = strtolower($url);
        $url = strip_tags($url);
        $url = stripslashes($url);
        $url = html_entity_decode($url);
        # Remove quotes (can't, etc.)
        $url = str_replace('\'', '', $url);

        # Replace non-alpha numeric with hyphens
        $url = trim(preg_replace('/[^a-z0-9]+/', '-', $url), '-');

        return $url;
    }
}
