<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscriptions extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            if(!is_super_admin())  // check if is not super admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            if(is_report_admin())  // check if is report admin :: ALLOW REPORT ADMIN TO VIEW SUBSCRIPTIONS
            {
                //redirect to 404  :: ALLOW REPORT ADMIN TO VIEW SUBSCRIPTIONS
                // redirect('/admin123/404');  :: ALLOW REPORT ADMIN TO VIEW SUBSCRIPTIONS
                //show_404();   :: ALLOW REPORT ADMIN TO VIEW SUBSCRIPTIONS
            }
            $this->load->helper("url");
            $this->load->model('Product_model');
            $this->load->model('Subscription_model');
            $this->load->library("pagination");
            //$this->load->helper('url_helper');
    }

	public function index()
	{   
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $header['page_title'] = 'Products';  // set page title
        $data['products'] = $this->Product_model->getRowsDropDown();

        // get GET params
        $products = is_null($this->input->get('products')) ? '' : trim(implode(',', $this->input->get('products')));

        $start = is_null($this->input->get('start')) ? '' : trim($this->input->get('start'));

        $end = is_null($this->input->get('end')) ? '' : trim($this->input->get('end'));

        $msisdn = is_null($this->input->get('msisdn')) ? '' : trim($this->input->get('msisdn'));

        $successful = is_null($this->input->get('successful')) ? '-1' : ($this->input->get('successful') == '-1' ? '' : trim($this->input->get('successful')) );

        $_POST['products'] = empty($products) ? NULL : $this->input->get('products');
        $_POST['start'] = empty($start) ? NULL : $start;
        $_POST['end'] = empty($end) ? NULL : $end;
        $_POST['msisdn'] = empty($msisdn) ? NULL : $msisdn;
        $_POST['successful'] = $successful == '-1' ? NULL : $successful;

        $this->load->helper('url');
        $base_url = base_url() . "admin123/Subscriptions/index";
        $output = array();

        $currentURL = current_url();
        $params   = $_SERVER['QUERY_STRING'];

        $fullURL = $currentURL . '?' . $params;

        $parsed_url = parse_url($fullURL); // var_dump($parsed_url); die;
        if(isset($parsed_url['query']))
        {
            parse_str($parsed_url['query'], $output);
        }

        if(isset($output['per_page']))
        {
            unset($output['per_page']);
        }

        $base_url .= '?' . http_build_query($output);

        // var_dump($base_url); die;
        // die;
        // print_r($fullURL);

        // Pagination
        // $config["base_url"] = base_url() . "admin123/Subscriptions/index";
        $config["base_url"] = $base_url;
        $config["total_rows"] = $this->Subscription_model->record_count_filter($products, $start, $end, $msisdn, $successful);
        $config["per_page"] = 10;
        // $config["uri_segment"] = 4;
        $config['use_page_numbers']  = TRUE;
        $config['page_query_string'] = TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        $data["rows"] = $this->Subscription_model->getRowsFilter($config["per_page"], $offset, $products, $start, $end, $msisdn, $successful);
        $data["links"] = $this->pagination->create_links();

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'subscriptions', $data);  // load content view
    }

    public function filter()
    {    
        $products = is_null($this->input->post('products')) ? '' : implode(',', $this->input->post('products'));
        var_dump($products); echo "<br /><br />";
        
        $start = is_null($this->input->post('start')) ? '' : $this->input->post('start');
        var_dump($start); echo "<br /><br />";
        
        $end = is_null($this->input->post('end')) ? '' : $this->input->post('end');
        var_dump($end); echo "<br /><br />";
        
        $msisdn = is_null($this->input->post('msisdn')) ? '' : $this->input->post('msisdn');
        var_dump($msisdn); echo "<br /><br />";
        
        $successful = is_null($this->input->post('successful')) ? '' : $this->input->post('successful');
        var_dump($successful); echo "<br /><br />";

        
        die;
        // var_dump($this->uri->segment(3)); die;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('category_id', 'Product Category', 'trim|required|numeric');
        $this->form_validation->set_rules('name', 'Product Name', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('price', 'Product Price', 'trim|numeric');
        $this->form_validation->set_rules('duration', 'Product Duration', 'trim|required|max_length[255]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Products';  // set page title
            $data['categories'] = $this->Category_model->getRows();

            // Pagination
            // $config = array();
            $config["base_url"] = base_url() . "admin123/Products/index";
            $config["total_rows"] = $this->Product_model->record_count();
            $config["per_page"] = 10;
            $config["uri_segment"] = 4;
            // $choice = $config["total_rows"] / $config["per_page"];
            // $config["num_links"] = round($choice);
            $config['use_page_numbers']  = TRUE;

            $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
            $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li><span><b>";
            $config['cur_tag_close'] = "</b></span></li>";

            $this->pagination->initialize($config);

            // page 1 (1-5) -- per_page = 5, offset = 0
            // page 2 (6-10) -- per_page = 5, offset = 5           (page_no * per_page) - per_page
            // page 3 (11-15) -- per_page = 5, offset = 10           (page_no * per_page) - per_page
            // page 4 (16-20) -- per_page = 5, offset = 15           (page_no * per_page) - per_page
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $offset = 0;
            if($page > 1)
            {
                $offset = ($page * $config["per_page"]) - $config["per_page"];
                // (page_no * per_page) - per_page
            }
            
            $data['sn'] = $offset == 0 ? 1 : $offset + 1;
            $data["rows"] = $this->Product_model->getRows($config["per_page"], $offset);
            $data["links"] = $this->pagination->create_links();



            
            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'products', $data);  // load content view
        }
        else
        {
            // check if slug exists
            $_POST['slug'] = $this->createPostSlug(trim($_POST['name']));
            $slugExists = $this->Product_model->slugExists($_POST['slug']);

            if ($slugExists !== false)
            {
                // get last number in slug if exist and increase
                $slug_array = explode("-", $slugExists);
                $duplicate_no = end($slug_array); // var_dump($duplicate_no); die;

                if(is_numeric($duplicate_no)) // duplicated already
                {
                    $duplicate_no++; // increase duplicate number by 1

                    array_pop($slug_array); // remove last duplicate number

                    $_POST['slug'] = implode('-', $slug_array) . '-' . $duplicate_no; // add new duplicate to array and form string

                    // echo "existing slug: $slugExists<br />new slug: " . $_POST['slug'] . "<br />"; die;
                }
                else // new duplicate
                {
                    // append 2 to slug
                    $_POST['slug'] .= '-2';

                    // echo "existing duplicate slug: $slugExists<br />new slug: " . $_POST['slug'] . "<br />";  die;
                }
            }

            // for image upload
            $config['upload_path']          = './assets/images/products/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240; // 10MB
            $new_name = "";
            $new_name_thumb = "";
            $user_id = $this->session->userdata("user_id");
            
            if(isset($_FILES["image"]['name']) && !empty($_FILES["image"]['name']))
            {
                $file_name = strtolower(trim($_FILES["image"]['name']));
                $tmp = explode(".", $file_name);
                $file_ext = end($tmp);

                array_pop($tmp);                //   var_dump($tmp); die;
                $new_name = implode('', $tmp);       // var_dump($new_name); die;

                $config['file_name'] = strtolower($new_name . '_' . date("YmdHis"));  // var_dump($config['file_name']); // die;
                $config['file_name_thumb'] = strtolower($new_name . '_' . date("YmdHis") . '_thumb' . '.' . $file_ext);  
                // var_dump($config['file_name_thumb']); die;
            }
            else
            {
                // redirect with file error
                $errors = "Please upload file!";
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', 1);
                redirect("/admin123/Products");
            }
            $this->load->library('upload', $config);

            if( !$this->upload->do_upload('image')) // upload image and check if error
            {
                $errors = str_replace("<p>","", $this->upload->display_errors());
                $errors = str_replace("</p>","", $errors);
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', 1);
                redirect("/admin123/Products");
            }
            else // upload successful .. now resize
            {
                $image_data =   $this->upload->data();

                $configer =  array(
                  'image_library'   => 'gd2',
                  'source_image'    =>  strtolower($image_data['full_path']),
                  'maintain_ratio'  =>  TRUE,
                  'width'           =>  650,
                  'height'          =>  650,
                );
                $this->load->library('image_lib', $configer);
                $this->image_lib->initialize($configer);
                $this->image_lib->resize();
                $this->image_lib->clear();
            }

            // for generate thumb image
            $config_resize['image_library'] = 'gd2'; // gd2
            // $config_resize['source_image'] = './assets/images/products/combined_web_languages_20170713124519.png';
            $config_resize['source_image'] = $config['upload_path'] . $config['file_name'] . '.' . $file_ext;
            
            $config_resize['create_thumb'] = TRUE;
            $config_resize['maintain_ratio'] = TRUE;
            $config_resize['width']         = 40;
            $config_resize['height']       = 40;

            $this->load->library('image_lib', $config_resize);
            $this->image_lib->initialize($config_resize);


            if( !$this->image_lib->resize())
            {
                $errors = str_replace("<p>","", $this->image_lib->display_errors());
                $errors = str_replace("</p>","", $errors);
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', 1);
                redirect("/admin123/Products");
            }


            $_POST['image_url'] =  $config['file_name'] . '.' . $file_ext;
            $_POST['image_url_thumb'] =  $config['file_name_thumb'];


            $this->Product_model->add();
            $this->session->set_flashdata('error', 'Record(s) added successfully.');
            $this->session->set_flashdata('error_code', 0);

            // re-generate slug url routes in routes file
            save_routes();

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Products';  // set page title
            $data['categories'] = $this->Category_model->getRows();

            // Pagination
            // $config = array();
            $config["base_url"] = base_url() . "admin123/Products/index";
            $config["total_rows"] = $this->Product_model->record_count();
            $config["per_page"] = 10;
            $config["uri_segment"] = 4;
            // $choice = $config["total_rows"] / $config["per_page"];
            // $config["num_links"] = round($choice);
            $config['use_page_numbers']  = TRUE;

            $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
            $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li><span><b>";
            $config['cur_tag_close'] = "</b></span></li>";

            $this->pagination->initialize($config);

            // page 1 (1-5) -- per_page = 5, offset = 0
            // page 2 (6-10) -- per_page = 5, offset = 5           (page_no * per_page) - per_page
            // page 3 (11-15) -- per_page = 5, offset = 10           (page_no * per_page) - per_page
            // page 4 (16-20) -- per_page = 5, offset = 15           (page_no * per_page) - per_page
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $offset = 0;
            if($page > 1)
            {
                $offset = ($page * $config["per_page"]) - $config["per_page"];
                // (page_no * per_page) - per_page
            }
            
            $data['sn'] = $offset == 0 ? 1 : $offset + 1;
            $data["rows"] = $this->Product_model->getRows($config["per_page"], $offset);
            $data["links"] = $this->pagination->create_links();

            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'products', $data);
        }
    }
}