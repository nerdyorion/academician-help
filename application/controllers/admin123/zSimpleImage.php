<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            if(!is_super_admin())  // check if is not super admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            if(is_report_admin())  // check if is report admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            $this->load->model('Category_model');
            $this->load->model('Product_model');
            $this->load->model('SimpleImage'); 
            $this->load->library("pagination");
            //$this->load->helper('url_helper');
    }

	public function index()
	{
            try
            {
                // Create a new SimpleImage object
                // $image = new \claviska\SimpleImage();
                $image = new $this->SimpleImage();


                // Magic! ✨
                $image
                    ->fromFile('/var/www/html/mobilefunlive/assets/images/products/combined_web_languages_20170713124519.png')                     // load image.jpg
                    ->autoOrient()                              // adjust orientation based on exif data
                    ->resize(40, 40)                          // resize to 40x40 pixels
                    // ->flip('x')                                 // flip horizontally
                    // ->colorize('DarkBlue')                      // tint dark blue
                    // ->border('black', 10)                       // add a 10 pixel black border
                    // ->overlay('watermark.png', 'bottom right')  // add a watermark image
                ->toFile('./assets/images/products/image2.png');
            }
            catch(Exception $e)
            {
                // Handle errors
                echo $e->getMessage();
            }
            die ();









    	$this->load->library('form_validation');
        $this->form_validation->set_rules('category_id', 'Product Category', 'trim|required|numeric');
        $this->form_validation->set_rules('name', 'Product Name', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('price', 'Product Price', 'trim|numeric');
        $this->form_validation->set_rules('duration', 'Product Duration', 'trim|required|max_length[255]');

    	if ($this->form_validation->run() === FALSE)
    	{
        	$errors = str_replace("<p>","", validation_errors());
        	$errors = str_replace("</p>","", trim($errors));
        	$this->session->set_flashdata('error', $errors);
        	$this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

			$data['error'] = $this->session->flashdata('error');
			$data['error_code'] = $this->session->flashdata('error_code');
        	$header['page_title'] = 'Products';  // set page title
        	$data['categories'] = $this->Category_model->getRows();

            // Pagination
            // $config = array();
            $config["base_url"] = base_url() . "admin123/products";
            $config["total_rows"] = $this->Product_model->record_count();
            $config["per_page"] = 2;
            $config["uri_segment"] = 3;

            $this->pagination->initialize($config);

            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["rows"] = $this->Product_model->getRows($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();



        	
        	$this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        	$this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        	$this->load->view($this->config->item('template_dir_admin') . 'products', $data);  // load content view
        }
    	else
    	{
            // for image upload
            $config['upload_path']          = './assets/images/products/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240; // 10MB
            $new_name = "";
            $new_name_thumb = "";
            $user_id = $this->session->userdata("user_id");
            
            if(isset($_FILES["image"]['name']))
            {
                $file_name = $_FILES["image"]['name'];
                $tmp = explode(".", $file_name);
                $file_ext = end($tmp);

                array_pop($tmp);                //   var_dump($tmp); die;
                $new_name = implode('', $tmp);       // var_dump($new_name); die;

                // $config['file_name'] = $new_name . '_' . date("YmdHis") . $file_ext; 
                $config['file_name'] = $new_name . '_' . date("YmdHis"); //  var_dump($config['file_name']); die;
                // $config['file_name_thumb'] = $new_name . '_thumb_' . date("YmdHis");  // var_dump($config['file_name_thumb']); die;
            }
            $this->load->library('upload', $config);


            // for resize image
            $this->load->library('image_lib');
            $config_resize['image_library'] = 'gd2';
            // $config_resize['source_image'] = '/path/to/image/mypic.jpg';
            $config_resize['source_image'] = $config['upload_path'] . $config['file_name'] . '.' . $file_ext;
            // var_dump($config_resize['source_image']); die;
            $config_resize['create_thumb'] = TRUE;
            $config_resize['maintain_ratio'] = TRUE;
            $config_resize['width']         = 40;
            $config_resize['height']       = 40;

            $this->load->library('image_lib', $config_resize);


            if ((is_null($this->input->post('category_id'))) || ($this->input->post('category_id') == 0))
            {
                $errors = "Please select category!";
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', 1);
                redirect("/admin123/Products");
            }
            else if ( !$this->upload->do_upload('image'))
            {
                $errors = str_replace("<p>","", $this->upload->display_errors());
                $errors = str_replace("</p>","", $errors);
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', 1);
                redirect("/admin123/Products");
            }
            else if ( !$this->image_lib->resize())
            {
                $errors = str_replace("<p>","", $this->image_lib->display_errors());
                $errors = str_replace("</p>","", $errors);
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', 1);
                redirect("/admin123/Products");
            }
            else
            {
                // $this->Product_model->add();
                $this->session->set_flashdata('error', 'Record(s) added successfully.');
                $this->session->set_flashdata('error_code', 0);

                $data['error'] = $this->session->flashdata('error');
                $data['error_code'] = $this->session->flashdata('error_code');
                $header['page_title'] = 'Products';  // set page title
                $data['categories'] = $this->Category_model->getRows();

                // Pagination
                // $config = array();
                $config["base_url"] = base_url() . "admin123/products";
                $config["total_rows"] = $this->Product_model->record_count();
                $config["per_page"] = 2;
                $config["uri_segment"] = 3;

                $this->pagination->initialize($config);

                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data["rows"] = $this->Product_model->getRows($config["per_page"], $page);
                $data["links"] = $this->pagination->create_links();

                $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
                $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
                $this->load->view($this->config->item('template_dir_admin') . 'products', $data);
            }
    	}
	}

    public function view($id = NULL)
    {
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $header['page_title'] = 'Product Details';
        $data['row'] = $this->Product_model->getRows($id);

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'product-details', $data);  // load content view
    }

    public function edit($id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('full_name', 'Full Name', 'required|max_length[2000]');
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|valid_email');
        $this->form_validation->set_rules('gender', 'Gender', 'required|max_length[255]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Update Product';  // set page title
            $data['row'] = $this->Product_model->getRows($id);
            if(empty($data['row']))
                redirect("/admin123/products");
            
            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'product-edit', $data);  // load content view
        }
        else
        {
            $header['page_title'] = 'Update Product';  // set page title
            $this->Product_model->update($id);
            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");
            // get new data after updating
            $data['row'] = $this->Product_model->getRows($id);
            if(empty($data['row']))
                redirect("/admin123/products");

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');

            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'product-edit', $data);

            //redirect('/products/edit/' . $id, 'refresh');
        }
    }

    public function delete($id)
    {
        $data['row'] = $this->Product_model->delete($id);
        redirect('/admin123/products', 'refresh');
    }

    private function createPostSlug($url){
        # Prep string with some basic normalization
        $url = strtolower($url);
        $url = strip_tags($url);
        $url = stripslashes($url);
        $url = html_entity_decode($url);
        # Remove quotes (can't, etc.)
        $url = str_replace('\'', '', $url);

        # Replace non-alpha numeric with hyphens
        $url = trim(preg_replace('/[^a-z0-9]+/', '-', $url), '-');

        return $url;
    }
}
