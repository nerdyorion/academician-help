<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Agent_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/login');
            }
            $this->load->model('Booking_model');
            $this->load->model('User_model');
    }

	public function index()
	{
        $user_id = (int) $this->session->userdata("user_id");
        $username = $this->session->userdata("user_username");
        $data['sn'] = 1;

        $header['page_title'] = 'Dashboard';
        $data['bookings_total'] = (int) $this->Booking_model->totalCount('agent', $user_id);
        $data['bookings_paid'] = (int) $this->Booking_model->paid('agent', $user_id);
        $data['bookings_pending'] = (int) $this->Booking_model->pending('agent', $user_id); // where booking_cost = 0

        $data['events_upcoming'] = (int) $this->Booking_model->upcoming('agent', $user_id);


        $data['last_login'] = $this->session->userdata("user_last_login");
        $data["rows"] = $this->Booking_model->getRowsByAgentIDUnpaid(10, 0, $user_id);
        //$data['rows'] = 'rows';
        $this->load->view($this->config->item('template_dir_agent') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_agent') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_agent') . 'home', $data);  // load content view
	}

    public function switchbacktoadmin($id) // log back in to admin account
    {
        $id = (int) $id;
        $data['row'] = $this->User_model->loginAs($id, true); // second param siginifies loggin back as admin
        if($data['row'])
        {
            redirect('/admin123', 'refresh');
        }
        else
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Unable to grant access. Please try again.");
            redirect('/dashboard', 'refresh');
        }
    }
}