<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends Agent_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/login');
            }
            $this->load->model('Notification_model');
            $this->load->library("pagination");
    }

	public function index()
	{
        $user_id = (int) $this->session->userdata("user_id");
        $username = $this->session->userdata("user_username");

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = 'Notifications';
        $data['username'] = $username;

        // Pagination
        $config["base_url"] = base_url() . "agent/notifications/index";
        $config["total_rows"] = $this->Notification_model->record_count($user_id); // $user_id for talent and agent
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $config['use_page_numbers']  = TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        $data["rows"] = $this->Notification_model->getRows($config["per_page"], $offset, $user_id); // get user notifications
        $data["links"] = $this->pagination->create_links();

        // Mark all as Read
        $this->Notification_model->markRead($user_id); // $user_id for talent and agent

        $this->load->view($this->config->item('template_dir_agent') . 'header', $header);
        $this->load->view($this->config->item('template_dir_agent') . 'menu');
        $this->load->view($this->config->item('template_dir_agent') . 'notifications', $data);  // load content view
	}
}
