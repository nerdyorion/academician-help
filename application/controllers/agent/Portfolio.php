<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio extends Artist_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/login');
            }
            $this->load->helper("url");
            $this->load->model('Portfolio_model');
            $this->load->library("pagination");
    }

	public function index()
	{
        $user_id = (int) $this->session->userdata("user_id");

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = 'Portfolio';

        // Pagination
        $config["base_url"] = base_url() . "artist/portfolio/index";
        $config["total_rows"] = $this->Portfolio_model->record_count($user_id);
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $config['use_page_numbers']  = TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        $data["rows"] = $this->Portfolio_model->getRowsByUserID($config["per_page"], $offset, $user_id);
        $data["links"] = $this->pagination->create_links();

        $this->load->view($this->config->item('template_dir_artist') . 'header', $header);
        $this->load->view($this->config->item('template_dir_artist') . 'menu');
        $this->load->view($this->config->item('template_dir_artist') . 'portfolio', $data);
	}

    public function create()
    {
        $user_id = (int) $this->session->userdata("user_id");

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Portfolio Name', 'trim|required|max_length[20000]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        else
        {
            // for image upload
            $config['upload_path']          = './assets/artist/images/portfolio/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240; // 10MB
            $new_name = "";
            $full_name = str_replace(" ","_", strtolower($this->session->userdata("user_full_name")));
            
            if(isset($_FILES["image"]['name']) && !empty($_FILES["image"]['name']))
            {
                $file_name = strtolower(trim($_FILES["image"]['name']));
                $tmp = explode(".", $file_name);
                $file_ext = end($tmp);

                array_pop($tmp);
                $new_name = $full_name . '_' . implode('', $tmp);

                $config['file_name'] = strtolower($new_name . '_' . date("YmdHis"));
                $config['file_name_thumb'] = 'thumb_' . $config['file_name'];
            }
            else
            {
                $errors = "Please upload file!";
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', 1);
                redirect("/artist/portfolio");
            }
            $this->load->library('upload', $config);

            if( !$this->upload->do_upload('image')) // upload image and check if error
            {
                $errors = str_replace("<p>","", $this->upload->display_errors());
                $errors = str_replace("</p>","", $errors);
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', 1);
                redirect("/artist/portfolio");
            }
            else // upload successful .. now resize
            {
                $image_data =   $this->upload->data();

                $configer =  array(
                  'image_library'   => 'gd2',
                  'source_image'    =>  strtolower($image_data['full_path']),
                  'maintain_ratio'  =>  TRUE,
                  'width'           =>  650,
                  'height'          =>  650,
                );
                $this->load->library('image_lib', $configer);
                $this->image_lib->initialize($configer);
                $this->image_lib->resize();
                $this->image_lib->clear();
            }

            // for generate thumb image
            $config_resize['image_library'] = 'gd2';
            $config_resize['source_image'] = $config['upload_path'] . $config['file_name'] . '.' . $file_ext;
            
            $config_resize['create_thumb'] = TRUE;
            $config_resize['maintain_ratio'] = TRUE;
            $config_resize['width']         = 40;
            $config_resize['height']       = 40;

            $this->load->library('image_lib', $config_resize);
            $this->image_lib->initialize($config_resize);


            if( !$this->image_lib->resize())
            {
                $errors = str_replace("<p>","", $this->image_lib->display_errors());
                $errors = str_replace("</p>","", $errors);
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', 1);
                redirect("/artist/portfolio");
            }

            $_POST['image_url'] =  'assets/artist/images/portfolio/' . $config['file_name'] . '.' . $file_ext;

            $this->Portfolio_model->add();
            $this->session->set_flashdata('error', 'Record(s) added successfully.');
            $this->session->set_flashdata('error_code', 0);
        }

        redirect("/artist/portfolio");
    }

    public function edit($id)
    {
        $id = (int) $id;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Portfolio Name', 'trim|required|max_length[20000]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Update Portfolio';  // set page title
            $data['row'] = $this->Portfolio_model->getRows(0, 0, $id);
            if(empty($data['row']))
                redirect("/artist/portfolio");
            
            $this->load->view($this->config->item('template_dir_artist') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_artist') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_artist') . 'portfolio-edit', $data);  // load content view
        }
        else
        {
            // for image upload
            $config['upload_path']          = './assets/artist/images/portfolio/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240; // 10MB
            $new_name = "";
            $full_name = str_replace(" ","_", strtolower($this->session->userdata("user_full_name")));
            
            if(isset($_FILES["image"]['name']) && !empty($_FILES["image"]['name']))
            {
                $file_name = strtolower(trim($_FILES["image"]['name']));
                $tmp = explode(".", $file_name);
                $file_ext = end($tmp);

                array_pop($tmp);
                $new_name = $full_name . '_' . implode('', $tmp);

                $config['file_name'] = strtolower($new_name . '_' . date("YmdHis"));
                $config['file_name_thumb'] = 'thumb_' . $config['file_name'];

                $this->load->library('upload', $config);

                if( !$this->upload->do_upload('image')) // upload image and check if error
                {
                    $errors = str_replace("<p>","", $this->upload->display_errors());
                    $errors = str_replace("</p>","", $errors);
                    $this->session->set_flashdata('error', $errors);
                    $this->session->set_flashdata('error_code', 1);
                    redirect("/artist/portfolio");
                }
                else // upload successful .. now resize
                {
                    $image_data =   $this->upload->data();

                    $configer =  array(
                      'image_library'   => 'gd2',
                      'source_image'    =>  strtolower($image_data['full_path']),
                      'maintain_ratio'  =>  TRUE,
                      'width'           =>  650,
                      'height'          =>  650,
                      );
                    $this->load->library('image_lib', $configer);
                    $this->image_lib->initialize($configer);
                    $this->image_lib->resize();
                    $this->image_lib->clear();
                }
                // for generate thumb image
                $config_resize['image_library'] = 'gd2';
                $config_resize['source_image'] = $config['upload_path'] . $config['file_name'] . '.' . $file_ext;

                $config_resize['create_thumb'] = TRUE;
                $config_resize['maintain_ratio'] = TRUE;
                $config_resize['width']         = 40;
                $config_resize['height']       = 40;

                $this->load->library('image_lib', $config_resize);
                $this->image_lib->initialize($config_resize);


                if( !$this->image_lib->resize())
                {
                    $errors = str_replace("<p>","", $this->image_lib->display_errors());
                    $errors = str_replace("</p>","", $errors);
                    $this->session->set_flashdata('error', $errors);
                    $this->session->set_flashdata('error_code', 1);
                    redirect("/artist/portfolio");
                }

                $_POST['image_url'] =  'assets/artist/images/portfolio/' . $config['file_name'] . '.' . $file_ext;

                $this->Portfolio_model->update($id);

                // delete former image
                unlink('./' . trim($this->input->post('image_url_old')));
                unlink('./' . trim($this->input->post('image_url_thumb_old')));

                $this->session->set_flashdata('error_code', 0);
                $this->session->set_flashdata('error', "Record updated successfully!");
            }
            else
            {
                // file not uploaded, update without image
                $this->Portfolio_model->updateWithoutImage($id);

                $this->session->set_flashdata('error_code', 0);
                $this->session->set_flashdata('error', "Record updated successfully!");
            }

            redirect("/artist/portfolio");

            ///////////////////////////////////////////////////////////////////////////

            /*
            $header['page_title'] = 'Update Portfolio';  // set page title
            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");

            // get new data after updating
            $data['row'] = $this->Portfolio_model->getRows(0, 0, $id);
            if(empty($data['row']))
                redirect("/artist/portfolio");

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');

            $this->load->view($this->config->item('template_dir_artist') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_artist') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_artist') . 'portfolio-edit', $data);
            */
        }
    }

    public function delete($id)
    {
        $this->Portfolio_model->delete($id);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");

        redirect('/artist/portfolio', 'refresh');
    }
}
