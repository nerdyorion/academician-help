<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends Artist_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/login');
            }
            $this->load->helper("url");
            $this->load->model('Service_model');
            $this->load->library("pagination");
    }

	public function index()
	{
        $user_id = (int) $this->session->userdata("user_id");
        $username = $this->session->userdata("user_username");

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = 'Services';
        $data['username'] = $username;

        // Pagination
        $config["base_url"] = base_url() . "artist/services/index";
        $config["total_rows"] = $this->Service_model->record_count($user_id);
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $config['use_page_numbers']  = TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        $data["rows"] = $this->Service_model->getRowsByUserID($config["per_page"], $offset, $user_id);
        $data["links"] = $this->pagination->create_links();

        $this->load->view($this->config->item('template_dir_artist') . 'header', $header);
        $this->load->view($this->config->item('template_dir_artist') . 'menu');
        $this->load->view($this->config->item('template_dir_artist') . 'services', $data);
	}

    public function create()
    {
        $user_id = (int) $this->session->userdata("user_id");

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Service Name', 'trim|required|max_length[20000]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        else
        {
            $this->Service_model->add();
            $this->session->set_flashdata('error', 'Record(s) added successfully.');
            $this->session->set_flashdata('error_code', 0);
        }

        redirect("/artist/services");
    }

    public function edit($id)
    {
        $id = (int) $id;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Service Name', 'trim|required|max_length[20000]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Update Service';  // set page title
            $data['row'] = $this->Service_model->getRows(0, 0, $id);
            if(empty($data['row']))
                redirect("/artist/services");
            
            $this->load->view($this->config->item('template_dir_artist') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_artist') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_artist') . 'service-edit', $data);  // load content view
        }
        else
        {
            $this->Service_model->update($id);

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");

            redirect("/artist/services");
        }
    }

    public function delete($id)
    {
        $this->Service_model->delete($id);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");

        redirect('/artist/services', 'refresh');
    }
}
