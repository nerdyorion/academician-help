<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends Artist_Controller {

	public function index()
	{
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('user_full_name');
        $this->session->unset_userdata('user_email');
		$this->session->unset_userdata('user_username');
		$this->session->unset_userdata('user_image_url');
		$this->session->unset_userdata('user_role_id');
		$this->session->unset_userdata('user_role_name');
		$this->session->unset_userdata('user_last_login');
		
		$this->session->unset_userdata('user_id_former_logged_in');
		$this->session->unset_userdata('user_full_name_former_logged_in');
        $this->session->unset_userdata('user_email_former_logged_in');
		$this->session->unset_userdata('user_username_former_logged_in');
		$this->session->unset_userdata('user_image_url_former_logged_in');
		$this->session->unset_userdata('user_role_id_former_logged_in');
		$this->session->unset_userdata('user_role_name_former_logged_in');
		$this->session->unset_userdata('user_last_login_former_logged_in');
        redirect('/login', 'refresh');
	}
}
