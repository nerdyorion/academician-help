<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyCustom404Ctrl extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
    }
	public function index()
	{
        $this->output->set_status_header('404');

		$this->load->view($this->config->item('template_dir_admin') . '404');
	}
}
