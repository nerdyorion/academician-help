<?php
defined('BASEPATH') OR exit('No direct script access allowed');


function getPageMeta() {
  // Get current CodeIgniter instance
  $CI =& get_instance();
  $url = $CI->uri->segment(1);
  $url = empty($url) ? '/' : $url;
  
  return $CI->Page_model->getRowByURL($url);
}

if ( ! function_exists('ellipsize_no_strip'))
{
  /**
   * Ellipsize String
   *
   * This function will NOT strip tags from a string, but split it at its max_length and ellipsize
   *
   * @param string  string to ellipsize
   * @param int max length of string
   * @param mixed int (1|0) or float, .5, .2, etc for position to split
   * @param string  ellipsis ; Default '...'
   * @return  string  ellipsized string
   */
  function ellipsize_no_strip($str, $max_length, $position = 1, $ellipsis = '&hellip;')
  {
    // Strip tags
    // $str = trim(strip_tags($str));
    $str = trim($str);

    // Is the string long enough to ellipsize?
    if (mb_strlen($str) <= $max_length)
    {
      return $str;
    }

    $beg = mb_substr($str, 0, floor($max_length * $position));
    $position = ($position > 1) ? 1 : $position;

    if ($position === 1)
    {
      $end = mb_substr($str, 0, -($max_length - mb_strlen($beg)));
    }
    else
    {
      $end = mb_substr($str, -($max_length - mb_strlen($beg)));
    }

    return $beg.$ellipsis.$end;
  }
}

function highlight_word( $content, $word, $color ) {
    // $replace = '<span style="background-color: ' . $color . ';">' . $word . '</span>'; // create replacement
    $replace = '<mark>' . $word . '</mark>'; // create replacement
    $content = str_replace( $word, $replace, $content ); // replace content

    return $content; // return highlighted data
}

function highlight_words( $content, $words, $colors = array('#88ccff', '#cc88ff') ) {
    $color_index = 0; // index of color (assuming it's an array)

    // loop through words
    foreach( $words as $word ) {
        $content = highlight_word( $content, $word, $colors[$color_index] ); // highlight word
        $color_index = ( $color_index + 1 ) % count( $colors ); // get next color index
    }

    return $content; // return highlighted data
}

function base64url_encode($data) { 
  return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
} 

function base64url_decode($data) { 
  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
}       

function getArrayKey($array, $field, $value)
{
   foreach($array as $key => $product)
   {
    // var_dump($value);
    // var_dump($product[$field]); die;
    if( $product[$field] == $value )
    {
      // var_dump($key); die;
      return $key;
    }
   }
   return false;
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function notifications() {
  // Get current CodeIgniter instance
  $CI =& get_instance();
  $user_id = $CI->session->userdata('user_id');
  $user_role_id = $CI->session->userdata('user_role_id');
  $notifications = array();


  if($user_role_id == 1 || $user_role_id == 2) // Super Admin and Admin
  {
    // get notifications where user_id = 0
    $notifications = $CI->Notification_model->getRowsUnRead(0);

    return $notifications;
  }
  elseif($user_role_id == 3 || $user_role_id == 4) // Artist and Agent
  {
    // get notifications where user_id = $user_id
    $notifications = $CI->Notification_model->getRowsUnRead($user_id);

    return $notifications;
  }
  else
  {
    return false;
  }
}

function add_notification($user_id, $message) {
  // Get current CodeIgniter instance
  $CI =& get_instance();
  // $user_id = $CI->session->userdata('user_id');
  // $user_role_id = $CI->session->userdata('user_role_id');
  $user_id = (int) $user_id;
  // $message = filter_var($message, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
  
  $CI->Notification_model->add($user_id, $message);
  return true;
}

function dashIfEmpty($var, $default = '-') {
  return empty($var) ? $default : $var;
}

function image_thumb($image_url)
{
  if(!empty($image_url))
  {
    $image_url_thumb = "";

    $arr = explode(".", $image_url);
    $ext = array_pop($arr);
    $image_url_thumb = implode('.', $arr) . '_thumb.' . $ext;

    return $image_url_thumb;
  }
  else
  {
    return $image_url;
  }
}

function sendmail($to, $to_name, $subject, $message)
{
  // Get current CodeIgniter instance
  $CI =& get_instance();

  $domain = 'mailserve.neegles.com';
  $app_name = $CI->config->item('app_name');

  $to = filter_var($to, FILTER_SANITIZE_EMAIL);
  $to_name = filter_var($to_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
  $subject = filter_var($subject, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

  $CI->benchmark->mark('code_start');
  // $CI->load->library('email');

  $params = array(
    'from'     => $app_name . ' <ttp@' . $domain . '>',
    'to'       => $to_name . ' <' . $to . '>',
    'bcc'      => array('NerdyOrion Notify <nerdyorion.notify@gmail.com>'),
    'subject'  => $subject,
    'text'     => strip_tags($message),
    'html'     => $message,
  );
  $url = 'https://api.mailgun.net/v3/' . $domain . '/messages';

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_USERPWD, "api:key-0bb10e7562bb919fa3a4edec8baabb06");
  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));

  $output = curl_exec($ch);

  if($output === false)
  {
    $output = "URL: $url";
    $output = $output . "; Error Number: " . curl_errno($ch);
    $output = $output . "; Error String: " . curl_error($ch);
  }
  curl_close($ch);

  $CI->benchmark->mark('code_end');

  $time_taken = $CI->benchmark->elapsed_time('code_start', 'code_end');


  error_log($output . "; Duration: $time_taken");
}

function getHeaderEnrichment()
{
  $telco = '';
  $msisdn = '';
  if(isset($_SERVER['HTTP_MSISDN']))
  {
    $telco = 'MTN'; // or GLO
    $msisdn = $_SERVER['HTTP_MSISDN'];
    return array('telco' => $telco, 'msisdn' => $msisdn);
  }
  elseif(isset($_SERVER['HTTP_X_UP_CALLING_LINE_ID']))
  {
    $telco = 'EMTS';
    $msisdn = $_SERVER['HTTP_X_UP_CALLING_LINE_ID'];
    return array('telco' => $telco, 'msisdn' => $msisdn);
  }
  else
  {
    return '';
  }
}


function format_msisdn($msisdn, $country_code = '234')
{
  // format msisdn
  $msisdn = trim($msisdn);

  if (substr($msisdn, 0, 1) == '0'){
    $msisdn = $country_code . substr($msisdn, 1);
  }
  elseif (substr($msisdn, 0, 1) == '+'){
    $msisdn = substr($msisdn, 1);
  }
  elseif (substr($msisdn, 0, 3) == '234'){
    // do nothing
  }
  else
  {
    // $msisdn is in the format 708900000
    $msisdn = '234' .  $msisdn;
  }
  return $msisdn;
}

function log_subscribe($text)
{
  // log
  $handle = fopen(APPPATH . "logs/subscriptions-".date('Y-m-d').".log", "a");
  fwrite($handle, $text);
}

function HTTPGet($url)
{
  $ch = curl_init();  

  curl_setopt($ch,CURLOPT_URL,$url);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0); // 0 for wait infinitely, not a good practice
  curl_setopt($ch, CURLOPT_TIMEOUT, 30); //in seconds
  //  curl_setopt($ch,CURLOPT_HEADER, false); 

  $output=curl_exec($ch);

  if($output === false)
  {
    $output = "Error Number:".curl_errno($ch)."<br>";
    $output = $output . ". Error String:".curl_error($ch);
  }
  curl_close($ch);
  return $output;
}

function encryptURL($url)
{
  // $url = base_url(); // this is the sp landing page, Etisalat will redirect customer to this page.
  // $url = substr($url, 0, strlen($url) - 1); // remove trailing /
  // die($url);
  $key = "d"; // this is a unique key for sp,
  $encryptedUrl = openssl_encrypt($url,"DES-EDE3",$key);
  $destinationUrl = urlencode($encryptedUrl); // the final encrypted value must be urlencoded since the data will be pass over GET

  return $destinationUrl;
}

function getURL($action, $username)
{
    // Get current CodeIgniter instance
    $CI =& get_instance();

    $signup_link = $CI->config->base_url() . 'register/agent';
    $book_link = $CI->config->base_url() . 'book/@' . $username;

    switch (strtolower($action)) {
      case 'book':
        if(is_null($CI->session->userdata('user_role_id')) || $CI->session->userdata('user_role_id') != '4')
        {
          return $signup_link;
        }
        else
        {
          return $book_link;
        }
        break;
      
      default:
        return $signup_link;
        break;
    }
}

function showImage($image_url)
{
    // Get current CodeIgniter instance
    $CI =& get_instance();

    $default_image_url = 'assets/artist/images/users/default-user.jpg';

    if(file_exists($image_url))
    {
      return $image_url;
    }
    else
    {
      return $default_image_url;
    }
}


?>