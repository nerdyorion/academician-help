<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Coupon_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count() {
        return $this->db->count_all("coupons");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $this->db->order_by('name', 'ASC');
            $this->db->select('id, name, code, description, discount_amount, is_fixed, date_created, expiry_date');
            $this->db->from('coupons'); 
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select('id, name, code, description, discount_amount, is_fixed, date_created, expiry_date');
        $this->db->from('coupons');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown($id = FALSE)
    {
        if ($id === FALSE)
        {
            $this->db->order_by('name', 'ASC');
            $this->db->select('id, name, code, description');
            $this->db->from('coupons'); 
            $query = $this->db->get();
            
            return $query->result_array();
        }
        $this->db->select('name, code');
        $this->db->from('coupons');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRandom($limit)
    {
        $limit = (int) $limit;

        $this->db->order_by('id', 'RANDOM');
        $this->db->select('id, name, code');
        $this->db->from('coupons');
        $this->db->limit($limit);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'name' => trim($this->input->post('name')),
            'code' => trim($this->input->post('code')),
            'description' => trim($this->input->post('description')),
            'discount_amount' => (float) trim($this->input->post('discount_amount')),
            'is_fixed' => (int) trim($this->input->post('is_fixed')),
            'date_created' => date('Y-m-d'), // added here due to timestamp
            'expiry_date' => empty($this->input->post('expiry_date')) ? NULL : trim($this->input->post('expiry_date')),
            'created_by' => $created_by
        );

        $this->db->insert('coupons', $data);
    }

    public function delete($id)
    {
        $this->db->delete('coupons', array('id' => (int) $id));
    }

    public function update($id)
    {
        // $expiry_date = empty($this->input->post('expiry_date')) ? NULL : trim($this->input->post('expiry_date'));

        $data = array(
            'name' => trim($this->input->post('name')),
            'code' => trim($this->input->post('code')),
            'description' => trim($this->input->post('description')),
            'discount_amount' => (float) trim($this->input->post('discount_amount')),
            'is_fixed' => (int) trim($this->input->post('is_fixed')),
            'expiry_date' => empty($this->input->post('expiry_date')) ? NULL : trim($this->input->post('expiry_date'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('coupons', $data);
    }

    public function exists($id)
    {
        $this->db->select('code');
        $this->db->from('coupons');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if(empty($row))
        {
            // coupon does not exist
            return false;
        }
        else
        {
            // coupon exist
            return true;
        }
    }

    public function getRowByCode($code)
    {
        $this->db->select('id, name, code, description, discount_amount, is_fixed, date_created, expiry_date');
        $this->db->from('coupons');
        $this->db->where('code', $code); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function is_used($coupon_id, $user_id)
    {
        // where coupon_is is id and is_paid == 1

        $this->db->select('coupons.id, coupons.name, coupons.code, coupons.description, coupons.discount_amount, coupons.is_fixed, coupons.date_created, coupons.expiry_date');
        $this->db->from('orders');
        $this->db->join('coupons', 'orders.coupon_id = coupons.id', 'left');
        $this->db->where('orders.user_id', (int) $user_id); 
        $this->db->where('orders.coupon_id', (int) $coupon_id); 
        $this->db->where('orders.paid', 1); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if(empty($row))
        {
            // coupon does not exist
            return false;
        }
        else
        {
            // coupon exist
            return $row;
        }
    }

    public function getNewUserCoupon()
    {
        $this->db->select('id, name, code, description, discount_amount, is_fixed, date_created, expiry_date');
        $this->db->from('coupons');
        $this->db->where('id', 1); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }
}