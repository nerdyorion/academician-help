<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Download_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count($type = "all", $download_type = "all", $user_id = FALSE)
    {
        if($user_id !== FALSE)
        {
            if($type == "student")
            {
                $this->db->where('user_id', (int) $user_id);
            }
            // $this->db->from('downloads');
            // return $this->db->count_all_results();
        }
        if($download_type == "free")
        {
            $this->db->where('type', 0);
        }
        if($download_type == "paid")
        {
            $this->db->where('type', 1);
        }

        $this->db->select('COUNT(id) AS count');
        $this->db->from('downloads');
        $query = $this->db->get();          // echo $this->db->last_query(); die();
        return $query->row_array()['count'];
    }

    public function add($past_exam_solution_id, $type=0)
    {
        $user_id = (int) $this->session->userdata('user_id');

        // for free_project_proposals, type = 0
        // for past_exam_solution, type = 1

        $data = array(
            'user_id' => $user_id,
            'past_exam_solution_id' => (int) $past_exam_solution_id,
            'type' => (int) $type
        );

        $this->db->insert('downloads', $data);
    }
}