<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Free_Project_Proposal_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function filter_record_count($id = FALSE, $level_id = FALSE, $title = FALSE)
    {

        $sql = "SELECT COUNT(free_project_proposals.id) AS count FROM free_project_proposals LEFT OUTER JOIN levels ON free_project_proposals.level_id = levels.id WHERE (free_project_proposals.file_url IS NOT NULL)";

        $where = '';

        if($id !== FALSE)
        {
            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (free_project_proposals.id = '". $id . "')";
            }
        }

        if($level_id !== FALSE)
        {
            $level_id = (int) $level_id;
            if($level_id != 0)
            {
                $where .= " AND (free_project_proposals.level_id = '". $level_id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(free_project_proposals.title LIKE '". $title_full . "') OR (free_project_proposals.description LIKE '". $title_full . "') OR (levels.name LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (free_project_proposals.title LIKE '". $title_word . "') OR (free_project_proposals.description LIKE '". $title_word . "') OR (levels.name LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $id = FALSE, $level_id = FALSE, $title = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT free_project_proposals.id, free_project_proposals.level_id, free_project_proposals.title, free_project_proposals.file_url, free_project_proposals.description, free_project_proposals.page_title, free_project_proposals.meta_title, free_project_proposals.meta_tags, free_project_proposals.meta_description, free_project_proposals.date_created, levels.name AS level_name FROM free_project_proposals LEFT OUTER JOIN levels ON free_project_proposals.level_id = levels.id WHERE (free_project_proposals.file_url IS NOT NULL)";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (free_project_proposals.id = '". $id . "')";
            }
        }

        if($level_id !== FALSE)
        {
            $level_id = (int) $level_id;
            if($level_id != 0)
            {
                $where .= " AND (free_project_proposals.level_id = '". $level_id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(free_project_proposals.title LIKE '". $title_full . "') OR (free_project_proposals.description LIKE '". $title_full . "') OR (levels.name LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (free_project_proposals.title LIKE '". $title_word . "') OR (free_project_proposals.description LIKE '". $title_word . "') OR (levels.name LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $sql = $sql . $where . " ORDER BY free_project_proposals.date_created DESC LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count() {
        return $this->db->count_all("free_project_proposals");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $this->db->order_by('free_project_proposals.date_created', 'DESC');
            $this->db->select('free_project_proposals.id, free_project_proposals.level_id, free_project_proposals.title, free_project_proposals.file_url, free_project_proposals.description, free_project_proposals.page_title, free_project_proposals.meta_title, free_project_proposals.meta_tags, free_project_proposals.meta_description, free_project_proposals.date_created, levels.name AS level_name');
            $this->db->from('free_project_proposals');
            $this->db->join('levels', 'free_project_proposals.level_id = levels.id', 'left');
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select('free_project_proposals.id, free_project_proposals.level_id, free_project_proposals.title, free_project_proposals.file_url, free_project_proposals.description, free_project_proposals.page_title, free_project_proposals.meta_title, free_project_proposals.meta_tags, free_project_proposals.meta_description, free_project_proposals.date_created, levels.name AS level_name');
        $this->db->from('free_project_proposals');
        $this->db->join('levels', 'free_project_proposals.level_id = levels.id', 'left');
        $this->db->where('free_project_proposals.id', (int) $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRecentItems($limit)
    {
        $limit = (int) $limit;

        $this->db->order_by('free_project_proposals.date_created', 'DESC');
        $this->db->select('free_project_proposals.id, free_project_proposals.title, levels.name AS level_name');
        $this->db->from('free_project_proposals');
        $this->db->join('levels', 'free_project_proposals.level_id = levels.id', 'left');
        $this->db->limit($limit);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'level_id' => (int) $this->input->post('level_id'),
            'title' => trim($this->input->post('title')),
            'file_url' => trim($this->input->post('file_url')),
            'description' => trim($this->input->post('description')),

            'page_title' => trim($this->input->post('page_title')),
            'meta_title' => trim($this->input->post('meta_title')),
            'meta_tags' => trim($this->input->post('meta_tags')),
            'meta_description' => trim($this->input->post('meta_description')),
            'created_by' => $created_by
        );

        $this->db->insert('free_project_proposals', $data);
    }

    public function delete($id)
    {
        $this->db->delete('free_project_proposals', array('id' => (int) $id));
    }

    public function update($id)
    {
        $data = array(
            'level_id' => (int) $this->input->post('level_id'),
            'title' => trim($this->input->post('title')),
            'file_url' => trim($this->input->post('file_url')),
            'description' => trim($this->input->post('description')),

            'page_title' => trim($this->input->post('page_title')),
            'meta_title' => trim($this->input->post('meta_title')),
            'meta_tags' => trim($this->input->post('meta_tags')),
            'meta_description' => trim($this->input->post('meta_description'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('free_project_proposals', $data);
    }

    public function updateWithoutFile($id)
    {
        $data = array(
            'level_id' => (int) $this->input->post('level_id'),
            'title' => trim($this->input->post('title')),
            // 'file_url' => trim($this->input->post('file_url')),
            'description' => trim($this->input->post('description')),

            'page_title' => trim($this->input->post('page_title')),
            'meta_title' => trim($this->input->post('meta_title')),
            'meta_tags' => trim($this->input->post('meta_tags')),
            'meta_description' => trim($this->input->post('meta_description'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('free_project_proposals', $data);
    }
}