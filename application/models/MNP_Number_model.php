<?php
 set_time_limit(0);

class MNP_Number_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function getRows($id = FALSE)
    {
    	if ($id === FALSE)
        {
            //$query = $this->db->get('SMSC_mnp');
            //return $query->result_array();
            $query = $this->db->order_by('date_created', 'DESC');
  			$this->db->from('SMSC_mnp'); 
            $query = $this->db->limit(20);
  			$query = $this->db->get();
  			
            return $query->result_array();
        }
        $query = $this->db->get_where('SMSC_mnp', array('id' => $id));
        return $query->row_array();
    }

    public function getCount($type)
    {
        if ($type == "ALL")
        {
            //return $this->db->count_all('SMSC_mnp');
            $this->db->select('total'); 
            $this->db->from('summary');   
            $this->db->where('category', 'mnp');
            return $this->db->get()->row()->total;
            /*
            $this->db->select('total'); 
    	    $this->db->from('summary');   
    	    $this->db->where('category', 'mnp');
            return $this->db->get()->row()->total;
    	    */
        }
        elseif ($type == "TODAY")
        {
            /*
            $sql = "SELECT COUNT(id) AS count FROM `SMSC_mnp` WHERE DATE(`date_created`) = CURDATE()";
            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['count'];

            $this->db->select('today'); 
            $this->db->from('summary');   
            $this->db->where('category', 'mnp');
            return $this->db->get()->row()->today;
    	    */
            $sql = "SELECT `today` AS today FROM `summary` WHERE `category` = 'mnp' AND DATE(`date_updated`) = CURDATE()";
            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['today'];
        }
        else
        {
            return 0;
        }
    }

    public function getCountOld($type)
    {
        if ($type == "ALL")
        {
            return $this->db->count_all('SMSC_mnp');
        }
        elseif ($type == "TODAY")
        {
            $sql = "SELECT COUNT(id) AS count FROM `SMSC_mnp` WHERE DATE(`date_created`) = CURDATE()";
            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['count'];
        }
        else
        {
            return 0;
        }
    }

    public function getRowByMSISDN()
    {
    	$msisdn = $this->input->post('msisdn');
        $this->db->like('msisdn', $msisdn);
  		$this->db->from('SMSC_mnp'); 
                $query = $this->db->limit(20);
  		$query = $this->db->get();
        return $query->result_array();
    }

    public function search()
    {
        $network_id = (int) $this->input->post('network_id');

        $msisdn_array = explode(",", $this->input->post('msisdn')); // get msisdn into array with spaces
        $msisdn = implode(",", array_map("trim", $msisdn_array));   // trim spaces 
        $split_commas_newlines = preg_split('/[\ \n\,]+/', $msisdn);        // split commas and newlines into array
        $msisdn = implode(",", array_map("trim", $split_commas_newlines));   // trim spaces 
        $msisdn_array = explode(",", $msisdn); // put back into array
        $count = count($split_commas_newlines);
        $where = '';
        //var_dump($msisdn); die();

        //$msisdn_array = explode(",", $this->input->post('msisdn'));
        //$msisdn_array = array_map("trim", $msisdn_array);

        $msisdn_first_item = array_shift($msisdn_array); // get first msisdn from array and reove it from array
        $msisdn_first_item = filter_var($msisdn_first_item, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        $where = "(SELECT * FROM `SMSC_mnp` WHERE msisdn='". $msisdn_first_item . "' LIMIT 1)";

        foreach ($msisdn_array as $msisdn) {
            //var_dump($msisdn_array); die();
            if((!empty($msisdn)) && (strlen($msisdn) > 5) )
            {
                $msisdn = filter_var($msisdn, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
                $where =  $where . " UNION (SELECT * FROM `SMSC_mnp` WHERE msisdn='". $msisdn . "' LIMIT 1)";
            }
        }
        //$where .= ";";

        $query = $this->db->query($where);
        return $query->result_array();
    }

    public function add($csv)
    {
        $date_format = 'Y-m-d H:i:s';
        $date_format_post = $this->input->post('date_format');
        if($date_format_post == '2')
        {
            $date_format = 'd/m/Y H:i:s';
        }else if($date_format_post == '3')
        {
            $date_format = 'm/d/Y H:i:s';
        }
        //$this->load->helper('url');

        //$slug = url_title($this->input->post('title'), 'dash', TRUE);

        //array_shift($csv);  // remove column headers from array
        	//var_dump($csv); die();
        $created_by = (int) $this->session->userdata('user_id');

        $sn = 1;
        foreach($csv as $csv_data)
        {
        	//var_dump($csv_data); die();
        	if((!empty($csv_data[0])) && (!empty($csv_data[1])) && (!empty($csv_data[2])) && (strtoupper(trim($csv_data[3])) != "DATE_TIME_STAMP") )
        	{
                	/** LOG TO TXT
                	// log each valid row in folder to check if all content was processed
                	$input_data = $csv_data[0] . ",".$csv_data[1]. "," . $csv_data[2]. "," . $csv_data[3];
                	$handle = fopen("assets/csv/mnp/test/" . $sn .".log", "a");
                	fwrite($handle, $input_data);   
                	$sn++;
                	*/

        		//date('Y-m-d H:i:s', strtotime($csv_data[1]) );
                	//$csv_data[3] = date($date_format, strtotime(trim($csv_data[3])));


                	$csv_data[3] = trim($csv_data[3]);

                	$tmp = explode(" ", $csv_data[3]);
                        //$hr_min_sec = end($tmp);
                	$hr_min_sec = (count($tmp) == 1) ? "00:00:00" : end($tmp);
                	$hr_min_sec = (strlen($hr_min_sec) == 4) || (strlen($hr_min_sec) == 5) ? $hr_min_sec . ":00" : $hr_min_sec;


                	$date1 = $tmp[0] . " " . $hr_min_sec;
                	//var_dump($date1);
                	//var_dump($date_format); die();

                	$out1 = DateTime::createFromFormat($date_format, trim($date1));
                	$csv_data[3] = $out1->format('Y-m-d H:i:s');
                	//$csv_data[3] = date('Y-m-d H:i:s', strtotime(trim($csv_data[3])));
                
        		//var_dump($csv_data[1]); die();
        		//var_dump(date('Y-m-d H:i:s', strtotime(trim($csv_data[1])))); die();
        		$data = array(
            		'msisdn' => trim($csv_data[0]),
            		'donor_id' => trim($csv_data[1]),
            		'recipient_id' => trim($csv_data[2]),
                    'date_time_stamp' => trim($csv_data[3]),
                    'created_by' => $created_by
                );

                // log msisdn, donor and recipient
                $input_data = trim($csv_data[0]) . ',' . strtoupper(trim($csv_data[1])) . ',' . strtoupper(trim($csv_data[2]));
                $handle = fopen("/home/emhollatags/MNP/bulksms-mnp-msisdn-".date('Y-m-d').".csv", "a");
                //$handle = fopen("../MNP/bulksms-mnp-msisdn-".date('Y-m-d').".csv", "a");
                fputcsv($handle, explode(',', $input_data));

        		$sql = 'INSERT INTO SMSC_mnp (msisdn, donor_id, recipient_id, date_time_stamp, created_by) VALUES (?, ?, ?, ?, ?)
        		ON DUPLICATE KEY UPDATE 
        		msisdn=VALUES(msisdn), 
        		donor_id=VALUES(donor_id), 
        		recipient_id=VALUES(recipient_id),  
        		date_time_stamp=VALUES(date_time_stamp),
                no_of_duplicates=no_of_duplicates + 1,
                last_updated=NOW()';

				$query = $this->db->query($sql, $data);
        		//return $this->db->insert('SMSC_mnp', $data);
        		//return $query;
        	}
        }
        $sql = 'UPDATE summary SET total = (SELECT COUNT(id) FROM `SMSC_mnp`), today = (SELECT COUNT(id) FROM `SMSC_mnp` WHERE DATE(`date_created`) = CURDATE()) WHERE category = "mnp"';
        $query = $this->db->query($sql);
    }
}