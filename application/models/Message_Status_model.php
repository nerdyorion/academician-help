<?php
class Message_Status_model extends CI_Model {

    public function __construct()
    {
        //$this->load->database();
    }

    public function HTTPGet($url)
    {
        $ch = curl_init();  
 
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0); // 0 for wait infinitely, not a good practice
        curl_setopt($ch, CURLOPT_TIMEOUT, 0); //in seconds
        //  curl_setopt($ch,CURLOPT_HEADER, false); 
 
        $output=curl_exec($ch);
 
        if($output === false)
        {
            $output = "Error Number:".curl_errno($ch)."<br>";
            $output = $output . ". Error String:".curl_error($ch);
        }
        curl_close($ch);
        return $output;
    }

    public function checkStatus()
    {
        define('API_URL', 'http://smsc-api.hollatags.com/?');
        $client_id = $this->input->post('client_id');
        $date = $this->input->post('date');

        ###
        $key = 'abc'; // Constant password will change in production.
        $pwd = $key . ':' . $date;

        $access_key =  password_hash($pwd, PASSWORD_DEFAULT);
        ###


        $msisdn_array = explode(",", $this->input->post('msisdn')); // get msisdn into array with spaces
        $msisdn = implode(",", array_map("trim", $msisdn_array));   // trim spaces 
        $split_commas_newlines = preg_split('/[\ \n\,]+/', $msisdn);        // split commas and newlines into array
        $msisdn = implode(",", array_map("trim", $split_commas_newlines));   // trim spaces 
        $count = count($split_commas_newlines);
        var_dump($msisdn); die();

        //$msisdn_array = explode(",", $this->input->post('msisdn'));
        //$msisdn = implode(",", array_map("trim", $msisdn_array));
        /*$api_query = '[ {
            "client_id":"cloudsms",
            "datetime":"2016-08-19",
            "msisdn":"2348100000002",
            "sender_id":"CHURCH",
            "message_id":"0000009d-5f19-44dc-9569-b9bab49e8d12",
            "status":"REJECTD",
            "status_description":"SUSBCRIBER ON DND"
        },
        {
            "client_id":"cloudsms",
            "datetime":"2016-08-19",
            "msisdn":"2348100000003",
            "sender_id":"Sport",
            "message_id":"46htr7jr-5459-43dc-9e69-k9bab49e8d12",
            "status":"DELIVRD",
            "status_description":"DELIVRD TO SUBSCRIBER"
        } ]';*/
        
        $response = $this->HTTPGet(API_URL . "access_key=$access_key&client_id=$client_id&date=$date&msisdn=$msisdn");
        $rows = json_decode($response, true); // add true to convert to associative array without object stdClass
        //var_dump($date);
        //var_dump($api_query);
        //var_dump($rows);
        //die();
        return $rows;
    }
}