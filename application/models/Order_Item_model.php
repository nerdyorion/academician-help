<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Order_Item_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function countOrderItemsByUserID($type = "all", $user_id = FALSE)
    {
        if($type == "free")
        {
            $this->db->where('orders.paid', 0);
        }
        if($type == "paid")
        {
            $this->db->where('orders.paid', 1);
        }

        $this->db->select('COUNT(order_items.id) AS count');
        $this->db->from('orders');
        $this->db->join('order_items', 'orders.id = order_items.order_id', 'left');
        $this->db->group_by('orders.user_id');
        $query = $this->db->get();          // echo $this->db->last_query(); die();
        return $query->row_array()['count'];
    }

    public function deleteByOrderID($order_id)
    {
        $this->db->delete('order_items', array('order_id' => (int) $order_id));
    }
}