<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Page_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
        $this->db->reset_query();
    }

    public function filter_record_count($id = FALSE, $title = FALSE)
    {
        $this->db->reset_query();

        $sql = "SELECT COUNT(pages.id) AS count FROM pages WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (pages.id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(pages.page_title LIKE '". $title_full . "') OR (pages.url LIKE '". $title_full . "') OR (pages.meta_title LIKE '". $title_full . "') OR (pages.meta_tags LIKE '". $title_full . "') OR (pages.meta_description LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (pages.page_title LIKE '". $title_word . "') OR (pages.url LIKE '". $title_word . "') OR (pages.meta_title LIKE '". $title_word . "') OR (pages.meta_tags LIKE '". $title_word . "') OR (pages.meta_description LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $id = FALSE, $title = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT pages.id, pages.page_title, pages.url, pages.meta_title, pages.meta_tags, pages.meta_description, pages.created_by, pages.date_created FROM pages WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (pages.id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(pages.page_title LIKE '". $title_full . "') OR (pages.url LIKE '". $title_full . "') OR (pages.meta_title LIKE '". $title_full . "') OR (pages.meta_tags LIKE '". $title_full . "') OR (pages.meta_description LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (pages.page_title LIKE '". $title_word . "') OR (pages.url LIKE '". $title_word . "') OR (pages.meta_title LIKE '". $title_word . "') OR (pages.meta_tags LIKE '". $title_word . "') OR (pages.meta_description LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where . " ORDER BY pages.page_title ASC LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count()
    {
        return $this->db->count_all("pages");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if ($id === FALSE)
        {
            $query = $this->db->order_by('pages.page_title', 'ASC');
            $this->db->select('pages.id, pages.page_title, pages.url, pages.meta_title, pages.meta_tags, pages.meta_description, pages.created_by, pages.date_created');
            $this->db->from('pages'); 
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get(); //echo $this->db->last_query(); die;
            return $query->result_array();
        }
        $this->db->select('pages.id, pages.page_title, pages.url, pages.meta_title, pages.meta_tags, pages.meta_description, pages.created_by, pages.date_created');
        $this->db->from('pages'); 
        $this->db->where('pages.id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();    //echo $this->db->last_query(); die;
        return $query->row_array();
    }

    public function getRowByURL($url)
    {
        $url = filter_var(trim($url), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        $this->db->select('pages.id, pages.page_title, pages.url, pages.meta_title, pages.meta_tags, pages.meta_description, pages.created_by, pages.date_created');
        $this->db->from('pages'); 
        $this->db->where('pages.url', $url); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function urlExists($url, $page_id = FALSE)
    {
        $page_id = (int) $page_id;

        $url = filter_var(trim($url), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        $this->db->select('id');
        $this->db->from('pages');
        $this->db->where('url', $url); 
        if($page_id !== FALSE)
        {
            $this->db->where('id !=', (int) $page_id);
        }
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if(empty($row))
        {
            // url does not exist
            return false;
        }
        else
        {
            // url exist
            return true;
        }
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'page_title' => $this->input->post('page_title'),
            'url' => trim($this->input->post('url')),
            'meta_title' => trim($this->input->post('meta_title')),
            'meta_tags' => trim($this->input->post('meta_tags')),
            'meta_description' => trim($this->input->post('meta_description')),
            'created_by' => $created_by
        );

        $this->db->insert('pages', $data);
    }

    public function delete($id)
    {
        $this->db->delete('pages', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'page_title' => $this->input->post('page_title'),
            'url' => trim($this->input->post('url')),
            'meta_title' => trim($this->input->post('meta_title')),
            'meta_tags' => trim($this->input->post('meta_tags')),
            'meta_description' => trim($this->input->post('meta_description'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('pages', $data);
    }
}