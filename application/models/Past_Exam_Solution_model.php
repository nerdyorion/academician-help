<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Past_Exam_Solution_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function filter_record_count($id = FALSE, $country_id = FALSE, $university_id = FALSE, $title = FALSE)
    {

        $sql = "SELECT COUNT(past_exam_solutions.id) AS count FROM past_exam_solutions LEFT OUTER JOIN universities ON past_exam_solutions.university_id = universities.id LEFT OUTER JOIN countries ON universities.country_id = countries.id WHERE (past_exam_solutions.file_url IS NOT NULL)";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (past_exam_solutions.id = '". $id . "')";
            }
        }

        if($country_id !== FALSE)
        {
            $country_id = (int) $country_id;
            if($country_id != 0)
            {
                $where .= " AND (universities.country_id = '". $country_id . "')";
            }
        }

        if($university_id !== FALSE)
        {
            $university_id = (int) $university_id;
            if($university_id != 0)
            {
                $where .= " AND (past_exam_solutions.university_id = '". $university_id . "')";
            }
        }

        if($title !== FALSE)
        {
            // $title = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            // $where .= " AND ((past_exam_solutions.title LIKE '". $title . "') OR (past_exam_solutions.description LIKE '". $title . "'))";
            
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(past_exam_solutions.title LIKE '". $title_full . "') OR (past_exam_solutions.description LIKE '". $title_full . "') OR (universities.name LIKE '". $title_full . "') OR (countries.name LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (past_exam_solutions.title LIKE '". $title_word . "') OR (past_exam_solutions.description LIKE '". $title_word . "') OR (universities.name LIKE '". $title_word . "') OR (countries.name LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $id = FALSE, $country_id = FALSE, $university_id = FALSE, $title = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT past_exam_solutions.id, past_exam_solutions.university_id, past_exam_solutions.title, past_exam_solutions.file_url, past_exam_solutions.price, past_exam_solutions.description, past_exam_solutions.page_title, past_exam_solutions.meta_title, past_exam_solutions.meta_tags, past_exam_solutions.meta_description, past_exam_solutions.date_created, countries.name AS country_name, universities.name AS university_name FROM past_exam_solutions LEFT OUTER JOIN universities ON past_exam_solutions.university_id = universities.id LEFT OUTER JOIN countries ON universities.country_id = countries.id WHERE (past_exam_solutions.file_url IS NOT NULL)";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (past_exam_solutions.id = '". $id . "')";
            }
        }

        if($country_id !== FALSE)
        {
            $country_id = (int) $country_id;
            if($country_id != 0)
            {
                $where .= " AND (universities.country_id = '". $country_id . "')";
            }
        }

        if($university_id !== FALSE)
        {
            $university_id = (int) $university_id;
            if($university_id != 0)
            {
                $where .= " AND (past_exam_solutions.university_id = '". $university_id . "')";
            }
        }

        if($title !== FALSE)
        {
            // $title = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            // $where .= " AND ((past_exam_solutions.title LIKE '". $title . "') OR (past_exam_solutions.description LIKE '". $title . "') OR (universities.name LIKE '". $title . "') OR (countries.name LIKE '". $title . "'))";
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(past_exam_solutions.title LIKE '". $title_full . "') OR (past_exam_solutions.description LIKE '". $title_full . "') OR (universities.name LIKE '". $title_full . "') OR (countries.name LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (past_exam_solutions.title LIKE '". $title_word . "') OR (past_exam_solutions.description LIKE '". $title_word . "') OR (universities.name LIKE '". $title_word . "') OR (countries.name LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $sql = $sql . $where . " ORDER BY past_exam_solutions.date_created DESC LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count() {
        return $this->db->count_all("past_exam_solutions");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $this->db->order_by('past_exam_solutions.date_created', 'DESC');
            $this->db->select('past_exam_solutions.id, past_exam_solutions.university_id, past_exam_solutions.title, past_exam_solutions.file_url, past_exam_solutions.price, past_exam_solutions.description, past_exam_solutions.page_title, past_exam_solutions.meta_title, past_exam_solutions.meta_tags, past_exam_solutions.meta_description, past_exam_solutions.date_created, countries.name AS country_name, universities.name AS university_name');
            $this->db->from('past_exam_solutions'); 
            $this->db->join('universities', 'past_exam_solutions.university_id = universities.id', 'left');
            $this->db->join('countries', 'universities.country_id = countries.id', 'left');
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select('past_exam_solutions.id, past_exam_solutions.university_id, past_exam_solutions.title, past_exam_solutions.file_url, past_exam_solutions.price, past_exam_solutions.description, past_exam_solutions.page_title, past_exam_solutions.meta_title, past_exam_solutions.meta_tags, past_exam_solutions.meta_description, past_exam_solutions.date_created, countries.name AS country_name, universities.name AS university_name');
        $this->db->from('past_exam_solutions'); 
        $this->db->join('universities', 'past_exam_solutions.university_id = universities.id', 'left');
        $this->db->join('countries', 'universities.country_id = countries.id', 'left');
        $this->db->where('past_exam_solutions.id', (int) $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRecentItems($limit)
    {
        $limit = (int) $limit;

        $this->db->order_by('date_created', 'DESC');
        $this->db->select('id, title, price');
        $this->db->from('past_exam_solutions');
        $this->db->limit($limit);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'university_id' => (int) $this->input->post('university_id'),
            'title' => trim($this->input->post('title')),
            'price' => trim($this->input->post('price')),
            'file_url' => trim($this->input->post('file_url')),
            'description' => trim($this->input->post('description')),

            'page_title' => trim($this->input->post('page_title')),
            'meta_title' => trim($this->input->post('meta_title')),
            'meta_tags' => trim($this->input->post('meta_tags')),
            'meta_description' => trim($this->input->post('meta_description')),
            'created_by' => $created_by
        );

        $this->db->insert('past_exam_solutions', $data);
    }

    public function delete($id)
    {
        $this->db->delete('past_exam_solutions', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'university_id' => (int) $this->input->post('university_id'),
            'title' => trim($this->input->post('title')),
            'price' => trim($this->input->post('price')),
            'file_url' => trim($this->input->post('file_url')),
            'description' => trim($this->input->post('description')),

            'page_title' => trim($this->input->post('page_title')),
            'meta_title' => trim($this->input->post('meta_title')),
            'meta_tags' => trim($this->input->post('meta_tags')),
            'meta_description' => trim($this->input->post('meta_description'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('past_exam_solutions', $data);
    }

    public function updateWithoutFile($id)
    {

        $data = array(
            'university_id' => (int) $this->input->post('university_id'),
            'title' => trim($this->input->post('title')),
            'price' => trim($this->input->post('price')),
            // 'file_url' => trim($this->input->post('file_url')),
            'description' => trim($this->input->post('description')),

            'page_title' => trim($this->input->post('page_title')),
            'meta_title' => trim($this->input->post('meta_title')),
            'meta_tags' => trim($this->input->post('meta_tags')),
            'meta_description' => trim($this->input->post('meta_description'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('past_exam_solutions', $data);
    }
}