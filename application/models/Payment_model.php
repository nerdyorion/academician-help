<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function add($order_id, $successful, $error_code, $error_message, $payload)
    {
        $order_id = (int) $order_id;

        $data = array(
            'order_id' => $order_id,
            'successful' => (int) $successful,
            'error_code' => trim($error_code),
            'error_message' => trim($error_message),
            'payload' => trim($payload)
        );

        $this->db->insert('payments', $data);
        
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function deleteByOrderID($order_id)
    {
        $this->db->delete('payments', array('order_id' => (int) $order_id));
    }

    public function updatePayPalToken($token, $expires_in)
    {
        $data = array(
            'token' => trim($token),
            'expires_in' => (int) trim($expires_in),
            'times_updated' => 'times_updated + 1'
        );
        $this->db->where('id', 1);
        $this->db->update('stash', $data);
    }
}