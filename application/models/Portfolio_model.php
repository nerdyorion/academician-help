<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Portfolio_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count($user_id = FALSE) {
        if($user_id !== FALSE)
        {
            $this->db->where('user_id', (int) $user_id);
            $this->db->from('portfolio');
            return $this->db->count_all_results();
        }
        return $this->db->count_all("portfolio");
    }

    public function get_all_slug()
    {
        $sql = "SELECT id, slug FROM portfolio ORDER BY slug ASC";
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        if ($id === FALSE)
        {   
            $limit = (int) $limit;
            $offset = (int) $offset;

            $sql = "SELECT portfolio.id, portfolio.user_id, portfolio.name, portfolio.description, portfolio.image_url, portfolio.type, portfolio.source, portfolio.media_id, users.full_name AS full_name FROM portfolio LEFT JOIN users ON portfolio.user_id = users.id ORDER BY portfolio.date_created DESC LIMIT $offset, $limit";

            $query = $this->db->query($sql); // var_dump($query); die();

            return $query->result_array();
        }
        $this->db->select('portfolio.id, portfolio.user_id, portfolio.name, portfolio.description, portfolio.image_url, portfolio.type, portfolio.source, portfolio.media_id, users.stage_name, users.full_name AS full_name');
        $this->db->from('portfolio'); 
        $this->db->join('users', 'portfolio.user_id = users.id', 'left');
        $this->db->where('portfolio.id', (int) $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsByUserID($limit, $offset, $user_id)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $user_id = (int) $user_id;

        $sql = "SELECT portfolio.id, portfolio.user_id, portfolio.name, portfolio.description, portfolio.image_url, portfolio.type, portfolio.source, portfolio.media_id, users.full_name AS full_name, users.stage_name FROM portfolio LEFT JOIN users ON portfolio.user_id = users.id WHERE portfolio.user_id = '$user_id' ORDER BY portfolio.date_created DESC LIMIT $offset, $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getAllRowsByUserID($user_id)
    {
        $user_id = (int) $user_id;

        $sql = "SELECT portfolio.id, portfolio.user_id, portfolio.name, portfolio.description, portfolio.image_url, portfolio.type, portfolio.source, portfolio.media_id, users.full_name AS full_name, users.stage_name FROM portfolio LEFT JOIN users ON portfolio.user_id = users.id WHERE portfolio.user_id = '$user_id' ORDER BY portfolio.date_created DESC ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRowsDropDown()
    {
        $this->db->select('id, name');
        $this->db->from('portfolio');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAllRows()
    {
        $sql = "SELECT portfolio.id, portfolio.user_id, portfolio.name, portfolio.description, portfolio.image_url, portfolio.type, portfolio.source, portfolio.media_id, users.full_name AS full_name, FROM portfolio LEFT JOIN users ON portfolio.user_id = users.id ORDER BY portfolio.date_created DESC ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRowsFrontEnd($limit, $offset, $category_id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if ($category_id === FALSE)
        {
            $sql = "SELECT portfolio.id, portfolio.slug, portfolio.image_url, portfolio.image_url_thumb, portfolio.name, portfolio.description, categories.name AS category_name, portfolio.price, portfolio.duration, portfolio.mtn_service_code, portfolio.mtn_service_keyword, portfolio.airtel_service_code, portfolio.airtel_service_keyword, portfolio.glo_service_code, portfolio.glo_service_keyword, portfolio.emts_service_code, portfolio.emts_service_keyword, portfolio.ntel_service_code, portfolio.ntel_service_keyword, (SELECT COUNT(id) FROM subscriptions WHERE subscriptions.product_id = portfolio.id AND subscriptions.successful = 1) AS count_subscriptions, portfolio.active  FROM portfolio LEFT JOIN categories ON portfolio.category_id = categories.id WHERE portfolio.active = 1 ORDER BY RAND() LIMIT $offset, $limit";

            $query = $this->db->query($sql); // var_dump($query); die();

            return $query->result_array();
        }

        $category_id = (int) $category_id;

        $sql = "SELECT portfolio.id, portfolio.slug, portfolio.image_url, portfolio.image_url_thumb, portfolio.name, portfolio.description, categories.name AS category_name, portfolio.price, portfolio.duration, portfolio.mtn_service_code, portfolio.mtn_service_keyword, portfolio.airtel_service_code, portfolio.airtel_service_keyword, portfolio.glo_service_code, portfolio.glo_service_keyword, portfolio.emts_service_code, portfolio.emts_service_keyword, portfolio.ntel_service_code, portfolio.ntel_service_keyword, (SELECT COUNT(id) FROM subscriptions WHERE subscriptions.product_id = portfolio.id AND subscriptions.successful = 1) AS count_subscriptions, portfolio.active  FROM portfolio LEFT JOIN categories ON portfolio.category_id = categories.id WHERE portfolio.category_id = '$category_id' AND portfolio.active = 1 ORDER BY RAND() LIMIT $offset, $limit";
        $query = $this->db->query($sql); // var_dump($query); die();

        return $query->result_array();
    }

    public function getRowsFrontEndSingleCategory($limit, $offset, $category_id)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $category_id = (int) $category_id;

        $sql = "SELECT portfolio.id, portfolio.slug, portfolio.image_url, portfolio.image_url_thumb, portfolio.name, portfolio.description, categories.name AS category_name, portfolio.price, portfolio.duration, portfolio.mtn_service_code, portfolio.mtn_service_keyword, portfolio.airtel_service_code, portfolio.airtel_service_keyword, portfolio.glo_service_code, portfolio.glo_service_keyword, portfolio.emts_service_code, portfolio.emts_service_keyword, portfolio.ntel_service_code, portfolio.ntel_service_keyword, (SELECT COUNT(id) FROM subscriptions WHERE subscriptions.product_id = portfolio.id AND subscriptions.successful = 1) AS count_subscriptions, portfolio.active  FROM portfolio LEFT JOIN categories ON portfolio.category_id = categories.id WHERE portfolio.category_id = '$category_id' AND portfolio.active = 1 ORDER BY portfolio.name LIMIT $offset, $limit";
        $query = $this->db->query($sql); // var_dump($query); die();

        return $query->result_array();
    }

    public function add()
    {
        $user_id = (int) $this->session->userdata('user_id');

        $data = array(
            'user_id' => $user_id,
            // 'type' => trim($this->input->post('type')),
            'type' => 'image',
            'name' => trim($this->input->post('name')),
            'description' => trim($this->input->post('description')),
            'image_url' => $this->input->post('image_url')
        );

        $this->db->insert('portfolio', $data);
    }

    public function addAudio()
    {
        $user_id = (int) $this->session->userdata('user_id');

        $data = array(
            'user_id' => $user_id,
            'type' => 'audio',
            'name' => trim($this->input->post('name')),
            'source' => trim($this->input->post('source')),
            'media_id' => trim($this->input->post('media_id')),
            'description' => trim($this->input->post('description')),
        );

        $this->db->insert('portfolio', $data);
    }

    public function addVideo()
    {
        $user_id = (int) $this->session->userdata('user_id');

        $data = array(
            'user_id' => $user_id,
            'type' => 'video',
            'name' => trim($this->input->post('name')),
            'source' => trim($this->input->post('source')),
            'media_id' => trim($this->input->post('media_id')),
            'description' => trim($this->input->post('description')),
        );

        $this->db->insert('portfolio', $data);
    }

    public function delete($id)
    {
        $this->db->select('portfolio.image_url');
        $this->db->from('portfolio');
        $this->db->where('portfolio.id', (int) $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();

        $this->db->delete('portfolio', array('id' => (int) $id));

        file_exists($row['image_url']) ? unlink($row['image_url']) : '' ;
        file_exists(image_thumb($row['image_url'])) ? unlink(image_thumb($row['image_url'])) : '' ;
    }

    public function update($id)
    {
        $data = array(
            'name' => trim($this->input->post('name')),
            'description' => trim($this->input->post('description')),
            'image_url' => $this->input->post('image_url')
        );
        $this->db->where('id', (int) $id);
        $this->db->update('portfolio', $data);
    }

    public function updateWithoutImage($id)
    {
        $data = array(
            'name' => trim($this->input->post('name')),
            'description' => trim($this->input->post('description'))
        );
        if(!is_null($this->input->post('source')))
        {
            $tmp = array('source' => trim($this->input->post('source')));
            $data = $data + $tmp; // or  // array_merge($data, $tmp);
        }
        if(!is_null($this->input->post('media_id')))
        {
            $tmp = array('media_id' => trim($this->input->post('media_id')));
            $data = $data + $tmp; // or  // array_merge($data, $tmp);
        }
        $this->db->where('id', (int) $id);
        $this->db->update('portfolio', $data);
    }
}