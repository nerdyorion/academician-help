<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Product_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count($category_id = FALSE) {
        if($category_id !== FALSE)
        {
            $this->db->where('category_id', (int) $category_id);
            $this->db->from('products');
            return $this->db->count_all_results();
        }
        return $this->db->count_all("products");
    }

    public function get_all_slug()
    {
        $sql = "SELECT id, slug FROM products ORDER BY slug ASC";
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        if ($id === FALSE)
        {   
            $limit = (int) $limit;
            $offset = (int) $offset;

            // var_dump($limit); var_dump($start); die;
            // $this->db->limit($limit, $start);
            $sql = "SELECT products.id, products.slug, products.image_url, products.image_url_thumb, products.name, categories.name AS category_name, products.price, products.duration, (SELECT COUNT(id) FROM subscriptions WHERE subscriptions.product_id = products.id AND subscriptions.successful = 1) AS count_subscriptions, products.active  FROM products LEFT JOIN categories ON products.category_id = categories.id ORDER BY products.name ASC LIMIT $offset, $limit";

            $query = $this->db->query($sql); // var_dump($query); die();
            
            // var_dump($query->result_array()); die();

            return $query->result_array();
        }
        $this->db->select('products.id, products.category_id, products.slug, products.name, products.slug, products.image_url, products.image_url_thumb, products.description, products.price, products.duration, products.mtn_service_code, products.mtn_service_keyword, products.airtel_service_code, products.airtel_service_keyword, products.glo_service_code, products.glo_service_keyword, products.emts_service_code, products.emts_service_keyword, products.ntel_service_code, products.ntel_service_keyword, products.active, categories.name AS category_name');
        $this->db->from('products'); 
        $this->db->join('categories', 'products.category_id = categories.id');
        $this->db->where('products.id', (int) $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown()
    {
        $this->db->select('id, name, active');
        $this->db->from('products');
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAllRows()
    {
        $sql = "SELECT products.id, products.slug, products.image_url, products.image_url_thumb, products.name, categories.name AS category_name, products.price, products.duration, (SELECT COUNT(id) FROM subscriptions WHERE subscriptions.product_id = products.id AND subscriptions.successful = 1) AS count_subscriptions, products.active  FROM products LEFT JOIN categories ON products.category_id = categories.id ORDER BY products.name ASC";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRowsFrontEnd($limit, $offset, $category_id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if ($category_id === FALSE)
        {
            $sql = "SELECT products.id, products.slug, products.image_url, products.image_url_thumb, products.name, products.description, categories.name AS category_name, products.price, products.duration, products.mtn_service_code, products.mtn_service_keyword, products.airtel_service_code, products.airtel_service_keyword, products.glo_service_code, products.glo_service_keyword, products.emts_service_code, products.emts_service_keyword, products.ntel_service_code, products.ntel_service_keyword, (SELECT COUNT(id) FROM subscriptions WHERE subscriptions.product_id = products.id AND subscriptions.successful = 1) AS count_subscriptions, products.active  FROM products LEFT JOIN categories ON products.category_id = categories.id WHERE products.active = 1 ORDER BY RAND() LIMIT $offset, $limit";

            $query = $this->db->query($sql); // var_dump($query); die();

            return $query->result_array();
        }

        $category_id = (int) $category_id;

        $sql = "SELECT products.id, products.slug, products.image_url, products.image_url_thumb, products.name, products.description, categories.name AS category_name, products.price, products.duration, products.mtn_service_code, products.mtn_service_keyword, products.airtel_service_code, products.airtel_service_keyword, products.glo_service_code, products.glo_service_keyword, products.emts_service_code, products.emts_service_keyword, products.ntel_service_code, products.ntel_service_keyword, (SELECT COUNT(id) FROM subscriptions WHERE subscriptions.product_id = products.id AND subscriptions.successful = 1) AS count_subscriptions, products.active  FROM products LEFT JOIN categories ON products.category_id = categories.id WHERE products.category_id = '$category_id' AND products.active = 1 ORDER BY RAND() LIMIT $offset, $limit";
        $query = $this->db->query($sql); // var_dump($query); die();

        return $query->result_array();
    }

    public function getRowsFrontEndSingleCategory($limit, $offset, $category_id)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $category_id = (int) $category_id;

        $sql = "SELECT products.id, products.slug, products.image_url, products.image_url_thumb, products.name, products.description, categories.name AS category_name, products.price, products.duration, products.mtn_service_code, products.mtn_service_keyword, products.airtel_service_code, products.airtel_service_keyword, products.glo_service_code, products.glo_service_keyword, products.emts_service_code, products.emts_service_keyword, products.ntel_service_code, products.ntel_service_keyword, (SELECT COUNT(id) FROM subscriptions WHERE subscriptions.product_id = products.id AND subscriptions.successful = 1) AS count_subscriptions, products.active  FROM products LEFT JOIN categories ON products.category_id = categories.id WHERE products.category_id = '$category_id' AND products.active = 1 ORDER BY products.name LIMIT $offset, $limit";
        $query = $this->db->query($sql); // var_dump($query); die();

        return $query->result_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'category_id' => (int) $this->input->post('category_id'),
            'name' => trim($this->input->post('name')),
            'slug' => trim($this->input->post('slug')),
            'image_url' => $this->input->post('image_url'),
            'image_url_thumb' => $this->input->post('image_url_thumb'),
            'price' => (int) $this->input->post('price'),
            'duration' => trim($this->input->post('duration')),
            'description' => trim($this->input->post('description')),
            'mtn_service_code' => trim($this->input->post('mtn_service_code')),
            'mtn_service_keyword' => trim($this->input->post('mtn_service_keyword')),
            'airtel_service_code' => trim($this->input->post('airtel_service_code')),
            'airtel_service_keyword' => trim($this->input->post('airtel_service_keyword')),
            'glo_service_code' => trim($this->input->post('glo_service_code')),
            'glo_service_keyword' => trim($this->input->post('glo_service_keyword')),
            'emts_service_code' => trim($this->input->post('emts_service_code')),
            'emts_service_keyword' => trim($this->input->post('emts_service_keyword')),
            'ntel_service_code' => trim($this->input->post('ntel_service_code')),
            'ntel_service_keyword' => trim($this->input->post('ntel_service_keyword')),
            'created_by' => $created_by
        );

        $this->db->insert('products', $data);
    }

    public function delete($id)
    {
        $this->db->delete('products', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'category_id' => (int) $this->input->post('category_id'),
            'name' => trim($this->input->post('name')),
            'image_url' => $this->input->post('image_url'),
            'image_url_thumb' => $this->input->post('image_url_thumb'),
            'price' => (int) $this->input->post('price'),
            'duration' => trim($this->input->post('duration')),
            'description' => trim($this->input->post('description')),
            'mtn_service_code' => trim($this->input->post('mtn_service_code')),
            'mtn_service_keyword' => trim($this->input->post('mtn_service_keyword')),
            'airtel_service_code' => trim($this->input->post('airtel_service_code')),
            'airtel_service_keyword' => trim($this->input->post('airtel_service_keyword')),
            'glo_service_code' => trim($this->input->post('glo_service_code')),
            'glo_service_keyword' => trim($this->input->post('glo_service_keyword')),
            'emts_service_code' => trim($this->input->post('emts_service_code')),
            'emts_service_keyword' => trim($this->input->post('emts_service_keyword')),
            'ntel_service_code' => trim($this->input->post('ntel_service_code')),
            'ntel_service_keyword' => trim($this->input->post('ntel_service_keyword'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('products', $data);
    }

    public function updateWithoutImage($id)
    {

        $data = array(
            'category_id' => (int) $this->input->post('category_id'),
            'name' => trim($this->input->post('name')),
            'price' => (int) $this->input->post('price'),
            'duration' => trim($this->input->post('duration')),
            'description' => trim($this->input->post('description')),
            'mtn_service_code' => trim($this->input->post('mtn_service_code')),
            'mtn_service_keyword' => trim($this->input->post('mtn_service_keyword')),
            'airtel_service_code' => trim($this->input->post('airtel_service_code')),
            'airtel_service_keyword' => trim($this->input->post('airtel_service_keyword')),
            'glo_service_code' => trim($this->input->post('glo_service_code')),
            'glo_service_keyword' => trim($this->input->post('glo_service_keyword')),
            'emts_service_code' => trim($this->input->post('emts_service_code')),
            'emts_service_keyword' => trim($this->input->post('emts_service_keyword')),
            'ntel_service_code' => trim($this->input->post('ntel_service_code')),
            'ntel_service_keyword' => trim($this->input->post('ntel_service_keyword'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('products', $data);
    }

    public function enable($id)
    {
        $data = array(
            'active' => 1
        );
        $this->db->where('id', (int) $id);
        $this->db->update('products', $data);
    }

    public function disable($id)
    {
        $data = array(
            'active' => 0
        );
        $this->db->where('id', (int) $id);
        $this->db->update('products', $data);
    }

    public function slugExists($slug, $id = FALSE)
    {
        if ($id === FALSE)
        {
            $this->db->select('id, slug');
            $this->db->from('products');
            $this->db->like('slug', $slug, 'after');
            $query = $this->db->order_by('id', 'DESC');
            $query = $this->db->limit(1);
            $query = $this->db->get();

            $row = $query->row_array();

            if(empty($row))
            {
                return false;
            }
            else
            {
                return $row['slug'];
            }
        }

        $this->db->select('id, slug');
        $this->db->from('products');
        $this->db->where('id !=', $id); 
        $this->db->like('slug', $slug, 'after');
        $query = $this->db->order_by('id', 'DESC');
        $query = $this->db->limit(1);
        $query = $this->db->get();

        $row = $query->row_array();

        if(empty($row))
        {
            return false;
        }
        else
        {
            return $row['slug'];
        }
    }
}