<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Role_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count() {
        return $this->db->count_all("roles");
    }

    public function getRows($id = FALSE)
    {
        if ($id === FALSE)
        {
            // $this->db->order_by('name', 'ASC');
            $this->db->order_by('id', 'ASC');
            $this->db->select('id, name');
            $this->db->from('roles'); 
            $query = $this->db->get();
            
            return $query->result_array();
        }
        $this->db->select('name');
        $this->db->from('roles');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function add()
    {
        $data = array(
            'name' => trim($this->input->post('name'))
        );

        $this->db->insert('roles', $data);
    }

    public function delete($id)
    {
        $this->db->delete('roles', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'name' => trim($this->input->post('name'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('roles', $data);
    }
}