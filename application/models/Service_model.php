<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Service_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count($user_id = FALSE) {
        if($user_id !== FALSE)
        {
            $this->db->where('user_id', (int) $user_id);
            $this->db->from('services');
            return $this->db->count_all_results();
        }
        return $this->db->count_all("services");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        if ($id === FALSE)
        {   
            $limit = (int) $limit;
            $offset = (int) $offset;

            $sql = "SELECT services.id, services.user_id, services.name, services.description, services.available_after_hours, services.available_on_weekends, users.full_name AS full_name FROM services LEFT JOIN users ON services.user_id = users.id ORDER BY services.name ASC LIMIT $offset, $limit";

            $query = $this->db->query($sql);

            return $query->result_array();
        }
        $this->db->select('services.id, services.user_id, services.name, services.description, services.available_after_hours, services.available_on_weekends, users.stage_name, users.full_name AS full_name');
        $this->db->from('services'); 
        $this->db->join('users', 'services.user_id = users.id', 'left');
        $this->db->where('services.id', (int) $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsByUserID($limit, $offset, $user_id)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $user_id = (int) $user_id;

        $sql = "SELECT services.id, services.user_id, services.name, services.description, services.available_after_hours, services.available_on_weekends, users.full_name AS full_name, users.stage_name FROM services LEFT JOIN users ON services.user_id = users.id WHERE services.user_id = '$user_id' ORDER BY services.name ASC LIMIT $offset, $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getAllRowsByUserID($user_id)
    {
        $user_id = (int) $user_id;

        $sql = "SELECT services.id, services.user_id, services.name, services.description, services.available_after_hours, services.available_on_weekends, users.full_name AS full_name, users.stage_name FROM services LEFT JOIN users ON services.user_id = users.id WHERE services.user_id = '$user_id' ORDER BY services.name ASC";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRowsDropDown()
    {
        $this->db->select('id, name');
        $this->db->from('services');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAllRows()
    {
        $sql = "SELECT services.id, services.user_id, services.name, services.description, services.available_after_hours, services.available_on_weekends, users.full_name AS full_name, FROM services LEFT JOIN users ON services.user_id = users.id ORDER BY services.name ASC";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function add()
    {
        $user_id = (int) $this->session->userdata('user_id');

        $data = array(
            'user_id' => $user_id,
            'name' => ucfirst(trim($this->input->post('name'))),
            'description' => trim($this->input->post('description'))
            // 'available_after_hours' => trim($this->input->post('available_after_hours')),
            // 'available_on_weekends' => trim($this->input->post('available_on_weekends'))
        );

        $this->db->insert('services', $data);
    }

    public function delete($id)
    {
        $this->db->delete('services', array('id' => (int) $id));
    }

    public function update($id)
    {
        $data = array(
            'name' => ucfirst(trim($this->input->post('name'))),
            'description' => trim($this->input->post('description'))
            // 'available_after_hours' => trim($this->input->post('available_after_hours')),
            // 'available_on_weekends' => trim($this->input->post('available_on_weekends'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('services', $data);
    }
}