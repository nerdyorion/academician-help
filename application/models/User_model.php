<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
        $this->db->reset_query();
    }

    public function filter_record_count($type = "all", $id = FALSE, $title = FALSE)
    {
        $this->db->reset_query();

        $sql = "SELECT COUNT(users.id) AS count FROM users LEFT JOIN roles ON users.role_id = roles.id LEFT JOIN countries ON users.country_id = countries.id WHERE ";

        $where = '';

        if($type != "all")
        {
            if($type == "student")
            {
                $where .= " AND (users.role_id = '3')";
            }
            if($type == "admin")
            {
                $where .= " AND (users.role_id = '1' OR users.role_id = '2')";
            }
        }

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (users.id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(users.full_name LIKE '". $title_full . "') OR (users.email LIKE '". $title_full . "') OR (users.gender LIKE '". $title_full . "') OR (countries.name LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (users.full_name LIKE '". $title_word . "') OR (users.email LIKE '". $title_word . "') OR (users.gender LIKE '". $title_word . "') OR (countries.name LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $type = "all", $id = FALSE, $title = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT users.id, users.role_id, users.first_name, users.last_name, users.full_name, users.email, users.gender, users.country_id, users.category_id, users.phone, users.city, users.address, users.landline, users.company, users.position, users.website, users.bio, users.featured, users.last_login, users.is_suspended, roles.name AS role_name, countries.name AS country_name FROM users LEFT JOIN roles ON users.role_id = roles.id LEFT JOIN countries ON users.country_id = countries.id WHERE ";

        $where = '';

        if($type != "all")
        {
            if($type == "student")
            {
                $where .= " AND (users.role_id = '3')";
            }
            if($type == "admin")
            {
                $where .= " AND (users.role_id = '1' OR users.role_id = '2')";
            }
        }

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (users.id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(users.full_name LIKE '". $title_full . "') OR (users.email LIKE '". $title_full . "') OR (users.gender LIKE '". $title_full . "') OR (countries.name LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (users.full_name LIKE '". $title_word . "') OR (users.email LIKE '". $title_word . "') OR (users.gender LIKE '". $title_word . "') OR (countries.name LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where . " ORDER BY users.full_name ASC LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count($type = "all")
    {
        if($type != "all")
        {
            if($type == "student")
            {
                $this->db->where('role_id', 3);
            }
            if($type == "admin")
            {
                $this->db->where('role_id', 1);
                $this->db->or_where('users.role_id', 2);
            }
            $this->db->from('users');
            return $this->db->count_all_results();
        }
        return $this->db->count_all("users");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if ($id === FALSE)
        {
            //$query = $this->db->get('dnd_users');
            //return $query->result_array();
            $query = $this->db->order_by('users.full_name', 'ASC');
            $this->db->select('users.id, users.role_id, users.first_name, users.last_name, users.full_name, users.email, users.gender, users.country_id, users.category_id, users.phone, users.city, users.address, users.landline, users.company, users.position, users.website, users.bio, users.featured, users.last_login, users.is_suspended, roles.name AS role_name, countries.name AS country_name');
            $this->db->from('users'); 
            $this->db->join('roles', 'users.role_id = roles.id', 'left');
            $this->db->join('countries', 'users.country_id = countries.id', 'left');
            $this->db->where('users.role_id', 1);
            $this->db->or_where('users.role_id', 2);
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get(); //echo $this->db->last_query(); die;
            return $query->result_array();
        }
        $this->db->select('users.id, users.role_id, users.first_name, users.last_name, users.full_name, users.email, users.gender, users.country_id, users.category_id, users.phone, users.city, users.address, users.landline, users.company, users.position, users.website, users.bio, users.featured, users.last_login, users.is_suspended, roles.name AS role_name, countries.name AS country_name');
        $this->db->from('users'); 
        $this->db->join('roles', 'users.role_id = roles.id', 'left');
            $this->db->join('countries', 'users.country_id = countries.id', 'left');
        $this->db->where('users.id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();    //echo $this->db->last_query(); die;
        return $query->row_array();
    }

    public function getRowsStudent($limit, $offset, $id = FALSE)
    {

        $query = $this->db->order_by('users.full_name', 'ASC');
        $this->db->select('users.id, users.role_id, users.first_name, users.last_name, users.full_name, users.email, users.gender, users.country_id, users.category_id, users.phone, users.city, users.address, users.landline, users.company, users.position, users.website, users.bio, users.featured, users.last_login, users.is_suspended, roles.name AS role_name, countries.name AS country_name');
        $this->db->from('users'); 
        $this->db->join('roles', 'users.role_id = roles.id', 'left');
        $this->db->join('countries', 'users.country_id = countries.id', 'left');
        $this->db->where('users.role_id', 3);
        $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

        $query = $this->db->get();    // echo $this->db->last_query(); die;
        return $query->result_array();
    }

    public function getFeaturedArtist()
    {
        // $query = $this->db->order_by('full_name', 'ASC');
        $query = $this->db->order_by('id', 'RANDOM');
        $this->db->select('users.id, users.first_name, users.last_name, users.full_name, users.email, users.gender, users.country_id, users.category_id, users.phone, users.city, users.address, users.landline, users.company, users.position, users.website, users.bio, users.featured, users.last_login, users.is_suspended, roles.name AS role_name, countries.name AS country_name, categories_talent.category_name AS artist_category, categories_social_influencer.category_name AS artist_category_social_influencer, categories_agent.category_name AS agent_category');
        $this->db->from('users'); 
        $this->db->join('roles', 'users.role_id = roles.id', 'left');
        $this->db->join('countries', 'users.country_id = countries.id', 'left');
        $this->db->join('categories_talent', 'users.category_id = categories_talent.id', 'left');
        $this->db->join('categories_agent', 'users.category_id = categories_agent.id', 'left');
        $this->db->join('categories_social_influencer', 'users.category_social_influencer_id = categories_social_influencer.id', 'left');
        $this->db->where('users.role_id', 3); 
        $this->db->where('users.featured', 1); 
        // $query = $this->db->limit(20);
        $query = $this->db->get();

        return $query->result_array();
    }

    public function getRowByUsername($username)
    {
        $username = filter_var(strtolower(trim($username)), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        $this->db->select('users.id, users.first_name, users.last_name, users.full_name, users.email, users.gender, users.country_id, users.category_id, users.phone, users.city, users.address, users.landline, users.company, users.position, users.website, users.bio, users.featured, users.last_login, users.is_suspended, roles.name AS role_name, countries.name AS country_name, categories_talent.category_name AS artist_category, categories_social_influencer.category_name AS artist_category_social_influencer, categories_agent.category_name AS agent_category');
        $this->db->from('users'); 
        $this->db->join('roles', 'users.role_id = roles.id', 'left');
        $this->db->join('countries', 'users.country_id = countries.id', 'left');
        $this->db->join('categories_talent', 'users.category_id = categories_talent.id', 'left');
        $this->db->join('categories_agent', 'users.category_id = categories_agent.id', 'left');
        $this->db->join('categories_social_influencer', 'users.category_social_influencer_id = categories_social_influencer.id', 'left');
        $this->db->where('users.username', $username); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowByEmail($email)
    {
        $email = filter_var(strtolower(trim($email)), FILTER_SANITIZE_EMAIL);

        $this->db->select('users.id, users.role_id, users.first_name, users.last_name, users.full_name, users.email, users.gender, users.country_id, users.category_id, users.phone, users.city, users.address, users.landline, users.company, users.position, users.website, users.bio, users.featured, users.last_login, users.is_suspended, roles.name AS role_name, countries.name AS country_name');
        $this->db->from('users'); 
        $this->db->join('roles', 'users.role_id = roles.id', 'left');
            $this->db->join('countries', 'users.country_id = countries.id', 'left');
        $this->db->where('users.email', $email); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function insertToken($user_id)
    {   
        // delete other tokens of user
        $this->db->delete('tokens', array('user_id' => (int) $user_id));

        $token = substr(sha1(rand()), 0, 30); 
        $date_created = date('Y-m-d H:i:s');
        
        $data = array(
                'token' => $token,
                'user_id' => (int) $user_id,
                'date_created' => $date_created
            );
        $this->db->insert('tokens', $data);
        return $token . $user_id;
        
    }
    
    public function isTokenValid($token)
    {
       $tkn = substr($token,0,30);
       $uid = substr($token,30);  

       // echo "tkn: $tkn <br/>uid: $uid"; die;  
       
        $q = $this->db->get_where('tokens', array(
            'tokens.token' => $tkn, 
            'tokens.user_id' => $uid), 1);                         
               
        if($this->db->affected_rows() > 0){
            $row = $q->row();             
            
            $date_created = $row->date_created;
            $createdTS = strtotime($date_created);
            // $today = date('Y-m-d'); 
            $today = date('Y-m-d H:i:s'); 
            $todayTS = strtotime($today);

            // echo "createdTS: $createdTS <br />todayTS: $todayTS"; die;
            
            // if($createdTS != $todayTS){ 
            //     return false;
            // }
            
            if(date('Y-m-d', $todayTS) != date('Y-m-d', $createdTS)){ // 24 hrs
                return false;
            }
            
            $user_info = $this->getRows(0, 0, $row->user_id); // print_r($user_info); die;
            return $user_info;
            
        }else{
            return false;
        }
        
    } 

    public function getUserFullProfile($username)
    {
        $this->db->select('users.id, users.first_name, users.last_name, users.full_name, users.email, users.gender, users.country_id, users.category_id, users.phone, users.city, users.address, users.landline, users.company, users.position, users.website, users.bio, users.featured, users.last_login, users.is_suspended, roles.name AS role_name, countries.name AS country_name, categories_talent.category_name AS artist_category, categories_social_influencer.category_name AS artist_category_social_influencer, categories_agent.category_name AS agent_category');
        $this->db->from('users'); 
        $this->db->join('roles', 'users.role_id = roles.id', 'left');
        $this->db->join('countries', 'users.country_id = countries.id', 'left');
        $this->db->join('categories_talent', 'users.category_id = categories_talent.id', 'left');
        $this->db->join('categories_agent', 'users.category_id = categories_agent.id', 'left');
        $this->db->join('categories_social_influencer', 'users.category_social_influencer_id = categories_social_influencer.id', 'left');
        $this->db->where('users.username', $username); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function get_all_slug()
    {
        $sql = "SELECT id, username FROM products ORDER BY slug ASC";
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function login()
    {
        $email = strtolower(trim($this->input->post('email')));
        $raw_password = strtolower(trim($this->input->post('password')));
        $this->db->select('users.id, users.full_name, users.email, users.password, users.last_login, users.is_suspended, roles.id AS role_id, roles.name AS role_name');
        $this->db->from('users'); 
        $this->db->join('roles', 'users.role_id = roles.id', 'left');
        $this->db->where('users.email', $email); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if (password_verify($raw_password, $row['password'])) {

            if((int) $row['is_suspended'] == 1)
            {
                // don't set login session, just return row
                return $row;
            }

            $this->session->set_userdata('user_id', (int) $row['id']);
            $this->session->set_userdata('user_full_name', $row['full_name']);
            $this->session->set_userdata('user_email', $row['email']);
            $this->session->set_userdata('user_role_id', (int) $row['role_id']);
            $this->session->set_userdata('user_role_name', $row['role_name']);
            $this->session->set_userdata('user_last_login', $row['last_login']);

            return $row;
        }
        else
        {
            return false;
        }
    }

    public function loginAs($id, $admin = FALSE)
    {
        $id = (int) $id;
        $this->db->select('users.id, users.full_name, users.email, users.password, users.last_login, users.is_suspended, roles.id AS role_id, roles.name AS role_name');
        $this->db->from('users'); 
        $this->db->join('roles', 'users.role_id = roles.id', 'left');
        $this->db->where('users.id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if (!empty($row)) {

            // set current logged in user as former logged in user
            $this->session->set_userdata('user_id_former_logged_in', (int) $this->session->userdata('user_id'));
            $this->session->set_userdata('user_full_name_former_logged_in', $this->session->userdata('user_full_name'));
            $this->session->set_userdata('user_email_former_logged_in', $this->session->userdata('user_email'));
            $this->session->set_userdata('user_role_id_former_logged_in', (int) $this->session->userdata('user_role_id'));
            $this->session->set_userdata('user_role_name_former_logged_in', $this->session->userdata('user_role_name'));
            $this->session->set_userdata('user_last_login_former_logged_in', $this->session->userdata('user_last_login'));

            if($admin !== FALSE)
            {
                // if admin is logging in back, remove the former agent details or talent details then
                $this->session->unset_userdata('user_id_former_logged_in');
                $this->session->unset_userdata('user_full_name_former_logged_in');
                $this->session->unset_userdata('user_email_former_logged_in');
                $this->session->unset_userdata('user_role_id_former_logged_in');
                $this->session->unset_userdata('user_role_name_former_logged_in');
                $this->session->unset_userdata('user_last_login_former_logged_in');
            }

            // remove current logged in user
            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('user_full_name');
            $this->session->unset_userdata('user_email');
            $this->session->unset_userdata('user_role_id');
            $this->session->unset_userdata('user_role_name');
            $this->session->unset_userdata('user_last_login');

            // add new logged in user to session
            $this->session->set_userdata('user_id', (int) $row['id']);
            $this->session->set_userdata('user_full_name', $row['full_name']);
            $this->session->set_userdata('user_email', $row['email']);
            $this->session->set_userdata('user_role_id', (int) $row['role_id']);
            $this->session->set_userdata('user_role_name', $row['role_name']);
            $this->session->set_userdata('user_last_login', $row['last_login']);

            return $row;
        }
        else
        {
            return false;
        }
    }

    public function usernameExists($username)
    {
        $username = filter_var(strtolower(trim($username)), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('username', $username); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if(empty($row))
        {
            // username does not exist
            return false;
        }
        else
        {
            // username exist
            return true;
        }
    }

    public function emailExists($email)
    {
        $email = filter_var(strtolower(trim($email)), FILTER_SANITIZE_EMAIL);

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('email', $email); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if(empty($row))
        {
            // email does not exist
            return false;
        }
        else
        {
            // email exist
            return true;
        }
    }

    public function isSuspended($id)
    {
        $id = (int) $id;

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('id', $id); 
        $this->db->where('is_suspended', 1); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if(empty($row))
        {
            // row does not exist
            return false;
        }
        else
        {
            // row exist
            return true;
        }
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');
        $raw_password = $this->input->post('password');
        $password = password_hash($raw_password, PASSWORD_BCRYPT);
        // Insert $hashAndSalt into database against user
        /*
        // and then to verify $password:
        if (password_verify($raw_password, $hashed_password)) {
            // Verified
        }
        */
        $data = array(
            'role_id' => (int) $this->input->post('role_id'),
            'full_name' => ucwords(trim($this->input->post('full_name'))),
            'email' => strtolower(trim($this->input->post('email'))),
            'phone' => trim($this->input->post('phone')),
            'password' => $password,
            'gender' => trim($this->input->post('gender')),
            'address' => trim($this->input->post('address')),
            'created_by' => $created_by
        );

        $this->db->insert('users', $data);
    }

    public function addSignup()
    {
        $role_id = (int) $this->input->post('role_id');

        $raw_password = $this->input->post('password');
        $password = password_hash($raw_password, PASSWORD_BCRYPT);

        $data = array(
            'role_id' => $role_id,
            'first_name' => ucfirst(trim($this->input->post('first_name'))),
            'last_name' => ucfirst(trim($this->input->post('last_name'))),
            'full_name' => ucfirst(trim($this->input->post('first_name'))) . ' ' . ucfirst(trim($this->input->post('last_name'))),
            'email' => strtolower(trim($this->input->post('email'))),
            'country_id' => (int) $this->input->post('country_id'),
            'password' => $password
        );

        $this->db->insert('users', $data);

        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function delete($id)
    {
        $this->db->delete('users', array('id' => (int) $id));
    }

    public function changePassword($id)
    {
        $current_password = strtolower($this->input->post('current_password'));
        $new_password = strtolower($this->input->post('new_password'));

        $query = $this->db->get_where('users', array('id' => (int) $id));
        $row = $query->row_array();
        if (password_verify($current_password, $row['password'])) {

            $password = password_hash($new_password, PASSWORD_BCRYPT);
            $data = array(
                'password' => $password
            );

            $this->db->update('users', $data, array('id' => $id));

            return true;
        }
        else
        {
            return false;
        }
    }

    public function updatePassword($id)
    {
        $new_password = strtolower($this->input->post('password'));

        $password = password_hash($new_password, PASSWORD_BCRYPT);
        $data = array(
            'password' => $password
        );

        $this->db->update('users', $data, array('id' => $id));

        return true;
    }

    public function updateProfile($id)
    {
        $data = array(
            'full_name' => ucwords(trim($this->input->post('full_name'))),
            'email' => strtolower(trim($this->input->post('email'))),
            'gender' => trim($this->input->post('gender')),
            'phone' => trim($this->input->post('phone')),
            'address' => trim($this->input->post('address'))
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    public function updateProfileFrontend($id)
    {
        $data = array(
            'first_name' => ucfirst(trim($this->input->post('first_name'))),
            'last_name' => ucfirst(trim($this->input->post('last_name'))),
            'gender' => trim($this->input->post('gender')),
            'phone' => trim($this->input->post('phone')),
            'address' => trim($this->input->post('address')),
            'country_id' => (int) $this->input->post('country_id')
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    public function updateBookingCost($id)
    {
        $data = array(
            'booking_cost' => (int) trim($this->input->post('booking_cost'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('users', $data);
    }

    public function setFeatured($id, $featured)
    {
        $data = array(
            'featured' => (int) $featured
        );
        $this->db->where('id', (int) $id);
        $this->db->update('users', $data);
    }

    public function setSuspend($id, $suspend)
    {
        $data = array(
            'is_suspended' => (int) $suspend
        );
        $this->db->where('id', (int) $id);
        $this->db->update('users', $data);
    }

    public function updateProfileBasicInfo($id)
    {
        // var_dump($this->input->post('category_social_influencer_id')); die;
        $category_social_influencer_id = NULL;
        $category_social_influencer_id = !is_null($this->input->post('category_social_influencer_id')) && (int) $this->input->post('category_id') == 1 ? (int) $this->input->post('category_social_influencer_id') : NULL;

        $data = array(
            'stage_name' => trim($this->input->post('stage_name')),
            'first_name' => ucfirst(trim($this->input->post('first_name'))),
            'last_name' => ucfirst(trim($this->input->post('last_name'))),
            'full_name' => ucfirst(trim($this->input->post('first_name'))) . ' ' . ucfirst(trim($this->input->post('last_name'))),
            'email' => strtolower(trim($this->input->post('email'))),
            'category_id' => (int) $this->input->post('category_id'),
            'category_social_influencer_id' => $category_social_influencer_id,
            'country_id' => (int) $this->input->post('country_id'),
            'gender' => ucfirst(trim($this->input->post('gender'))),

            'phone' => trim($this->input->post('phone')),
            'city' => trim($this->input->post('city')),
            'landline' => trim($this->input->post('landline')),
            'company' => trim($this->input->post('company')),
            'position' => trim($this->input->post('position')),
            'website' => trim($this->input->post('website')),
            'instagram_url' => trim($this->input->post('instagram_url')),
            'twitter_url' => trim($this->input->post('twitter_url')),
            'facebook_url' => trim($this->input->post('facebook_url')),
            'bio' => trim($this->input->post('bio'))
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    public function updateProfileBasicInfoAgent($id)
    {
        $data = array(
            'first_name' => ucfirst(trim($this->input->post('first_name'))),
            'last_name' => ucfirst(trim($this->input->post('last_name'))),
            'full_name' => ucfirst(trim($this->input->post('first_name'))) . ' ' . ucfirst(trim($this->input->post('last_name'))),
            'category_id' => (int) $this->input->post('category_id'),
            'country_id' => (int) $this->input->post('country_id'),
            'gender' => ucfirst(trim($this->input->post('gender'))),

            'phone' => trim($this->input->post('phone')),
            'city' => trim($this->input->post('city')),
            'landline' => trim($this->input->post('landline')),
            'company' => trim($this->input->post('company')),
            'position' => trim($this->input->post('position')),
            'website' => trim($this->input->post('website'))
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    public function updateGenre($id)
    {
        $data = array(
            'genre_gospel' => trim($this->input->post('genre_gospel')),
            'genre_hiphop' => trim($this->input->post('genre_hiphop')),
            'genre_rap' => trim($this->input->post('genre_rap')),
            'genre_afro_pop' => trim($this->input->post('genre_afro_pop')),
            'genre_fuji' => trim($this->input->post('genre_fuji')),
            'genre_r_and_b' => trim($this->input->post('genre_r_and_b')),
            'genre_jazz' => trim($this->input->post('genre_jazz'))
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    public function updateProfileBankInfo($id)
    {
        $data = array(
            'booking_cost' => trim($this->input->post('booking_cost')),
            'bank_name' => ucfirst(trim($this->input->post('bank_name'))),
            'account_type' => ucfirst(trim($this->input->post('account_type'))),
            'branch_code' => trim($this->input->post('branch_code')),
            'account_name' => ucfirst(trim($this->input->post('account_name'))),

            'account_no' => trim($this->input->post('account_no'))
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    public function updateProfileAddService($id)
    {
        $data = array(
            'user_id' => (int) $id,
            'name' => ucfirst(trim($this->input->post('name'))),
            'description' => trim($this->input->post('description'))
            // 'available_after_hours' => trim($this->input->post('available_after_hours')),
            // 'available_on_weekends' => trim($this->input->post('available_on_weekends'))
        );

        $this->db->insert('services', $data);
    }

    public function updateProfilePic($id)
    {
        $image_url = trim($this->input->post('image_url'));
        $image_url_old = trim($this->input->post('image_url_old'));

        $image_url_old_thumb = image_thumb($image_url_old);

        if($image_url_old != 'assets/artist/images/users/default-user.jpg' && $image_url_old != '' && $image_url_old != NULL)
        {
            file_exists($image_url_old) ? unlink($image_url_old) : '' ;
            file_exists($image_url_old_thumb) ? unlink($image_url_old_thumb) : '' ;
        }

        $data = array(
            'image_url' => $image_url
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);

        // update image_url in session
        $this->session->set_userdata('user_image_url', image_thumb($image_url));
    }

    public function updateLastLogin($id)
    {
        //$id = 1;
        $date = date('Y-m-d H:i:s');
        
        $data = array(
            'last_login' => $date
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        //$this->session->set_userdata('user_last_login', $date);
    }

    public function update($id)
    {

        $data = array(
            'role_id' => (int) $this->input->post('role_id'),
            'full_name' => ucwords(trim($this->input->post('full_name'))),
            'email' => strtolower(trim($this->input->post('email'))),
            'phone' => trim($this->input->post('phone')),
            'gender' => trim($this->input->post('gender')),
            'address' => trim($this->input->post('address'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('users', $data);
    }
}