
<!--=================================
page-title-->

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
  <div class="container">
    <div class="row"> 
      <div class="col-lg-12"> 
      <div class="page-title-name">
          <h1>Page Not Found</h1>
          <p>We know the secret of your success</p>
        </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Page Not Found</span> </li>
       </ul>
     </div>
   </div>
  </div>
</section>

<!--=================================
page-title -->


<!--=================================
 error-->

<section class="page-section-ptb">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 text-center">
                     <div class="error-block text-center clearfix">
                      <div class="error-text">
                        <h2>404</h2>
                        <span>Error </span>
                      </div>
                      <h1 class="theme-color mb-40">Ooopps :(</h1>
                      <p>The Page you were looking for, couldn't be found.</p>
                   </div>   
                    <div class="error-info">
                        <p class="mb-50">The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</p>
                        <a class="button xs-mb-10 " href="./">back to home</a>
                        <a class="button border black" href="javascript:void(0);" onclick="history.go(-1); return false;">back to Prev page</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<!--=================================
 error-->


<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->


  <?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html>