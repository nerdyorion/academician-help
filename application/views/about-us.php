
<!--=================================
  page-title-->

  <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
    <div class="container">
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="page-title-name">
            <h1>About Us</h1>
            <p>We know the secret of your success</p>
          </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a href="#">Company</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>About Us</span> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<!--=================================
  page-title -->


<!--=================================
 About-->

 <!--
<section class="page-section-ptb">
 <div class="container">
  <div class="row">
    <div class="col-lg-5 col-md-5">
       <div class="section-title mb-20">
            <h6>Meet us</h6>
            <h2>Who we are</h2>
            <p>We are dedicated to providing you with the best experience possible.</p>
          </div>
        <p> Academician Help is a household name in the tutoring and assignment help service. This service is a subsidiary of <a href="http://brilloconnetz.com" target="_blank">brilloconnetz.com</a> with offices in the United Kingdom, Sri Lanka, and Nigeria. Our service includes <a href="software-engineering">tutoring</a>, <a href="law-dissertation">dissertation help</a>, <a href="management-assignments-essays">essay writing help</a>, <a href="it-computer-coursework">coursework help</a>, <a href="proof-reading-editing">proofreading and editing</a>.</p>
    </div>
    <div class="col-lg-6 col-md-6 col-md-offset-1 sm-mt-40">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 mb-40"> 
          <div class="feature-text left-icon mb-30">
                <div class="feature-icon">
                  <span class="ti-server theme-color" aria-hidden="true"></span>
                </div>
                <div class="feature-info">
                  <h5 class="text-back pt-10">Our Vision</h5>
                </div>
            </div>  
            <p> Our vision is to be a leading force in the academic industry in the next 5 years offering quality tutoring &amp; other form of academic help to students all around the world. </p>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 mb-40">           
            <div class="feature-text left-icon mb-30">
                <div class="feature-icon">
                  <span class="ti-heart theme-color" aria-hidden="true"></span>
                </div>
                <div class="feature-info">
                  <h5 class="text-back pt-10">Our Mission</h5>
                </div>
            </div>      
            <p> To provide quality, affordable and plagiarism free tuition, dissertation help, essay writing help, course work help for IT, Law and Management students from GCSE level to PHD level</p>
          </div>
        </div>
    </div>
  </div>
 </div>
</section>
-->

<section class="service white-bg page-section-ptb">
  <div class="container">
    <div class="row">
     <div class="col-lg-8 col-md-8 col-md-offset-2">
       <div class="section-title text-center">
        <h6>About us</h6>
        <h2 class="title-effect">Who we are and what we do</h2>
        <p>Take Your Success To New Heights With AcademicianHelp.</p>
      </div>
    </div>
  </div>
  <div class="row">
   <div class=" col-lg-offset-2 col-lg-8  col-md-offset-2 col-md-8 col-sm-8">
     <div class="text-center">
      <p> Academician Help is a household name in the tutoring and assignment help service. This service is a subsidiary of <a href="http://brilloconnetz.com" target="_blank">brilloconnetz.com</a> with offices in the United Kingdom, Sri Lanka, and Nigeria. Our service includes <a href="Software-Engineering">tutoring</a>, <a href="Law-Dissertation">dissertation help</a>, <a href="Management-Assignments-Essays">essay writing help</a>, <a href="IT-Computer-Coursework">coursework help</a>, <a href="Proof-Reading-Editing">proofreading and editing</a>.</p>
    </div>
  </div>
</div>
</div>
</section>

<!--=================================
 About-->



<!--=================================
Our services  -->


<section class="theme-bg page-section-ptb">
   <div class="container">
     <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="section-title text-center">
          <h6 class="text-white">We're good at</h6>
          <h2 class="text-white title-effect dark">Our Services</h2>
      </div>


       <div class="row">
         <div class="col-lg-4 col-md-4 col-sm-4 mb-30">
              <h1 class="text-white">01<span class="text-black">.</span></h1>
              <h3 class="text-white">Tutoring</h3>
              <p class="text-white">Teaching IT/Computer Science related modules such as software engineering, programming, cloud computing, database management, web development, artificial intelligence and formal design.</p>
              <!-- <a class=" mt-10 button icon-color" href="#">Read More... <i class="fa fa-angle-right"></i></a> -->
         </div>
         <div class="col-lg-4 col-md-4 col-sm-4 mb-30">
              <h1 class="text-white">02<span class="text-black">.</span></h1>
              <h3 class="text-white">Writing</h3>
              <p class="text-white">Helping with IT/computer science coursework and dissertation, management essay and dissertation as well as law related essays and dissertation.</p>
              <!-- <a class=" mt-10 button icon-color" href="#">Read More... <i class="fa fa-angle-right"></i></a> -->
         </div>
         <div class="col-lg-4 col-md-4 col-sm-4">
              <h1 class="text-white">03<span class="text-black">.</span></h1>
              <h3 class="text-white">Proof Reading &amp; Editing</h3>
              <p class="text-white">Helping with document formatting and editing as well as proof reading of document to rid them of grammatical mistakes and structural problems.</p>
              <!-- <a class=" mt-10 button icon-color" href="#">Read More... <i class="fa fa-angle-right"></i></a> -->
         </div>
      </div>
  </div>
</section>
<!--=================================
 Our services -->

<!--=================================
 vision-->

 <section class="service white-bg page-section-ptb">
  <div class="container">
  <!-- ============================================ -->
  <div class="service-3">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-md-push-6">
       <img class="img-responsive full-width" src="assets/images/about/bg1.jpg" alt=""> 
     </div>
     <div class="col-lg-6 col-md-6 col-sm-12 sm-mt-30 sm-mb-30 col-md-pull-6">
      <div class="service-blog text-right"> 
       <h3 class="theme-color">Our Vision</h3>
       <p>Our vision is to be a leading force in the academic industry in the next 5 years offering quality tutoring &amp; other form of academic help to students all around the world.</p>
    </div>
  </div>
  
</div>
<!-- ============================================ -->
<div class="row">

 <div class="col-lg-6 col-md-6 col-sm-12">
   <img class="img-responsive full-width" src="assets/images/about/bg2.jpg" alt=""> 
 </div>
 <div class="col-lg-6 col-md-6 col-sm-12 sm-mt-30 sm-mb-30">
  <div class="service-blog left text-left"> 
   <h3 class="theme-color">Our Mission</h3>
   <p>To provide quality, affordable and plagiarism free tuition, dissertation help, essay writing help, course work help for IT, Law and Management students from GCSE level to PHD level</p>
</div>
</div>

</div>
</div>
</div>
</section>

<!--=================================
 vision-->


<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->


  <?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html> 