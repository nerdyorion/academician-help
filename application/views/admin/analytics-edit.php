  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Update RBT Analytics</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>analytics">RBT Analytics</a></li>
            <li class="active">Edit</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Users</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#update" aria-controls="update" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-pencil"></i></span><span class="hidden-xs"> Update</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="update">
                <div class="col-md-12">
                <?php echo form_open('admin123/analytics/edit/' . $row["id"], 'class="form-horizontal"'); ?>
                  <div class="form-group">
                    <label for="count_lagos" class="col-sm-3 control-label">Artist:</label>
                    <div class="col-sm-9">
                      <p><b><a href="<?php echo base_url() . '@' . $row['username']; ?>" data-toggle="tooltip" data-original-title="View <?php echo $row['stage_name']; ?>" target="_blank"><?php echo $row['stage_name']; ?></a></b></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="count_lagos" class="col-sm-3 control-label">Lagos: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <input type="number" class="form-control" name="count_lagos" min="0" id="count_lagos" value="<?php echo $row['count_lagos']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="count_north1" class="col-sm-3 control-label">North 1: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <input type="number" class="form-control" name="count_north1" min="0" id="count_north1" value="<?php echo $row['count_north1']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="count_north2" class="col-sm-3 control-label">North 2: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <input type="number" class="form-control" name="count_north2" min="0" id="count_north2" value="<?php echo $row['count_north2']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="count_south_east" class="col-sm-3 control-label">South East: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <input type="number" class="form-control" name="count_south_east" min="0" id="count_south_east" value="<?php echo $row['count_south_east']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="count_south_south" class="col-sm-3 control-label">South South: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <input type="number" class="form-control" name="count_south_south" min="0" id="count_south_south" value="<?php echo $row['count_south_south']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="count_south_west" class="col-sm-3 control-label">South West: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <input type="number" class="form-control" name="count_south_west" min="0" id="count_south_west" value="<?php echo $row['count_south_west']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                      <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Cancel</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">

</script>

</body>
</html>