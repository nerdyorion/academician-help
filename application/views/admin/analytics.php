  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">RBT Analytics</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li class="active">RBT Analytics</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Artists</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Artist</th>
                          <th>Lagos</th>
                          <th>North 1</th>
                          <th>North 2</th>
                          <th>South East</th>
                          <th>South South</th>
                          <th>South West</th>
                          <th>Total</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="6" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php $sn = 1; foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td><a href="<?php echo base_url() . '@' . $row['username']; ?>" data-toggle="tooltip" data-original-title="View <?php echo $row['stage_name']; ?>" target="_blank"><?php echo $row['stage_name']; ?></a></td>
                          <td><?php echo number_format(dashIfEmpty($row['count_lagos'], 0)); ?></td>
                          <td><?php echo number_format(dashIfEmpty($row['count_north1'], 0)); ?></td>
                          <td><?php echo number_format(dashIfEmpty($row['count_north2'], 0)); ?></td>
                          <td><?php echo number_format(dashIfEmpty($row['count_south_east'], 0)); ?></td>
                          <td><?php echo number_format(dashIfEmpty($row['count_south_south'], 0)); ?></td>
                          <td><?php echo number_format(dashIfEmpty($row['count_south_west'], 0)); ?></td>
                          <td><?php echo number_format(dashIfEmpty($row['count_total'], 0)); ?></td>
                          <td class="text-nowrap">
                            <?php if(!empty($row['analytics_id'])): ?>
                              <a href="admin123/analytics/edit/<?php echo $row['analytics_id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> 
                              <a href="admin123/analytics/delete/<?php echo $row['analytics_id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a> 
                            <?php else: ?>
                              <span data-toggle="modal" data-target="#addRBTDataModal" class="open-rbt-dialog" data-id="<?php echo $row['id']; ?>" data-artist-id="<?php echo $row['id']; ?>"><a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Add RBT Analytics Data"> <i class="fa fa-plus text-inverse m-r-10"></i> </a></span>
                            <?php endif; ?>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!--
                <div class="col-md-3 pull-right">
                  <p><a href="?page=2">Pagination</a>.</p>
                </div>
                -->
                <!-- Modal -->
                <div id="addRBTDataModal" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <?php echo form_open('/admin123/analytics/add', 'class="form-horizontal", role="form"'); ?>
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add / Update RBT Analytics Data</h4>
                      </div>
                      <div class="modal-body white-box">
                        <!-- <p>Some text in the modal.</p> -->
                        <div class="form-group">
                          <label for="count_lagos" class="col-sm-3 control-label">Lagos: <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <input type="number" class="form-control" name="count_lagos" min="0" id="count_lagos" value="" placeholder="" required="required">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="count_north1" class="col-sm-3 control-label">North 1: <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <input type="number" class="form-control" name="count_north1" min="0" id="count_north1" value="" placeholder="" required="required">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="count_north2" class="col-sm-3 control-label">North 2: <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <input type="number" class="form-control" name="count_north2" min="0" id="count_north2" value="" placeholder="" required="required">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="count_south_east" class="col-sm-3 control-label">South East: <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <input type="number" class="form-control" name="count_south_east" min="0" id="count_south_east" value="" placeholder="" required="required">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="count_south_south" class="col-sm-3 control-label">South South: <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <input type="number" class="form-control" name="count_south_south" min="0" id="count_south_south" value="" placeholder="" required="required">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="count_south_west" class="col-sm-3 control-label">South West: <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <input type="number" class="form-control" name="count_south_west" min="0" id="count_south_west" value="" placeholder="" required="required">
                          </div>
                        </div>

                        <input type="hidden" name="user_id" id="user_id" value="" />
                      </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  $(document).on("click", ".open-rbt-dialog", function () {
   var user_id = $(this).data('id');
   $(".modal-body #user_id").val( user_id );
   });
  

</script>

</body>
</html>