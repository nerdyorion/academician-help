<!-- Custom CSS -->
<link href="assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Update Coupon</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>coupons">Coupons</a></li>
            <li class="active">Edit</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Users</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#update" aria-controls="update" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-pencil"></i></span><span class="hidden-xs"> Update</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="update">
                <div class="col-md-12">
                <?php echo form_open_multipart('admin123/coupons/edit/' . $row['id'], 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="2000" id="name" value="<?php echo $row['name']; ?>" placeholder="" required="required" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="code" class="col-sm-2 control-label">Code: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="code" maxlength="255" id="code" value="<?php echo $row['code']; ?>" placeholder="" required="required" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="description" maxlength="20000" id="description" placeholder="" required="required"><?php echo $row['description']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="discount_amount" class="col-sm-2 control-label">Discount: <span class="text-danger">*</span></label>
                    <div class="col-sm-5">
                      <input type="number" class="form-control" name="discount_amount" id="discount_amount" min="1" required="required" value="<?php echo $row['discount_amount']; ?>" />
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="is_fixed" id="is_fixed" required="required">
                        <option value="0" <?php echo (int) $row['is_fixed'] == 0 ? 'selected="selected"' : ''; ?>>Percent (%)</option>
                        <option value="1" <?php echo (int) $row['is_fixed'] == 1 ? 'selected="selected"' : ''; ?>>Fixed Amount (&pound;)</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="expiry_date" class="col-sm-2 control-label">Expiry Date: </label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" data-date-format="yyyy-mm-dd" name="expiry_date" maxlength="10" id="expiry_date" placeholder="" value="<?php echo $row['expiry_date']; ?>" />
                      <p class="help-block">Please leave blank if coupon doesn't expiry</p>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                      <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

  $('#expiry_date').datepicker({
    //format: "yyyy-mm-dd"
    format: "yyyy-mm-dd",
    startDate: "1900-01-01",
    endDate: "2116-01-01",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
  });

    function validate()
    {
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      return true;
    }
</script>

</body>
</html>