<!-- Custom CSS -->
<link href="assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Coupons</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li class="active">Coupons</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Coupons</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="<?php echo current_url(); ?>#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
              <li role="presentation"><a href="<?php echo current_url(); ?>#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add New</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Description</th>
                          <th>Discount</th>
                          <th>Date</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="4" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td>
                            <?php echo dashIfEmpty($row['name']); ?><br />
                            <b>Code</b>: <?php echo dashIfEmpty($row['code']); ?>
                          </td>
                          <td><?php echo dashIfEmpty($row['description']); ?></td>
                          <td>
                            <?php if($row['is_fixed'] == 1): ?>
                              &pound;<?php echo number_format(($row['discount_amount']), 2); ?>
                            <?php else: ?>
                              <?php echo number_format(($row['discount_amount'])); ?>%
                            <?php endif; ?>
                          </td>
                          <td>
                            <small>
                              <b>Added</b>: <?php echo $row['date_created']; ?><br />
                              <b>Expires</b>: <?php echo is_null($row['expiry_date']) || empty($row['expiry_date']) ? 'Never' : ($row['expiry_date']); ?>
                              </small>
                            </td>
                          <td class="text-nowrap">
                            <a href="admin123/coupons/edit/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                            <?php if($row['id'] > 1): ?>
                            <a href="admin123/coupons/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a>
                          <?php endif; ?>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!--
                <div class="col-md-3 pull-right">
                  <p><a href="?page=2">Pagination</a>.</p>
                </div>
                -->
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane" id="add">
                <div class="col-md-12">
                <?php echo form_open('admin123/coupons/create', 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="2000" id="name" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="code" class="col-sm-2 control-label">Code: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="code" maxlength="255" id="code" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="description" maxlength="20000" id="description" placeholder="" required="required"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="discount_amount" class="col-sm-2 control-label">Discount: <span class="text-danger">*</span></label>
                    <div class="col-sm-5">
                      <input type="number" class="form-control" name="discount_amount" id="discount_amount" min="1" required="required" />
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="is_fixed" id="is_fixed" required="required">
                        <option value="0">Percent (%)</option>
                        <option value="1">Fixed Amount (&pound;)</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="expiry_date" class="col-sm-2 control-label">Expiry Date: </label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" data-date-format="yyyy-mm-dd" name="expiry_date" maxlength="10" id="expiry_date" placeholder="">
                      <p class="help-block">Please leave blank if coupon doesn't expiry</p>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div><div class="col-md-3 pull-right pagination">
              <p><?php echo $links; ?></p>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

  $('#expiry_date').datepicker({
    //format: "yyyy-mm-dd"
    format: "yyyy-mm-dd",
    startDate: "1900-01-01",
    endDate: "2116-01-01",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
  });

    function validate()
    {
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      return true;
    }
</script>

</body>
</html>