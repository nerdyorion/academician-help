
<link rel="stylesheet" href="assets/bower_components/html5-editor/bootstrap-wysihtml5.css" />

  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Update Free Project Proposal</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>Free-Project-Proposals">Free Project Proposals</a></li>
            <li class="active">Edit</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Users</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#update" aria-controls="update" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-pencil"></i></span><span class="hidden-xs"> Update</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="update">
                <div class="col-md-12">
                <?php echo form_open_multipart('admin123/Free-Project-Proposals/edit/' . $row['id'], 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="level_id" class="col-sm-2 control-label">Level: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="level_id" id="level_id" required="required">
                        <option value="0">-- Select --</option>
                        <?php foreach ($levels as $level): ?>
                          <option value="<?php echo $level['id']; ?>" <?php echo $row['level_id'] == $level['id'] ? 'selected="selected"' : ''; ?>><?php echo $level['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Title: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="title" maxlength="8000" id="title" value="<?php echo $row['title']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="file_url" class="col-sm-2 control-label">File: </label>
                    <div class="col-sm-10">
                      <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                          <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                          <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                        <span class="fileinput-new">Select file</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="file_url" id="file_url"></span>
                        <input type="hidden" name="file_url_old" value="<?php echo $row['file_url']; ?>" />
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control textarea_editor" name="description" id="description" maxlength="800000" rows="5"><?php echo $row['description']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="page_title" class="col-sm-2 control-label">Page Title: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="page_title" maxlength="2000" id="page_title" value="<?php echo $row['page_title']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="meta_title" class="col-sm-2 control-label">Meta Title: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="meta_title" maxlength="2000" id="meta_title" value="<?php echo $row['meta_title']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="meta_tags" class="col-sm-2 control-label">Meta Tags: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="meta_tags" id="meta_tags" required="required"><?php echo $row['meta_tags']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="meta_description" class="col-sm-2 control-label">Meta Description: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="meta_description" id="meta_description" required="required"><?php echo $row['meta_description']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                      <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script src="assets/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="assets/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $('.textarea_editor').wysihtml5();
  });

    function validate()
    {
      var level_id = document.getElementById("level_id").value;
      var fileName = $("#file_url").val();

      if(level_id == 0 ){
        alert('Please specify level.');
        return false;
      }
      else{
        // return true;
      }

      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      return true;
    }
</script>

</body>
</html>