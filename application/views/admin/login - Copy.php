<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="<?php echo base_url();?>">
<link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
<title><?php echo $page_title; ?> | <?php echo $this->config->item('app_name'); ?></title>
<!-- Bootstrap CSS -->
<link href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shiv and Respond.js IE8 support -->
<!--[if lt IE 9]>
        <script src="assets/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="assets/https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body >
<section id="wrapper" class="login-register">
  <div class="login-box">
    <div class="white-box">
      <form class="form-horizontal m-t-20" action="">
        <div class="form-group ">
          <div class="col-xs-12">
            <h3>Sign In</h3>
            <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="Username">
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <input class="form-control" type="password" required="" placeholder="Password">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-md-5 col-xs-12">
            <div class="checkbox checkbox-primary">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup"> Remember me </label>
            </div>
          </div>
          <div class="col-md-7 col-xs-12 m-t-5 text-right"><a href="assets/recoverpw.html" class="text-dark"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a></div>
        </div>
        <div class="form-group text-center m-t-40">
          <div class="col-xs-12">
            <button class="btn btn-success btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
          </div>
        </div>
        <div class="form-group m-t-30 m-b-0">
          <div class="col-sm-12 text-center">
            <p>Don't have an account? <a href="assets/register.html" class="text-primary m-l-5"><b>Sign Up</b></a></p>
          </div>
        </div>
      </form>
    </div>
    <footer class="cus-footer text-center">
      <div class="social"> <a href="javascript:void(0)" class="btn  btn-twitter"> <i aria-hidden="true" class="fa fa-twitter"></i> </a> <a href="javascript:void(0)" class="btn  btn-facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
      2016 © Myadmin.</footer>
  </div>
</section>
<!-- jQuery -->
<script src="assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>

</html>
