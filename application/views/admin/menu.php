  <div class="navbar-default sidebar nicescroll" role="navigation">
    <div class="sidebar-nav navbar-collapse ">
      <ul class="nav" id="side-menu">
        <li class="nav-small-cap">Menu</li>
        <li class=""> <a href="<?php echo base_url() . "admin123/"; ?>" class="waves-effect"><i class="fa fa-dashboard"></i> Dashboard</a> </li>
          <li> <a href="#" class="waves-effect"><i class="ti-pie-chart fa-fw"></i> Setup<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
              <li> <a href="admin123/countries" class="waves-effect"><i class="ti-user fa fa-globe" aria-hidden="true"></i> Countries</a> </li>
              <li> <a href="admin123/coupons" class="waves-effect"><i class="ti-tags fa fa-tags" aria-hidden="true"></i> Coupons</a> </li>
              <li> <a href="admin123/levels" class="waves-effect"><i class="ti-user fa fa-step-forward" aria-hidden="true"></i> Levels</a> </li>
              <li> <a href="admin123/universities" class="waves-effect"><i class="ti-location-pin fa fa-university" aria-hidden="true"></i> Universities</a> </li>
            </ul>
          </li>
          <li> <a href="admin123/orders" class="waves-effect"><i class="ti-user fa fa-tag" aria-hidden="true"></i> Orders</a> </li>
          <li> <a href="admin123/students" class="waves-effect"><i class="ti-user fa fa-user" aria-hidden="true"></i> Students</a> </li>
          
          <li> <a href="admin123/Past-Exam-Solution" class="waves-effect"><i class="ti-bar-chart fa fa-book"></i> Past Exam Solution</a> </li>
          <li> <a href="admin123/Free-Project-Proposals" class="waves-effect"><i class="ti-bar-chart fa fa-book"></i> Free Project Proposals</a> </li>
          <li> <a href="admin123/Live-Chat" class="waves-effect"><i class="ti-comment fa fa-comment"></i> Live Chat</a> </li>
          <li> <a href="https://login.mailchimp.com/" class="waves-effect" target="_blank"><i class="ti-user fa fa-users"></i> Subscribers</a> </li>
        <?php if(($_SESSION['user_role_id'] == 1) && (strtoupper($_SESSION['user_role_name']) == "SUPER ADMINISTRATOR")): ?>
          <li> <a href="admin123/users" class="waves-effect"><i class="ti-user fa fa-user"></i> Users (Admin)</a> </li>
          <li> <a href="admin123/pages" class="waves-effect"><i class="ti-list fa fa-list"></i> Pages Meta Info</a> </li>
        <?php endif; ?>
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>