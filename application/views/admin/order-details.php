  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Order Details</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>orders">Orders</a></li>
            <li class="active">Details</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <div class="white-box">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <th>Student</th>
                    <td><?php echo $row['full_name']; ?></td>
                  </tr>
                  <tr>
                    <th>Product(s)</th>
                    <td>
                      <?php $products = explode('###', $row['items']); ?>
                      <ul>
                        <?php foreach($products as $item_array): ?>
                          <?php $item = explode('@@@', $item_array); //var_dump($item); die; ?>
                          <li><a href="/Past-Exam-Solution?id=<?php echo $item[0]; ?>"><?php echo $item[1]; ?> (&pound;<?php echo $item[2]; ?>)</a></li>
                        <?php endforeach; ?>
                      </ul>
                    </td>
                  </tr>
                  <tr>
                    <th>Price</th>
                    <td>&pound;<?php echo number_format($row['total_price']); ?></td>
                  </tr>
                  <tr>
                    <th>Status</th>
                    <td><?php echo $row['paid'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;PAID&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;UNPAID&nbsp;</a>'; ?></td>
                  </tr>
                  <tr>
                    <th>Payment Status</th>
                    <td><?php echo $row['successful'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;PAID&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;UNPAID&nbsp;</a>'; ?></td>
                  </tr>
                  <tr>
                    <th>Date</th>
                    <td><?php echo $row['date_created'] == "0000-00-00 00:00:00" || is_null($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                  </tr>
                  <tr>
                    <th>Gateway Error Code</th>
                    <td><?php echo dashIfEmpty($row['error_code']); ?></td>
                  </tr>
                  <tr>
                    <th>Gateway Error Mesage</th>
                    <td><?php echo dashIfEmpty($row['error_message']); ?></td>
                  </tr>
                  <tr>
                    <th>Gateway Payload</th>
                    <td><?php echo dashIfEmpty($row['payload']); ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <p class="text-center">
              <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
            </p>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

  <?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
  <script src="assets/js/jasny-bootstrap.js"></script>

</body>
</html>
