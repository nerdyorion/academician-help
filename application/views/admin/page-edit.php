  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Update Page</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>pages">Pages</a></li>
            <li class="active">Edit</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Pages</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#update" aria-controls="update" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-pencil"></i></span><span class="hidden-xs"> Update</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="update">
                <div class="col-md-12">
                <?php echo form_open('admin123/pages/edit/' . $row["id"], 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="page_title" class="col-sm-2 control-label">Page Title: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="page_title" maxlength="2000" id="page_title" value="<?php echo $row['page_title']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="url" class="col-sm-2 control-label">URL: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="url" maxlength="2000" id="url" value="<?php echo $row['url']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="meta_title" class="col-sm-2 control-label">Meta Title: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="meta_title" maxlength="2000" id="meta_title" value="<?php echo $row['meta_title']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="meta_tags" class="col-sm-2 control-label">Meta Tags: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="meta_tags" id="meta_tags" required="required"><?php echo $row['meta_tags']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="meta_description" class="col-sm-2 control-label">Meta Description: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="meta_description" id="meta_description" required="required"><?php echo $row['meta_description']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  function validate()
  {
    var $btn = $('button[type="submit"]').button('loading');
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }
</script>

</body>
</html>