  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Past Exam Solution Details</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>Past-Exam-Solution">Past Exam Solution</a></li>
            <li class="active">Details</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <div class="white-box">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <th>University</th>
                    <td><?php echo dashIfEmpty($row['university_name']); ?></td>
                  </tr>
                  <tr>
                    <th>Title</th>
                    <td><?php echo dashIfEmpty($row['title']); ?></td>
                  </tr>
                  <tr>
                    <th>Price</th>
                    <td><?php echo dashIfEmpty('&pound; ' . number_format($row['price'])); ?></td>
                  </tr>
                  <tr>
                    <th>Description</th>
                    <td><?php echo dashIfEmpty($row['description']); ?></td>
                  </tr>
                  <tr>
                    <th>Page Title</th>
                    <td><?php echo dashIfEmpty($row['page_title']); ?></td>
                  </tr>
                  <tr>
                    <th>Meta Title</th>
                    <td><?php echo dashIfEmpty($row['meta_title']); ?></td>
                  </tr>
                  <tr>
                    <th>Meta Tags</th>
                    <td><?php echo dashIfEmpty($row['meta_tags']); ?></td>
                  </tr>
                  <tr>
                    <th>Meta Description</th>
                    <td><?php echo dashIfEmpty($row['meta_description']); ?></td>
                  </tr>
                  <tr>
                    <th colspan="2 text-center"><a href="admin123/Past-Exam-Solution/download/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Download" target="_blank"> <i class="fa fa-download text-inverse m-r-10"></i> View / Download File</a> </th>
                  </tr>
                </tbody>
              </table>
            </div>
            <p class="text-center">
              <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
            </p>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>

</body>
</html>
