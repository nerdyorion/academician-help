  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Dashboard</h4>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-md-8 col-lg-9 col-sm-12">
          <div class="white-box">
            <div class="row row-in">
              <div class="col-lg-3 col-sm-6">
                <div class="col-in text-center">
                  <h5 class="text-danger"><strong>Bookings <br/>(Total)</strong></h5>
                  <h3 class="counter" style="word-wrap: break-word; line-height: 1em;"><?php echo number_format($bookings_total); ?></h3>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                <div class="col-in text-center b-r-none">
                  <h5 class="text-muted text-warning"><strong>Bookings <br/>(Paid)</strong></h5>
                  <h3 class="counter" style="word-wrap: break-word; line-height: 1em;"><?php echo number_format($bookings_paid); ?></h3>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                <div class="col-in text-center">
                  <h5 class="text-muted text-purple"><strong>Bookings <br/>(Pending)</strong></h5>
                  <h3 class="counter" style="word-wrap: break-word; line-height: 1em;"><?php echo number_format($bookings_pending); ?></h3>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                <div class="col-in text-center b-0">
                  <h5 class="text-info"><strong>Upcoming <br/>Events</strong></h5>
                  <h3 class="counter" style="word-wrap: break-word; line-height: 1em;"><?php echo number_format($events_upcoming); ?></h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-lg-3 col-sm-6 ">
          <div class="bg-orange m-b-20">
            <div id="myCarousel" class="carousel vcarousel slide vertical p-20">
              <!-- Carousel items -->
              <div class="carousel-inner ">
                <div class="active item"> <i class="fa fa-map-marker fa-2x text-white"></i>
                  <p class="text-white"><?php echo $last_login == "0000-00-00 00:00:00" || $last_login == NULL ? "-" : date('M d, Y h:i A', strtotime($last_login )); ?></p>
                  <h4 class="text-white">Last <span class="font-bold">Login</span><br />&nbsp;</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--row -->
        <h2>Unpaid Bookings</h2>
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Event</th>
                  <th>Date</th>
                  <th>Artist</th>
                  <th>Booking Cost (N)</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <?php if(empty($rows)): ?>
                  <tr>
                    <td colspan="5" align="center">No data returned.</td>
                  </tr>
                <?php else: ?>
                  <?php foreach ($rows as $row): ?>
                    <tr>
                      <td><?php echo $sn++; ?></td>
                      <td title="<?php echo dashIfEmpty($row['event_name']); ?>"><?php echo dashIfEmpty($row['event_name']); ?></td>
                      <td title="<?php echo $row['event_date'] . ' ' . $row['event_start_time'] . '-' . $row['event_end_time']; ?>"><?php echo $row['event_date'] . ' ' . $row['event_start_time'] . '-' . $row['event_end_time']; ?></td>
                      <td><a href="<?php echo base_url() . '@' . $row['username']; ?>" data-toggle="tooltip" data-original-title="View <?php echo $row['stage_name']; ?>" target="_blank"><?php echo $row['stage_name']; ?></a></td>
                      <td title="<?php echo $row['booking_cost'] == 0 ? 'Pending' : number_format($row['booking_cost']); ?>"><?php echo $row['booking_cost'] == 0 ? '<a href="javascript:void(0);" class="bg-warning" style="color: #ffffff;">&nbsp;Pending&nbsp;</a>' : number_format($row['booking_cost']); ?></td>
                      <td>
                        <?php echo $row['accepted'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;Accepted&nbsp;</a>' : ''; ?> 
                        <?php echo $row['rejected'] == 1 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;Rejected&nbsp;</a>' : ''; ?> 
                        <?php echo $row['rejected'] != 1 && $row['paid'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;Unpaid&nbsp;</a>' : ''; ?>
                        <?php echo $row['paid'] == 1 && $row['accepted'] == 0 ? '<a href="javascript:void(0);" class="bg-info" style="color: #ffffff;">&nbsp;Paid&nbsp;</a>' : ''; ?>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
          <!-- <p><?php //echo $links; ?></p> -->
        </div>
        <div class="clearfix"></div>
        <em>*All paid bookings would have their status changed to "Accepted" once payment is confirmed.</em>
        <div class="col-md-12 text-center">
          <p><a href="/agent/bookings" class="btn btn-default">View All</a></p>
        </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

  <?php $this->load->view($this->config->item('template_dir_agent') . 'footer'); echo "\n"; ?>
</body>
</html>
