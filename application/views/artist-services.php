  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <!-- <li data-target="#myCarousel" data-slide-to="0" class="active"></li> -->
    </ol>

    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <!-- <img src="assets/images/banner.png" alt="Image"> -->
        <img src="assets/images/register-banner.png" alt="Image" />
        <div class="carousel-caption">
          <h3 style="color: #f9f9f9;"><?php echo $row['stage_name']; ?> Artist Services Page</h3>
          <p style="color: #f9f9f9;">see all services below</p>
        </div>      
      </div>

    </div>
  </div>

  <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          Artist Services below (click to book)
          <br />
          <pre />
          <?php foreach ($services as $item): ?>
            <li><a href="#"><?php echo $item['name']; ?></a></li>
          <?php endforeach; ?>
          </pre>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->

<?php $this->load->view('footer'); echo "\n"; ?>

</div><!-- /.container -->

</body>
</html>