  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">RBT Analytics</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "artist/"; ?>">Dashboard</a></li>
            <li class="active">RBT Analytics</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-md-8 col-lg-9 col-sm-12">
          <div class="white-box">
            <div class="row row-in">
              <div class="col-lg-4 col-sm-4">
                <div class="col-in text-center">
                  <h5 class="text-danger"><strong>Lagos</strong></h5>
                  <h3 class="counter" style="word-wrap: break-word; line-height: 1em;"><?php echo number_format(dashIfEmpty($row['count_lagos'], 0)); ?></h3>
                </div>
              </div>
              <div class="col-lg-4 col-sm-4">
                <div class="col-in text-center">
                  <h5 class="text-danger"><strong>North 1</strong></h5>
                  <h3 class="counter" style="word-wrap: break-word; line-height: 1em;"><?php echo number_format(dashIfEmpty($row['count_north1'], 0)); ?></h3>
                </div>
              </div>
              <div class="col-lg-4 col-sm-4">
                <div class="col-in text-center">
                  <h5 class="text-danger"><strong>North 2</strong></h5>
                  <h3 class="counter" style="word-wrap: break-word; line-height: 1em;"><?php echo number_format(dashIfEmpty($row['count_north2'], 0)); ?></h3>
                </div>
              </div>
              <div class="col-lg-4 col-sm-4">
                <div class="col-in text-center b-r-none">
                  <h5 class="text-muted text-warning"><strong>South <br />East</strong></h5>
                  <h3 class="counter" style="word-wrap: break-word; line-height: 1em;"><?php echo number_format(dashIfEmpty($row['count_south_east'], 0)); ?></h3>
                </div>
              </div>
              <div class="col-lg-4 col-sm-4">
                <div class="col-in text-center">
                  <h5 class="text-muted text-purple"><strong>South <br />South</strong></h5>
                  <h3 class="counter" style="word-wrap: break-word; line-height: 1em;"><?php echo number_format(dashIfEmpty($row['count_south_south'], 0)); ?></h3>
                </div>
              </div>
              <div class="col-lg-4 col-sm-4">
                <div class="col-in text-center b-0">
                  <h5 class="text-info"><strong>South <br />West</strong></h5>
                  <h3 class="counter" style="word-wrap: break-word; line-height: 1em;"><?php echo number_format(dashIfEmpty($row['count_south_west'], 0)); ?></h3>
                </div>
              </div>
            </div>
            <!-- <p><b>Total</b>: <?php echo number_format(dashIfEmpty($row['count_total'], 0)); ?></p> -->
          </div>
        </div>
        
        <div class="col-md-4 col-lg-3 col-sm-6 ">
          <div class="bg-orange m-b-20">
            <div id="myCarousel" class="carousel vcarousel slide vertical p-20">
              <div class="carousel-inner ">
                <div class="active item"> <i class="fa fa-map-marker fa-2x text-white"></i>
                  <p class="text-white">Total</p>
                  <h4 class="text-white"><span class="font-bold"><?php echo number_format(dashIfEmpty($row['count_total'], 0)); ?></span><br />&nbsp;</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
       
      </div>
      <!--row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_artist') . 'footer'); echo "\n"; ?>
</body>
</html>
