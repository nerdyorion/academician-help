  <style type="text/css">
    table.table.table-bordered tbody tr td{
       vertical-align: middle;
    }
  </style>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Bookings</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "artist/"; ?>">Dashboard</a></li>
            <li class="active">Bookings</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Products</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Event</th>
                          <th>Date</th>
                          <th>Agent</th>
                          <th>Booking Cost (N)</th>
                          <th>Status</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="5" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td title="<?php echo dashIfEmpty($row['event_name']); ?>"><?php echo dashIfEmpty($row['event_name']); ?></td>
                          <td title="<?php echo $row['event_date'] . ' ' . $row['event_start_time'] . '-' . $row['event_end_time']; ?>"><?php echo $row['event_date'] . ' ' . $row['event_start_time'] . '-' . $row['event_end_time']; ?></td>
                          <td><?php echo $row['full_name']; ?></td>
                          <td title="<?php echo $row['booking_cost'] == 0 ? 'Pending' : number_format($row['booking_cost']); ?>"><?php echo $row['booking_cost'] == 0 ? '<a href="javascript:void(0);" class="bg-warning" style="color: #ffffff;">&nbsp;Pending&nbsp;</a>' : number_format($row['booking_cost']); ?></td>
                          <td>
                            <?php echo $row['accepted'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;Accepted&nbsp;</a>' : ''; ?> 
                            <?php echo $row['rejected'] == 1 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;Rejected&nbsp;</a>' : ''; ?> 
                            <?php echo $row['rejected'] != 1 && $row['paid'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;Unpaid&nbsp;</a>' : ''; ?>
                          </td>
                          <!-- <td title="<?php //echo $row['description']; ?>"><?php //echo ellipsize($row['event_description'], 50); ?></td> -->
                          <!-- <td><?php // echo $row['available_after_hours'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;No&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;Yes&nbsp;</a>'; ?></td> -->
                          <!-- <td><?php // echo $row['available_on_weekends'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;No&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;Yes&nbsp;</a>'; ?></td> -->
                          <td class="text-nowrap">
                            <a href="artist/bookings/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-trash text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                  <!-- <p><?php //echo $links; ?></p> -->
                </div>
                
                <div class="col-md-3 pull-right pagination">
                  <p><?php echo $links; ?></p>
                </div>
                
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_artist') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>

</body>
</html>