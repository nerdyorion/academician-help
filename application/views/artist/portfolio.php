  <style type="text/css">
    table.table.table-bordered tbody tr td{
       vertical-align: middle;
    }
  </style>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Portfolio</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "artist/"; ?>">Dashboard</a></li>
            <li class="active">Portfolio</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Products</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
              <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add Image</span></a></li>
              <li role="presentation"><a href="#addAudio" aria-controls="addAudio" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add Audio</span></a></li>
              <li role="presentation"><a href="#addVideo" aria-controls="addVideo" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add Video</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th></th>
                          <th>Name</th>
                          <th>Description</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="6" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td>
                            <?php if($row['type'] == 'image'): ?> <!-- 338448041 -->
                              <div class="media">
                                <div class="media-left">
                                  <a href="<?php echo $row['image_url']; ?>" target="_blank">
                                    <img class="media-object" src="<?php echo image_thumb($row['image_url']); ?>" alt="<?php echo $row['name']; ?>">
                                  </a>
                                </div>
                              </div>
                            <?php else: ?>
                              <div class="media">
                                <div class="media-left">
                                  <a href="<?php echo base_url() . 'portfolio/' . $row['id']; ?>" target="_blank">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                    <!-- <img class="media-object" src="<?php echo base_url() . "assets/images/play-sm.png"; ?>" alt="<?php echo $row['name']; ?>"> -->
                                  </a>
                                </div>
                              </div>
                            <?php endif; ?>
                          </td>
                          <td title="<?php echo $row['name']; ?>"><?php echo $row['name']; ?></td>
                          <td title="<?php echo $row['description']; ?>"><?php echo dashIfEmpty(ellipsize($row['description'], 50)); ?></td>
                          <td class="text-nowrap">
                            <a href="artist/portfolio/edit/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> 
                            <a href="artist/portfolio/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-trash text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a> 
                            <a href="<?php echo base_url() . 'portfolio/' . $row['id']; ?>" data-toggle="tooltip" data-original-title="View" target="_blank"> <i class="fa fa-external-link text-inverse m-r-10"></i> </a> 
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                  <!-- <p><?php //echo $links; ?></p> -->
                </div>
                
                <div class="col-md-3 pull-right pagination">
                  <p><?php echo $links; ?></p>
                </div>
                
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane" id="add">
                <div class="col-md-12">
                <?php echo form_open_multipart('artist/portfolio/create', 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <input type="hidden" name="type" value="image" />
                  <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label">Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="20000" id="name" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="image" class="col-sm-2 control-label">Image (650x650 px): <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                          <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                          <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                        <span class="fileinput-new">Select file</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="image" id="image" required="required"></span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="description" id="description" rows="5"></textarea>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane" id="addAudio">
                <div class="col-md-12">
                <?php echo form_open_multipart('artist/portfolio/createAudio', 'class="form-horizontal"'); ?>
                  <input type="hidden" name="type" value="audio" />
                  <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label">Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="20000" id="name" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="source" class="col-sm-2 control-label">Source: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="source" id="source" required="required">
                          <option value="soundcloud" selected="selected">SoundCloud</option>
                          <option value="youtube">YouTube</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label">Audio ID: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="media_id" maxlength="50" id="media_id" value="" placeholder="" required="required">
                      <span class="help-block">E.g 338448041 in SoundCloud, KYXpl9iyrs0 in <a href="https://youtube.com/watch?v=KYXpl9iyrs0" target="_blank">https://youtube.com/watch?v=KYXpl9iyrs0</a></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="description" id="description" rows="5"></textarea>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane" id="addVideo">
                <div class="col-md-12">
                <?php echo form_open_multipart('artist/portfolio/createVideo', 'class="form-horizontal"'); ?>
                  <input type="hidden" name="type" value="video" />
                  <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label">Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="20000" id="name" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="source" class="col-sm-2 control-label">Source: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="source" id="source" required="required">
                          <option value="youtube" selected="selected">YouTube</option>
                          <option value="vimeo">Vimeo</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label">Video ID: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="media_id" maxlength="50" id="media_id" value="" placeholder="" required="required">
                      <span class="help-block">E.g 247839331 in <a href="https://vimeo.com/247839331" target="_blank">https://vimeo.com/247839331</a>, KYXpl9iyrs0 in <a href="https://youtube.com/watch?v=KYXpl9iyrs0" target="_blank">https://youtube.com/watch?v=KYXpl9iyrs0</a></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="description" id="description" rows="5"></textarea>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_artist') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  function validate()
  {
    
    if(document.getElementById("image").files.length == 0 ){
      alert('Please upload image.');
      return false;
    }
    else {
      var fileName = $("#image").val();
      fileName = fileName.toLowerCase();
      
      if((fileName.lastIndexOf("jpg")===fileName.length-3) || (fileName.lastIndexOf("jpeg")===fileName.length-4) || (fileName.lastIndexOf("png")===fileName.length-3) || (fileName.lastIndexOf("gif")===fileName.length-3))
      {
        //alert("OK");
        return true;
      }
      else
      {
        alert("Please upload valid image file.");
        return false;
      }
    }
  }
</script>

</body>
</html>