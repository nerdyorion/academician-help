  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Product Details</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>products">Products</a></li>
            <li class="active">Details</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <div class="white-box">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <th>Product Name</th>
                    <td><?php echo $row['name']; ?></td>
                  </tr>
                  <tr>
                    <th>Category</th>
                    <td><?php echo $row['category_name']; ?></td>
                  </tr>
                  <tr>
                    <th>URL</th>
                    <td><a href="<?php echo base_url() . $row['slug']; ?>" target="_blank"><?php echo base_url() . $row['slug']; ?></a></td>
                  </tr>
                    <th>Description</th>
                    <td><?php echo $row['description']; ?></td>
                  </tr>
                  <tr>
                    <th>Price (NGN)</th>
                    <td><?php echo $row['price']; ?></td>
                  </tr>
                  <tr>
                    <th>Duration</th>
                    <td><?php echo $row['duration']; ?></td>
                  </tr>
                  <tr>
                    <th>Service Codes</th>
                    <td>
                      <div>
                        <ul class="nav nav-tabs">
                          <li class="active"><a data-toggle="tab" href="#mtn">MTN</a></li>
                          <li><a data-toggle="tab" href="#airtel">AIRTEL</a></li>
                          <li><a data-toggle="tab" href="#glo">GLO</a></li>
                          <li><a data-toggle="tab" href="#emts">EMTS</a></li>
                          <li><a data-toggle="tab" href="#ntel">NTEL</a></li>
                        </ul>

                        <div class="tab-content">
                          <div id="mtn" class="tab-pane fade in active">
                            <p><b>Code</b>: <?php echo empty($row['mtn_service_code']) ? '-' : $row['mtn_service_code']; ?></p>
                            <p><b>Keyword</b>: <?php echo empty($row['mtn_service_keyword']) ? '-' : $row['mtn_service_keyword']; ?></p>
                          </div>
                          <div id="airtel" class="tab-pane fade">
                            <p><b>Code</b>: <?php echo empty($row['airtel_service_code']) ? '-' : $row['airtel_service_code']; ?></p>
                            <p><b>Keyword</b>: <?php echo empty($row['airtel_service_keyword']) ? '-' : $row['airtel_service_keyword']; ?></p>
                          </div>
                          <div id="glo" class="tab-pane fade">
                            <p><b>Code</b>: <?php echo empty($row['glo_service_code']) ? '-' : $row['glo_service_code']; ?></p>
                            <p><b>Keyword</b>: <?php echo empty($row['glo_service_keyword']) ? '-' : $row['glo_service_keyword']; ?></p>
                          </div>
                          <div id="emts" class="tab-pane fade">
                            <p><b>Code</b>: <?php echo empty($row['emts_service_code']) ? '-' : $row['emts_service_code']; ?></p>
                            <p><b>Keyword</b>: <?php echo empty($row['emts_service_keyword']) ? '-' : $row['emts_service_keyword']; ?></p>
                          </div>
                          <div id="ntel" class="tab-pane fade">
                            <p><b>Code</b>: <?php echo empty($row['ntel_service_code']) ? '-' : $row['ntel_service_code']; ?></p>
                            <p><b>Keyword</b>: <?php echo empty($row['ntel_service_keyword']) ? '-' : $row['ntel_service_keyword']; ?></p>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>




                  <tr>
                    <th>Active ?</th>
                    <td><?php echo $row['active'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;No&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;Yes&nbsp;</a>'; ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <p class="text-center">
              <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
            </p>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_artist') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>

</body>
</html>
