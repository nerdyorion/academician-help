  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Update Service</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "artist/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "artist/"; ?>services">Services</a></li>
            <li class="active">Edit</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Users</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#update" aria-controls="update" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-pencil"></i></span><span class="hidden-xs"> Update</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="update">
                <div class="col-md-12">
                <?php echo form_open_multipart('artist/services/edit/' . $row['id'], 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="20000" id="name" value="<?php echo $row['name']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="description" id="description" rows="5"><?php echo $row['description']; ?></textarea>
                    </div>
                  </div>
                  <!-- <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label"> </label>
                    <div class="col-sm-10">
                      <div class="checkbox checkbox-primary">
                        <input name="available_after_hours" id="available_after_hours" type="checkbox" value="1" <?php echo empty($row['available_after_hours']) ? '' : 'checked="checked"'; ?>>
                        <label for="available_after_hours"> Available after hours? </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label"> </label>
                    <div class="col-sm-10">
                      <div class="checkbox checkbox-primary">
                        <input name="available_on_weekends" id="available_on_weekends" type="checkbox" value="1" <?php echo empty($row['available_on_weekends']) ? '' : 'checked="checked"'; ?>>
                        <label for="available_on_weekends"> Available on Weekends? </label>
                      </div>
                    </div>
                  </div> -->
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_artist') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>

</body>
</html>