
<!--=================================
  page-title-->

  <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
    <div class="container">
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="page-title-name">
            <h1>Shopping cart</h1>
            <p>We know the secret of your success</p>
          </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Shopping cart </span> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<!--=================================
  page-title -->


<!--=================================
  shop -->

  <section class="wishlist-page page-section-ptb">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
         <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>Product</th>
                <th>Price</th>
                <th>Remove </th>
              </tr>
            </thead>
            <tbody>
              <?php if(empty($rows['cart_items'])): ?>
               <tr>
                 <td class="description" colspan="3">
                   Your cart is empty! <br />
                   <a class="button small mt-0" href="Past-Exam-Solution">Continue Shopping</a>
                 </td>
               </tr>
             <?php else: ?>
              <?php foreach($rows['cart_items'] as $key => $item): ?>
               <tr>
                 <td class="description">
                  <a href="Past-Exam-Solution?id=<?php echo $item[0]['product_id']; ?>"><?php echo urldecode($item[0]['product_name']); ?></a>
                </td>
                <td class="price">&pound;<?php echo $item[0]['product_price']; ?></td>
                <td class="total">
                 <!-- <a href="#"><i class="fa fa-close"></i></a> -->
                 <a href="javascript:void(0);" class="remove-from-cart" data-id="<?php echo $item[0]['product_id']; ?>"> <i class="fa fa-close"></i> </a>
               </td>
             </tr>
           <?php endforeach; ?>
         <?php endif; ?>
         <?php if(!empty($rows['cart_items'])): ?>
           <tr>
             <td class="description"></td>
             <td colspan="2" class="total">
              <h4>Total: <mark>&pound;<span id="cartTotal"><?php echo $rows['cart_total']; ?></span></mark></h4>
             </td>
           </tr>
         <?php endif; ?>
       </tbody>
     </table>
     <?php if(!empty($rows['cart_items'])): ?>
       <br />
       <a class="button small mt-0 pull-right" href="checkout">Checkout &rarr;</a>
     <?php endif; ?>
   </div>
 </div>
</div>
</div>
</section>

<!--=================================
  shop -->


<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>
<!--=================================
  action box- -->


  <?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html> 