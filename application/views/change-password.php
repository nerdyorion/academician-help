<style type="text/css">
.sidebar-widgets-wrap .recent-item .recent-title a {
  color: #73a02e;
  font-weight: normal;
}
.sidebar-widgets-wrap .recent-item .recent-title a:hover {
  color: #333;
  /*color: #84ba3f;*/
  /*font-weight: bold;*/
}
</style>

<!--=================================
  page-title-->

  <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
    <div class="container">
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="page-title-name">
            <h1>Welcome <?php echo isset($_SESSION['user_full_name']) ? explode(" ", $_SESSION['user_full_name'])[0] : "User"?></h1>
            <p>We know the secret of your success</p>
          </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Dashboard</span> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<!--=================================
  page-title -->

 <!--=================================
   shop-06-sub-banner -->
<!-- 
   <section class="shop-06-sub-banner black-bg pt-40 pb-40">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 sm-mb-30">
          <div class="feature-text left-icon">
            <div class="feature-icon">
              <span class="ti-loop text-white"></span>
            </div>
            <div class="feature-info">
              <h6 class="pt-15 text-white">Satisfaction Guaranteed</h6>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 sm-mb-30">
          <div class="feature-text left-icon">
           <div class="feature-icon">
            <span class="ti-gift text-white"></span>
          </div>
          <div class="feature-info">
            <h6 class="pt-15 text-white">Payment Secure</h6>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6 xs-mb-30">
       <div class="feature-text left-icon">
         <div class="feature-icon">
          <span class="ti-user text-white"></span>
        </div>
        <div class="feature-info">
          <h6 class="pt-15 text-white">Online Support</h6>
        </div>
      </div>
    </div>
      </div>
    </div>
  </section>
-->
<!--=================================
  shop-06-sub-banner -->


<!--=================================
  side-nav-menu -->

  <section class="shop grid page-section-ptb  alerts-and-callouts">
    <div class="container">
      <div class="row">
        <?php $this->load->view('side-nav-menu'); echo "\n"; ?>
        <div class="col-lg-9 col-md-9 sm-mt-40">
          <div class="row">
           <div class="product listing">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="product-des text-left">
                <div class="product-info mt-20">
                <div class="section-title line-dabble">
                  <h4 class="title">Change Password</h4>
                  <!-- <p> Update your password.</p> -->
                </div>
                  <!-- <h3>Change Password</h3>
                  <p> Update your password.</p> -->

                  <?php if($error_code == 0 && !empty($error)): ?>
                    <div class="alert alert-success alert-dismissable fade in">
                      <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Success!</strong> <?php echo $error; ?>
                    </div>
                  <?php elseif($error_code == 1 && !empty($error)): ?>
                    <div class="alert alert-danger alert-dismissable fade in">
                      <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Error!</strong> <?php echo $error; ?>
                    </div>
                  <?php else: ?>
                  <?php endif; ?>
                  <?php echo form_open('/Change-Password', 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="section-field mb-20">
                    <label class="mb-10" for="current_password">Current Password* </label>
                    <input id="current_password" name="current_password" class="Password form-control" type="password" placeholder="***********" required="required" />
                  </div>
                  <div class="section-field mb-20">
                    <label class="mb-10" for="new_password">New Password* </label>
                    <input id="new_password" name="new_password" class="Password form-control" type="password" placeholder="***********" required="required" />
                  </div>
                  <div class="section-field mb-30">
                    <label class="mb-10" for="confirm_password">Confirm Password* </label>
                    <input id="confirm_password" name="confirm_password" class="Password form-control" type="password" placeholder="***********" required="required" />
                  </div>
                  <button class="button" type="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span>Update Password</span> <i class='fa fa-check'></i>">
                  <span>Update Password</span>
                  <i class="fa fa-check"></i>
                </button> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>

<!--=================================
  side-nav-menu -->

<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->

  <?php $this->load->view('footer'); echo "\n"; ?>

  <script type="text/javascript">
    function validate()
    {

      var current_password = document.getElementById("current_password").value;
      var new_password = document.getElementById("new_password").value;
      var confirm_password = document.getElementById("confirm_password").value;
      if(new_password != confirm_password ){
        alert('Passwords do not match.');
        document.getElementById("confirm_password").focus();
        return false;
      }
      var $btn = $('button[type="submit"]').button('loading');
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      return true;
    }
  </script>

</body>
</html> 