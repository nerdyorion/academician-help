<div id="cover-spin"><a href="javascript:void(0);" class="hide-loader" style="position: absolute; left:46%;top:50%;"> <i class="fa fa-close"></i> Hide Loader</a></div>
<style type="text/css">
  #cover-spin {
    position:fixed;
    width:100%;
    left:0;right:0;top:0;bottom:0;
    background-color: rgba(255,255,255,0.7);
    z-index:9999;
    display:none;
}

@-webkit-keyframes spin {
  from {-webkit-transform:rotate(0deg);}
  to {-webkit-transform:rotate(360deg);}
}

@keyframes spin {
  from {transform:rotate(0deg);}
  to {transform:rotate(360deg);}
}

#cover-spin::after {
    content:'';
    display:block;
    position:absolute;
    left:48%;top:40%;
    width:40px;height:40px;
    border-style:solid;
    border-color:black;
    border-top-color:transparent;
    border-width: 4px;
    border-radius:50%;
    -webkit-animation: spin .8s linear infinite;
    animation: spin .8s linear infinite;
}
</style>
<!--=================================
  page-title-->

  <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
    <div class="container">
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="page-title-name">
            <h1>Checkout</h1>
            <p>We know the secret of your success</p>
          </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Checkout </span> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<!--=================================
  page-title -->


<!--=================================
  shop -->

  <section class="wishlist-page page-section-ptb">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <?php if($error_code == 0 && !empty($error)): ?>
            <div class="alert alert-success alert-dismissable fade in">
              <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Success!</strong> <?php echo $error; ?>
            </div>
          <?php elseif($error_code == 1 && !empty($error)): ?>
            <div class="alert alert-danger alert-dismissable fade in">
              <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Error!</strong> <?php echo $error; ?>
            </div>
          <?php else: ?>
          <?php endif; ?>
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th>Product</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                <?php if(empty($rows['cart_items'])): ?>
                 <tr>
                   <td class="description" colspan="3">
                     Your cart is empty! <br />
                     <a class="button small mt-0" href="Past-Exam-Solution">Continue Shopping</a>
                   </td>
                 </tr>
               <?php else: ?>
                <?php foreach($rows['cart_items'] as $key => $item): ?>
                 <tr>
                   <td class="description">
                    <a href="Past-Exam-Solution?id=<?php echo $item[0]['product_id']; ?>"><?php echo urldecode($item[0]['product_name']); ?></a>
                  </td>
                  <td class="price">&pound;<?php echo $item[0]['product_price']; ?></td>
                </tr>
              <?php endforeach; ?>
            <?php endif; ?>
            <?php if(!empty($rows['cart_items'])): ?>
             <tr>
               <td class="description" colspan="1">
                <div class="row">
                  <div class="col-md-5 pull-right section-field mb-20">
                    <input id="coupon_code" name="coupon_code" class="web form-control" type="text" placeholder="Coupon Code" required="required" value="<?php echo isset($_GET['coupon_code']) ? trim($_GET['coupon_code']) : ''; ?>" />
                  </div>
                </div>
              </td>
               <td class="description">
                <button class="button small mt-0" id="coupon_btn">Apply</button>
                <button class="button small mt-0" id="coupon_remove_btn">Remove</button>
              </td>
            </tr>
            <!-- Coupon Discount -->
            <?php if($calculated_discount_amount > 0): ?>
              <tr>
                <td class="description"></td>
                <td colspan="2" class="total">
                  <h4>Sub-Total: <mark>&pound;<span id="cartTotal"><?php echo $rows['cart_total']; ?></span></mark></h4>
                </td>
              </tr>

              <tr>
                <td class="description"></td>
                <td colspan="2" class="total">
                  <h4>Discount: <mark>-&pound;<span id=""><?php echo number_format($calculated_discount_amount, 2); ?></span></mark></h4>
                </td>
              </tr>
            <?php endif; ?>

             <tr>
               <td class="description"></td>
               <td colspan="2" class="total">
                <h4>Total: <mark>&pound;<span id="cartTotal"><?php echo $rows['cart_total'] - $calculated_discount_amount; ?></span></mark></h4>
              </td>
            </tr>
          <?php endif; ?>
        </tbody>
      </table>
      <?php if(!empty($rows['cart_items'])): ?>
        <br />
        <?php echo form_open('/checkout/secure', 'class="pull-right", id="checkout"'); ?>
        <!-- Stripe -->
        <!-- <?php // echo 'pk_test_fcsSbTHLEnLlK7wzpGkqYOcj'; ?> -->
        <!-- <?php // echo 'pk_live_bJWvO3vZLPG5tU1WuO4R1e5t'; ?> -->
        <div style="display: inline; float: left; padding-right: 10px;">
          <script
          src="https://checkout.stripe.com/checkout.js" class="stripe-button"
          data-key="pk_live_bJWvO3vZLPG5tU1WuO4R1e5t" 
          data-amount="<?php echo ($rows['cart_total'] - $calculated_discount_amount) * 100; ?>"
          data-name="Academician Help"
          data-description="Your one-stop for all tutoring, writing, editing and proof-reading needs."
          data-image="http://academicianhelp.co.uk/assets/images/favicon.ico"
          data-locale="auto"
          data-zip-code="true"
          data-currency="gbp">
        </script>
      </div>

      <!-- PayPal -->
      <!-- <script src="https://www.paypalobjects.com/api/checkout.js"></script>
      <div id="paypal-button" style="display: inline; float: left;"></div> -->
    </form>

    <!-- PayPal -->
    <div style="display: inline; float: right; padding-right: 10px;">
      <form target="_blank" action="https://www.paypal.com/cgi-bin/webscr" method="post" style="display: inline; float: left; padding-right: 10px;">
      <!-- <form target="_blank" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" style="display: inline; float: left; padding-right: 10px;"> -->
        <input type="hidden" name="cmd" value="_xclick">
        <input type="hidden" name="business" value="a.adeseye007@gmail.com">
        <input type="hidden" name="item_name" value="Past Exam Solution (<?php echo count($rows['cart_items']) > 1 ? count($rows['cart_items']) . ' items' : '1 item'; ?>)">
        <input type="hidden" name="return" value="<?php echo base_url();?>checkout/secure?complete=paypal">
        <input type="hidden" name="item_number" value="<?php echo date('YmdHis'); ?>">
        <input type="hidden" name="amount" value="<?php echo $rows['cart_total'] - $calculated_discount_amount; ?>">
        <input type="hidden" name="quantity" value="1">
        <input type="hidden" name="currency_code" value="GBP">

        <input type="image" name="submit" style="cursor: pointer;" 
        src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/checkout-logo-medium.png"
        alt="Check out with PayPal" />
      </form>
    </div>
  <?php endif; ?>
</div>
</div>
</div>
</div>
</section>

<!--=================================
  shop -->


<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>
<!--=================================
  action box- -->


  <?php $this->load->view('footer'); echo "\n"; ?>

  <script type="text/javascript">

    $('#coupon_btn').click(function () {

      var coupon_code = $('#coupon_code').val();

      if(coupon_code == '' || coupon_code == null)
      {
        $('#coupon_code').focus();
      }
      else
      {
        window.location.href = "checkout?coupon_code=" + coupon_code;
      }
    });

    $('#coupon_remove_btn').click(function () {

      var coupon_code = $('#coupon_code').val();

      if(coupon_code == '' || coupon_code == null)
      {
        $('#coupon_code').focus();
      }
      else
      {
        window.location.href = "checkout?remove_coupon_code=" + coupon_code;
      }
    });

    $('.stripe-button-el').click(function () {
      $('#cover-spin').show();
    });

    $('.hide-loader').click(function () {
      $('#cover-spin').hide();
    });

    // $('Checkout').click(function () {
    //   $('#cover-spin').hide();
    // });
    
    

    var csrfData = {};
    csrfData['<?php echo $this->security->get_csrf_token_name(); ?>'] = '<?php echo $this->security->get_csrf_hash(); ?>';

    $.ajaxSetup({
        // data: csrfData
        // headers: {
        //     'X-CSRF-TOKEN': '<?php echo $this->security->get_csrf_hash(); ?>'
        // }
        headers: {
            'x-csrf-token': '{{<?php echo $this->security->get_csrf_hash(); ?>}}'
        }
        // headers: {
        //     '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
        // }
    });
  </script>

  <!-- <script type="text/javascript" src="assets/js/paypal.js"></script> -->

 </body>
 </html> 