
<!--=================================
  page-title-->

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
  <div class="container">
    <div class="row"> 
      <div class="col-lg-12"> 
      <div class="page-title-name">
          <h1>Contact Us</h1>
          <p>We know the secret of your success</p>
        </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a href="#">Company</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Contact Us</span> </li>
       </ul>
     </div>
     </div>
  </div>
</section>

<!--=================================
page-title -->


<!--=================================
 contact-->

<section class="contact white-bg page-section-ptb">
  <div class="container">
   <div class="row">
       <div class="col-lg-8 col-md-8 col-md-offset-2">
          <div class="section-title text-center">
            <h6>let's work together</h6>
            <h2>Let’s Get In Touch!</h2>
            <p>It would be great to hear from you! If you got any questions</p>
          </div>
      </div>
      </div>
     <div class="row">
       <div class="touch-in white-bg">
       <div class="col-lg-4 col-md-4 col-sm-4">
           <div class="contact-box text-center">
              <i class="ti-map-alt theme-color"></i>
              <h5 class="uppercase mt-20">Address</h5>
              <p class="mt-20">3 Woodward Close, Grays, Essex, RM17 5RP</p>
           </div>
       </div>
       <div class="col-lg-4 col-md-4 col-sm-4">
           <div class="contact-box text-center">
              <i class="ti-mobile theme-color"></i>
              <h5 class="uppercase mt-20">Phone</h5>
              <p class="mt-20"> +447786074967</p>
           </div>
       </div>
       <div class="col-lg-4 col-md-4 col-sm-4">
           <div class="contact-box text-center">
              <i class="ti-email theme-color"></i>
              <h5 class="uppercase mt-20">Email</h5>
              <p class="mt-20">tutor@academicianhelp.co.uk</p>
           </div>
        </div>
       </div>
     </div>
      <div class="row">
      <div class="col-lg-12 col-md-12 text-center">
       <p class="mt-50 mb-30">It would be great to hear from you! If you got any questions, please do not hesitate to send us a message. We are looking forward to hearing from you! We reply within <br /><span class="tooltip-content-2" data-original-title="Mon-Fri 10am–7pm (GMT +1)" data-toggle="tooltip" data-placement="top"> 24 hours!</span></p>
      </div>
     </div>
  </div>
</section>

<!--=================================
 contact-->
 

<!--=================================
 contact map -->

<section class="contact-map clearfix o-hidden">
   <div class="container-fluid p-0">
     <div class="row">
      <div class="col-lg-12">
      <div style="width: 100%; height: 350px;" id="map" class="g-map" data-type='black'></div>
     </div>
     </div>
   </div>
</section>

<!--=================================
 contact map -->


<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->


  <?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html> 