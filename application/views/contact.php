  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    </ol>

    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="assets/images/banner.png" alt="Image">
        <div class="carousel-caption">
          <h3 style="color: #000000;">MobileFun</h3>
          <p style="color: #000000;">Get In Touch...</p>
        </div>      
      </div>

    </div>
  </div>

  <div style="width: 90%; margin: auto;">

    <div class="clearfix"></div>

    <div class="container-fluid text-center bg-grey">
      <!-- <h2>MOBILE CONTENTS</h2> -->
      <!-- <h4>What we have created</h4> -->

      <div class="row text-center">

        <ol class="breadcrumb text-left">
          <li><a href="<?php echo base_url(); ?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
          <li class="active">Contact</li>
        </ol>
        <div class="col-sm-12" style="border-bottom: 2px solid #feb25a; margin-bottom: 10px; ">
          <h3 class="text-left">Contact Us</h3>
        </div>

        <div class="jumbotron" style="display: inline-block; width: 100%">
          <div class="col-sm-12">

            <div class="google-maps" id="map_canvas">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31716.501615750938!2d3.4517453682126864!3d6.450144301343024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103bf452da3bd44b%3A0x47331fb41adc9d28!2sLekki+Phase+1%2C+Lekki!5e0!3m2!1sen!2sng!4v1498659168368" width="600" height="450" frameborder="0" style="border:0"></iframe>
            </div>

            <div class="clearfix"></div>
            <div class="clearfix"></div>

            <h3 class="text-left">Our Location</h3>
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="row">
                  <div class="col-sm-4">
                    <strong>MobileFun Store</strong><br/>
                    <address>
                      <a href="//maps.google.com/maps?q=6.449904%2C+3.473924&amp;hl=en-gb&amp;t=m&amp;z=15" target="_blank">
                        No 1, Kola Adeyina Close, Off Jerry Iriabe, Lekki Phase 1, Lagos, Nigeria.
                      </a>
                    </address>
                  </div>
                  <div class="col-sm-4">
                    <div class="icon material-design-phone370">
                      <strong>Telephone</strong><br>
                      <a href="callto:+442039502729">+442039502729</a>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="icon fa-key">
                      <strong>Opening Times</strong><br/>
                      24/7
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"></div>
          </div>

        </div>


      </div>

    </div>

    <div class="clearfix"></div>

  </div>


<?php $this->load->view('footer'); echo "\n"; ?>

<script type="text/javascript">
  $(window).resize(function(){ 
    $('#map_canvas').height($( window ).height());
</script>

</body>
</html>