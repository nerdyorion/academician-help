
<!--=================================
page-title-->

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
  <div class="container">
    <div class="row"> 
      <div class="col-lg-12"> 
      <div class="page-title-name">
          <h1>FAQs</h1>
          <p>We know the secret of your success</p>
        </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a href="#">company</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>FAQ</span> </li>
       </ul>
     </div>
     </div>
  </div>
</section>

<!--=================================
page-title -->

<!--=================================
 faq-->

<section class="faq white-bg page-section-ptb">
  <div class="container">
    <div class="row">
       <div class="col-lg-12 col-md-12">
         <div class="section-title text-center">
            <h6>Have a question?</h6>
            <h2 class="title-effect">Frequently Asked Questions</h2>
            <p>why the sky's the limit when using AcademicianHelp.</p>
          </div>
      </div>
    </div>
    <div class="row"> 
      <div class="col-lg-12 col-md-12"> 
        <div class="accordion shadow">
        <div class="acd-group acd-active">
        <a href="#" class="acd-heading">01. How does your service work?</a>
        <div class="acd-des">
          <!-- <h5>Amet of lorem ipsum dolor sit lorem ipsum?</h5> -->
          <p>You need to fill the request form either on the home page or any of our services page. We will then assign the work to one of our expert who will evaluate your work and then come up with a quotation usually within 24 hours depending on the work complexity. We will then email you to let you know of the cost, you will need to pay half of the cost before work begins on your order. After we have completed the work, we will ask that you make the balance payment before sending you the work. Also, note that you can ask for to see the work progress in between before the actual completion.</p>
        </div>
        </div>
        <div class="acd-group">
        <a href="#" class="acd-heading">02. How do I know that your work will meet your guarantees?</a>
        <div class="acd-des">
          <!-- <h5>Amet of lorem ipsum dolor sit lorem ipsum?</h5> -->
          <p>Be rest assured that our team of experts have several years of experience in the field they manage. All of them have a 2.1 and above and a Master’s degree in their area of specialty. Your work is also cross checked by our quality control team to ensure that it meets the required standard. If for any reason the work does not meet your specification, we will amend it for free if it we are notified on or before a week’s time.</p>
          </div>
        </div>
        <div class="acd-group">
        <a href="#" class="acd-heading">03. What are the areas your researchers cover?</a>
        <div class="acd-des">
          <!-- <h5>Amet of lorem ipsum dolor sit lorem ipsum?</h5> -->
          <p>Our experts are skilled in tutoring of IT/Computer Science related modules as well as writing services which includes: essay writing and coursework help in the IT/Computer Science, Management, or Law fields. We can also help with final year project, masters’ dissertation or PHD thesis in these fields.</p>
          </div>
        </div>
        <div class="acd-group">
        <a href="#" class="acd-heading">04. Can I submit the work I receive as my own?</a>
          <div class="acd-des">
            <p>The work that we give are model answers and should be used as a reference material to write your own. Submitting of the work without acknowledging us is plagiarism and we do not encourage that.</p>
            </div>
          </div>
        <div class="acd-group">
        <a href="#" class="acd-heading">05. Is my identity confidential?</a>
          <div class="acd-des">
          <p>Be rest assured that your privacy will not be breached. We do not divulge any information about you to anyone working with us</p>
          </div>
        </div>
        <div class="acd-group">
        <a href="#" class="acd-heading">06. What happens if I think there a mistake in the report that needs to be corrected?</a>
          <div class="acd-des">
              <p>If for any reason the work does not meet your specification, we will amend it for free when notified and we find your complaint valid.</p>
             </div>
          </div>
        <div class="acd-group">
        <a href="#" class="acd-heading">07. What is your email address for the customer support?</a>
          <div class="acd-des">
              <p>Our customer service email address is tutor@academicianhelp.co.uk</p>
             </div>
          </div>
        <div class="acd-group">
        <a href="#" class="acd-heading">08. How do I receive my assignment work?</a>
          <div class="acd-des">
              <p>Your work will be sent to you as an attachment using the email address you provided.</p>
             </div>
          </div>
        <div class="acd-group">
        <a href="#" class="acd-heading">09. What time will my work arrive?</a>
          <div class="acd-des">
              <p>We will try to complete the work before the deadline day you have specified except in extremely short deadline situation which can lead to delivery of the work on deadline day.</p>
             </div>
          </div>
        <div class="acd-group">
        <a href="#" class="acd-heading">10. How do I pay and when?</a>
          <div class="acd-des">
              <p>You can pay through Paypal or Bank Transfer. Payment details will be sent to you once your order has been accessed and the cost decided. Your first payment will be at the beginning of the work while your last payment will be before the final work is sent to you.</p>
             </div>
          </div>
        <div class="acd-group">
        <a href="#" class="acd-heading">11. What if we do not deliver work before the deadline?</a>
          <div class="acd-des">
              <p>We do not miss deadline and we do not take on a job that we cannot deliver on time, if for any reason this is the case, a full refund will be issued and the draft that has been done so far will be sent to you for free.</p>
             </div>
          </div>
        <div class="acd-group">
        <a href="#" class="acd-heading">12. How do you know I won't cheat?</a>
          <div class="acd-des">
              <p>It is difficult to ensure that you won’t cheat. We will rely on your honesty that you won’t submit the work as your own work because that will be plagiarism of our work.</p>
             </div>
          </div>
        <div class="acd-group">
        <a href="#" class="acd-heading">13. If my supervisor require that I change some parts of the work, can you do this for me?</a>
          <div class="acd-des">
              <p>Firstly, we want to reiterate that the work you send should be entirely yours and not what we send to you. However, we will be happy to make changes to some parts of the work should you need so for free; except the change is different from what you expected which might lead to payment of some extra fees.</p>
             </div>
          </div>
        <div class="acd-group">
        <a href="#" class="acd-heading">14. When am I entitled for a refund?</a>
          <div class="acd-des">
              <p>If the assignment is not delivered on time or contain huge amount of plagiarism which rarely happens because we pass our work through industry standard plagiarism checkers.</p>
             </div>
          </div>
        <div class="acd-group">
        <a href="#" class="acd-heading">15. Who handles my order?</a>
          <div class="acd-des">
              <p>Your order will be handled by one of our expert whom you will have access to via email or skype or telephone.</p>
             </div>
          </div>
        <div class="acd-group">
        <a href="#" class="acd-heading">16. Can I pay in more than 2 instalments?</a>
          <div class="acd-des">
              <p>Yes, this is allowed for orders with deadline of more than 20 days e.g. final year projects or dissertations.</p>
             </div>
          </div>
        </div>
        </div> 
     </div> 
     <div class="row">
        <div class="col-lg-12 text-center mt-40">
           <h6> If you do not find the answer to your question listed within our FAQ's, you can always contact us directly at <a class="theme-color" href="mailto:tutor@academicianhelp.co.uk"> tutor@academicianhelp.co.uk</a> </h6>
        </div>     
     </div>
  </div> 
</section>

<!--=================================
 careers-->

  
<!--=================================
action box- -->

<section class="action-box theme-bg full-width">
  <div class="container">
    <div class="row">
     <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
       </a> 
    </div>
  </div>
 </div>
</section>
 
<!--=================================
action box- -->
 

  <?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html> 