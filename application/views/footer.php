<style type="text/css">
  #currencyBtn .notActive{
    /*color: #3276b1;
    background-color: #fff;*/
}
#currencyBtn .active:hover, #deadlineBtn .active:hover{
  /*color: #3276b1;*/
  background-color: #286090;
}

</style>


  <style type="text/css">
  div.fixed {
    position: fixed;
    bottom: 0;
    left: 0;
    width: auto;
    border: 3px solid #73AD21;
    z-index: 1;
  </style>
  <?php if(!is_null($this->session->userdata('user_id_former_logged_in')) && !is_null($this->session->userdata('user_full_name_former_logged_in'))): ?>
    <!-- Trigger -->
    <div class="fixed">
      <button type="button" class="bet_time btn btn-info btn-sm" onclick="location.href='<?php echo base_url() . "/User-Home/switchbacktoadmin/" . $this->session->userdata('user_id_former_logged_in'); ?>';">Log back in <?php echo $this->session->userdata('user_full_name_former_logged_in'); ?>?</button>
    </div>
  <?php endif; ?>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="section-title mb-10">
          <h6>Find out how much your order will cost.</h6>
          <h2>Price Calculator</h2>
          <!-- <p>We are an innov</p> -->
        </div>
      </div>
      <div class="modal-body">
        <div class="row mb-20">
          <h6>Currency</h6>
          <div class="form-group">
            <div class="col-sm-12 col-md-12">
              <div class="input-group" style="width: 100%;">
                <div id="currencyBtn" class="btn-group" style="width: 100%;">
                  <a class="btn btn-primary btn-sm active" data-toggle="currency" data-title="GBP" title="GBP" style="width: 33%;">GBP</a>
                  <a class="btn btn-primary btn-sm notActive" data-toggle="currency" data-title="USD" title="USD" style="width: 33%; border-left: 1px solid #fff;">USD</a>
                  <a class="btn btn-primary btn-sm notActive" data-toggle="currency" data-title="EUR" title="EUR" style="width: 34%; border-left: 1px solid #fff;">EUR</a>
                </div>
                <input type="hidden" name="currency" id="currency" value="GBP">
              </div>
            </div>
          </div>
        </div>
          <!-- 
          1day = 50
          2-3 days = 48
          4-5 days = 46
          6-8 days = 44
          >9 days = 42
          >20 days = 40 
        -->
        <div class="row mb-20">
          <h6>Deadline (Days)</h6>
          <div class="form-group">
            <div class="col-sm-12 col-md-12">
              <div class="input-group" style="width: 100%;">
                <div id="deadlineBtn" class="btn-group" style="width: 100%;">
                  <a class="btn btn-primary btn-sm notActive" data-toggle="deadline" data-title="1" title="1 Day" style="width: 13%; padding: 5px 5px;">1</a>
                  <a class="btn btn-primary btn-sm notActive" data-toggle="deadline" data-title="2" title="2-3 Days" style="width: 17%; border-left: 1px solid #fff; padding: 5px 5px;">2-3</a>
                  <a class="btn btn-primary btn-sm notActive" data-toggle="deadline" data-title="4" title="4-5 Days" style="width: 17%; border-left: 1px solid #fff; padding: 5px 5px;">4-5</a>
                  <a class="btn btn-primary btn-sm notActive" data-toggle="deadline" data-title="6" title="6-8 Days" style="width: 17%; border-left: 1px solid #fff; padding: 5px 5px;">6-8</a>
                  <a class="btn btn-primary btn-sm notActive" data-toggle="deadline" data-title="9" title="&gt;9 Days" style="width: 18%; border-left: 1px solid #fff; padding: 5px 5px;">&gt;9</a>
                  <a class="btn btn-primary btn-sm active" data-toggle="deadline" data-title="20" title="&gt;20 Days" style="width: 18%; border-left: 1px solid #fff; padding: 5px 5px;">&gt;20</a>
                </div>
                <input type="hidden" name="deadline" id="deadline" value="20">
              </div>
            </div>
          </div>
        </div>

        <div class="row mb-30">
          <h6>Pages</h6>
          <div class="form-group">
            <div class="col-sm-4 col-md-4 col-lg-4">
              <div class="input-group">
                <span class="input-group-btn">
                  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="pages">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>
                <input type="text" id="pages" name="pages" class="form-control input-number" min="2" value="2" step="2" max="500">
                <span class="input-group-btn">
                  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="pages">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
              </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
              <p style="margin-top: 15px;" id="wordCount">500 Words</p>
            </div>
          </div>
        </div>

        <div class="row mb-30">
          <h5>Price: <mark id="totalPrice">40 GBP</mark></h5>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="button gray" data-dismiss="modal">Close</button>
        <button type="button" class="button icon" onclick="location.href='Request-Quote';">Order Now <i class="fa fa-paper-plane"></i></button>
        <div class="row mt-10 pull-right" style="font-size: 0.9em;"><small>* Price shown is approximate. Actual price depends on 1) Type of Work 2) Number of Pages 3) Urgency 4) Academic Level</small></div>
      </div>
    </div>
  </div>
</div>

<div class="modal-backdrop fade" style="display: none;"></div>


<!--=================================
 footer -->
 
 <footer class="footer page-section-pt black-bg">
   <div class="container">
    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-6 xs-mb-30">
        <div class="footer-useful-link footer-hedding">
          <h6 class="text-white mb-30 mt-10 text-uppercase">Navigation</h6>
          <ul>
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li><a href="About-Us">About Us</a></li>
            <li><a href="How-It-Works">How It Works</a></li>
            <li><a href="faq">FAQ</a></li>
            <li><a href="Contact-Us">Contact Us</a></li>
          </ul>
        </div>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-6 xs-mb-30">
        <div class="footer-useful-link footer-hedding">
          <h6 class="text-white mb-30 mt-10 text-uppercase">Useful Link</h6>
          <ul>
            <li><a href="Request-Quote">Get Quote</a></li>
            <li><a href="testimonials">Testimonials</a></li>
            <li><a href="Law-Assignments-Essays">Law Assignments/Essays</a></li>
            <li><a href="programming">Programming Tutoring</a></li>
            <li><a href="Proof-Reading-Editing">Proof Reading &amp; Editing</a></li>
          </ul>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6 xs-mb-30">
        <h6 class="text-white mb-30 mt-10 text-uppercase">Contact Us</h6>
        <ul class="addresss-info"> 
          <li><i class="fa fa-map-marker"></i> <p>Address: 3 Woodward Close, Grays, Essex, RM17 5RP</p> </li>
          <li><i class="fa fa-phone"></i> <a href="tel:+442039502729"> <span>+442039502729</span> </a> </li>
          <li><i class="fa fa-envelope-o"></i>Email: tutor@academicianhelp.co.uk</li>
        </ul>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6">
        <h6 class="text-white mb-30 mt-10 text-uppercase">Subscribe to Our Newsletter</h6>
        <p>Sign Up to our Newsletter to get the latest news and offers.</p>
        <div class="footer-Newsletter">
          <div id="mc_embed_signup_scroll">
            <?php echo form_open('subscribe/ajax', 'class="validate", name="mc-embedded-subscribe-form", id="mc-embedded-subscribe-form", role="form"'); ?>
            <!-- <form action="subscribe/ajax" method="POST" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate"> -->
             <div id="msg"> </div>                 
             <div id="mc_embed_signup_scroll_2">
              <input id="mce-EMAIL" class="form-control" type="text" placeholder="Email address" name="email1" value="">
            </div>
            <div id="mce-responses" class="clear">
              <div class="response" id="mce-error-response" style="display:none"></div>
              <div class="response" id="mce-success-response" style="display:none"></div>
            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
            <div style="position: absolute; left: -5000px;" aria-hidden="true">
              <input type="text" name="b_b7ef45306f8b17781aa5ae58a_6b09f39a55" tabindex="-1" value="">
            </div>
            <div class="clear">
              <button type="submit" name="submitbtn" id="mc-embedded-subscribe" class="button border mt-20 form-button">  Get notified </button>
            </div>
          </form>
          <div id="ajaxloader2" style="display:none"><img class="center-block mt-30 mb-30" src="assets/images/pre-loader/loader-02.svg" alt=""></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <!-- <img src="assets/images/comodo_secure_seal_113x59_transp.png" style="margin-top: 10px;" /> -->
      <script language="JavaScript" type="text/javascript">
        TrustLogo("<?php echo base_url(); ?>assets/images/comodo_secure_seal_113x59_transp.png", "CL1", "none");
      </script>
      <a  href="https://ssl.comodo.com" id="comodoTL">Comodo SSL</a>
    <img src="assets/images/cards.png" style="margin-top: 10px;" class="img-responsive" />
    </div>
  </div>
  <div class="footer-widget mt-20">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 xs-mb-20">
        <p style="text-align: center;"><small>Disclaimer: Academicianhelp.co.uk offers services that are only to be used as model answers and skill improvement and should not be submitted as your own work.</small></p>
        <hr style="background-color: 1px solid #262626;">
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 xs-mb-20">
       <p class="mt-15"> &copy;Copyright <span id="copyright"> <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script></span> <a href="http://brilloconnetz.com" target="_blank"> BrilloConnetz </a> All Rights Reserved </p>
     </div>
     <div class="col-lg-6 col-md-6 col-sm-6 text-right xs-text-left">
      <div class="footer-widget-social">
       <ul> 
        <li><a href="https://www.facebook.com/academicianhelp" target="_blank"><i class="fa fa-facebook"></i></a></li>
        <li><a href="https://twitter.com/AcademicianHelp" target="_blank"><i class="fa fa-twitter"></i></a></li>
        <!-- <li><a href="#"><i class="fa fa-dribbble"></i> </a></li>
          <li><a href="#"><i class="fa fa-linkedin"></i> </a></li> -->
        </ul>
      </div>
    </div>
  </div>    
</div>
</div>
</footer>

<!--=================================
 footer -->
 
</div>




<div id="back-to-top"><a class="top arrow" href="<?php echo current_url(); ?>#top"><i class="fa fa-angle-up"></i> <span>TOP</span></a></div>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5a7483234b401e45400c9f44/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

<!--=================================
 jquery -->

 <!-- jquery -->
 <script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>

 <!-- plugins-jquery -->
 <script type="text/javascript" src="assets/js/plugins-jquery.js"></script>

 <!-- plugin_path -->
 <script type="text/javascript">var plugin_path = 'assets/js/';</script>
 

 <!-- REVOLUTION JS FILES -->
 <script type="text/javascript" src="assets/revolution/js/jquery.themepunch.tools.min.js"></script>
 <script type="text/javascript" src="assets/revolution/js/jquery.themepunch.revolution.min.js"></script>

 <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
 <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.actions.min.js"></script>
 <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
 <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
 <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
 <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.migration.min.js"></script>
 <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
 <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
 <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
 <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.video.min.js"></script>
 <!-- revolution custom --> 
 <script type="text/javascript" src="assets/revolution/js/revolution-custom.js"></script> 

 <!-- custom -->
 <script type="text/javascript" src="assets/js/custom.js"></script>
 <script type="text/javascript" src="assets/js/calculator.js"></script>
 <script type="text/javascript" src="assets/js/cart.js"></script>