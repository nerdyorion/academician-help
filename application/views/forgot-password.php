<div id="cover-spin"><a href="javascript:void(0);" class="hide-loader" style="position: absolute; left:46%;top:50%;"> <i class="fa fa-close"></i> Hide Loader</a></div>
<style type="text/css">
  #cover-spin {
    position:fixed;
    width:100%;
    left:0;right:0;top:0;bottom:0;
    background-color: rgba(255,255,255,0.7);
    z-index:9999;
    display:none;
}

@-webkit-keyframes spin {
  from {-webkit-transform:rotate(0deg);}
  to {-webkit-transform:rotate(360deg);}
}

@keyframes spin {
  from {transform:rotate(0deg);}
  to {transform:rotate(360deg);}
}

#cover-spin::after {
    content:'';
    display:block;
    position:absolute;
    left:48%;top:40%;
    width:40px;height:40px;
    border-style:solid;
    border-color:black;
    border-top-color:transparent;
    border-width: 4px;
    border-radius:50%;
    -webkit-animation: spin .8s linear infinite;
    animation: spin .8s linear infinite;
}
</style>
<!--=================================
page-title-->

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
  <div class="container">
    <div class="row"> 
      <div class="col-lg-12"> 
      <div class="page-title-name">
          <h1>Forgot Password</h1>
          <p>We know the secret of your success</p>
        </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Forgot Password</span> </li>
       </ul>
     </div>
     </div>
  </div>
</section>

<!--=================================
page-title -->


<!--=================================
 login-->

<section class="white-bg page-section-ptb">
  <div class="container">
    <div class="row">
       <div class="col-lg-12 col-md-12">
        <div class="section-title text-center">
            <h6 class="">send password reset link</h6>
            <h2 class="title-effect">Forgot Password</h2>
        <?php if($error_code == 0 && !empty($error)): ?>
          <div class="alert alert-success alert-dismissable fade in">
            <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?php echo $error; ?>
          </div>
        <?php elseif($error_code == 1 && !empty($error)): ?>
          <div class="alert alert-danger alert-dismissable fade in">
            <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> <?php echo $error; ?>
          </div>
        <?php else: ?>
        <?php endif; ?>
          </div>
      </div>
    </div>
     <div class="row">
       <div class="col-md-6 col-md-offset-3">
        <div class="pb-50 clearfix">
        <?php echo form_open('/Forgot-Password/send_token', 'class="form-horizontal", onsubmit="return validate();"'); ?>
         <div class="section-field mb-20">
             <label class="mb-10" for="email">Email* </label>
               <input id="email" class="web form-control" type="email" placeholder="e.g johndoe@yahoo.com" name="email" />
            </div>
            <div class="section-field">
              <div class="remember-checkbox mb-30">
                 <!-- <input type="checkbox" class="form-control" name="two" id="two" />
                 <label for="two"> Remember me</label> -->
                 <a href="login" class="pull-right">&larr; Login</a>
                </div>
              </div>
              <button class="button" type="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span>Reset Password</span> <i class='fa fa-check'></i>">
                <span>Reset Password</span>
                <i class="fa fa-check"></i>
             </button> 
           </form>
          </div>
           <hr />
        </div>
      </div>
  </div>
</section>

<!--=================================
 login-->


<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->


  <?php $this->load->view('footer'); echo "\n"; ?>

  <script type="text/javascript">
    function validate()
    {

      if($('#email').val() != '')
      {
        $('#cover-spin').show();
      }

      $('.hide-loader').click(function () {
        $('#cover-spin').hide();
      });

      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);

      return true;
    }
  </script>

</body>
</html>