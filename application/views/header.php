<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <base href="<?php echo base_url();?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <title><?php echo $page_title; ?> | <?php echo $this->config->item('app_name'); ?></title>

  <!-- Open Graph Tags -->
  <meta property="og:type" content="website" />
  <meta property="og:title" content="AcademicianHelp" />
  <meta property="og:description" content="<?php echo $meta_description; ?>" />
  <meta property="og:url" content="http://www.academicianhelp.co.uk/" />
  <meta property="og:site_name" content="Academician Help" />
  <meta property="og:image" content="http://www.academicianhelp.co.uk/logo.png" />
  <meta property="og:locale" content="en_US" />
  <meta name="twitter:card" content="summary" />


  <!-- For Google -->
  <meta name="title" content="<?php echo $meta_title; ?>">
  <meta name="description" content="<?php echo $meta_description; ?>" />
  <meta name="keywords" content="<?php echo $meta_tags; ?>" />

  <meta name="author" content="brilloconnetz.com" />
  <meta name="copyright" content="<?php echo $this->config->base_url(); ?>Terms-And-Conditions" />
  <meta name="application-name" content="AcademicianHelp" />

  <!-- For Facebook -->
  <meta property="og:title" content="AcademicianHelp" />
  <meta property="og:type" content="article" />
  <meta property="og:image" content="<?php echo $this->config->base_url(); ?>assets/images/logo.png" />
  <meta property="og:url" content="<?php echo $this->config->base_url(); ?>" />
  <meta property="og:description" content="<?php echo $meta_description; ?>" />

  <!-- For Twitter -->
  <meta name="twitter:card" content="summary" />
  <meta name="twitter:title" content="AcademicianHelp" />
  <meta name="twitter:description" content="<?php echo $meta_description; ?>" />
  <meta name="twitter:image" content="<?php echo $this->config->base_url(); ?>assets/images/logo.png" />
  <link href="assets/images/favicon.ico" rel="shortcut icon" type="image/ico" />
  <title><?php echo $page_title; ?> | <?php echo $this->config->item('app_name'); ?></title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/images/favicon.ico" />

  <!-- font -->
  <link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,500,500i,600,700,800,900|Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800">

  <!-- Plugins -->
  <link rel="stylesheet" type="text/css" href="assets/css/plugins-css.css" />

  <!-- revoluation -->
  <link rel="stylesheet" type="text/css" href="assets/revolution/css/settings.css" media="screen" />

  <!-- Typography -->
  <link rel="stylesheet" type="text/css" href="assets/css/typography.css" />

  <!-- Shortcodes -->
  <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css" />

  <!-- Style -->
  <link rel="stylesheet" type="text/css" href="assets/css/style.css" />

  <!-- shop -->
  <link href="assets/css/shop.css" rel="stylesheet" type="text/css" />

  <!-- Responsive -->
  <link rel="stylesheet" type="text/css" href="assets/css/responsive.css" /> 

  <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/fa96488ad1df6b133f7e5d5c2/7d9fd43d07a6529e38c35a7b8.js");</script>
  
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-55037416-9"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-55037416-9');
  </script>

<script type="text/javascript"> //<![CDATA[ 
var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
//]]>
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '801971850003468'); 
fbq('track', 'PageView');

<?php if($this->uri->segment(1) == 'Request-Quote'): ?>
fbq('track', 'RequestQuote');
<?php endif; ?>

<?php if($this->uri->segment(1) == 'Past-Exam-Solution'): ?>
fbq('track', 'PastExamSolution');
<?php endif; ?>

<?php if($this->uri->segment(1) == 'Free-Project-Proposals'): ?>
fbq('track', 'FreeProjectProposals');
<?php endif; ?>

</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=801971850003468&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

</head>

<body>

  <div class="wrapper">

<!--=================================
 preloader -->
 
<!--  <div id="pre-loader">
  <img src="assets/images/pre-loader/loader-01.svg" alt="">
</div> -->

<!--=================================
 preloader -->

 
<!--=================================
 header -->

 <header id="header" class="header transparent">
  <div class="topbar">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">
          <div class="topbar-call text-left">
            <ul>
              <li><i class="fa fa-envelope-o theme-color"></i> tutor@academicianhelp.co.uk</li>
              <li><i class="fa fa-phone"></i> <a href="tel:+442039502729"> <span>+442039502729</span> </a> </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
          <div class="topbar-social text-right">
            <ul>
              <?php if(is_logged_in()): ?>
                <li><a href="dashboard"><mark><span class="ti-home"></span> Dashboard</mark></a></li>
                <li><a href="logout"><span class="ti-shift-left"></span> Logout</a></li>
              <?php else: ?>
                <li><a href="register"><span class="ti-user"></span> Register</a></li>
                <li><a href="login"><span class="ti-shift-right"></span> Sign In</a></li>
              <?php endif; ?>
              <li><a href="https://www.facebook.com/academicianhelp" target="_blank"><span class="ti-facebook"></span></a></li>
              <li><a href="https://twitter.com/AcademicianHelp" target="_blank"><span class="ti-twitter"></span></a></li>
            <!-- <li><a href="#"><span class="ti-instagram"></span></a></li>
            <li><a href="#"><span class="ti-google"></span></a></li>
            <li><a href="#"><span class="ti-linkedin"></span></a></li>
            <li><a href="#"><span class="ti-dribbble"></span></a></li> -->
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<!--=================================
 mega menu -->

 <div class="menu">  
  <!-- menu start -->
  <nav id="menu" class="mega-menu">
    <!-- menu list items container -->
    <section class="menu-list-items">
     <div class="container"> 
      <div class="row"> 
       <div class="col-lg-12 col-md-12"> 
        <!-- menu logo -->
        <ul class="menu-logo">
          <li>
            <a href="<?php echo base_url(); ?>"><img id="logo_img" src="assets/images/logo.png" alt="logo"> </a>
          </li>
        </ul>
        <!-- menu links -->
        <div class="menu-bar">
         <ul class="menu-links">
           <li><a href="<?php echo base_url(); ?>">Home </a></li>
           <!-- <li class="active"><a href="./">Home </a></li> -->
           <li><a href="javascript:void(0)"> Services <i class="fa fa-angle-down fa-indicator"></i></a>
             <!-- drop down multilevel  -->
             <ul class="drop-down-multilevel">
              <li><a href="javascript:void(0)">Tutoring<i class="ti-plus fa-indicator"></i></a>
               <ul class="drop-down-multilevel">

                <li><a href="programming">Programming </a></li>
                <li><a href="Cloud-Computing">Cloud Computing </a></li>
                <li><a href="Software-Engineering">Software Engineering </a></li>
                <li><a href="Database-Management">Database Management </a></li>
                <li><a href="Formal-Design">Formal Design </a></li>
                <li><a href="Artificial-Intelligence">Artificial Intelligence </a></li>
                <li><a href="Web-Development">Web Development </a></li>
              </ul>
            </li>
            <li><a href="javascript:void(0)">Writing<i class="ti-plus fa-indicator"></i></a>
             <ul class="drop-down-multilevel">
              <li><a href="IT-Computer-Coursework">IT/Computer Coursework </a></li>
              <li><a href="IT-Computer-Dissertation">IT/Computer Dissertation </a></li>
              <li><a href="Management-Assignments-Essays">Management Assignments/Essays </a></li>
              <li><a href="Management-Dissertation">Management Dissertation </a></li>
              <li><a href="Law-Assignments-Essays">Law Assignments/Essays </a></li>
              <li><a href="Law-Dissertation">Law Dissertation </a></li>
            </ul>
          </li>
          <li><a href="Proof-Reading-Editing">Proof Reading &amp; Editing</a></li>
        </ul>
      </li>
      <li><a href="javascript:void(0)"> Company <i class="fa fa-angle-down fa-indicator"></i></a>
       <!-- drop down multilevel  -->
       <ul class="drop-down-multilevel">
        <li><a href="How-It-Works">How It Works </a></li>
        <li><a href="About-Us">About Us</a></li>
        <li><a href="Contact-Us">Contact Us</a></li>
        <li><a href="faq">FAQ</a></li>
        <li><a href="samples">Samples</a></li>
        <li><a href="Terms-And-Conditions">Terms &amp; Condition</a></li>
        <li><a href="testimonials">Testimonials </a></li>
      </ul>
    </li>
    <li><a href="Request-Quote">Request Quote </a></li>
    <!-- <li><a href="price-calculator">Price Calculator </a></li> -->
    <!-- <li><a href="javascript:void(0);" onclick="$('#pricecalculator_modal').modal('show');"">Price Calculator </a></li> -->
    <li><a href="javascript:void(0);" data-toggle="modal" data-target=".bs-example-modal-lg">Price Calculator </a></li>
    <li><a href="javascript:void(0)"> Solution Library <i class="fa fa-angle-down fa-indicator"></i></a>
      <ul class="drop-down-multilevel">
        <li><a href="Past-Exam-Solution">Past Exam Solution </a></li>
        <li><a href="Free-Project-Proposals">Free Project Proposals</a></li>
      </ul>
    </li>
    <li><a href="http://blog.academicianhelp.co.uk" target="_blank">Blog </a></li>
  </ul>
  <div class="search-cart">
    <!--
    <div class="search">
      <a class="search-btn not_click" href="javascript:void(0);"></a>
      <div class="search-box not-click">
        <input type="text" class="not-click form-control" placeholder="Search" value="" name="s" />
        <i class="fa fa-search"></i>
      </div>
    </div>
  -->
  <div class="shpping-cart">
    <?php $total = 0; ?>
    <a class="cart-btn" href="javascript:void(0)"> <i class="fa fa-shopping-cart icon"></i> <strong class="item" id="cartCount"><?php echo !is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart')) ? count($this->session->userdata('cart')) : 0; ?></strong></a>
    <div class="cart" id="cart">
      <div class="cart-title">
       <h6 class="uppercase mb-0">Shopping cart</h6>
     </div>
     <?php if(!is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart'))): ?>
      <?php foreach($this->session->userdata('cart') as $cart): ?>
        <?php $total += $cart['product_price']; ?>
        <div class="cart-item">
          <div class="cart-name clearfix">
            <a href="Past-Exam-Solution?id=<?php echo $cart['product_id']; ?>"><?php echo urldecode(ellipsize($cart['product_name'], 33)); ?></a>
            <div class="cart-price">
              <ins>&pound;<?php echo number_format($cart['product_price']); ?></ins>
            </div>
          </div>
          <div class="cart-close">
            <a href="javascript:void(0);" class="remove-from-cart" data-id="<?php echo $cart['product_id']; ?>"> <i class="fa fa-times-circle"></i> </a>
          </div>
        </div>
      <?php endforeach; ?>
    <?php else: ?>
      <div class="cart-item">
        <div class="cart-name clearfix">
          <a href="Past-Exam-Solution">Your cart is empty</a>
        </div>
      </div>
    <?php endif; ?>
    <div class="cart-total">
      <?php if(!is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart'))): ?>
        <h6 class="mb-15"> Total: &pound;<?php echo number_format($total); ?></h6>
        <a class="button" href="cart">View Cart</a>
        <a class="button black" href="checkout">Checkout</a>
      <?php else: ?>
        <a class="button" href="Past-Exam-Solution" id="#purchase-link">Purchase Items Now</a>
      <?php endif; ?>
    </div>
  </div>
</div>

</div>
</div>
</div>
</div>
</section>
</nav>
<!-- menu end -->
</div>
</header>

<!--=================================
 header -->