
<!--=================================
 banner -->

 <section class="rev-slider">
  <div id="rev_slider_11_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="webster-slider-1" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
    <!-- START REVOLUTION SLIDER 5.4.5.2 fullwidth mode -->
    <div id="rev_slider_11_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.5.2">
     <ul>  <!-- SLIDE  -->
      <li data-index="rs-24" data-transition="random-static,random-premium,random" data-slotamount="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off"  data-randomtransition="on" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-masterspeed="default,default,default,default"  data-thumb="assets/revolution/assets/slider-01/slide1_70x70.jpg"  data-rotate="0,0,0,0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
        <!-- MAIN IMAGE (revolution/assets/slider-01/1a353-01.jpg) -->
        <img src="assets/revolution/assets/slider-01/slide1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
        <!-- LAYERS -->
        <!-- LAYER NR. 1 -->
        <div class="tp-caption   tp-resizeme" 
        id="slide-24-layer-1" 
        data-x="68" 
        data-y="center" data-voffset="-30" 
        data-width="['auto']"
        data-height="['auto']"
        data-type="text" 
        data-responsive_offset="on" 
        data-frames='[{"delay":1000,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
        data-textAlign="['left','left','left','left']"
        data-paddingtop="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[0,0,0,0]"
        data-paddingleft="[0,0,0,0]"
        style="z-index: 5; white-space: nowrap; word-wrap: break-word; font-size: 40px; line-height: 40px; font-weight: 200; color: rgba(255,255,255,1); letter-spacing: px;font-family:Montserrat ;">Welcome to <strong>Academician Help</strong> <br/>a one stop website for all academic help &amp; tutoring <!-- with dissertation, assignment as well as tutoring for computer science, information technology, software engineering, management science and law students. --> </div>
        <!-- LAYER NR. 2 -->
        <div class="tp-caption   tp-resizeme  rev-color" 
        id="slide-24-layer-2" 
        data-x="right" data-hoffset="60" 
        data-y="center" data-voffset="-57" 
        data-width="['auto']"
        data-height="['auto']"
        data-type="text" 
        data-responsive_offset="on" 
        data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
        data-textAlign="['left','left','left','left']"
        data-paddingtop="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[0,0,0,0]"
        data-paddingleft="[0,0,0,0]"
        style="z-index: 6; white-space: nowrap; font-size: 150px; line-height: 150px; font-weight: 600; color: #84ba3f; letter-spacing: px;font-family:Dosis;"><!-- Academician Help --> </div>
        <!-- LAYER NR. 3 -->
        <div class="tp-caption   tp-resizeme" 
        id="slide-24-layer-5" 
        data-x="center" data-hoffset="-360" 
        data-y="center" data-voffset="80" 
        data-width="['auto']"
        data-height="['auto']"
        data-type="text" 
        data-responsive_offset="on" 
        data-frames='[{"delay":3500,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
        data-textAlign="['left','left','left','left']"
        data-paddingtop="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[0,0,0,0]"
        data-paddingleft="[0,0,0,0]"
        style="z-index: 7; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); letter-spacing: px;font-family:Montserrat ;text-transform:uppercase;"><i class="fa-icon-check"></i> Final year projects  &nbsp; &nbsp;&nbsp; | </div>
        <!-- LAYER NR. 4 -->
        <div class="tp-caption   tp-resizeme" 
        id="slide-24-layer-7" 
        data-x="center" data-hoffset="-139" 
        data-y="center" data-voffset="79" 
        data-width="['auto']"
        data-height="['auto']"
        data-type="text" 
        data-responsive_offset="on" 
        data-frames='[{"delay":4000,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
        data-textAlign="['left','left','left','left']"
        data-paddingtop="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[0,0,0,0]"
        data-paddingleft="[0,0,0,0]"
        style="z-index: 8; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); letter-spacing: px;font-family:Montserrat ;text-transform:uppercase;"><i class="fa-icon-check"></i> Essays  &nbsp; &nbsp;&nbsp; | </div>
        <!-- LAYER NR. 5 -->
        <div class="tp-caption   tp-resizeme" 
        id="slide-24-layer-8" 
        data-x="center" data-hoffset="30" 
        data-y="center" data-voffset="80" 
        data-width="['auto']"
        data-height="['auto']"
        data-type="text" 
        data-responsive_offset="on" 
        data-frames='[{"delay":4500,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
        data-textAlign="['left','left','left','left']"
        data-paddingtop="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[0,0,0,0]"
        data-paddingleft="[0,0,0,0]"
        style="z-index: 9; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); letter-spacing: px;font-family:Montserrat ;text-transform:uppercase;"><i class="fa-icon-check"></i> Coursework  &nbsp; &nbsp;&nbsp; | </div>

        <!-- LAYER NR. 6 -->
        <div class="tp-caption   tp-resizeme" 
        id="slide-24-layer-9" 
        data-x="center" data-hoffset="220" 
        data-y="center" data-voffset="79" 
        data-width="['auto']"
        data-height="['auto']"
        data-type="text" 
        data-responsive_offset="on" 
        data-frames='[{"delay":5000,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
        data-textAlign="['left','left','left','left']"
        data-paddingtop="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[0,0,0,0]"
        data-paddingleft="[0,0,0,0]"
        style="z-index: 10; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); letter-spacing: ;font-family:Montserrat ;text-transform:uppercase;"><i class="fa-icon-check"></i> Assignment  &nbsp; &nbsp;&nbsp; | </div>
        <!-- LAYER NR. 7 -->
        <div class="tp-caption   tp-resizeme" 
        id="slide-24-layer-10" 
        data-x="center" data-hoffset="400" 
        data-y="center" data-voffset="80" 
        data-width="['auto']"
        data-height="['auto']"
        data-type="text" 
        data-responsive_offset="on" 
        data-frames='[{"delay":5500,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
        data-textAlign="['left','left','left','left']"
        data-paddingtop="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[0,0,0,0]"
        data-paddingleft="[0,0,0,0]"
        style="z-index: 11; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); letter-spacing: ;font-family:Montserrat ;text-transform:uppercase;"><i class="fa-icon-check"></i> Dissertation </div>
        <!-- LAYER NR. 8 -->
        <div class="tp-caption rev-btn  tp-resizeme  rev-button" onclick="location.href='Request-Quote';"
        id="slide-24-layer-15" 
        data-x="center" data-hoffset="" 
        data-y="center" data-voffset="160" 
        data-width="['auto']"
        data-height="['auto']"
        data-type="button" 
        data-responsive_offset="on" 
        data-frames='[{"delay":6000,"speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power0.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(0,0,0);bs:solid;bw:0 0 0 0;"}]'
        data-textAlign="['inherit','inherit','inherit','inherit']"
        data-paddingtop="[12,12,12,12]"
        data-paddingright="[35,35,35,35]"
        data-paddingbottom="[12,12,12,12]"
        data-paddingleft="[35,35,35,35]"
        style="z-index: 12; white-space: nowrap; font-size: 12px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); letter-spacing: px;font-family:Montserrat ;text-transform:uppercase;background-color:rgb(132,186,63);border-color:rgba(0,0,0,1);border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Get Quote </div>
    </li>
    <!-- SLIDE  -->
    <li data-index="rs-23" data-transition="random-static,random-premium,random" data-slotamount="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off"  data-randomtransition="on" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-masterspeed="default,default,default,default"  data-thumb="assets/revolution/assets/slider-01/slide8_70x70.jpg"  data-rotate="0,0,0,0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
        <!-- MAIN IMAGE -->
        <img src="assets/revolution/assets/slider-01/slide8.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
        <!-- LAYERS -->
        <!-- LAYER NR. 9 -->
        <div class="tp-caption   tp-resizeme" 
        id="slide-23-layer-2" 
        data-x="60" 
        data-y="340" 
        data-width="['auto']"
        data-height="['auto']"
        data-type="text" 
        data-responsive_offset="on" 
        data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
        data-textAlign="['left','left','left','left']"
        data-paddingtop="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[0,0,0,0]"
        data-paddingleft="[0,0,0,0]"
        style="z-index: 5; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 300; color: rgba(255,255,255,1); letter-spacing: px;font-family:Montserrat ;">Are You <strong>Ready</strong> to  </div>
        <!-- LAYER NR. 10 -->
        <div class="tp-caption   tp-resizeme" 
        id="slide-23-layer-7" 
        data-x="60" 
        data-y="411" 
        data-width="['auto']"
        data-height="['auto']"
        data-type="text" 
        data-responsive_offset="on" 
        data-frames='[{"delay":1080,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
        data-textAlign="['left','left','left','left']"
        data-paddingtop="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[0,0,0,0]"
        data-paddingleft="[0,0,0,0]"
        style="z-index: 6; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Montserrat ;">Ace your <strong>Assignments, Project work</strong>? </div>

        <!-- LAYER NR. 11 -->
        <div class="tp-caption   tp-resizeme" 
        id="slide-23-layer-3" 
        data-x="60" 
        data-y="center" data-voffset="85" 
        data-width="['auto']"
        data-height="['auto']"
        data-type="text" 
        data-responsive_offset="on" 
        data-frames='[{"delay":2090,"speed":1500,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
        data-textAlign="['left','left','left','left']"
        data-paddingtop="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[0,0,0,0]"
        data-paddingleft="[0,0,0,0]"
        style="z-index: 7; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 200; color: rgba(255,255,255,1); letter-spacing: px;font-family: Montserrat ;">The quality of our work and pricing are unbeatable,
        <br/> you will be glad you used our services. </div>

        <!-- LAYER NR. 12 -->
        <div class="tp-caption rev-btn  tp-resizeme  rev-button" onclick="location.href='Request-Quote';" 
        id="slide-23-layer-12" 
        data-x="60" 
        data-y="center" data-voffset="180" 
        data-width="['auto']"
        data-height="['auto']"
        data-type="button" 
        data-responsive_offset="on" 
        data-frames='[{"delay":3080,"speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power0.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(0,0,0);bc:rgb(0,0,0);bs:solid;bw:0 0 0 0;"}]'
        data-textAlign="['inherit','inherit','inherit','inherit']"
        data-paddingtop="[12,12,12,12]"
        data-paddingright="[30,30,30,30]"
        data-paddingbottom="[12,12,12,12]"
        data-paddingleft="[30,30,30,30]"
        style="z-index: 8; white-space: nowrap; font-size: 12px; line-height: 22px; font-weight: 700; color: #ffffff; letter-spacing: px;font-family:Montserrat ;text-transform:uppercase;background-color:rgb(132,186,63);border-color:rgba(0,0,0,1);border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Get Quote Now </div>
        <!-- LAYER NR. 13 -->
        <div class="tp-caption rev-btn  tp-resizeme" onclick="location.href='testimonials';" 
        id="slide-23-layer-13" 
        data-x="235" 
        data-y="center" data-voffset="180" 
        data-width="['auto']"
        data-height="['auto']"
        data-type="button" 
        data-responsive_offset="on" 
        data-frames='[{"delay":3560,"speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power0.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,1);"}]'
        data-textAlign="['inherit','inherit','inherit','inherit']"
        data-paddingtop="[10,10,10,10]"
        data-paddingright="[30,30,30,30]"
        data-paddingbottom="[10,10,10,10]"
        data-paddingleft="[30,30,30,30]"
        style="z-index: 9; white-space: nowrap; font-size: 12px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); letter-spacing: px;font-family:Montserrat ;text-transform:uppercase;background-color:rgba(0,0,0,0);border-color:rgb(255,255,255);border-style:solid;border-width:2px 2px 2px 2px;border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Our Success Stories </div>
    </li>
</ul>
<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>
</div>
</section>

<!--=================================
 banner -->


<!--=================================
 special-feature --> 

 <section class="page-section-ptb position-re">
   <div class="container">
     <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="section-title text-center">
          <h6>Many reasons to</h6>
          <h2 class="title-effect">Why Choose Academician Help?</h2>
          <!-- <p>Webster's ultimate easy to use and customizable UI elements make it most customizable template on the market. </p> -->
      </div>

      <div class="row">   
          <div class="col-lg-12 col-md-12">
            <div class="tab tab-border nav-center">
              <ul class="nav nav-tabs">
               <li class="active"><a href="#research-05" data-toggle="tab"> <i class="fa fa-check-square-o"></i> Plagiarism Free Service</a></li>
               <li><a href="#design-05" data-toggle="tab"> <i class="fa fa-lock"></i> Confidentiality Guaranteed</a></li>
               <li><a href="#develop-05" data-toggle="tab"> <i class="fa fa-envelope-o"></i> 24/7 Email</a></li>
               <li><a href="#direct-05" data-toggle="tab"> <i class="fa fa-sign-in"></i> Direct Access</a></li>
               <li><a href="#flexible-05" data-toggle="tab"> <i class="fa fa-money"></i> Flexible Payment</a></li>
           </ul>
           <div class="tab-content">
              <div class="tab-pane fade in active" id="research-05">
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="feature-text left-icon mb-80">
                    <div class="feature-icon">
                      <span class="ti-check-box theme-color" aria-hidden="true"></span>
                  </div>
                  <div class="feature-info">
                      <h5>Plagiarism Free Service</h5>
                  </div>
              </div>
          </div>
          <p>We pass your order through plagiarism checkers like Grammarly to ensure that your assignment plagiarism count is at the acceptable level, usually 10% for a final year project and less for shorter report.</p>
      </div>
      <div class="tab-pane fade" id="design-05">
        <div class="col-lg-6 col-md-6 col-sm-6">
          <div class="feature-text left-icon mb-80">
            <div class="feature-icon">
              <span class="ti-lock theme-color" aria-hidden="true"></span>
          </div>
          <div class="feature-info">
              <h5>Confidentiality Guaranteed</h5>
          </div>
      </div>
  </div>
  <p>Be rest assured that your privacy will not be breached. We do not divulge any information about you to anyone working with us.</p>
</div> 
<div class="tab-pane fade" id="develop-05">
    <div class="col-lg-6 col-md-6 col-sm-6">
      <div class="feature-text left-icon mb-80">
        <div class="feature-icon">
          <span class="ti-email theme-color" aria-hidden="true"></span>
      </div>
      <div class="feature-info">
          <h5>24/7 Email</h5>
      </div>
  </div>
</div>
<p>Our emails are working round the clock, be rest assured that you can send us an email at any time of the day and expect a reply between minutes to the next few hours.</p>
</div>
<div class="tab-pane fade" id="direct-05">
    <div class="col-lg-6 col-md-6 col-sm-6">
      <div class="feature-text left-icon mb-80">
        <div class="feature-icon">
          <span class="ti-shift-right theme-color" aria-hidden="true"></span>
      </div>
      <div class="feature-info">
          <h5>Direct Access to Experts</h5>
      </div>
  </div>
</div>
<p>Get direct access to our experts and build a personal relationship with them.</p>
</div>
<div class="tab-pane fade" id="flexible-05">
    <div class="col-lg-6 col-md-6 col-sm-6">
      <div class="feature-text left-icon mb-80">
        <div class="feature-icon">
          <span class="ti-money theme-color" aria-hidden="true"></span>
      </div>
      <div class="feature-info">
          <h5>Flexible Payment Option</h5>
      </div>
  </div>
</div>
<p>For projects that are more than 20 days, we allow a flexible payment structure to be determined with your expert.</p>
</div>
</div> 
</div>
</div>
</div>
</div>
</div>
</div> 
</section>

<!--=================================
 special-feature --> 

 <!--=================================
   awesome-features  -->

   <section class="awesome-features gray-bg page-section-ptb pos-r">
    <div class="container">
     <div class="row">     
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="section-title text-center">
          <h6>How we are the best! </h6>
          <h2 class="title-effect">How Our Service Works</h2>
          <!-- <p>Truly ideal solutions for your business. Create a website that you are gonna be proud of. </p> -->
      </div>
      <div class="row">  
          <div class="col-lg-12 col-md-12">
            <div class="tab border nav-center">
              <ul class="nav nav-tabs">
               <li class="active"><a href="#research-07" data-toggle="tab"> <i class="fa fa-file-word-o"></i> Upload Work</a></li>
               <li><a href="#design-07" data-toggle="tab"> <i class="fa fa-calculator"></i> Await Estimate</a></li>
               <li><a href="#develop-07" data-toggle="tab"> <i class="fa fa-money"></i> Half Payment</a></li>
               <li><a href="#result-07" data-toggle="tab"> <i class="fa fa-file"></i> Request Draft</a></li>
               <li><a href="#result-08" data-toggle="tab"> <i class="fa fa-check-square-o"></i> Work is Ready</a></li>
               <li><a href="#result-09" data-toggle="tab"> <i class="fa fa-refresh"></i> Amendments</a></li>
           </ul>
           <div class="tab-content">
              <div class="tab-pane fade in active" id="research-07">
                <div class="col-lg-12 col-md-12 col-sm-12 mb-60">
                  <!-- <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 mb-60"> -->
                    <!-- <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8"> -->
                      <div class="feature-text left-icon round border">
                        <div class="feature-icon">
                          <span class="ti-file"></span>
                      </div>
                      <div class="feature-info">
                          <!-- <h5 class="text-back">Many Style Available</h5> -->
                          <p>First place an order with the relevant information regarding your assignment, tutoring  or dissertation.</p>
                      </div>
                  </div>
              </div>
          </div>
          <div class="tab-pane fade" id="design-07">
            <div class="col-lg-12 col-md-12 col-sm-12 mb-60">
              <div class="feature-text left-icon round border">
                <div class="feature-icon">
                  <span class="ti-panel"></span>
              </div>
              <div class="feature-info">
                  <p>After you have uploaded your work, our team of professional will evaluate your requirements and then give an estimated cost for the work.</p>
              </div>
          </div>
      </div>
  </div> 
  <div class="tab-pane fade" id="develop-07">
    <div class="col-lg-12 col-md-12 col-sm-12 mb-60">
      <div class="feature-text left-icon round border">
        <div class="feature-icon">
          <span class="ti-money"></span>
      </div>
      <div class="feature-info">
          <p>You will need to make at least a deposit of 50% before we start working on your order.</p>
      </div>
  </div>
</div>
</div>
<div class="tab-pane fade" id="result-07">
    <div class="col-lg-12 col-md-12 col-sm-12 mb-60">
      <div class="feature-text left-icon round border">
        <div class="feature-icon">
          <span class="ti-agenda"></span>
      </div>
      <div class="feature-info">
          <p>Our team will give you an update whenever you ask for it before deadline.</p>
      </div>
  </div>
</div>
</div>
<div class="tab-pane fade" id="result-08">
    <div class="col-lg-12 col-md-12 col-sm-12 mb-60">
      <div class="feature-text left-icon round border">
        <div class="feature-icon">
          <span class="ti-check-box"></span>
      </div>
      <div class="feature-info">
          <p>Once the work is ready, our team will contact you to make a final deposit of 50% and then send the work to you.</p>
      </div>
  </div>
</div>
</div>
<div class="tab-pane fade" id="result-09">
    <div class="col-lg-12 col-md-12 col-sm-12 mb-60">
      <div class="feature-text left-icon round border">
        <div class="feature-icon">
          <span class="ti-reload"></span>
      </div>
      <div class="feature-info">
          <p>If you think something is not done properly or does not meet the requirement or you simply want to change some information, we will be glad to do it at no cost.</p>
      </div>
  </div>
</div>
</div>
<div align="center">
    <a class="button border icon" href="Request-Quote">
      <span>Order Now</span>
      <i class="fa fa-sign-in"></i>
  </a>
</div>
</div> 
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<!--=================================
 awesome-features  -->


<!--=================================
 special-feature --> 

 <section class="page-section-ptb position-re">
   <div class="container">
     <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="section-title text-center">
          <h6>For your benefit</h6>
          <h2 class="title-effect">Tutoring, Writing, Editing &amp; Proof Reading Services</h2>
      </div>

      <div class="row">   
          <div class="col-lg-12 col-md-12">
            <p>Sometimes, student find it hard to grasp a course concept understand an assignment brief or lack the initiative of how to start. Here is where we can render our expertise. Our team of experts can teach various <a href="Software-Engineering">IT related modules</a>, as well as offer plagiarism free model report writing for <a href="Management-Assignments-Essays">assignments</a>, <a href="IT-Computer-Coursework">coursework</a> and <a href="IT-Computer-Dissertation">dissertations for Information technology, Computer Science, Software Engineer</a>, <a href="Management-Dissertation">Management Science</a>, and <a href="Law-Dissertation">Law related students</a>.  Our <a href="Proof-Reading-Editing">proof reading and editing services</a> ensures that your work is structured the write way and rid of grammatical errors.</p>
            <p>We have a track record of helping student achieve an A in these fields. Our range of student covers GCSE, A-Level, Undergraduate, Masters as well as PHD students. Kindly note that our model report and help are not to be submitted as your own work but only serve as a model to work with in writing your own report.</p>
            <p>We offer good competitive prices for our report writing services in comparison to other major website out there. You can get a rough estimate of what your order will cost by clicking on the <a href="javascript:void(0);" data-toggle="modal" data-target=".bs-example-modal-lg">pricing calculator </a> at the top menu. We are also deadline driven, our team of experts ensures that a solution is given to you on time, so be rest assured that your order will be delivered on time.</p>
        </div>
    </div>
</div>
</div>
</div> 
</section>

<!--=================================
 special-feature --> 


<!--=================================
 special-feature --> 

 <section class="page-section-ptb position-re theme-bg">
   <div class="container">
     <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="section-title text-center">
          <h6 class="text-white">Get Started</h6>
          <h2 class="text-white title-effect dark">Request Quote</h2>
      </div>


      <div class="row">
          <div class="col-sm-12">
           <div id="formmessage">Success/Error Message Goes Here</div>
           <?php echo form_open_multipart('Request-Quote/ajax', 'class="form-horizontal", id="contactform", role="form"'); ?>
           <!-- <form class="form-horizontal" id="contactform" role="form" method="post" action="Request-Quote/ajax" enctype="multipart/form-data"> -->
            <div class="contact-form dark-form clearfix">
             <div class="section-field">
                <input id="name" type="text" placeholder="Fullname" class="form-control"  name="name" />
            </div> 
            <div class="section-field">
               <input type="email" placeholder="Email*" class="form-control" name="email" />
           </div>
           <div class="section-field">
              <input type="text" placeholder="Phone" class="form-control" name="phone" />
          </div>
          <div class="section-field textarea">
             <textarea class="form-control input-message" placeholder="Relevant Details*" rows="7" name="message"></textarea>
         </div>
         <div class="section-field file">
            <input type="file" class="form-control" name="attachments[]" multiple="multiple" />
        </div>
        <div class="clearfix"></div>
        <div class="section-field">
            <!-- <p>Submit the word you see below:</p> -->
            <?php echo $captcha_image; ?>
            <input type="text" name="captcha" id="captcha" value="" placeholder="Enter Captcha*" class="form-control" />
        </div>
        <style type="text/css">.remember-checkbox label:before { border: 2px solid #ffffff; }</style>
        <div class="section-field" style="clear: both;">
            <div class="remember-checkbox mb-30">
                <input type="checkbox" class="form-control" id="terms" name="terms" style="border: 1px solid #ffffff;" />
                <label for="terms" style="color: #ffffff;">
                    <a href="Terms-And-Conditions" target="_blank" style="color: #ffffff;">Terms &amp; Conditions</a>
                </label>
            </div>
        </div>
        <div class="section-field submit-button" style="clear: both;">
           <input type="hidden" name="action" value="sendEmail"/>
           <button id="submit" name="submit" type="submit" value="Send" class="button white"> Request Quote <i class="fa fa-paper-plane"></i></button>
       </div>
   </div>
</form>
<div id="ajaxloader" style="display:none"><img class="center-block mt-30 mb-30" src="assets/images/pre-loader/loader-02.svg" alt=""></div>
</div>
</div>
</div>
</div>
</div> 
</section>

<!--=================================
 special-feature --> 




<!--=================================
 About-->

 <section class="service white-bg page-section-ptb">
  <div class="container">
    <div class="row">
     <div class="col-lg-8 col-md-8 col-md-offset-2">
       <div class="section-title text-center">
        <h6>Samples of work</h6>
        <h2 class="title-effect">see for yourself</h2>
        <p>Take Your Success To New Heights With AcademicianHelp.</p>
    </div>
</div>
</div>
<div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12">

    <div class="row">
        <!-- <div class="col-lg-12"></div> -->
        <div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30" title="Dissertation - An investigation into the integration of Three E-learning Open Source Software">
            <a href="assets/sample-docs/dissertation/Dissertation - An investigation into the integration of Three E-learning Open SOurce Software.pdf" download>
                <div class="team team-hover team-overlay text-center">
                    <div class="team-photo">
                        <img class="img-responsive center-block" src="assets/images/samples/open-source.png" alt="" /> 
                    </div>    
                    <div class="team-description"> 
                        <div class="team-info"> 
                            <h5 class="text-white">An Investigation into the i...</h5>
                            <span class="text-white">DOWNLOAD</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30" title="Dissertation - Student Accommodation System">
            <a href="assets/sample-docs/dissertation/Dissertation - Student Accommodation System.pdf" download>
                <div class="team team-hover team-overlay text-center">
                    <div class="team-photo">
                        <img class="img-responsive center-block" src="assets/images/samples/accomodation.jpg" alt="" /> 
                    </div>    
                    <div class="team-description"> 
                        <div class="team-info"> 
                            <h5 class="text-white">Student Accommodation System</h5>
                            <span class="text-white">DOWNLOAD</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30" title="Management - Essay">
            <a href="assets/sample-docs/management/Management - Essay.pdf" download>
                <div class="team team-hover team-overlay text-center">
                    <div class="team-photo">
                        <img class="img-responsive center-block" src="assets/images/samples/essay.jpg" alt=""> 
                    </div>    
                    <div class="team-description"> 
                        <div class="team-info"> 
                            <h5 class="text-white">Essay</h5>
                            <span class="text-white">DOWNLOAD</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30" title="Law - Skeleton Argument for Junior Counsel for The Respondent">
            <a href="assets/sample-docs/law/Law - SKELETON ARGUMENT FOR JUNIOR COUNSEL FOR THE RESPONDENT.pdf" download>
                <div class="team team-hover team-overlay text-center">
                    <div class="team-photo">
                        <img class="img-responsive center-block" src="assets/images/samples/argument.jpg" alt=""> 
                    </div>    
                    <div class="team-description"> 
                        <div class="team-info"> 
                            <h5 class="text-white">Skeleton Argument ...</h5>
                            <span class="text-white">DOWNLOAD</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div> 
    <div align="center">
        <br /><br />
        <a class="button border icon" href="samples">
            <span>View More Free Samples</span>
            <i class="fa fa-sign-in"></i>
        </a>
    </div>
</div>
</div>
</div>
</section>

<!--=================================
 About-->


 <!--=================================
   Our Testimonial -->

   <section class="page-section-ptb bg-overlay-black-80 parallax" style="background-image:url(assets/images/bg/bg1.jpg);">
     <div class="container">
      <div class="row">
       <div class="col-md-8 col-md-offset-2"><div class="owl-carousel" data-nav-dots="true" data-items="1" data-md-items="1" data-sm-items="1">
        <div class="item"><div class="testimonial light">
          <!-- <div class="testimonial-avatar"> <img alt="" src="assets/images/team/01.jpg"> </div> -->
          <div class="testimonial-info"> Ade of AcademicianHelp is an excellent tutor and writer, he is very enthusiastic, patience, calm and caring. Nothing is ever too much for him and I feel extremely lucky to have him as a tutor.</div>
          <div class="author-info"> <strong>Shirley - <span>Tutoring</span></strong> </div>
      </div>
  </div>
  <div class="item">
     <div class="testimonial light">
      <!-- <div class="testimonial-avatar"> <img alt="" src="assets/images/team/03.jpg"> </div> -->
      <div class="testimonial-info"> I can tell you are passionate about your work. That passion makes the BEST writer/teachers/tutors. I really liked your enthusiasm! You made materials seem interesting no matter what it was.</div>
      <div class="author-info"> <strong>Goitsomone - <span>Writing</span></strong> </div>
  </div>
</div>
<div class="item">
   <div class="testimonial light">
    <!-- <div class="testimonial-avatar"> <img alt="" src="assets/images/team/02.jpg"> </div> -->
    <div class="testimonial-info"> Superb tutor and extremely knowledgeable of Java. I would highly recommend this tutor to anyone who needs help with Java. Very honest, patient and flexible person to meet your tutoring needs.</div>
    <div class="author-info"> <strong>Brad - <span>Tutoring</span></strong> </div>
</div>
</div>
<div class="item">
 <div class="testimonial light">
  <!-- <div class="testimonial-avatar"> <img alt="" src="assets/images/team/03.jpg"> </div> -->
  <div class="testimonial-info"> This teacher has been a blessing for my academic life. I struggled keeping up with the speed of my course. We had to develop an individual project in a very short period of time that I thought was quite challenging.</div>
  <div class="author-info"> <strong>Adrian - <span>Project Work</span></strong> </div>
</div>
</div>
<div class="item">
 <div class="testimonial light">
  <!-- <div class="testimonial-avatar"> <img alt="" src="assets/images/team/03.jpg"> </div> -->
  <div class="testimonial-info"> Aisvarya of Academicianhelp is awesome, and 100% genuine person to talk to, I found her very pleasant when we’re talking in some detail over the phone. Aisvarya knowledge in computing is very vast, so I am very sure if anybody is looking for a personal tutor.</div>
  <div class="author-info"> <strong>Jez - <span>Tutoring</span></strong> </div>
</div>
</div>
<div class="item">
 <div class="testimonial light">
  <!-- <div class="testimonial-avatar"> <img alt="" src="assets/images/team/03.jpg"> </div> -->
  <div class="testimonial-info"> AcademicianHelp has a set of talented, professional, and knowledgeable tutor. They have good understanding of their student's abilities and they are able to adapt their write up and teaching to suit their individual learning needs.</div>
  <div class="author-info"> <strong>Osama - <span>Writing</span></strong> </div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>   

 <!--=================================
   Our Testimonial -->


<!--=================================
  frequently  -->

  <section class="theme-bg pos-r page-section-ptb">
   <div class="container">
    <div class="row">
     <div class="col-sm-6">
       <div class="section-title">
        <h6 class="text-white">Frequently asked questions</h6>
        <h2 class="text-white title-effect dark">Have a Question?</h2>
        <p class="text-white">First look at these three.</p>
    </div>
    <p class="text-white">Click below to get answers to your likely questions.</p>
    <a class="button border white mt-20" href="faq"> View all </a>
</div>
<div class="col-sm-6">
  <div class="accordion animated dark-bg">
    <div class="acd-group acd-active">
      <a href="#" class="acd-heading acd-active">01. How do I know that your work will meet your guarantees?</a>
      <div class="acd-des text-white">Be rest assured that our team of experts have several years of experience in the field they manage. All of them have a 2.1 and above and a Master’s degree in their area of specialty. Your work is also cross checked by our quality control team to ensure that it meets the required standard. If for any reason the work does not meet your specification, we will amend it for free if it we are notified on or before a week’s time.</div>
  </div>
  <div class="acd-group">
      <a href="#" class="acd-heading">02. What are the areas your researchers cover?</a>
      <div class="acd-des text-white">Our experts are skilled in tutoring of IT/Computer Science related modules as well as writing services which includes: essay writing and coursework help in the IT/Computer Science, Management, or Law fields. We can also help with final year project, masters’ dissertation or PHD thesis in these fields.</div>
  </div>
  <div class="acd-group">
      <a href="#" class="acd-heading">03. What happens if I think there a mistake in the report that needs to be corrected?</a>
      <div class="acd-des text-white">If for any reason the work does not meet your specification, we will amend it for free when notified and we find your complaint valid.</div>
  </div>
</div>
</div>
</div>
</div>   
</section>

<!--=================================
  frequently  -->

 <!--=================================
  raindrops -->

  <section id="raindrops" class="raindrops" style="height: 50px;"></section>

 <!--=================================
  raindrops  -->

<!--=================================
 action-box -->

 <section class="contact-box contact-box-top theme-bg">
  <div class="container">
    <div class="row pt-20 pb-40">
      <div class="col-lg-4 col-sm-4 xs-mb-30">
        <div class="contact-box">
          <div class="contact-icon">
            <i class="ti-map text-white"></i>
        </div>
        <div class="contact-info">
            <h5 class="text-white"> 3 Woodward Close, Grays</h5>
            <span class="text-white">Essex, RM17 5RP</span>
        </div>
    </div>
</div>
<div class="col-lg-4 col-sm-4 xs-mb-30">
    <div class="contact-box">
      <div class="contact-icon">
        <i class="ti-headphone text-white"></i>
    </div>
    <div class="contact-info">
        <h5 class="text-white">+447786074967</h5>
        <span class="text-white">Mon-Fri 8:30am-6:30pm</span>
    </div>
</div>
</div>
<div class="col-lg-4 col-sm-4">
    <div class="contact-box">
      <div class="contact-icon">
        <i class="ti-email text-white"></i>
    </div>
    <div class="contact-info">
        <h5 class="text-white">tutor@academicianhelp.co.uk</h5>
        <span class="text-white">24 X 7 online support</span>
    </div>
</div>
</div>
</div>
</div>
</section>

<!--=================================
 action-box -->

<!--=================================
  google-map -->
  
  <section class="google-map black-bg">
   <div class="container">
     <div class="row">
       <div class="col-lg-12">
        <div class="map-icon">
        </div>
    </div>
</div>
</div>
<div class="map-open">
   <div style="width: 100%; height: 300px;" id="map" class="g-map" data-type='black'></div>
</div>
</section>

<!--=================================
  google-map -->

  <?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html> 