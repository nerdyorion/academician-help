
<!--=================================
page-title-->

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
  <div class="container">
    <div class="row"> 
      <div class="col-lg-12"> 
      <div class="page-title-name">
          <h1>Login</h1>
          <p>We know the secret of your success</p>
        </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Login</span> </li>
       </ul>
     </div>
     </div>
  </div>
</section>

<!--=================================
page-title -->


<!--=================================
 login-->

<section class="white-bg page-section-ptb">
  <div class="container">
    <div class="row">
       <div class="col-lg-12 col-md-12">
        <div class="section-title text-center">
            <h6 class="">Log in with your id</h6>
            <h2 class="title-effect">Login to your Account</h2>
        <?php if($error_code == 0 && !empty($error)): ?>
          <div class="alert alert-success alert-dismissable fade in">
            <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?php echo $error; ?>
          </div>
        <?php elseif($error_code == 1 && !empty($error)): ?>
          <div class="alert alert-danger alert-dismissable fade in">
            <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> <?php echo $error; ?>
          </div>
        <?php else: ?>
        <?php endif; ?>
          </div>
      </div>
    </div>
     <div class="row">
       <div class="col-md-6 col-md-offset-3">
        <div class="pb-50 clearfix">
        <?php echo form_open('/login/secure', 'class="form-horizontal", onsubmit="return validate();"'); ?>
        <input type="hidden" name="return" value="<?php echo isset($_GET['return']) ? trim($_GET['return']) : ''; ?>" />
         <div class="section-field mb-20">
             <label class="mb-10" for="email">Email* </label>
               <input id="email" class="web form-control" type="email" placeholder="e.g johndoe@yahoo.com" name="email" />
            </div>
            <div class="section-field mb-20">
             <label class="mb-10" for="Password">Password* </label>
               <input id="Password" class="Password form-control" type="password" placeholder="***********" name="password" />
            </div>
            <div class="section-field">
              <div class="remember-checkbox mb-30">
                 <!-- <input type="checkbox" class="form-control" name="two" id="two" />
                 <label for="two"> Remember me</label> -->
                 <a href="Forgot-Password" class="pull-right">Forgot Password?</a>
                </div>
              </div>
              <button class="button" type="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span>Log in</span> <i class='fa fa-check'></i>">
                <span>Log in</span>
                <i class="fa fa-check"></i>
             </button> 
             <p class="mt-20 mb-0">Don't have an account? <a href="register"> Create one here</a></p>
           </form>
          </div>
           <hr />
          <!-- <div class="login-social mt-50 text-center clearfix">
            <h4 class="theme-color mb-30">Login with Social media</h4>
            <ul>
                <li><a class="fb" href="#"><i class="fa fa-facebook"></i> Log in Facebook</a></li>
                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i> Log in Twitter</a></li>
                <li><a class="pinterest" href="#"><i class="fa fa-pinterest-p"></i> Log in google+</a></li>
            </ul>
          </div> -->
        </div>
      </div>
  </div>
</section>

<!--=================================
 login-->


<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->


  <?php $this->load->view('footer'); echo "\n"; ?>

  <script type="text/javascript">
    function validate()
    {
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      return true;
    }
  </script>

</body>
</html>