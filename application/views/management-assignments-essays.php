
<!--=================================
  page-title-->

  <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
    <div class="container">
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="page-title-name">
            <h1>Management Assignments/Essays</h1>
            <p>We know the secret of your success</p>
          </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a href="#">Services</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a href="#">Writing</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Management Assignments/Essays</span> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<!--=================================
  page-title -->

<!--=================================
 faq-->

 <section class="faq white-bg page-section-ptb">
  <div class="container">
    <div class="row">
     <div class="col-lg-12 col-md-12">
       <div class="section-title text-center">
        <h6>Get the best!</h6>
        <h2 class="title-effect">Management Assignments/Essays Writing Help Online</h2>
        <p>This service provides management assignment/essay help in project management, marketing, operational management, human resource assignments, supply chain management assignments and other business management modules.</p>
      </div>
    </div>
  </div>
  <div class="row"> <div class="col-lg-12 col-md-12">
    <div class="tab border nav-center">
      <ul class="nav nav-tabs">
       <li class="active"><a href="#research-07" data-toggle="tab"> <i class="fa fa-file-word-o"></i> Topics We Handle</a></li>
       <li><a href="#design-07" data-toggle="tab"> <i class="fa fa-star"></i> Why Choose Us</a></li>
       <li><a href="#develop-07" data-toggle="tab"> <i class="fa fa-money"></i> Referral Program</a></li>
     </ul>
     <div class="tab-content">
      <div class="tab-pane fade in active" id="research-07">
        <!-- <div class="col-lg-12 col-md-12 col-sm-12 mb-60"> -->
          <div class="col-lg-offset-2 col-lg-10 col-md-offset-2 col-md-10 col-sm-offset-2 col-sm-10 mb-60">
            <!-- <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8"> -->
              <h4 class="mb-20">List of few most popular topics we handle</h4>
              <p class="mb-20">We are the known name in the United Kingdom, Sri Lanka, and Nigeria for management assignments/essays tutoring services</p>
              <div class="col-lg-12 col-md-12">
                <div class="col-lg-4 col-md-4 text-center">
                  <div class="team team-round full-border">
                    <div class="team-photo">
                      <img class="img-responsive center-block" src="assets/images/portfolio/mgt-essay.jpg" alt=""> 
                    </div>
                  </div>
                  <!-- <img class="img-responsive image-content" src="images/portfolio/teach3.jpg" alt=""> -->
                </div>
                <div class="col-lg-8 col-md-8">
                  <div class="col-sm-12">
                    <ul class="list list-unstyled">
                      <li><i class="fa fa-check"></i> <a href="Management-Dissertation">Management Dissertation </a></li>
                      <li><i class="fa fa-check"></i> <a href="Management-Assignments-Essays">Management Assignments/Essays </a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="design-07">
            <div class="col-lg-offset-2 col-lg-10 col-md-offset-2 col-md-10 col-sm-offset-2 col-sm-10 mb-60">
              <h4 class="mb-20">Why Choose AcademicianHelp to Help you</h4>
              <div class="col-lg-12 col-md-12">
                <div class="row">
                  <div class="col-lg-5 col-md-5 col-sm-5">
                   <div class="feature-text left-icon mt-30">
                    <div class="feature-icon">
                      <span class="ti-check-box theme-color" aria-hidden="true"></span>
                    </div>              
                    <div class="feature-info">
                      <h5 class="text-back">100% Plagiarism Free Service</h5>
                      <p>We pass your order through plagiarism checkers like Grammarly to ensure that your assignment plagiarism count is at the acceptable level, usually 10% for a final year project and less for shorter report.</p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1">
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5">
                  <div class="feature-text left-icon mt-30">
                    <div class="feature-icon">
                      <span class="ti-lock theme-color" aria-hidden="true"></span>
                    </div>              
                    <div class="feature-info">
                      <h5 class="text-back">Confidentiality Guaranteed</h5>
                      <p>Be rest assured that your privacy will not be breached. We do not divulge any information about you to anyone working with us.</p>
                    </div>
                  </div>
                </div>
              </div>
                <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5">
                  <div class="feature-text left-icon mt-30">
                    <div class="feature-icon">
                     <span class="ti-email theme-color" aria-hidden="true"></span>
                   </div>              
                   <div class="feature-info">
                    <h5 class="text-back">24/7 Email</h5>
                    <p>Our emails are working round the clock, be rest assured that you can send us an email at any time of the day and expect a reply between minutes to the next few hours.</p>
                  </div>
                </div>
              </div>
                <div class="col-lg-1 col-md-1 col-sm-1">
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5">
                  <div class="feature-text left-icon mt-30">
                    <div class="feature-icon">
                      <span class="ti-shift-right theme-color" aria-hidden="true"></span>
                    </div>              
                    <div class="feature-info">
                      <h5 class="text-back">Direct Access</h5>
                      <p>Get direct access to our experts and build a personal relationship with them.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5">
                  <div class="feature-text left-icon mt-30">
                    <div class="feature-icon">
                     <span class="ti-money theme-color" aria-hidden="true"></span>
                   </div>              
                   <div class="feature-info">
                    <h5 class="text-back">Flexible Payment Option</h5>
                    <p>For projects that are more than 20 days, we allow a flexible payment structure to be determined with your expert.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <div class="tab-pane fade" id="develop-07">
        <div class="col-lg-offset-2 col-lg-10 col-md-offset-2 col-md-10 col-sm-offset-2 col-sm-10 mb-60">
          <h4 class="mb-20">Suggest a client to us and earn 5% of their total order</h4>
          <p class="mb-20">Refer our website to your friend and you are guaranteed to earn 5% of their total order instantly.</p>
        </div>
      </div>

    <div align="center">
      <a class="button border icon" href="Request-Quote">
        <span>Order Now</span>
        <i class="fa fa-sign-in"></i>
      </a>
    </div>
    <div class="row">
      <div class="col-lg-12 text-center mt-40">
       <h6> What are you waiting for? Let's <a class="theme-color" href="Request-Quote">get started</a> </h6>
     </div>     
   </div>
   <div class="row">
    <div class="col-lg- col-md- col-sm-12 mb-30">
      <p>You might be stuck with your management assignment/essay or you simply do not know how to start, we are here to help you out, our team of management science expert are trained to write the best essay model for you.</p>
    </div>
  </div>
<!-- 
  <div align="center">
    <a class="button border icon" href="Request-Quote">
      <span>Order Now</span>
      <i class="fa fa-sign-in"></i>
    </a>
  </div> -->
</div> 
</div>
</div>
</div> 
<!-- 
<div class="row">
  <div class="col-lg-12 text-center mt-40">
   <h6> What are you waiting for? Let's <a class="theme-color" href="Request-Quote">get started</a> </h6>
 </div>     
</div> -->
</div> 
</section>

<!--=================================
 careers-->


 <!--=================================
   awesome-features  -->

   <section class="awesome-features gray-bg page-section-ptb pos-r">
    <div class="container">
     <div class="row">     
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="section-title text-center">
          <h6>How we are the best! </h6>
          <h2 class="title-effect">Consult experts for Management Assignments/Essay</h2>
          <!-- <p>Truly ideal solutions for your business. Create a website that you are gonna be proud of. </p> -->
        </div>
        <div class="row">  
          <div class="col-lg-12 col-md-12">
            <p>Our team of experts have a record of accomplishment in helping many students with their management assignments/essays whom have successfully completed them while also having a firm foundational grasp of what the work entails. Our experts can help with:</p>
            <ul class="list list-mark">
              <li> Management Science </li>
              <li> Business Studies </li>
              <li> Business Management </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
 awesome-features  -->


 <!--=================================
   awesome-features  -->

   <section class="awesome-features page-section-ptb pos-r">
    <div class="container">
     <div class="row">     
       <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="section-title text-center">
          <h6>We cover the main concepts </h6>
          <h2 class="title-effect">Major Management Assignments/Essay Topics</h2>
          <!-- <p>Truly ideal solutions for your business. Create a website that you are gonna be proud of. </p> -->
        </div>
        <div class="row">  
          <div class="col-lg-12 col-md-12">
            <p>Learn how to structure and write any assignment/essay</p>
            <ul class="list list-mark">
              <li> Project Management </li>
              <li> Marketing </li>
              <li> Operational Management </li>
              <li> Human Resource </li>
              <li> Supply Chain Management </li>
              <li> etc </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
 awesome-features  -->



<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->


  <?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html> 