<style type="text/css">
.sidebar-widgets-wrap .recent-item .recent-title a {
  color: #73a02e;
  font-weight: normal;
}
.sidebar-widgets-wrap .recent-item .recent-title a:hover {
  color: #333;
  /*color: #84ba3f;*/
  /*font-weight: bold;*/
}
</style>

<!--=================================
  page-title-->

  <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
    <div class="container">
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="page-title-name">
            <h1>Welcome <?php echo isset($_SESSION['user_full_name']) ? explode(" ", $_SESSION['user_full_name'])[0] : "User"?></h1>
            <p>We know the secret of your success</p>
          </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Dashboard</span> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<!--=================================
  page-title -->

 <!--=================================
   shop-06-sub-banner -->
<!-- 
   <section class="shop-06-sub-banner black-bg pt-40 pb-40">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 sm-mb-30">
          <div class="feature-text left-icon">
            <div class="feature-icon">
              <span class="ti-loop text-white"></span>
            </div>
            <div class="feature-info">
              <h6 class="pt-15 text-white">Satisfaction Guaranteed</h6>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 sm-mb-30">
          <div class="feature-text left-icon">
           <div class="feature-icon">
            <span class="ti-gift text-white"></span>
          </div>
          <div class="feature-info">
            <h6 class="pt-15 text-white">Payment Secure</h6>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6 xs-mb-30">
       <div class="feature-text left-icon">
         <div class="feature-icon">
          <span class="ti-user text-white"></span>
        </div>
        <div class="feature-info">
          <h6 class="pt-15 text-white">Online Support</h6>
        </div>
      </div>
    </div>
      </div>
    </div>
  </section>
-->
<!--=================================
  shop-06-sub-banner -->


<!--=================================
  side-nav-menu -->

  <section class="shop grid page-section-ptb  alerts-and-callouts">
    <div class="container">
      <div class="row">
        <?php $this->load->view('side-nav-menu'); echo "\n"; ?>
        <div class="col-lg-9 col-md-9 sm-mt-40">
          <div class="row">
           <div class="product listing">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="product-des text-left">
                <div class="product-info mt-20">
                  <h3>Orders</h3>
                  <img src="<?php echo base_url(); ?>assets/images/order-complete.png" class="img-responsive" style="max-height: 150px; float: left;" class="align-left" /><br /><br /><br />
                  <h4 class="text-left align-left"> Your order has been placed successfully. </h4><p><br/><a href='orders'>Proceed to download&raquo;</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
  side-nav-menu -->

<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->

  <?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html> 