<style type="text/css">
.sidebar-widgets-wrap .recent-item .recent-title a {
  color: #73a02e;
  font-weight: normal;
}
.sidebar-widgets-wrap .recent-item .recent-title a:hover {
  color: #333;
  /*color: #84ba3f;*/
  /*font-weight: bold;*/
}
</style>

<!--=================================
  page-title-->

  <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
    <div class="container">
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="page-title-name">
            <h1>Welcome <?php echo isset($_SESSION['user_full_name']) ? explode(" ", $_SESSION['user_full_name'])[0] : "User"?></h1>
            <p>We know the secret of your success</p>
          </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Dashboard</span> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<!--=================================
  page-title -->

<!--=================================
  side-nav-menu -->

  <section class="shop grid page-section-ptb  alerts-and-callouts">
    <div class="container">
      <div class="row">
        <?php $this->load->view('side-nav-menu'); echo "\n"; ?>
        <div class="col-lg-9 col-md-9 sm-mt-40">
          <div class="row">
           <div class="product listing">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="product-des text-left">
                <div class="product-info mt-20">

                <div class="section-title line-dabble">
                  <h4 class="title">Orders</h4>
                  <!-- <p> You are welcome to your dashboard; view orders, download purchased items, update profile.</p> -->
                </div>
                  <!-- <h3>Orders</h3>
                  <p> View orders and download items.</p> -->

                  <?php if(empty($rows)): ?>
                   <p>You have not made any orders yet! <br />
                       <a class="button small mt-0" href="Past-Exam-Solution">Continue Shopping</a>
                    </p>
                 <?php else: ?>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Product(s)</th>
                          <th>Price</th>
                          <th>Status</th>
                          <th>Date</th>
                          <th>Download</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach($rows as $row): ?>
                         <tr>
                          <td><?php echo $sn++; ?></td>
                          <td class="description">
                            <?php $products = explode('###', $row['items']); ?>
                            <ul>
                            <?php foreach($products as $item_array): ?>
                              <?php $item = explode('@@@', $item_array); ?>
                                <li><a href="Past-Exam-Solution?id=<?php echo $item[0]; ?>"><?php echo $item[1]; ?> (&pound;<?php echo $item[2]; ?>)</a></li>
                            <?php endforeach; ?>
                          </ul>
                            
                          </td>
                          <td class="price">&pound;<?php echo number_format($row['total_price']); ?></td>
                          <td class="price"><mark <?php echo $row['paid'] == 1 ? '' : 'style="background-color: #FF0800"'; ?>><?php echo $row['paid'] == 1 ? 'PAID' : 'UNPAID'; ?></mark></td>
                          <td class="price"><?php echo $row['date_created'] == "0000-00-00 00:00:00" || is_null($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                          <td class="price">
                            <?php if($row['paid'] == 1): ?>
                              <a href="orders/download/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Download" target="_blank"> <i class="fa fa-download text-inverse m-r-10"></i> </a> 
                            <?php endif; ?>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <?php endif; ?>
            <div class=" pull-right">

              <nav aria-label="...">
                <ul class="pagination">
                  <?php echo $links; ?>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</section>

<!--=================================
  side-nav-menu -->

<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->

  <?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html> 