<style type="text/css">
.sidebar-widgets-wrap .recent-item .recent-title a {
  color: #73a02e;
  font-weight: normal;
}
.sidebar-widgets-wrap .recent-item .recent-title a:hover {
  color: #333;
  /*color: #84ba3f;*/
  /*font-weight: bold;*/
}
</style>
<style type="text/css">
.dropdown.dropdown-lg .dropdown-menu {
  margin-top: -1px;
  padding: 6px 20px;
}
.input-group-btn .btn-group {
  display: flex !important;
}
.btn-group .btn {
  border-radius: 0;
  margin-left: -1px;
}
.btn-group .btn:last-child {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
}
.btn-group .form-horizontal .btn[type="submit"] {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}
.form-horizontal .form-group {
  margin-left: 0;
  margin-right: 0;
}
.form-group .form-control:last-child {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}

@media screen and (min-width: 768px) {
  #adv-search {
    width: 500px;
    margin: 0 auto;
  }
  .dropdown.dropdown-lg {
    position: static !important;
  }
  .dropdown.dropdown-lg .dropdown-menu {
    min-width: 500px;
  }
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: auto;
  border: 3px solid #73AD21;
  z-index: 1;
}
</style>

<!--=================================
  page-title-->

  <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
    <div class="container">
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="page-title-name">
            <h1>Past Examination Papers</h1>
            <p>We know the secret of your success</p>
          </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a href="#">Solution Library</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Past Examination Papers</span> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<!--=================================
  page-title -->

 <!--=================================
   shop-06-sub-banner -->

   <a name="addedToCartAlert" id="addedToCartAlert"></a>
   <section class="shop-06-sub-banner black-bg pt-40 pb-40">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 sm-mb-30">
          <div class="feature-text left-icon">
            <div class="feature-icon">
              <span class="ti-loop text-white"></span>
            </div>
            <div class="feature-info">
              <h6 class="pt-15 text-white">Satisfaction Guaranteed</h6>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 sm-mb-30">
          <div class="feature-text left-icon">
           <div class="feature-icon">
            <span class="ti-gift text-white"></span>
          </div>
          <div class="feature-info">
            <h6 class="pt-15 text-white">Payment Secure</h6>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6 xs-mb-30">
       <div class="feature-text left-icon">
         <div class="feature-icon">
          <span class="ti-user text-white"></span>
        </div>
        <div class="feature-info">
          <h6 class="pt-15 text-white">Online Support</h6>
        </div>
      </div>
    </div>
        <!-- <div class="col-lg-3 col-md-3 col-sm-6">
       <div class="newsletter francy text-center">
         <input class="form-control placeholder" type="text" placeholder="Email address" name="email1" value="">
          <div class="clear">
            <button type="submit" name="submitbtn" class="button form-button">Subscribe</button>
          </div>
          </div>
        </div> -->
      </div>
    </div>
  </section>

<!--=================================
  shop-06-sub-banner -->


<!--=================================
  shop -->

  <section class="shop grid page-section-ptb  alerts-and-callouts">
    <div class="container">
      <div class="row">
        <div class="bs-callout bs-callout-success text-center hidden" style="margin-top:-40px;">
          <h4 id="addedToCartName">-</h4>
          &pound;<span id="addedToCartPrice">-</span> <br />
          added to Cart <br />
          <a class="button small mt-0" href="checkout">Checkout</a>
        </div>
        <!--
      <div class="input-group" id="adv-search">
        <input type="text" name="artist_name" id="artist_name" value="<?php echo isset($_GET['name']) ? trim($_GET['name']) : ''; ?>" class="form-control" placeholder="Search for artist" />
        <div class="input-group-btn">
          <div class="btn-group" role="group">
            <div class="dropdown dropdown-lg">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Filter <span class="caret"></span></button>
              <div class="dropdown-menu dropdown-menu-right" role="menu">
                
                  <?php echo form_open('Past-Exam-Solution', 'class="form-horizontal", method="get", role="form"'); ?>
                  <div class="form-group">
                    <label for="filter">Filter by</label>
                    <select class="form-control" name="category" id="category">
                      <option value="0" <?php //echo !isset($_GET['category']) ? 'selected="selected"' : ''; ?>>All Categories</option>
                      <?php //foreach ($categories as $row): ?>
                        <option value="<?php //echo $row['id']; ?>" <?php $category = isset($_GET['category']) ? trim($_GET['category']) : '-'; //echo $category == $row['id'] ? 'selected="selected"' : ''; ?>><?php //echo $row['category_name']; ?></option>
                      <?php //endforeach; ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="from">Budget</label>
                    <div class="row col-md-12">
                      <div class="col-md-5">
                        <input class="form-control col-md-3" type="number" min="0" value="<?php //echo isset($_GET['from']) ? trim($_GET['from']) : '0'; ?>" step="50000" name="from" id="from" />
                      </div>
                      <div class="col-md-1">
                        to
                      </div>
                      <div class="col-md-5">
                        <input class="form-control col-md-3" type="number" min="0" value="<?php //echo isset($_GET['to']) ? trim($_GET['to']) : '100000000'; ?>" step="50000" name="to" id="to" />
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="location">Location</label>
                    <select class="form-control" name="location" id="location">
                      <option value="0" <?php echo !isset($_GET['location']) ? 'selected="selected"' : ''; ?>>All</option>
                      <option value="1" <?php echo isset($_GET['location']) && $_GET['location'] == "1" ? 'selected="selected"' : ''; ?>>Lagos</option>
                    </select>
                  </div>
                  <button type="submit" class="btn btn-primary" style="color: #ffffff;"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
              </div>
            </div>
            <button type="button" class="btn btn-primary" style="color: #ffffff;" onclick="search();"><i class="fa fa-search" aria-hidden="true"></i></button>
            <button type="button" class="btn btn-primary" style="color: #ffffff;" onclick="location.href = '<?php echo base_url(); ?>artists';"><i class="fa fa-refresh" aria-hidden="true"></i></button>
          </div>
        </div>
      </div>
    -->
    <div class="col-lg-3 col-md-3">
      <div class="sidebar-widgets-wrap">
       <div class="sidebar-widget mb-40">
        <h5 class="mb-20">search</h5>
        <div class="widget-search">
          <?php echo form_open('Past-Exam-Solution', 'class="form-horizontal", method="get", id="searchForm", onsubmit="return validate();"'); ?>
          <i class="fa fa-search" id="searchIcon"></i>
          <input type="search" id="title" name="title" value="<?php echo isset($_GET['title']) ? trim($_GET['title']) : ''; ?>" class="form-control placeholder" placeholder="Search....">
        </form>
      </div>
    </div>
    <div class="sidebar-widget mb-40">
      <h5 class="mb-20">Universities</h5>
      <div class="widget-link">
        <ul>
          <?php foreach ($universities as $row): ?>
            <li> <a href="Past-Exam-Solution?university_id=<?php echo $row['id']; ?>"> <i class="fa fa-angle-double-right"></i> <?php echo $row['name']; ?> </a></li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
    <div class="sidebar-widget mb-40">
      <h5 class="mb-20">Recent items</h5>
      <?php foreach ($recent_items as $row): ?>
        <div class="recent-item clearfix">
          <div class="recent-info">
            <div class="recent-title">
             <a href="Past-Exam-Solution?id=<?php echo $row['id']; ?>" class="text-success"><?php echo ellipsize($row['title'], 40); ?></a> 
           </div>
           <div class="recent-meta">
             <ul class="list-style-unstyled">
              <li class="color"><?php echo dashIfEmpty('&pound;' . number_format($row['price'])); ?></li>
            </ul>    
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>
</div>
<div class="col-lg-9 col-md-9 sm-mt-40">
  <div class="row" style="padding-left: 30px;">
    <?php echo form_open('Past-Exam-Solution', 'class="form-inline", method="get", role="form"'); ?>
    <div class="form-group">
      <label for="country_id">Country:</label>
      <select class="wide  mb-30" id="country_id" name="country_id">
        <option value="0" selected="selected">All</option>
        <?php foreach ($countries as $country): ?>
          <option value="<?php echo $country['id']; ?>" <?php $country_id = isset($_GET['country_id']) ? trim($_GET['country_id']) : '-'; echo $country_id == $country['id'] ? 'selected="selected"' : ''; ?>><?php echo $country['name']; ?></option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="form-group">
      <label for="university_id">University:</label>
      <select class="wide  mb-30" id="university_id" name="university_id">
        <option value="0" selected="selected">All</option>
        <?php foreach ($universities_all as $university): ?>
          <option value="<?php echo $university['id']; ?>" <?php $university_id = isset($_GET['university_id']) ? trim($_GET['university_id']) : '-'; echo $university_id == $university['id'] ? 'selected="selected"' : ''; ?>><?php echo $university['name']; ?></option>
        <?php endforeach; ?>
      </select>
    </div>
    <button type="submit" class="btn btn-default">Search</button>
  </form>
</div>

<div class="row">
  <div class="col-lg-12 text-center mt-40">
    <h6> Can't find your past examination paper? <a class="theme-color" href="Request-Quote">Upload your past paper to get one of our experts solve it for you</a> </h6>
  </div>     
</div>
<?php if(empty($rows)): ?>
  <div class="row">
   <div class="product listing">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="product-des text-left">
        <div class="product-info mt-20">
          <p>... no record(s) found ...</p>
        </div>
      </div>
    </div>
  </div>
</div>
<?php else: ?>
  <?php foreach ($rows as $row): ?>
    <div class="row">
     <div class="product listing">
      <div class="col-lg-12 col-md-12 col-sm-12 mb-40">

        <div class="product-des text-left">
         <div class="product-title">
           <a href="Past-Exam-Solution/view/<?php echo $row['id']; ?>" data-id="<?php echo $row['id']; ?>" data-price="<?php echo $row['price']; ?>" data-name="<?php echo $row['title']; ?>"><?php echo dashIfEmpty($row['title']); ?></a>
         </div>
         <div class="product-price">
          <ins><?php echo dashIfEmpty('&pound;' . number_format($row['price'])); ?></ins>
        </div>
        <div class="product-price">
          <i class="fa fa-university"></i> <?php echo dashIfEmpty($row['university_name']); ?>
        </div>
        <div class="product-info mt-20">
          <?php if(isset($_GET['title'])): ?>
            <?php 
            $words = explode(' ', trim($_GET['title']));
            
            $row['description'] = strlen(dashIfEmpty($row['description'])) > 300 ? ellipsize(dashIfEmpty($row['description']), 300) . ' <a href="Past-Exam-Solution/view/' . $row['id'] . '"><b>View&rarr;</b></a>': dashIfEmpty($row['description']);

            // var_dump($row['description']);  var_dump($words); die;

            $description = highlight_words( $row['description'], $words );
            ?>
            <p><?php echo dashIfEmpty($description); ?></p>
          <?php else: ?>
            <p><?php echo strlen(dashIfEmpty($row['description'])) > 300 ? ellipsize_no_strip(dashIfEmpty($row['description']), 300) . ' <a href="Past-Exam-Solution/view/' . $row['id'] . '"><b>View&rarr;</b></a>': dashIfEmpty($row['description']); ?></p>
          <?php endif; ?>
          <!-- <a class="button small mt-0 add-to-cart" href="javascript:void(0);" data-id="<?php echo $row['id']; ?>" data-price="<?php echo $row['price']; ?>" data-name="<?php echo $row['title']; ?>" data-loading-text="<i class='icon-spinner icon-spin icon-large'></i> ADD TO CART">add to cart</a> -->
          <button class="button small mt-0 add-to-cart" href="javascript:void(0);" data-id="<?php echo $row['id']; ?>" data-price="<?php echo $row['price']; ?>" data-name="<?php echo $row['title']; ?>" data-loading-text="<i class='fa fa-spinner fa-spin'></i> ADD TO Cart">add to cart</button>
        </div>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>
<?php endif; ?>
</div>
<div class=" pull-right">

  <nav aria-label="...">
    <ul class="pagination">
      <?php echo $links; ?>
    </ul>
  </nav>
</div>

<div class="row">
  <div class="col-lg-12 text-center mt-40">
    <h6> Can't find your past examination paper? <a class="theme-color" href="Request-Quote">Upload your past paper to get one of our experts solve it for you</a> </h6>
  </div>     
</div>
</div>
</div>
</section>

<!--=================================
  welcome -->

<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->

  <?php $this->load->view('footer'); echo "\n"; ?>

  <script type="text/javascript">
    function validate ()
    {
      var title = document.getElementById("title").value;
      if(title == "" || title == null)
      {
        return false;
      }
      else
      {
        return true;
      }
    }
    $(document).ready(function() {
      $("#country_id").change(function() {
        var country_id = $(this).val();
        if(country_id != "" && country_id != null) {
          $.ajax({
            url:"Past-Exam-Solution/getUniversitiesByCountryID/" + country_id,
            // data:{c_id:country_id},
            type:'GET',
            success:function(response) {
              var resp = $.trim(response);
              $("#university_id").html(resp);
            }
          });
        } else {
          $("#university_id").html("<option value='0'>All</option>");
        }
      });

      $('#searchIcon').click(function () {
        $('#searchForm').submit();
      });
    });
  </script>

</body>
</html> 