  <style type="text/css">
    del:after {
      /*border-color: #F05522;*/
      border-color: #000000;
    }
    .price {
      color: #000000;
    }
  </style>
  <div style="width: 90%; margin: auto;">

    <div class="clearfix"></div>

    <div class="container-fluid text-center bg-grey">
      <!-- <h2>MOBILE CONTENTS</h2> -->
      <!-- <h4>What we have created</h4> -->

      <div class="row text-center">

        <ol class="breadcrumb text-left">
          <li><a href="<?php echo base_url(); ?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
          <!-- <li><a href="#">Library</a></li> -->
          <li class="active"><?php echo $page_title; ?></li>
        </ol>
        <div class="col-sm-12" style="border-bottom: 2px solid #feb25a; margin-bottom: 10px; ">
          <h3 class="text-left"><?php echo $page_title; ?></h3>
        </div>

        <?php if(empty($rows)): ?>
          <div class="col-sm-12">
            <p class="text-muted">No services yet, please check back.</p>
          </div>
        <?php else: ?>
          <?php foreach ($rows as $row): ?>
            <?php $confirm = "Click OK to continue."; 
            // $confirm = "You are about to subscribe to " . htmlspecialchars_decode($row['name']) . ", service costs " . $row['price'] . ", click OK to continue.";
            $service_code = '';
            $service_keyword = '';
            $service_code_text = strtoupper($HTTP_TELCO);
            $service_code_error = true;
            $service_keyword_error = true;

            switch ($service_code_text) {
                case 'MTN':
                    $service_code = $row['mtn_service_code'];
                    $service_keyword = $row['mtn_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'AIRTEL':
                    $service_code = $row['airtel_service_code'];
                    $service_keyword = $row['airtel_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'GLO':
                    $service_code = $row['glo_service_code'];
                    $service_keyword = $row['glo_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'EMTS':
                    $service_code = $row['emts_service_code'];
                    $service_keyword = $row['emts_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                case 'NTEL':
                    $service_code = $row['ntel_service_code'];
                    $service_keyword = $row['ntel_service_keyword'];
                    $service_code_error = $service_code == '' ? true : false;
                    $service_keyword_error = $service_keyword == '' ? true : false;
                    break;
                
                default:
                    # code...
                    break;
            }
            if(($service_code_error == true) || ($service_keyword_error == true))
            {
              // "Service not yet availabe for selected network!!! Please check back later. Thank you.";
              // OR Header not enriched
            }
            ?>
            <div class="col-sm-3">
              <div class="thumbnail">
                <a href="<?php echo getURL($row['name'], $row['slug'], $row['price'], $row['duration']); ?>" class="thumbnail link-thumbnail" <?php echo (!empty($HTTP_TELCO)) && (!empty($HTTP_MSISDN)) ? 'onclick="return preSubscribe(\'' . $SUBSCRIBE_API .'\', \'' . $HTTP_MSISDN .'\', \'' . $service_keyword .'\', \'' . $service_code .'\', \'' . $service_code_text .'\');"' : ''; ?>>
                  <img src="assets/images/products/<?php echo $row['image_url']; ?>" alt="<?php echo $row['name']; ?>" class="img-responsive" title="Subscribe to <?php echo $row['name']; ?>"><!-- 200x170 -->
                </a>
                <div class="caption">
                  <div style="min-height: 40px; "><h4><?php echo $row['name']; ?></h4></div>
                  <p class="price"><del>N</del><?php echo $row['price']; ?> / <?php echo $row['duration'] == '1 Day' ? 'Daily' : ($row['duration'] == '1 Week' ? 'Weekly' : ($row['duration'] == '1 Month' ? 'Monthly' : $row['duration'] )); ?></p>
                  <p><a href="<?php echo getURL($row['name'], $row['slug'], $row['price'], $row['duration']); ?>" class="btn btn-primary" role="button" <?php echo (!empty($HTTP_TELCO)) && (!empty($HTTP_MSISDN)) ? 'onclick="return preSubscribe(\'' . $SUBSCRIBE_API .'\', \'' . $HTTP_MSISDN .'\', \'' . $service_keyword .'\', \'' . $service_code .'\', \'' . $service_code_text .'\');"' : ''; ?>>Subscribe</a></p>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>

        <div class="clearfix"></div>
        
        <div class="row col-md-12">
        <div class="col-md-3 pull-right pagination">
          <p><?php echo $links; ?></p>
        </div>
        
        <div class="col-md-2 pull-left">
          <p><a href="<?php echo $category == 'crbt' ? 'mobile-contents' : 'music-crbt' ; ?>" class="btn btn-primary" role="button">View <?php echo $category == 'crbt' ? 'Mobile Contents' : 'Music / CRBT' ; ?> &rarr;</a></p>
        </div>
        </div>
        
      </div>

    </div>

    <div class="clearfix"></div>

  </div>


<?php $this->load->view('footer'); echo "\n";  // load footer view ?>

<?php if(!empty($HTTP_TELCO) && !empty($HTTP_MSISDN)): ?>
<!-- MD5 -->
<!-- <script src="assets/js/md5.js"></script> -->

<script type="text/javascript">
  function preSubscribe(SUBSCRIBE_API, HTTP_MSISDN, service_keyword, service_code, service_code_text)
  {
    // alert("SUBSCRIBE_API: " + SUBSCRIBE_API);
    // alert("HTTP_MSISDN: " + HTTP_MSISDN);
    // alert("service_keyword: " + service_keyword);
    // alert("service_code: " + service_code);
    // alert("service_code_text: " + service_code_text);

    var url = SUBSCRIBE_API + "msisdn=" + HTTP_MSISDN + "&keyword=" + service_keyword + "&shortcode=" + service_code + "&operator=" + service_code_text;

    // alert("url: " + url);

    $.get(url, function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
        // return true;
    });

    if(confirm('Click OK to continue.'))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
</script>

<?php endif; ?>

</body>
</html>