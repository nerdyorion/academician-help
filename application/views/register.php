<div id="cover-spin"><a href="javascript:void(0);" class="hide-loader" style="position: absolute; left:46%;top:50%;"> <i class="fa fa-close"></i> Hide Loader</a></div>
<style type="text/css">
  #cover-spin {
    position:fixed;
    width:100%;
    left:0;right:0;top:0;bottom:0;
    background-color: rgba(255,255,255,0.7);
    z-index:9999;
    display:none;
}

@-webkit-keyframes spin {
  from {-webkit-transform:rotate(0deg);}
  to {-webkit-transform:rotate(360deg);}
}

@keyframes spin {
  from {transform:rotate(0deg);}
  to {transform:rotate(360deg);}
}

#cover-spin::after {
    content:'';
    display:block;
    position:absolute;
    left:48%;top:40%;
    width:40px;height:40px;
    border-style:solid;
    border-color:black;
    border-top-color:transparent;
    border-width: 4px;
    border-radius:50%;
    -webkit-animation: spin .8s linear infinite;
    animation: spin .8s linear infinite;
}
</style>
<!--=================================
page-title-->

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
  <div class="container">
    <div class="row"> 
      <div class="col-lg-12"> 
      <div class="page-title-name">
          <h1>Signup</h1>
          <p>We know the secret of your success</p>
        </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Signup</span> </li>
       </ul>
     </div>
     </div>
  </div>
</section>

<!--=================================
page-title -->


<!--=================================
 login-->

<section class="white-bg page-section-ptb">
  <div class="container">
    <div class="row">
       <div class="col-lg-12 col-md-12">
        <div class="section-title text-center">
            <h6 class="">Signup with your id</h6>
            <h2 class="title-effect">Signup to your Account</h2>
        <?php if($error_code == 0 && !empty($error)): ?>
          <div class="alert alert-success alert-dismissable fade in">
            <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?php echo $error; ?>
          </div>
        <?php elseif($error_code == 1 && !empty($error)): ?>
          <div class="alert alert-danger alert-dismissable fade in">
            <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> <?php echo $error; ?>
          </div>
        <?php else: ?>
        <?php endif; ?>
          </div>
      </div>
    </div>
     <div class="row">
       <div class="col-md-6 col-md-offset-3">
        <div class="pb-50 clearfix">
        <?php echo form_open('/register/secure', 'class="form-horizontal", onsubmit="return validate();"'); ?>
        <div class="section-field mb-20">
          <label class="mb-10" for="first_name">First name* </label>
          <input id="first_name" name="first_name" class="web form-control" type="text" placeholder="e.g john" required="required" value="<?php echo $this->session->flashdata('first_name'); ?>" />
        </div>
        <div class="section-field mb-20">
          <label class="mb-10" for="last_name">Last name* </label>
          <input id="last_name" name="last_name" class="web form-control" type="text" placeholder="e.g doe" required="required" value="<?php echo $this->session->flashdata('last_name'); ?>" />
        </div>
        <div class="section-field mb-20">
          <label class="mb-10" for="email">Email* </label>
          <input id="email" name="email" class="web form-control" type="email" placeholder="e.g johndoe@yahoo.com" required="required" value="<?php echo $this->session->flashdata('email'); ?>" />
        </div>
            <div class="section-field mb-20">
             <label class="mb-10" for="Password">Password* </label>
               <input id="Password" name="password" class="Password form-control" type="password" placeholder="***********" required="required" />
            </div>

            <div class="section-field mb-30">
             <label class="mb-10" for="confirm_password">Confirm Password* </label>
               <input id="confirm_password" name="confirm_password" class="Password form-control" type="password" placeholder="***********" required="required" />
            </div>
        <div class="section-field mb-20">
          <label class="mb-10">Country </label>
          <div class="box">
            <select class="wide fancyselect mb-30" id="country_id" name="country_id">
              <option value="0" selected="selected">-- Select --</option>
                <?php foreach ($countries as $row): ?>
                  <option value="<?php echo $row['id']; ?>" <?php echo $row['id'] == $this->session->flashdata('country_id') ? 'selected="selected"' : ''; ?>><?php echo $row['name']; ?></option>
                <?php endforeach; ?>
            </select>
          </div>
        </div> 
            <div class="section-field">
              <div class="remember-checkbox mb-30">
                 <input type="checkbox" class="form-control" name="newsletter" id="newsletter" value="1" checked="checked" />
                 <label for="newsletter"> Receive Newsletter</label>
                 <!-- <a href="Forgot-Password" class="pull-right">Forgot Password?</a> -->
                 <!-- Receive Newsletter -->
                </div>
              </div>
              <button class="button" type="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span>Signup</span> <i class='fa fa-check'></i>">
                <span>Signup</span>
                <i class="fa fa-check"></i>
             </button> 
             <p class="mt-20 mb-0">Already have an account? <a href="login"> Login</a></p>
           </form>
          </div>
           <hr />
          <!-- <div class="login-social mt-50 text-center clearfix">
            <h4 class="theme-color mb-30">Login with Social media</h4>
            <ul>
                <li><a class="fb" href="#"><i class="fa fa-facebook"></i> Log in Facebook</a></li>
                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i> Log in Twitter</a></li>
                <li><a class="pinterest" href="#"><i class="fa fa-pinterest-p"></i> Log in google+</a></li>
            </ul>
          </div> -->
        </div>
      </div>
  </div>
</section>

<!--=================================
 login-->


<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->


  <?php $this->load->view('footer'); echo "\n"; ?>

  <script type="text/javascript">
    function validate()
    {

      if($('#first_name').val() != '' && $('#last_name').val() != '' && $('#email').val() != '' && $('#password').val() != '' )
      {
        $('#cover-spin').show();
      }

      $('.hide-loader').click(function () {
        $('#cover-spin').hide();
      });

    var $btn = $('button[type="submit"]').button('loading');
            $(':input[type="submit"]').prop('disabled', true);
            $('button[type="submit"]').prop('disabled', true);
            return true;
      // var password = document.getElementById("password").value;
      // var confirm_password = document.getElementById("confirm_password").value;
      // // var country_id = document.getElementById("country_id").value;
      // if(password != confirm_password ){
      //   $('#confirm_password').parent().addClass('has-error');

      //       // clear others
      //       // $('#country_id').parent().parent().removeClass('has-error');

      //       document.getElementById("confirm_password").focus();
      //       return false;
      //     }
      //     else {
      //       $(':input[type="submit"]').prop('disabled', true);
      //       $('button[type="submit"]').prop('disabled', true);
      //       return true;
      //     }
    }
  </script>

</body>
</html>