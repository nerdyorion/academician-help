
<!--=================================
  page-title-->

  <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
    <div class="container">
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="page-title-name">
            <h1>Request Quote</h1>
            <p>We know the secret of your success</p>
          </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Request Quote</span> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<!--=================================
  page-title -->

<!--=================================
 faq-->

 <section class="faq white-bg page-section-ptb">
  <div class="container">
    <div class="row">
     <div class="col-lg-12 col-md-12">
       <div class="section-title text-center">
        <h6>Get Started</h6>
        <h2 class="title-effect">Request Quote</h2>
        <!-- <p>why the sky's the limit when using AcademicianHelp.</p> -->
      </div>
    </div>
  </div>
      <div class="row">
          <div class="col-sm-12">
           <div id="formmessage">Success/Error Message Goes Here</div>
           <?php echo form_open_multipart('Request-Quote/ajax', 'class="form-horizontal", id="contactform", role="form"'); ?>
           <!-- <form class="form-horizontal" id="contactform" role="form" method="post" action="Request-Quote/ajax" enctype="multipart/form-data"> -->
            <div class="contact-form clearfix">
             <div class="section-field">
                <input id="name" type="text" placeholder="Fullname" class="form-control"  name="name" />
            </div> 
            <div class="section-field">
               <input type="email" placeholder="Email*" class="form-control" name="email" />
           </div>
           <div class="section-field">
              <input type="text" placeholder="Phone" class="form-control" name="phone" />
          </div>
          <div class="section-field textarea">
             <textarea class="form-control input-message" placeholder="Relevant Details*" rows="7" name="message"></textarea>
         </div>
         <div class="section-field file">
            <input type="file" class="form-control" name="attachments[]" multiple="multiple" />
        </div>
        <div class="clearfix"></div>
        <div class="section-field">
          <!-- <p>Submit the word you see below:</p> -->
          <?php echo $captcha_image; ?>
          <input type="text" name="captcha" id="captcha" value=""  placeholder="Enter Captcha*" class="form-control" />
        </div>
        <div class="section-field" style="clear: both;">
            <div class="remember-checkbox mb-30">
                <input type="checkbox" class="form-control" id="terms" name="terms" style="border: 1px solid #ffffff;" />
                <label for="terms">
                    <a href="Terms-And-Conditions" target="_blank">Terms &amp; Conditions</a>
                </label>
            </div>
        </div>
        <div class="section-field submit-button" style="clear: both;">
           <input type="hidden" name="action" value="sendEmail"/>
           <button id="submit" name="submit" type="submit" value="Send" class="button"> Request Quote <i class="fa fa-paper-plane"></i></button>
       </div>
   </div>
</form>
<div id="ajaxloader" style="display:none"><img class="center-block mt-30 mb-30" src="assets/images/pre-loader/loader-02.svg" alt=""></div>
</div>
</div>
  <div class="row">
    <div class="col-lg-12 text-center mt-40">
     <h6> What are you waiting for? Let's <a class="theme-color" href="Request-Quote">get started</a> </h6>
   </div>     
 </div>
</div> 
</section>

<!--=================================
 careers-->


<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->


  <?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html> 