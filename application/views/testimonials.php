
<!--=================================
  page-title-->

  <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
    <div class="container">
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="page-title-name">
            <h1>Testimonials</h1>
            <p>We know the secret of your success</p>
          </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a href="#">company</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Testimonials</span> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<!--=================================
  page-title -->

<!--=================================
 faq-->

 <section class="faq white-bg page-section-ptb">
  <div class="container">
    <div class="row">
     <div class="col-lg-12 col-md-12">
       <div class="section-title text-center">
        <h6>Our success stories</h6>
        <h2 class="title-effect">Testimonials</h2>
        <!-- <p>why the sky's the limit when using AcademicianHelp.</p> -->
      </div>
    </div>
  </div>
  <div class="row"> 
    <div class="col-lg-12 col-md-12">
      <blockquote class="theme-bg quote">
          Ade of AcademicianHelp is an excellent tutor and writer, he is very enthusiastic, patience, calm and caring. Nothing is ever too much for him and I feel extremely lucky to have him as a tutor. He has excellent knowledge of android, java and all aspects of computer science and able to explain anything that I stumble across. He is very helpful and always available to help me at any time of the day. I am very happy with the tutoring that Ade provides. He is one in a million, a star and I definitely would recommend him to anyone.
          <cite>- Shirley</cite>
         </blockquote>
    </div> 
    <div class="col-lg-12 col-md-12">
      <blockquote class="black-bg quote">
          I can tell you are passionate about your work. That passion makes the BEST writer/teachers/tutors. I really liked your enthusiasm! You made materials seem interesting no matter what it was. The way that you presented the material made it all seem logical and simple even when the ideas were challenging. Your teaching style is great and it’s tutors like you that make me want to keep working toward being one myself. It is obvious that you are very passionate about Computer Science, Networking and Programming. I leaned so much and want to thank you for this.
          <cite>- Goitsomone</cite>
         </blockquote>
    </div> 
    <div class="col-lg-12 col-md-12">
      <blockquote class="theme-bg quote">
          Superb tutor and extremely knowledgeable of Java. I would highly recommend this tutor to anyone who needs help with Java. Very honest, patient and flexible person to meet your tutoring needs. I was very lucky to get use this tutor`s services to meet and exceed my university Java study and course requirements
          <cite>- Brad</cite>
         </blockquote>
    </div> 
    <div class="col-lg-12 col-md-12">
      <blockquote class="black-bg quote">
          This teacher has been a blessing for my academic life. I struggled keeping up with the speed of my course. We had to develop an individual project in a very short period of time that I thought was quite challenging. I was desperate and stressed out, I couldn't sleep properly. Then I decided to contact Academicianhelp (about one week before the deadline!). After agreeing on a time that will suit us both, we had classes practically every day. He helped me A LOT and shared with me his knowledge in programming and computing, in general, which is plentiful. In the end, I could submit my coursework on time and my university professor even congratulated me for my great effort. He is without a doubt the best tutor I ever had. It is such a pity that there isn't a higher score than "Excellent"... So if you're going to contact this man from Academicianhelp, you better not talk to him, because I want all his time for myself! Buahaha
          <cite>- Adrian</cite>
         </blockquote>
    </div> 
    <div class="col-lg-12 col-md-12">
      <blockquote class="theme-bg quote">
          Aisvarya of Academicianhelp is awesome, and 100% genuine person to talk to, I found her very pleasant when we’re talking in some detail over the phone. Aisvarya knowledge in computing is very vast, so I am very sure if anybody is looking for a personal tutor, then Aisvarya would be top of my list. She is very, very professional, communication is excellent, and she will set out a clear structure plan to complete your objectives. Aisvarya was always on hand to help me when I was struggling. I found her to be very supportive, friendly, clearly very well educated in computing, honest and genuine. I can guarantee Aisvarya to any person looking for a person tutor in computing.
          <cite>- Jez</cite>
         </blockquote>
    </div> 
    <div class="col-lg-12 col-md-12">
      <blockquote class="black-bg quote">
          AcademicianHelp has a set of talented, professional, and knowledgeable tutor. They have good understanding of their student's abilities and they are able to adapt their write up and teaching to suit their individual learning needs. They are communicative and friendly. The sessions were success.
          <cite>- Osama</cite>
         </blockquote>
    </div> 
    <div class="col-lg-12 col-md-12">
      <blockquote class="theme-bg quote">
          Aisvarya from AcademicianHelp is a very good tutor and writer who explains concepts in a simple way and will make you understand easier complicated subjects. She is very helpful, honest, and intelligent. I strongly recommend her as a tutor for anyone who struggles with programming matters.
          <cite>- Yasmine</cite>
         </blockquote>
    </div> 
    <div class="col-lg-12 col-md-12">
      <blockquote class="black-bg quote">
          I couldn't have asked for a better tutor good with tight deadlines and is very thorough in her work. Also provided me with aftercare (which is nice as some people would just take the money and throw you away) Highly recommended.
          <cite>- Sharisse</cite>
         </blockquote>
    </div> 
  </div> 
  <div class="row">
    <div class="col-lg-12 text-center mt-40">
     <h6> What are you waiting for? Let's <a class="theme-color" href="Request-Quote">get started</a> </h6>
   </div>     
 </div>
</div> 
</section>

<!--=================================
 careers-->


<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->


  <?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html> 