/** Cart by Orion */

$('.add-to-cart').click(function(e){
    e.preventDefault();
    var $btn = $(this).button('loading');
    $(this).prop('disabled', true);
    
    id = $(this).data('id');
    price = $(this).data('price');
    name = $(this).data('name');

    $.ajax({
        type: 'GET',
        url: '/cart/add/' + id + '/' + price + '/' + name,
        success: function( data )
        {
            if(data == '1')
            {
                $('#addedToCartName').html(name);
                $('#addedToCartPrice').html(price);
                $(document).scrollTop( $("#addedToCartAlert").offset().top ); 
                $('.bs-callout').removeClass('hidden').fadeIn('slow').animate({opacity: 1.0}, 8000).delay(2000).fadeOut('slow');

                // var jump = $(this).attr('href');
                // var new_position = $('#'+jump).offset();
                // window.scrollTo(new_position.left,new_position.top);
                // return false;
                // $("#formmessage").html(response).show().delay(20000).fadeOut('slow');
            }
            else
            {
                // error 500
                $('.bs-callout').removeClass('hidden').show().delay(2000).fadeOut('slow');
            }

            $btn.button('reset');
            $(this).prop('disabled', false);
        },
        error: function(xhr, status, error)
        {
            // check status && error
            // console.log(status);
            // console.log(error);
        },
        dataType: 'text'
    });

    getCartHeaderBlock();

});

// $(document).on('click', '.fClick', function(){ 
//     alert("hey!");
// }); 
// $('.remove-from-cart').click(function(e){
$(document).on('click', '.remove-from-cart', function(){ 
    // e.preventDefault();

    $(this).prop('disabled', true);
    
    id = $(this).data('id');

    $(this).parent().parent().fadeOut('slow');

    $.ajax({
        type: 'GET',
        url: '/cart/remove/' + id,
        success: function( data )
        {
            if(data == '1')
            {

                // $('#addedToCartName').html(name);
                // $('#addedToCartPrice').html(price);
                // $(document).scrollTop( $("#addedToCartAlert").offset().top ); 
                // $('.bs-callout').removeClass('hidden').fadeIn('slow').animate({opacity: 1.0}, 8000).delay(2000).fadeOut('slow');
                getCartHeaderBlock(); // cart total is also updated here
            }
            else
            {
                // error 500
                // $('.bs-callout').removeClass('hidden').show().delay(2000).fadeOut('slow');
            }
        },
        error: function(xhr, status, error)
        {
            // check status && error
            // console.log(status);
            // console.log(error);
        },
        dataType: 'text'
    });
});

function getCartHeaderBlock()
{
    $.ajax({
        url:'cart/fetch',
        type:'get',
        cache: false,
        dataType: 'json',
        success:function(response){
            // console.log(response);
            // console.log("cart_items datatype", typeof(response.cart_items));
            if(response.cart_items != null && response.cart_items.length > 0 ) // has items in cart
            {
                // empty cart header
                $('#cart .cart-item').remove();
                $('#cart .cart-total').remove();

                // clear cart count
                $('#cartCount').text(0);

                // add cart item lines to cart header
                for (var i = 0; i < response.cart_items.length; i++)
                {
                    item = response.cart_items[i][0]; // console.log(item);

                    $("#cart").append('<div class="cart-item"><div class="cart-name clearfix"><a href="Past-Exam-Solution?id=' + 
                        item.product_id + '">' 
                        // + item.product_name.trimToLength(33) 
                        // + jQuery.trim(item.product_name).substring(0, 33).split(" ").slice(0, -1).join(" ") + "..." 
                        + item.product_name_short
                        // + jQuery.trim(title).substring(0, 33).split(" ").slice(0, -1).join(" ") + "..." 
                        + '</a><div class="cart-price"><ins>&pound;' + 
                        item.product_price + '</ins></div></div><div class="cart-close"><a href="javascript:void(0);" class="remove-from-cart" data-id="' + 
                        item.product_id + '"> <i class="fa fa-times-circle"></i> </a></div></div>');
                }

                // add cart total line to cart header
                $("#cart").append('<div class="cart-total"><h6 class="mb-15"> Total: &pound;' + 
                    response.cart_total + '</h6><a class="button" href="cart">View Cart</a><a class="button black" href="checkout">Checkout</a></div>'
                );

                // add cart count
                $('#cartCount').text(response.cart_items.length);

                // add cart total on Cart page
                $('#cartTotal').text(response.cart_total);

                // if we're on cart page and there are no more items refresh page
                var current_page = location.pathname.substring(1);

                if(current_page == "cart" && response.cart_items.length == 0)
                {
                    // refresh
                    location.reload(true);
                }
            }
            else // has no item in cart
            {
                // empty cart header
                $('#cart .cart-item').remove();
                $('#cart .cart-total').remove();

                // clear cart count
                $('#cartCount').text(0);

                // add "cart is empty" line
                $("#cart").append('<div class="cart-item"><div class="cart-name clearfix"><a href="Past-Exam-Solution">Your cart is empty</a></div></div>');

                // add "purchase items now" link
                $("#cart").append('<div class="cart-total"><a class="button" href="Past-Exam-Solution" id="#purchase-link">Purchase Items Now</a></div>');

                // add cart total on Cart page
                $('#cartTotal').text(response.cart_total);

                // if we're on cart page and there are no more items, refresh page
                var current_page = location.pathname.substring(1);

                if(current_page == "cart" && response.cart_items.length == 0)
                {
                    // refresh
                    location.reload(true);
                }
            }

            $("#ajaxloader").hide();
            $("#contactform").show();

            $("#formmessage").html(response).show().delay(20000).fadeOut('slow');
      }
  });
    // cart-items: array(product_id, product_name, number_format($cart['product_price']))
    // cart-total: total(number_format(array_sum($this->session->userdata('cart'))))

}

// var shortText = jQuery.trim(title).substring(0, 10).split(" ").slice(0, -1).join(" ") + "...";

String.prototype.trimToLength = function(m) {
  return (this.length > m) 
    ? jQuery.trim(this).substring(0, m).split(" ").slice(0, -1).join(" ") + "..."
    : this;
};